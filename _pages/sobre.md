---
layout: about
title: Sobre
permalink: /about/
---

# Sobre mim

Sou Paulo Henrique de Lima Santana e esse é o meu site pessoal. Nasci em Maceió - AL, morei em Curitiba de 1993 a 2022, e moro em Belo Horizonte desde 2022.

## Formação:

Sou Bacharel em
[Ciência da Computação<i class="fa fa-external-link"></i>](http://web.inf.ufpr.br/bcc)
pela
[Universidade Federal do Paraná (UFPR)<i class="fa fa-external-link"></i>](https://www.ufpr.br)
desde 2010.

## Atividades profissionais:

Sou administrador de sistemas na [Collabora<i class="fa fa-external-link"></i>](https://www.collabora.com/).

Fui organizador do temário do Fórum Internacional Software Livre em 2015
[(FISL16)<i class="fa fa-external-link"></i>](http://softwarelivre.org/fisl16)
e em 2016
[(FISL17)<i class="fa fa-external-link"></i>](http://softwarelivre.org/fisl17).

Meu primeiro emprego foi em um escritório de contabilidade onde cheguei a ser
encarregado de departamento pessoal.

## Atividades voluntárias:

[Desenvolvedor Oficial (DD)<i class="fa fa-external-link"></i>](https://contributors.debian.org/contributor/phls@debian.org/)
do [Projeto Debian](http://debian.org).

Membro [Instituto para Conservação de Tecnologias Livres (ICTL)<i class="fa fa-external-link"></i>](http://ictl.org.br),
entidade sediada em Curitiba.

Fundador e membro da [Comunidade Curitiba Livre<i class="fa fa-external-link"></i>](http://curitibalivre.org.br).

Membro associado da [Associação Software Livre.Org<i class="fa fa-external-link"></i>](http://asl.org.br),
entidade sediada em Porto Alegre.

Curador da [Área de Software Livre<i class="fa fa-external-link"></i>](http://softwarelivre.org/slcampusparty)
responsável por indicar os palestrantes relacionados a este tema na
[Campus Party Brasil<i class="fa fa-external-link"></i>](https://brasil.campus-party.org) de 2013 a 2017, na
Campus Party Recife de 2012 a 2016, e na Campus Party Minas Gerais em 2016.

Organizador voluntário de eventos de Software Livre realizados em Curitiba como:

 * Conferência Mundial de Desenvolvedores(as) do Projeto Debian [(DebConf19)<i class="fa fa-external-link"></i>](https://debconf19.debconf.org)
 * [MiniDebConf Curitiba<i class="fa fa-external-link"></i>](https://minidebconf.curitiba.br)
 * [Debian Day<i class="fa fa-external-link"></i>](https://wiki.debian.org/DebianDay)
 * Festival Latinoamericano de Instalação de Software Livre [(FLISOL)<i class="fa fa-external-link"></i>](https://flisol.info)
 * Software Freedom Day [(SFD)<i class="fa fa-external-link"></i>](https://www.softwarefreedomday.org)
 * Education Freedom Day (EFD)
 * Document Freedom Day (DFD)
 * [Circuito Curitibano de Software Livre<i class="fa fa-external-link"></i>](http://circuito.curitibalivre.org.br)
 * VI Fórum de Tecnologia em Software Livre (2014)

Durante a vida acadêmica, particei de entidades estudantis assumindo os cargos de:

 * Presidente da Executiva Nacional dos Estudantes de Computação (ENEC) nas
gestões 2004/2005 e 2005/2006.
 * Tesoureiro do Centro de Estudos de Informática da UFPR (CEI) nas gestões
2000/2001 e 2008/2009.

E participei da organização dos seguintes de eventos estudantis:

 * XXVII Encontro Nacional dos Estudantes de Computação (ENECOMP) em 2009 em Curitiba.
 * XXIV Encontro Nacional dos Estudantes de Computação (ENECOMP) em 2006 em Poços de Caldas.
 * I Semana de Computação da Universidade Federal do Paraná (SECOMP UFPR) em 2005 em Curitiba.
 * I e II Semana de Software Livre da Universidade Federal do Paraná (SSL UFPR) em 2004 em Curitiba.
 * XXI Encontro Nacional dos Estudantes de Computação (ENECOMP) em 2003 em Campinas.
