---
layout: contact
title: Contato
permalink: /contato/
---

# Contato

 * E-mail: paulo (at) phls (dot) com (dot) br
 * Telegram: [phls00](https://t.me/phls00)
 * Linkedin: [paulo-henrique-de-lima-santana-9394b14b](https://www.linkedin.com/in/paulo-henrique-de-lima-santana-9394b14b/)
 * Github: [phls](https://github.com/phls)
 * Gitlab: [phls](https://gitlab.com/phls)
 * Salsa (Debian): [phls](https://salsa.debian.org/phls)
 * Twitter: [phls00](http://www.twitter.com/phls00)
 * Mastodon: [phls](https://masto.donte.com.br/@phls)
 * Identi.ca: [phls00](https://identi.ca/phls00)
 * Facebook: [phls00](http://www.facebook.com/phls00)
 * Instagram: [phls00](http://instagram.com/phls00)
 * Skype: phls00
 * Talk: phls00@gmail.com
 * Jabber: phls@jabber.org
 * ICQ: 86344835
