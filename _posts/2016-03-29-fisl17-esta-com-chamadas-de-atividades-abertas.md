---
layout: post
title: FISL17 está com chamadas de atividades abertas
date: 2016-03-29 16:00
author: Paulo Henrique de Lima Santana
categories: geral
tags: eventos fisl
---

# FISL17 está com chamadas de atividades abertas

Nunca é demais lembrar que estão abertas chamadas de atividades para o 
[FISL17 - Fórum Internacional Software Livre<i class="fa fa-external-link"></i>](http://softwarelivre.org/fisl17),
que acontecerá de 13 a 16 de julho de 2016 na PUCRS em Porto Alegre.

**07 de abril: chamada de trabalhos (palestras, oficinas e encontros
comunitários)**

<http://softwarelivre.org/fisl17/o-evento/hospedagem-oficial/aberta-chamada-de-trabalhos-para-o-fisl17>

**10 de abril: chamada para minieventos de comunidades de software livre**

<http://softwarelivre.org/fisl17/o-evento/hospedagem-oficial/comunidades-de-software-livre-venham-fazer-seu-minievento-no-fisl>

**10 de abril: chamada para o Workshop Software Livre (WSL)**

<http://softwarelivre.org/fisl17/o-evento/hospedagem-oficial/chamada-de-trabalhos-aberta-para-o-wsl>

**E a partir de 01 de abril estará aberta as inscrições para participantes:**

<http://softwarelivre.org/fisl17/inscricoes/participantes>

**Quer ajudar a ASL.Org a continuar organizando suas atividades, especialmente
o FISL?**

Faça uma doação para a entidade:

<http://softwarelivre.org/portal/noticias/asl.org-lanca-campanha-de-doacao-para-realizar-o-fisl17>

![Banner FISL17](/assets/img/bannersitefisl170.png){:width="600px"}


