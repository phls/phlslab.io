---
layout: post
title: Dicas de Bruxelas
date: 2020-02-08 09:00
author: Paulo Henrique de Lima Santana
categories: geral
tags: bruxelas bélgica debian fosdem
---

# Dicas de Bruxelas

Resolvi escrever esse texto para passar algumas dicas sobre Bruxelas, resumindo
muito do que eu escrevi nos últimos textos. Mas antes é preciso levar em
consideração algumas coisas:

* Estive duas vezes em Bruxelas. Em 2019 fiquei 11 dias (28/01 a 07/02), e em
  2020 fiquei 8 dias (29/01 a 05/02). Ou seja, fiquei entre o final de janeiro e
  começo de fevereiro.
* Fui com pouco dinheiro e parte das minhas despesas foram reembolsadas pelo
  Debian porque eu estava participando de atividades relacionadas ao Projeto.
  Então as dicas são para gastar pouco mesmo. Os objetivos principais das
  viagens foram participar do
  [sprint](https://wiki.debian.org/Sprints/2020/DebConfVideoTeamSprintAtFOSDEM2020)
  do time de vídeo e do [FOSDEM](https://fosdem.org).
* Em 2019 fui com a Adriana e ficamos hospedado de graça na casa de um casal que
  todos os anos recebe pessoas que vão participar do FOSDEM. E em 2020 eu estava
  sozinho e fiquei hospedado no [Hostel
  Galia](https://www.booking.com/hotel/be/hostel-galia.pt-br.html).

## Chegada no aeroporto de Bruxelas

Se você quiser ir do [aeroporto](https://www.brusselsairport.be/en/passengers)
para a cidade de Bruxelas usando transporte público, você pode ir de
[trem](https://www.brusselsairport.be/en/passengers/access-parking/train) ou de
[ônibus](https://www.brusselsairport.be/en/passengers/access-parking/bus/public-bustransport).
Nunca fiz esse trajeto de trem, então vou falar sobre os ônibus.

Existem basicamente duas opções principais para chegar ao centro de Bruxelas:

* Linha ["272 Brussels North"](https://www.delijn.be/en/lijnen/lijn/3/272/0/272_Brussel_-_Zaventem) , da empresa [De Lijn](https://www.delijn.be/en).
  Essa empresa opera linhas da região metropolitana de Bruxelas, por isso eles
  tem um cartão próprio.
* Linha ["12 Brussels Airport - Brussels
  City"](http://www.stib-mivb.be/horaires-dienstregeling2.html?l=en&_line=12&_directioncode=V),
  da empresa STIB-MIVB. Durante a semana após às 20h e durante o final de semana
  essa linha muda para
  "[21](http://www.stib-mivb.be/horaires-dienstregeling2.html?l=en&_line=21&_directioncode=V)".
  Essa empresa opera os ônibus e trens na cidade de Bruxelas. Vou comentar mais
  a respeito sobre transporte público em Bruxelas.

Nesse
[mapa](https://media.brusselsairport.be/bruweb/default/0001/21/1674c622984dd116d5bb47f2482a6ebd2c68d172.png)
é possível ver onde os ônibus param. Basicamente você irá desembarcar, pegar a
sua mala, sair para o saguão, descer um andar de elevador ou escada rolante,
passar pela porta de saída do aeroporto, e já dar de cara com as paradas dos
ônibus.

Se você for pegar o ônibus 272, você irá pagar diretamente para o motorista (os
ônibus não tem cobradores). Se não estou enganado, a passagem custa € 2,50.
Quando você paga diretamente para o motorista em dinheiro, a passagem custa mais
caro. Por isso, se você for ficar em algum lugar onde irá pegar os ônibus da De
Lijn constamente, vale a pena comprar o cartão com várias passagens. Isso
aconteceu comigo em 2019 porque o [local](https://www.linuxbe.com/) do sprint do
time de vídeo era na cidade de Diegem, então comprei um cartão em uma loja com
[10 passagens](https://www.delijn.be/en/vervoerbewijzen/tickets/lijnkaart.html)
por € 16,00, ou seja, cada passagem saiu por € 1,60. Veja mais em [Via a ticket
machine at the stop, in Lijnwinkel stores or in any of 3,000 retail
outlets](https://www.delijn.be/en/vervoerbewijzen/tickets/). Não sei se em
alguma loja no aeroporto vende esse cartão. No ano passado não usei todas as
passagens, então guardei o cartão com o saldo de três. Esse ano quando peguei o
272 na chegada e na volta, pude usar meu saldo restante. Ainda tenho o saldo de
uma passagem, talvez para usar ano que vem? :-)

O ponto final da linha 272 é na estação "Gare Noord”, ou "Gare Du Nord" ou
“Station Brussel Noord”. Essa estação é bem grande e lá você pode pegar outros
ônibus, trens ou metrôs para outros lugares. Do aeroporto até essa estação dá
quase uma hora.

Se você optar por pegar o ônibus 12, aí você tem duas opções:
* Pagar diretamente para o motorista o valor de € 6,00.
* Comprar um ticket na máquina de venda automática que fica ao lado do ponto do
  ônibus. Nesse caso você pagará € 4,50, vai pegar o tícket e passar no leitor
  dentro do ônibus. Nessa máquina você só pode pagar com cartão de crédito ou
  moedas. A passagem dessa linha do aeroporto é mais cara mesmo, mais adiante
  vou comentar sobre as passagens dentro de Bruxelas.

Se você precisar de moedas, tem uma casa de câmbio dentro do aeroporto onde vocẽ
pode trocar dinheiro em papel por moedas. Eu troquei € 25,00 por 25 moedas lá.

## Transporte público

Bruxelas tem um sistema de [transporte
público](http://www.stib-mivb.be/horaires-dienstregeling2.html?l=en) incrível
formado por ônibus, trem (chamado de tram) e metrô. Por isso nesses dois anos eu
andei tranquilamente usando apenas transporte público e em nenhum momento peguei
uber ou taxi.

Esse é o trem elétrico (tram)
 
![Bruxelas](/assets/img/bruxelas-2020-40.jpg)

Esse é o metrô

![Bruxelas](/assets/img/bruxelas-2020-41.jpg)

O site da [STIB-MIVB](http://www.stib-mivb.be/index.htm?l=en) é bem completo e
tem muitas informações. Lá você pode [planejar sua
rota](http://www.stib-mivb.be/tripplanner/?l=en) e ele vai te indicar as
alternativas de transporte. Além disso, você pode baixar o app para
[Android](https://play.google.com/store/apps/details?id=be.stib.mivb.mobile&hl=en)
e [iOS](https://apps.apple.com/be/app/id1325048789?l=en) e ainda pesquisar uma
rota no google maps que ele via te indicar as opções. Tem [mapas das
linhas](http://www.stib-mivb.be/article.html?_guid=00266bf9-0883-3410-bc80-cd51a4d340c3&l=en)
e informações sobre
[tarifas](http://www.stib-mivb.be/article.html?_guid=d0707200-2683-3410-479e-b21a51d668f0&l=en),
que podem ser um pouco complicadas de entender no começo.

Se você vai usar o transporte público, o melhor é comprar um cartao
[MOBIB](http://www.stib-mivb.be/article.html?_guid=30af0085-2483-3410-5394-a71daf08acd1&l=en).
Ele custa € 5,00 e pode ser comprado nas estações. Com o cartão, você pode
carregar as passagens na máquinas de venda automáticas que ficam nas estações ou
nos pontos dos trens. Eu fiz o meu cartão MOBIB e sempre que carreguei paguei a
opção com 10 passagens por € 14,00, ou seja, cada passagem sai por € 1,40. 

Os ônibus e trens param em pontos nas ruas, então você entra e passa o cartão no
leitor que descontará a passagem. Eles não tem cobradores então na teoria, você
pode não passar o cartão e andar de graça, mas se for pego por um fiscal, irá
pagar uma multa, portanto não faça merda e pague suas passagens! Nas estações
você pode pegar trem ou metrô e tem catracas eletrônicas nas entradas para você
passar o cartão.

A passagem tem validade por uma hora, então você pode entrar no ônibus, trem,
estação, passar o cartão, fazer o trajeto, desembarcar, e pegar outro ônibus,
trem ou metrô sem pegar novamente dentro do prazo de uma hora da primeira
cobrança.

Eu carregando o meu cartão MOBIB na máquina de vendas no ponto do trem.

![Bruxelas](/assets/img/bruxelas-2020-39.jpg)

É sério, use transporte público em Bruxelas. Eu pessoalmente acho divertido o
desafio de montar os roteiros para ver quais opções de ônibus, trem e/ou metrô
eu terei que pegar. Faz parte do passeio olhar a cidade pelo transporte público.

## Pontos turísticos que visitei em Bruxelas

O melhor lugar para pesquisar os pontos turístico de Bruxelas é site [Visit
Brussels](https://visit.brussels/en/). Lá existe o [Brussels
Card](https://visit.brussels/en/sites/brusselscard) que dá gratuitade e
descontos em lugares durante 24, 48 ou 72 horas, e ainda com a opção de
transporte público gratuito. Eu nunca comprei esse cartão, mas gosto de visitar
o site para procurar lugares para visitar, por exemplo, para ver a lista de
[museus](https://visit.brussels/en/category/museums-visits),
[monumentos](https://visit.brussels/en/category/monuments-sites) e
[parques](https://visit.brussels/en/lists/parks-in-brussels).

Nos dois anos, os lugares que visitei foram:

* [Grand-Place](https://visit.brussels/en/place/Grand-Place): é a praça central
  e o principal ponto turístico da cidade.
* [Menneken Pis](https://visit.brussels/en/place/Manneken-Pis) e [Jeanneke
  Pis](https://visit.brussels/en/place/Jeanneke-Pis) - estátuas do menino e da
  menina fazendo xixi.
* Parlamento Europeu (apenas passei em frente).
* [Atomium](https://visit.brussels/en/place/Atomium).
* [The Belgian Comic Strip
  Center](https://visit.brussels/en/place/The-Belgian-Comic-Strip-Center) -
  museu das histórias em quadrinhos, com destaque para os Smurfs e o TinTim.
* [Galeries Royales
  Saint-Hubert](https://visit.brussels/en/place/Galeries-Royales-Saint-Hubert) -
  galeria com várias lojas de chocolates.
* Rue Neuve - rua para pedestres com várias lojas e os shoppings Inno e City 2. 
* [Parc du Cinquantenaire](https://visit.brussels/en/place/Cinquantenaire-Park)
  - Praça do Cinquentenário que tem três museus mas só fui no [Royal Military
    Museum (War Heritage
    Institute)](https://visit.brussels/en/place/Royal-Military-Museum-War-Heritage-Institute).
* [Basilica of
  Koekelberg](https://visit.brussels/en/place/Basilica-of-Koekelberg-Panorama) e
  [Parc Elisabeth](https://visit.brussels/en/place/Parc-Elisabeth).
* [The Park of Brussels (Parc
  Royal)](https://visit.brussels/en/place/The-Park-of-Brussels) e [Royal Palace
  (Palais Royal)](https://visit.brussels/en/place/Palais-Royal).
* Neuhaus Factory Shop: loja outlet da fábrica de chocolates.
* [House of European
  History](https://visit.brussels/en/place/House-of-European-History).
* [Estátua do Jean-Claude Van
  Damme](https://visit.brussels/en/place/Jean-Claude-Van-Damme-statue) :-)
 
## Outros lugares ou coisas para fazer

Fui em alguns outros lugares que não são pontos turísticos, para comer ou
comprar alguma coisa.

* [Shopping DOCKS](https://docksbruxsel.be/): as lojas do shopping fazem
  promoções no final do mês de janeiro e vendem roupas de frio com descontos.
* [SportsDirect.com](https://www.sportsdirect.com): loja de roupas e sapatos com
  promoções. A loja fica ao lado do shopping Docks. 
* Comer waffle: essa é a principal comida turística e tem vários lugares em
  volta do Grand Place. O melhor, na minha opinião, é da rede
  [Australian](https://www.australianice.be/fr/index.html).
* Primark: loja de roupas com promoções.
* [Delirium](https://www.deliriumvillage.com/bar/delirium-cafe/): principal bar
  de cervejas de Bruxelas.
* Loja da fábrica Leonidas: loja outled da fábrica de chocolates.
* Mercados Aldi e Lidl: duas redes de mercados de comidas e bebidas com várias
  unidades pela cidade que vendem produtos mais baratos.
* Carrefour: melhor lugar para comprar cervejas de garrafa. Tem várias unidades
  pequenas espalhadas pela cidade, e o
  [hipermercado](https://goo.gl/maps/nKB1E93CJGkuSZHi6).
* [Master Frites](https://www.facebook.com/masterfritesbrussels/): lanchonete
  muito boa que vende sanduíches, batata frita, e outras comidas.
* [Media Markt](https://www.mediamarkt.be/fr/): loja de eletrônicos. Tem várias
  unidades pela cidade, entre elas uma no Docxs e outra no centro na Rue Neuve.

## Outras dicas

Comprei um chip da operadora [BASE](https://www.base.be/) por € 15,00 com 1Gb
para dados.

## Vídeos: O mundo segundo os brasileiros

É legal assistir esses vídeos do programa "O mundo segundo os brasileiros" em
Bruxelas:

* <https://www.youtube.com/watch?v=gXCp9A027-I>
* <https://www.youtube.com/watch?v=hTUqGhjKIbY>
* <https://www.youtube.com/watch?v=oovXwCcgMo8>
* <https://www.youtube.com/watch?v=7VEYcuxQytM>
* <https://www.youtube.com/watch?v=bhyyEaKgIjE>



