---
layout: post
title: Caravana de Curitiba para a Latinoware 2011
date: 2011-04-21 16:00
author: Paulo Henrique de Lima Santana
categories: geral
tags: eventos latinoware
---

# Caravana de Curitiba para a Latinoware 2011

De 19 a 21 de outubro de 2011 acontecerá a **VIII Latinoware - Conferência
Latinoamericana de Software Livre**, em Foz do Iguaçu.

Como acontece todos os anos, novamente foi disponibilizado um ônibus pela
organização do evento para a caravana de Curitiba, e eu (Paulo Santana) estou
responsável pela organização.

Para participar basta se inscrever no evento e no próprio sistema de inscrição
selecionar a caravana de Curitiba. O custo é de R$ 40,00 que deverá ser pago no
momento do embarque.

Poderá viajar qualquer pessoa interessada em participar da Latinoware. O ônibus
NÃO é exclusivo para estudantes e NÃO é exclusivo para a comunidade da UFPR, é
aberto a todos.

Interessados em participar da caravana podem se inscrever na lista de discussão
abaixo para mais informações:

<http://groups.google.com/group/caravana-curitiba>

 * Saída de Curitiba: 18 de outubro
 * Horário: 18:00h
 * Local: Centro Politécnico da UFPR

 * Saída de Foz do Iguaçu: 22 de outubro
 * Horário: 16:00h
 * Local: Hotel

Obs: o hotel que ficaremos hospedados será confirmado em breve.