---
layout: post
title: Eventos de Software Livre em 2019
date: 2019-09-22 16:00
author: Paulo Henrique de Lima Santana
categories: eventos
tags: eventos
---

# Eventos de Software Livre em 2019

Obs: essa lista está em constante atualização.

Acesse também a agenda de eventos de Software Livre e Código Aberto no Brasil:

<http://agenda.softwarelivre.org>

## JANEIRO

~~**Campus Party Brasil 2019 - CPBR12**  
12 a 17 de fevereiro de 2019  
São Paulo - SP  
<http://brasil.campus-party.org>~~

## FEVEREIRO

## MARÇO

~~**Open Data Day**  
02 de março de 2019  
Várias cidades  
<http://opendataday.org>~~

~~**3º Encontro PotiLivre Caraúbas**  
16 de março de 2019  
Caraúbas - RN  
<https://encontro.potilivre.org>~~

~~**DFD 2019 - Document Freedom Day**  
27 de março de 2019  
Várias cidades  
<http://documentfreedom.org>~~

~~**Tchelinux Passo Fundo**  
30 de março de 2019  
Passo Fundo - RS  
<https://passofundo.tchelinux.org>~~

## ABRIL

~~**PHP Experience ‘2019 – Edição Pocket**  
05 e 06 de abril de 2019  
São Paulo - SP  
<https://eventos.imasters.com.br/phpexperience>~~

~~**WordCamp Floripa**  
13 de abril de 2019  
Florianópolis - SC  
<https://2019.floripa.wordcamp.org>~~

~~**Tchelinux Sant'Ana do Livramento**  
13 de abril de 2019  
Sant'Ana do Livramento - RS  
<https://livramento.tchelinux.org>~~

~~**Python Sudestel 2019**  
27 e 28 de abril de 2019  
Vitória - ES  
<https://2019.pythonsudeste.org>~~

~~**FLISOL 2019 - Festival Latino-americano de Instalação de Software Livre**  
27 de abril de 2019  
Várias cidades  
<http://www.flisol.org.br>~~

~~**Tchelinux Santa Cruz do Sul**  
27 de abril de 2019  
Santa Cruz do Sul - RS  
<https://santacruz.tchelinux.org>~~

## MAIO

~~**CryptoRave - 6ª edição**  
03 a 05 de maio de 2019 
São Paulo - SP  
<https://cryptorave.org>~~

~~**WordCamp Porto Alegre**  
03 de maio de 2019  
Porto Alegre - RS  
<https://2019.portoalegre.wordcamp.org>~~

~~**PentahoDay 2019**  
10 e 11 de maio de 2019  
Curitiba - PR  
<http://www.pentahobrasil.com.br/eventos/pentahoday2019>~~

~~**Tchelinux Caxias do Sul**  
11 de maio de 2019  
Caxias do Sul - RS  
<https://caxias.tchelinux.org>~~

~~**III Conferência PHPRS**  
17 e 18 de maio de 2019  
Porto Alegre - RS  
<https://conference.phprs.com.br>~~

~~**Tchelinux Rio Grande**  
24 de maio de 2019  
Rio Grande - RS  
<https://riogrande.tchelinux.org>~~

## JUNHO

~~**Caipyra - Conferência de Python**  
07 a 10 de junho de 2018  
São Carlos - SP  
<http://caipyra.python.org.br>~~

~~**Tchelinux Camaquã**  
08 de junho de 2019  
Camaquã - RS  
<https://camaqua.tchelinux.org>~~

~~**Tchelinux Novo Hamburgo**  
15 de junho de 2019  
Novo Hamburgo - RS  
<https://nh.tchelinux.org>~~

## JULHO

~~**V Python Nordeste**  
19 a 21 de julho de 2019  
Recife - PE  
<https://2019.pythonnordeste.org>~~

~~**DebConf19 - Conferência Mundial de Desenvolvedores(as) do Projeto Debian**  
21 a 28 de julho de 2019  
Curitiba - PR  
<https://debconf19.debconf.org>~~

## AGOSTO

~~**PGConf.Brasil 2019**  
1 a 3 de agosto de 2019  
São Paulo - SP  
<https://www.pgconf.com.br/2019>~~

~~**Linux Developer Conference Brazil**  
02 a 04 de agosto de 2019  
São Paulo - SP  
<http://linuxdev-br.net>~~

~~**Debian Day ou Dia do Debian**  
16 (ou 18) de agosto de 2019  
Várias cidades  
<https://wiki.debian.org/DebianDay/2019>~~

~~**Tchelinux Pelotas**  
24 de agosto de 2019  
Pelotas - RS  
<https://pelotas.tchelinux.org>~~

~~**Tchelinux Santo Ângelo**  
31 de agosto de 2019  
Santo Ângelo - RS  
<https://santoangelo.tchelinux.org>~~

## SETEMBRO

~~**XVI FGSL - Fórum Goiano de Software Livre**  
04 a 08 de setembro de 2019  
Goiânia - GO  
<http://fgsl.net>~~

~~**Python Sul 2019**  
12 a 14 de setembro de 2019  
Curitiba - PR  
<http://pythonsul.org>~~

~~**Tchelinux Alegrete**  
14 de setembro de 2019  
Alegrete - RS  
<https://alegrete.tchelinux.org>~~

~~**SFD 2019 - Software Freedom Day**  
21 de setembro de 2019  
Várias cidades  
<http://softwarefreedomday.org>~~

~~**Tchelinux Erechim**  
28 de setembro de 2019  
Erechim - RS  
<https://erechim.tchelinux.org>~~

## OUTUBRO

~~**WordCamp São Paulo 2019**  
05 de outubro de 2019  
São Paulo - SP  
<https://saopaulo.wordcamp.org/2019/>~~

~~**Tchelinux Bagé**  
05 de outubro de 2019  
Bagé - RS  
<https://bage.tchelinux.org>~~

~~**Tchelinux Bento Gonçalves**  
29 de outubro de 2019  
Bento Gonçalves - RS  
<https://bento.tchelinux.org>~~

~~**PHPeste**  
19 e 20 de outubro de 2019  
Recife - PE  
<http://phpeste.net>~~

~~**PythonBrasil[15] - 15ª Conferência Brasileira da Comunidade Python**  
23 a 28 de outubro de 2019  
Ribeirão Preto - SP  
<http://2019.pythonbrasil.org.br>~~

## NOVEMBRO

~~**Tchelinux Santa Maria**  
09 de novembro de 2019  
Santa Maria - RS  
<https://santamaria.tchelinux.org>~~

~~**Coda.BR 2019 - Conferência Brasileira de Jornalismo de Dados e Métodos Digitais**  
23 e 24 de novembro de 2019  
São Paulo - SP  
<https://coda.escoladedados.org>~~

~~**16º Latinoware - Conferência Latino-americana de Software Livre**  
27 a 29 de novembro de 2019  
Foz do Iguaçu - PR  
<https://2019.latinoware.org>~~

~~**13ª RubyConf Brasil**  
28 e 29 de novembro de 2019  
São Paulo - SP  
<https://evento.moblee.com.br/u9AYvvXEL>~~

~~**PHP com Rapadura Conference 2019**  
30 de novembro de 2019  
Fortaleza - CE  
<http://conference.phpcomrapadura.org>~~

## DEZEMBRO

~~**Tchelinux Porto Alegre**  
14 de dezembro de 2019  
Porto Alegre - RS  
<https://poa.tchelinux.org>~~

~~**14ª PHP Conference Brasil**  
05 a 07 de dezembro de 2019  
Porto Aegre - RS  
<http://www.phpconference.com.br>~~

## Anos anteriores

[2018](http://softwarelivre.org/blog/eventos-de-software-livre-em-2018)  
[2017](http://softwarelivre.org/blog/eventos-de-software-livre-em-2017)  
[2016](http://softwarelivre.org/blog/eventos-de-software-livre-em-2016)  
[2015](http://softwarelivre.org/blog/eventos-de-software-livre-em-2015)  
[2014](http://softwarelivre.org/blog/eventos-de-software-livre-em-2014)  
[2013](http://softwarelivre.org/blog/eventos-de-software-livre-em-2013)  
[2012](http://softwarelivre.org/blog/eventos-de-software-livre-em-2012)  
[2011](http://softwarelivre.org/blog/eventos-de-software-livre-em-2011)  
