---
layout: post
title: Script para publicar feeds rss no quitter.se
date: 2015-05-05 16:00
author: Paulo Henrique de Lima Santana
categories: geral
tags: software-livre redes-sociais
---

# Script para publicar feeds rss no quitter.se

O [quitter.se<i class="fa fa-external-link"></i>](https://quitter.se/)
é uma [alternativa livre<i class="fa fa-external-link"></i>](https://gnu.io/)
para o twitter. Eu já havia criado um perfil pessoal no quitter, mas fazia
bastante tempo que não usava. Ano passado (2014) quando trabalhei na comunicação
do [FISL<i class="fa fa-external-link"></i>](http://www.fisl.org.br/), também
criei um [perfil<i class="fa fa-external-link"></i>](https://quitter.se/fisl)
para o evento mas ele estava sem uso.

Como eu já usei o serviço gratuito do
[dlvr.it](https://dlvr.it/) para pegar o
[feed rss<i class="fa fa-external-link"></i>](http://softwarelivre.org/fisl16/noticias/feed)
do site e postar no
[perfil<i class="fa fa-external-link"></i>](https://twitter.com/fisl_oficial)
do twitter, procurei uma alternativa para postar esse feed no quitter, assim o
perfil passaria a ficar movimentado. Não achei nenhuma solução nos sites
gratuitos como o dlvr.it,
[twitterfeed<i class="fa fa-external-link"></i>](http://twitterfeed.com/), ou
[IFTTT<i class="fa fa-external-link"></i>](https://ifttt.com/).

Depois de muito pesquisar principalmente em sites relacionados aos projetos
[GNU Social](https://gnu.io/) e
[pump.io](http://pump.io/),
decidi entrar no canal #social do irc.freenode.net para perguntar se alguém por lá conhecia uma solução. E logo depois que fiz a pergunta, uma pessoa me respondeu dizendo que havia criado um script chamado gnurss para puxar o feed rss e publicar nas redes do GNU Social :-)

A página que contém a descrição do script é essa:
<http://elbinario.net/2015/02/11/gnusrss-publicando-feeds-en-gnu-social>

O processo de instalação é esse:

$ aptitude install python3-pip libcurl4-gnutls-dev librtmp-dev  
$ pip-3.2 install pycurl PySocks  
$ git clone https://gitlab.com/drymer_/gnusrss.git  
$ cd gnusrss/  
$ mv example.py fisl.py  
$ vi fisl.py  

Configuração do arquivo fisl.py:

username = 'fisl'  
password = 'xxxx'  
api_url = 'http://quitter.se/api/statuses/update.xml'  
tor = "no"  
txt = 'feed-fisl.txt'  
txt_tmp = 'feed-fisl_tmp.txt'  

O script vai puxar os feeds e guardar no arquivo "feed-fisl.txt" o título da
notícia e o link para ela. Será publicado no quitter o título da notícia e o
link encurtado para ela.

O comando para puxar os feeds e publicar no quitter é:

$ python3.2 fisl.py

Para testar o envio de uma publicação para o quitter:

$ curl -u usuario:senha https://quitter.se/api/statuses/update.xml -d status='teste'

Se tudo der certo, para rodar a cada 1 hora no crontab:

00 * * * * cd /root/gnusrss && python3.2 fisl.py

[Arquivos<i class="fa fa-external-link"></i>](http://softwarelivre.org/phls/script-gnurss.zip)
do script gnurss
