---
layout: post
title: Campanhas de financiamento coletivo da comunidade Software Livre
date: 2016-03-09 16:00
author: Paulo Henrique de Lima Santana
categories: geral
tags: software-livre
---

# Campanhas de financiamento coletivo da comunidade Software Livre

Quer ajudar algum projeto, evento, ou espaço da comunidade Software Livre?
Conheça algumas campanhas de financiamento coletivo organizadas por entidades ou
pessoas e contribua financeiramente para o seu sucesso.

## ASL.Org - Associação Software Livre.Org

A [ASL.Org<i class="fa fa-external-link"></i>](http://asl.org.br)
está com uma campanha de arrecadação para ajudar a financiar os trabalhos da
entidade, em especial o
[FISL17 - Fórum Internacional Software Livre<i class="fa fa-external-link"></i>](http://softwarelivre.org/fisl17)
que acontecerá de 13 a 16 de julho de 2016 no Centro de Eventos da PUCRS em
Porto Alegre

Valor mínimo da doação é R$ 10,00 mas com uma doação de R$ 170,00 você recebe
um ingresso cortesia para o FISL17 e uma camiseta oficial do evento.

**Meta fixa: 1.000 contribuições.**

Prazo final para doação: 31 de março.

Site para doação: <http://segue.fisl17.softwarelivre.org>

## Cryptorave

Os organizadores da
[Cryptorave<i class="fa fa-external-link"></i>](http://cryptorave.org/)
que acontecerá nos dias 06 e 07 de maio no Centro Cultural São Paulo estão com
uma campanha de arrecadação para ajudar a financiar o evento.

Os valores para doação vão de R$ 10,00 até R$ 1.000,00 e as recompensas variam de acordo com o valor doado.

**Meta fixa: R$ 45.000,00**

Prazo final para doação: 14 de abril de 2016

Site para doação: <https://www.catarse.me/cryptorave2016>

## Libre Graphics Meeting 2016

O
[Libre Graphics Meeting<i class="fa fa-external-link"></i>](http://libregraphicsmeeting.org/2016)
é o encontro anual de desenvolvedores e usuários de aplicativos gráficas de
software livre como Inkscape, Scribus, GIMP, Blender, Krita, Open Clip Art
Library, Open Font Library, etc, e que acontecerá de 15 a 18 de abrll de 2016
em Londres.

Você pode doar qualquer valor, e não há recompensas exceto o prazer de poder
contribuir para a realização do evento.

**Meta fixa: U$ 10.000,00**

Prazo final para doação: não informado.

Site para doação: <https://pledgie.com/campaigns/30935>

## Free Software Foundation

A [Free Software Foundation<i class="fa fa-external-link"></i>](http://www.fsf.org/)
mantém em seu site uma campanha contínua de arrecadação de doações que começa
com U$ 25,00.

Site para doação: <https://my.fsf.org/donate>

Você também pode se tornar um membro da FSF contribuindo mensalmente com o valor
de U$ 10,00 ou anualmente com U$ 120,00, e entidade envia para você um cartão de
associação USB bootável rodando Trisquel.

Se você contribuir com U$ 500,00 anuais, eles colocam seu nome na página de
contribuidores "Thank GNUs": <https://www.gnu.org/thankgnus/thankgnus.en.html>

Site para se tornar um membro:
<https://my.fsf.org/associate/support_freedom/join_fsf>

## Software Freedom Conservancy

A [Software Freedom Conservancy<i class="fa fa-external-link"></i>](https://sfconservancy.org/)
mantém em seu site uma campanha para angariar apoiadores que poderão contribuir
com um valor mensal mínimo de U$ 10 ou anual mínimo de U$ 120,00.

Se vocẽ escolher a doação anual ou no mínimo U$ 60,00 mensais, a entidade envia
uma camiseta para você. A entidade também mantém uma página com a lista de
doadores: <https://sfconservancy.org/sponsors/#supporters>

**Meta fixa: 2.500 contribuidores**

Site para doação: <https://sfconservancy.org/supporter/#donate-box>

## Account backup and restore for diaspora*

Um desenvolvedor Russo chamado Senya criou uma campanha de arrecadação para
financiar o desenvolvimento de uma funcionalidade de migração de conta
(backup/restore) para a rede social
[diaspora<i class="fa fa-external-link"></i>](https://diasporabr.com.br/).
Ele promete dedicar pelo menos 40 horas semanais para o trabalho e pretende
terminar em 3 meses. Caso ele acabe antes, ele vai dedicar o tempo para resolver
issues do diaspora.

Os valores para doação vão de € 5,00 a € 350,00  e as recompensas variam de acordo com o valor doado.

**Meta fixa: € 3.500,00**

Prazo final para doação: 21 de março de 2016

Site para doação:
<https://www.indiegogo.com/projects/account-backup-and-restore-for-diaspora--3#>

## Hackerspaces

Alguns hackerspaces brasileiros mantém campanhas de financiamento coletivo
recorrente, ou seja, o doador se compromete a doar mensalmente um valor que
ajudará a custear o espaço.

Raul Hacker Club: <https://unlock.fund/pt-BR/raulhc>

Calango Hacker Clube: <https://unlock.fund/pt-BR/calangohc>

Tarrafa Hacker Clube: <https://unlock.fund/pt-BR/tarrafahc>

Matehackers Hackerspace: <https://unlock.fund/pt-BR/matehackers>

Garoa Hacker Clube: <https://unlock.fund/pt-BR/garoa>