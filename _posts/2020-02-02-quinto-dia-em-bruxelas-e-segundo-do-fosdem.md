---
layout: post
title: Quinto dia em Bruxelas e segundo do FOSDEM
date: 2020-02-02 23:59
author: Paulo Henrique de Lima Santana
categories: geral
tags: bruxelas bélgica debian fosdem
---

# Quinto dia em Bruxelas e segundo do FOSDEM

Veja o texto anterior: [Quarto dia em Bruxelas e primeiro do FOSDEM](/quarto-dia-em-bruxelas-e-primeiro-do-fosdem).

Acordei mais cedo porque pretendia chegar às 9h no FOSDEM já que eu havia me
inscrito como voluntário para operar a câmera das 10h às 12h. Quando cheguei lá,
fui pegar outra camisa de voluntário, só que dessa vez a verde do time de vídeo.
Como tive que pegar a camisa na sala do time de vídeo, aproveitei para perguntar
qual era o hardware dos equipamentos que são usados no FOSDEM. O cara foi
bastante atencioso e explicou tudo.

![Bruxelas](/assets/img/bruxelas-2020-20.jpg)

![Bruxelas](/assets/img/bruxelas-2020-18.jpg)

Fui para o auditório e fiquei até meio-dia. Mas como o voluntário ainda não
havia passado distribuindo os vales pra gente comer, fiquei para mais uma palestra e
antes das 13h ele passou lá e entregou. O outro voluntário comentou que a gente
não precisava ficar na fila pra pedir a comida porque tínhamos preferência no
atendimento. Usei dessa prioridade porque tava com uma fome danada e pedi os
mesmos sanduíches do dia anterior.

![Bruxelas](/assets/img/bruxelas-2020-19.jpg)

Depois de almoçar fiquei novamente andando pelos standes, conversando, tomando
mais cervejas do OpenSuse :-) Fui para a [Community
devroom](https://fosdem.org/2020/schedule/track/community_devroom/) assistir
umas palestras. A primeira foi muito interessante, de um desenvolvedor que falou
sobre [Recognising Burnout](https://fosdem.org/2020/schedule/event/burnout/). O
cara até se emocionou durante a fala, vale a pena assistir o vídeo. Reconhecí
alguns dos sintomas que ele citou durante os meses que estive organizando a
DebConf19. A segunda foi [How Does Innersource Impact on the Future of Upstream
Contributions?](https://fosdem.org/2020/schedule/event/innersourceupstream/) do
Bradley Kuhn, que foi boa também.

Parece que o tempo passa mais rápido demais durante o FOSDEM, porque quando a
gente vê, o evento já acabou. Não tinha nada programado para a noite, então
fiquei com o Samuel e o Kanashiro pra ver o que eles iam fazer. Saímos em um
grupo para jantar perto da ULB. Dessa vez comi um macarrão a bolonhesa que tava
muito bom. Esse prato mais uma água ficou em € 15,10. Ainda ficamos um bom tempo
conversando, dessa vez além dos 3 brasileiros tinham pessoas da Alemanha,
Irlanda, França, Índia e Kosovo.

Fomos todos embora no mesmo trem e fizemos uma foto do grupo. Cheguei no hostel
bem cansado e fui dormir.

![Bruxelas](/assets/img/bruxelas-2020-21.jpg)

Fiquei com a impressão que esse ano tinham menos pessoas no
[FOSDEM](https://fosdem.org/2020/). Pelo menos ano passado a área dos standes
parecia bem mais cheia. Não sei, pode ser só impressão mesmo. A única parte
chata é que nos dois dias ficou uma chuvinha chata que ia e voltava, mas nao
chegou a chover forte. Estava menos frio que no ano passado, mas acho um pouco
irritante ficar com frio do lado de fora e quando a gente entra nos auditórios
um calor danado que dá pra suar se a gente não tirar os casacos. Então era um
tira e põe casaco porque o FOSDEM é distribuído em alguns prédios e é necessário
ir para a área externa para andar entre eles.

O evento foi incrível novamente e quero participar ano que vem! O pessoal do
Debian já começou a conversar sobre repetir a MiniDebCamp antes do FOSDEM 2021,
então tomara que dê tudo certo e que eu possa voltar.

![Bruxelas](/assets/img/bruxelas-2020-22.jpg)

Próximo texto: [Sexto dia em Bruxelas](/sexto-dia-em-bruxelas)