---
layout: post
title: Eventos de Software Livre em 2012
date: 2012-12-31 16:00
author: Paulo Henrique de Lima Santana
categories: eventos
tags: eventos
---

# Eventos de Software Livre em 2012

Obs: essa lista está em constante atualização.

Acesse também a agenda de eventos de Software Livre e Código Aberto no Brasil:

<http://agenda.softwarelivre.org>

## FEVEREIRO

~~**Campus Party Brasil 2012**  
06 a 12 de fevereiro de 2012  
São Paulo - SP  
<http://www.campus-party.com.br/2012/software-livre.html>~~

## MARÇO

~~**Document Freedom Day**  
28 de março de 2012  
Várias cidades  
<http://documentfreedom.org>~~

## ABRIL

~~**FLISOL 2012 - Festilval Latino Americano de Instalação de Software Livre**  
28 de abril de 2012  
Várias cidades  
<http://www.flisol.info>~~

## MAIO

~~**II Fórum da Revista Espírito Livre**  
29 de maio de 2012  
Vila Velha - ES  
<http://revista.espiritolivre.org/forum>~~

## JUNHO

~~**WordCamp Curitiba 2012**  
15 e 16 de junho de 2012  
Curitiba - PR  
<https://2012.curitiba.wordcamp.org>~~

~~**IV FSLDC - Fórum de Software Livre de Duque de Caxias**  
30 de junho de 2012  
Duque de Caxias - RJ  
<http://forumsoftwarelivre.com.br>~~

## JULHO

~~**IV FTSL - Fórum de Tecnologia em Software Livre**  
23 e 24 de julho de 2012  
Curitiba - PR  
<http://www.ftsl.org.br>~~

~~**13º FISL - Fórum Internacional de Software Livre**  
25 a 28 de julho de 2012  
Porto Alegre - RS  
<http://www.fisl.org.br>~~

~~**Campus Party Recife - Área de Software Livre**  
26 a 30 de julho de 2012  
Recife - PE  
<http://recife.campus-party.org>~~

## AGOSTO

~~**IV Encontro de Software Livre da UNESP em Ilha Solteira**  
10 e 11 de Agosto de 2012  
UNESP - Campus de Ilha Solteira - SP  
<http://www.feis.unesp.br/index.php/ivencsoftlivre>~~

~~**V GNUGRAF**  
17 e 18 de agosto de 2012  
Rio de Janeiro - RJ  
<http://gnugraf.org>~~

~~**Debian Day ou Dia do Debian**  
16 (ou 18) de agosto de 2012  
Várias cidades  
<http://wiki.debianbrasil.org/Eventos/DebianDayBrasil2012>~~

~~**TcheLinux Bagé 2012**  
18 de agosto de 2012  
Bagé - RS  
<http://cursos.unipampa.edu.br/cursos/engenhariadecomputacao/2012/08/09/tchelinux-bage-2012-programacao>~~

~~**EMSL'12 - Encontro Mineiro de Software Livre**  
22 a 24 de agosto de 2012  
Teófilo Otoni  
<http://emsl.softwarelivre.org>~~

~~**WordCamp São Paulo 2012**  
25 de agosto de 2012  
São Paulo - SP  
<http://saopaulo.wordcamp.org>~~

~~**Fórum Tchelinux.org - Edição Pelotas 2012**  
25 de agosto de 2012  
Pelotas - RS  
<http://tchelinux.org/site/doku.php?id=evento_2012_pelotas>~~

## SETEMBRO

~~**Joomla! Day Brasil 2012**  
07 e 08 de de setembro de 2012  
Belo Horizonte - MG  
<http://joomladaybrasil.org>~~

~~**SFD 2012 - Software Freedom Day**  
15 de setembro de 2012  
Várias cidades  
<http://softwarefreedomday.org>~~

~~**Seminário de Tecnologia em Software Livre TcheLinux Caxias do Sul**  
15 de setembro de 2012  
Caxias do Sul - RS  
<http://tchelinux.org/site/doku.php?id=evento_2012_setembro_caxias_do_sul>~~

~~**III Fórum da Revista Espírito Livre**  
27 de setembro de 2012  
Colatina - ES  
<http://revista.espiritolivre.org/iiiforumrel>~~

## OUTUBRO

~~**IV Fórum da Revista Espírito Livre**  
04 de outubro de 2012  
Serra - ES  
<http://revista.espiritolivre.org/ivforumrel>~~

~~**IX Latinoware - Conferência Latino Americana de Software Livre**  
17 a 19 de outubro de 2012  
Foz do Iguaçu - PR  
<http://www.latinoware.org>~~

~~**II Seminário de Tecnologia em Software Livre TcheLinux Bento Gonçalves**  
27 de outubro de 2012  
Bento Gonçalves - RS  
<http://tchelinux.org/site/doku.php?id=evento_2012_outubro_bento_goncalves>~~

## NOVEMBRO

~~**CONISLI - Congresso Internacional de Software Livre**  
09 e 10 de novembro de 2012  
Guarulhos - SP  
<http://www.conisli.org.br>~~

~~**IV FASOL - Fórum Amazônico de Software Livre**  
12 a 14 de novembro de 2012  
Santarém - PA  
<http://www.fasol.org.br>~~

~~**BlenderPro 2012 - Conferência Nacional de Blender**  
15 a 18 de novembro de 2012  
Brasília - DF  
<http://www.blender.pro.br>~~

~~**PythonBrasil[8]**  
21 a 24 de novembro de 2012  
Rio de Janeiro - RJ  
<http://www.pythonbrasil.org.br>~~

~~**Fórum Software Livre Tchelinux.org**  
24 de novembro de 2012  
Porto Alegre - RS  
<http://tchelinux.org/site/doku.php?id=evento_2012_11_poa>~~

~~**7o. SOLISC - Congresso Catarinense de Software Livre**  
30 de novembro de 2012  
São José - SC  
<http://www.solisc.org.br>~~

~~**9o. FGSL - Fórum Goiano de Software Livre**  
30 de novembro a 01 de dezembro de 2012  
Goiânia - GO  
<http://fgsl.aslgo.org.br>~~

## DEZEMBRO

~~**V CONSEGI - Congresso Internacional Software Livre e Governo Eletrônico**  
03 a 07 de dezembro de 2012  
Belém - PA  
<http://www.consegi.gov.br>~~

~~**COMSOLiD+5 - 5º Encontro da Comunidade Maracanauense de Software Livre e Inclusão Digital**  
06 a 08 de dezembro de 2012  
Maracanaú - CE  
<http://www.comsolid.org>~~

## Anos anteriores

[2011](<http://softwarelivre.org/blog/eventos-de-software-livre-em-2011)  
