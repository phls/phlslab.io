---
layout: post
title: Eventos de Software Livre em 2016
date: 2016-12-31 16:00
author: Paulo Henrique de Lima Santana
categories: eventos
tags: eventos
---

# Eventos de Software Livre em 2016

Obs: essa lista está em constante atualização.

Acesse também a agenda de eventos de Software Livre e Código Aberto no Brasil:

<http://agenda.softwarelivre.org>

## JANEIRO

~~**Campus Party Brasil 2016 - CPBR9**  
26 a 31 de janeiro de 2016  
São Paulo - SP  
<http://brasil.campus-party.org>~~

~~**2º Encontro PyLadies Floripa**  
30 de janeiro de 2016  
Florianópolis - SC  
<https://www.facebook.com/events/1660347674205807>~~

## FEVEREIRO

~~**PHP com Rapadura in Fortaleza**  
26 e 27 de Fevereiro de 2016  
Fortaleza - CE  
<http://phpcomrapadura.org>~~

## MARÇO

~~**PgDay Curitiba**  
03 de março de 2016  
Curitiba - PR  
<http://www.pgdaycuritiba.pr.gov.br>~~

~~**Open Data Day**  
05 de março de 2016  
Várias cidades  
<http://opendataday.org>~~

~~**Mini-DebConf Curitiba 2016**  
05 e 06 de março de 2016  
Curitiba - PR  
<http://br2016.mini.debconf.org>~~

~~**Open Education Week**  
07 a 11 de março de 2016  
Várias cidades  
<http://www.openeducationweek.org>~~

~~**Workshop gratuito de programação para mulheres - Django Girls Rio de Janeiro**  
12 de março de 2016  
Rio de Janeiro - RJ  
<https://djangogirls.org/riodejaneiro1>~~

~~**Workshop gratuito de programação para mulheres - Django Girls Campinas**  
12 de março de 2016  
Campinas - SP  
<https://djangogirls.org/campinas>~~

~~**Darkmira PHP Tour Brasil**  
18 a 20 de março de 2016  
Brasília - DF  
<https://br.darkmiratour.com>~~

~~**DFD 2016 - Document Freedom Day**  
30 de março de 2016  
Várias cidades  
<http://documentfreedom.org>~~

## ABRIL

~~**Arduino Day**  
02 de abril de 2016  
Várias cidades  
<http://day.arduino.cc>~~

~~**Zabbix Conference na América Latina**  
15 e 16 de abril de 2016  
Porto Alegre - RS  
<http://www.zabbix.com/conference_latam_2016.php>~~

~~**FLISOL 2016 - Festival Latino-americano de Instalação de Software Livre**  
16 de abril de 2016  
Várias cidades  
<http://www.flisol.org.br>~~

~~**Joomla Day Bahia**  
21 e 22 de abril de 2016  
Salvador - BA  
<http://jdayba.com.br>~~

~~**Seminário de Tecnologia em Software Livre TchêLinux**  
23 de abril de 2016  
Lajeado  - RS  
<http://lajeado.tchelinux.org>~~

## MAIO

~~**12º Fórum Espírito Livre**  
04 a 06 de maio de 2016  
Vila Velha - ES  
<http://forum.espiritolivre.org/12ed>~~

~~**1ª Semana da Computação da UFF**  
04 a 07 de maio de 2016  
Rio das Ostras - RJ  
<https://semanadacomputacaouff.wordpress.com>~~

~~**CryptoRave**  
06 a 07 de maio de 2016  
São Paulo - SP  
<https://cryptorave.org>~~

~~**Conferência PHPRS 2016**  
07 de maio de 2016  
Porto Alegre - RS  
<http://www.eventick.com.br/conferencia-phprs-2016>~~

~~**Workshop gratuito de programação para mulheres - Django Girls Recife**  
13 e 14 de maio de 2016  
Recife - PE  
<https://djangogirls.org/recife2016>~~

~~**Scratch Day**  
14 de maio de 2016  
Várias cidades  
<http://day.scratch.mit.edu>~~

~~**SciPy Latin America 2016 - Computação Científica com Python**  
16 a 20 de maio de 2016  
Florianópolis - SC  
<http://scipyla.org/conf/2016>~~

~~**Rails Girls Novo Hamburgo**  
20 e 21 de maio de 2016  
Novo Hamburgo - RS  
<http://railsgirls.com/novohamburgo>~~

~~**LaKademy 2016 - 4º Encontro LatinoAmericano dos Colaboradores do KDE**  
26 a 29 de maio de 2016  
Rio de Janeiro - RJ  
<https://br.kde.org/lakademy2016>~~

~~**1º Encontro de Dev's PHP com Rapadura**  
28 de maio de 2016  
Fortaleza - CE  
<http://encontro.phpcomrapadura.org>~~

## JUNHO

~~**13º Fórum Espírito Livre**  
08 a 10 de junho de 2016  
Santa Tereza - ES  
<http://forum.espiritolivre.org/13ed>~~

~~**IV Python Nordeste**  
09 a 11 de junho de 2016  
Teresina - PI  
<http://2016.pythonnordeste.org>~~

~~**Seminário de Tecnologia em Software Livre TchêLinux**  
11 de junho de 2016  
Bagé  - RS  
<http://bage.tchelinux.org>~~

~~**RuPy Campinas 2015**  
18 de junho de 2016  
Campinas - SP  
<http://campinas.rupy.com.br>~~

~~**BHack Conference**  
25 e 26 de junho de 2016  
Belo Horizonte - MG  
<http://www.bhack.com.br>~~

~~**DrupalCamp Campinas 2016**  
25 de junho de 2016  
Campinas - SP  
<http://campinas2016.drupalcamp.com.br>~~

~~**Caipyra - Conferência de Python em Ribeirão Preto**  
25 e 26 de junho de 2016  
Ribeirão Preto - SP  
<http://2016.caipyra.python.org.br>~~

~~**COMSOLiD 9 - 9° Encontro da Comunidade Maracanauense de Software Livre e Inclusão Digital**  
29 de junho a 01 de julho de 2016  
Maracanaú - CE  
<http://www.comsolid.org>~~

## JULHO

~~**LuaConf 2016**  
09 de julho de 2016  
Rio de Janeiro - RJ  
<http://luaconf.com>~~

~~**FISL17 - Fórum Internacional Software Livre**  
13 a 16 de julho de 2016  
Porto Alegre - RS  
<http://www.fisl.org.br>~~

~~**WordCamp Belo Horizonte**  
23 de julho de 2016  
Belo Horizonte - MG  
<https://2016.belohorizonte.wordcamp.org>~~

## AGOSTO

~~**Workshop gratuito de programação para mulheres - Django Girls Porto Alegre**  
12 e 13 de agosto de 2016  
Porto Alegre - RS  
<https://djangogirls.org/portoalegre>~~

~~**WordCamp Fortaleza**  
13 e 14 de agosto de 2016  
Fortaleza - CE  
<https://fortaleza.wordcamp.org/2016>~~

~~**Debian Day ou Dia do Debian**  
16 (ou 13) de agosto de 2016  
Várias cidades  
<https://wiki.debian.org/DebianDay/2016>~~

~~**1º PHPSP+IMA**  
20 de agosto de 2016  
Campinas - SP  
<http://phpspima.com.br>~~

~~**2º Tech Day do GURU-PR**  
20 de agosto de 2016  
Curitiba - PR  
<http://www.gurupr.org/eventos/2-tech-day-do-guru-pr>~~

~~**Campus Party Recife 2016 - CPRecife5**  
20 e 21 de agosto de 2016  
Recife - PR  
<http://recife.campus-party.org>~~

~~**VIII FTSL - Fórum de Tecnologia em Software Livre**  
31 de agosto a 02 de setembro de 2016  
Curitiba - PR  
<http://www.ftsl.org.br>~~

## SETEMBRO

~~**PgDay Curitiba**  
02 de setembro de 2016  
Curitiba - PR  
<http://pgdaycuritiba.blogspot.com.br>~~

~~**1ª Conferência das Comunidades Python do Sudeste**  
02 e 03 de setembro de 2016  
Belo Horizonte - MG  
<http://2016.pythonsudeste.org>~~

~~**GNUGRAF**  
03 de setembro de 2016  
Niterói - RJ  
<http://gnugraf.org>~~

~~**Consoline - Congresso de Software Livre do Nordeste**  
03 de setembro de 2016  
Recife - PE  
<http://www.softwarelivrene.org>~~

~~**SFD 2016 - Software Freedom Day**  
17 de setembro de 2016  
Várias cidades  
<http://softwarefreedomday.org>~~

~~**2º Encontro de Dev's PHP com Rapadura**  
17 de setembro de 2016  
Fortaleza - CE  
<http://encontro.phpcomrapadura.org>~~

~~**RubyConf Brasil 2016**  
23 e 24 de setembro de 2016  
São Paulo - SP  
<http://rubyconfbrcfp.com.br>~~

~~**WordCamp Rio de Janeiro**  
24 de setembro de 2016  
Rio de Janeiro - RJ  
<https://2016.riodejaneiro.wordcamp.org>~~

~~**V Fasol - Fórum Amazônico de Software Livre**  
29 e 30 de setembro de 2016  
Santarém - PA  
<http://opentapajos.org/fasol2016>~~

## OUTUBRO

~~**Seminário de Tecnologia em Software Livre TchêLinux**  
01 de outubro de 2016  
Caxias do Sul - RS  
<http://caxias.tchelinux.org>~~

~~**PGDay Campinas 2015**  
06 de outubro de 2016  
Campinas - SP  
<http://pgdaycampinas.com.br>~~

~~**Rails Girls Belo Horizonte**  
07 a 08 de outubro de 2016  
Belo Horizonte - MG  
<http://railsgirls.com/belohorizonte>~~

~~**OpenStack Day 2016**  
08 de outubro de 2016  
São Paulo - SP  
<http://openstackbr.com.br/events/2016/openstack-day>~~

~~**PHPest - Conferência de PHP do Nordeste**  
08 e 09 de outubro de 2016  
Salvador - BA  
<http://phpeste.net>~~

~~**PythonBrasil[12] - 12ª Conferência Brasileira da Comunidade Python**  
13 a 18 de outubro de 2016  
Florianópolis - SC  
<http://2016.pythonbrasil.org.br>~~

~~**Workshop gratuito de programação para mulheres - Django Girls Florianópolis**  
14 de outubro de 2016  
Florianópolis - SC  
<https://djangogirls.org/florianopolis>~~

~~**XIII Latinoware - Conferência Latino-americana de Software Livre**  
19 a 21 de outubro de 2016  
Foz do Iguaçu - PR  
<http://www.latinoware.org>~~

~~**4º Café com Software Livre**  
22 de outubro de 2016  
Indaial - SC  
<http://blusol.org>~~

~~**1º e-HAL - Encontro Brasileiro de Hardware Aberto e Livre**  
29 a 31 de outubro de 2016  
São Paulo - SP  
<http://e-hal.org.br>~~

## NOVEMBRO

~~**Campus Party Belo Horizonte 2016**  
09 a 13 de novembro de 2016  
Belo Horizonte - MG  
<http://minasgerais.campus-party.org>~~

~~**XXX ENECOMP - Congresso Nacional dos Estudantes de Computação**  
16 e 19 de novembro de 2016  
Goiânia - GO  
<http://2016.enecomp.org.br>~~

~~**XIII FGSL - Fórum Goiano de Software Livre**  
18 e 19 de novembro de 2016  
Goiânia - GO  
<http://2016.fgsl.net>~~

~~**TchêLinux Porto Alegre - Especial 10 anos do grupo**  
19 de junho de 2016  
Porto Alegre  - RS  
<http://poa.tchelinux.org>~~

~~**PotiCon - Conferência Potiguar de Software Livre**  
19 e 20 de novembro de 2016  
Natal - RN  
<http://www.potilivre.org/poticon>~~

~~**UEaDSL – Universidade, EaD e Software Livre**  
21 a 25 novembro de 2016  
a distância  
<https://www.ufmg.br/ead/ueadsl>~~

~~**Workshop gratuito de programação para mulheres - Django Girls Maceió**  
26 de novembro de 2016  
Maceió - AL  
<https://djangogirls.org/maceio>~~

~~**Workshop gratuito de programação para mulheres - Django Girls Rio de Janeiro**  
26 de novembro de 2016  
Rio de Janeiro - RJ  
<https://djangogirls.org/riodejaneiro>~~

## DEZEMBRO

~~**Rails Girls Floripa**  
02 e 03 de dezembro de 2016  
Florianópolis - SC  
<http://railsgirls.com/florianopolis>~~

~~**11ª PHP Conference Brasil**  
07 e 16 de dezembro de 2016  
Osasco - SP  
<http://www.phpconference.com.br>~~

~~**Rails Girls São Paulo**  
09 e 10 de dezembro de 2016  
São Paulo - SP  
<http://railsgirls.com/saopaulo>~~

~~**Rails Girls Salvador**  
09 e 10 de dezembro de 2016  
Salvador - BA  
<http://railsgirls.com/salvador201612>~~

~~**WordCamp São Paulo**  
10 de dezembro de 2016  
São Paulo  
<https://2016.saopaulo.wordcamp.org>~~


## Anos anteriores

[2015](http://softwarelivre.org/blog/eventos-de-software-livre-em-2015)  
[2014](http://softwarelivre.org/blog/eventos-de-software-livre-em-2014)  
[2013](http://softwarelivre.org/blog/eventos-de-software-livre-em-2013)  
[2012](http://softwarelivre.org/blog/eventos-de-software-livre-em-2012)  
[2011](http://softwarelivre.org/blog/eventos-de-software-livre-em-2011)  
