---
layout: post
title: Como foi a vinda do Richard Stallman para Curitiba em 2017 - parte 1
date: 2017-09-04 16:00
author: Paulo Henrique de Lima Santana
categories: eventos
tags: richard-stallman curitiba-livre software-livre
---

# Como foi a vinda do Richard Stallman para Curitiba em 2017 - parte 1 

[Richard Stallman (RMS)<i class="fa fa-external-link"></i>](https://stallman.org/),
fundador do movimento
[Software Livre<i class="fa fa-external-link"></i>](https://pt.wikipedia.org/wiki/Software_livre), do
[Projeto GNU<i class="fa fa-external-link"></i>](http://gnu.org/), e da
[Free Software Foundation (FSF)<i class="fa fa-external-link"></i>](http://fsf.org/),
e um dos autores da
[Licença Pública Geral GNU (GNU GPL)<i class="fa fa-external-link"></i>](https://www.gnu.org/licenses)
esteve no Brasil durante alguns dias de maio e junho de 2017 palestrando em
algumas cidades.

![Logos GNU FSF GPL](/assets/img/banner-logos-gnu-fsf-gpl.png){:width="600px"}

Esse texto é um relato da passagem do Stallman em duas cidades que eu ajudei a
organizar as palestras:

 * Curitiba - onde eu moro e faço parte da Comunidade
[Curitiba Livre<i class="fa fa-external-link"></i>](http://curitibalivre.org.br).
 * Brasília - durante a 1ª edição da Campus Party Brasília onde eu também
palestrei (e sou curador de Software Livre da Campus Party Brasil em São Paulo).

Dividi o texto em duas publicações para a leitura não ficar muito cansativa:

 1. O planejamento para a vinda do Stallman (este texto).
 1. O Stallman em [Curitiba<i class="fa fa-external-link"></i>](http://phls.com.br/como-foi-a-vinda-do-richard-stallman-para-curitiba-em-2017-parte-2).

## Porque escrevi esse texto

Existe um texto que circula na internet há muitos anos relatando a vinda do
Stallman para palestrar em Curitiba em 2000. O texto é de uma pessoa da empresa
Conectiva responsável por trazê-lo e apresenta uma visão extremamente negativa
sobre o Stallman. Ele relata como foi hospedar o Stallman na sua casa, com
detalhes que alguns (como eu) acharam bastante antiéticos por expor a intimidade
do convidado. Eu só posso acreditar que autor do texto ficou tão traumatizado
com a presença do Stallman em sua casa que decidiu escrever um texto o
ridicularizando e isso soou muito engraçado para alguns, e nem um pouco para
outros (como eu). Além do texto em si, na mesma postagem existem muitos
comentários depreciativos com relação ao Stallman.

Nos dias que o Stallman esteve em Curitiba (1 a 3 de junho de 2017), ele ficou
hospedado em minha casa. Não espere encontrar aqui relatos do tipo "se o Stallman
toma banho ou não". Esse é um dos tipos de fofoca que o relato de 2000 citado
anteriormente ajudou a criar.

Espero que esse texto ajude a tirar dúvidas de outras pessoas interessadas em
promover uma palestra do Stallman na sua cidade. Existem muitos detalhes que
precisam ser observados, especialmente sobre como lidar com o Stallman, o que eu
confesso que não é uma tarefa muito fácil. Mas é plenamente viável se você tiver
paciência, jogo de cintura, e principalmente se você não tratá-lo como um ídolo
endeusado que precisa ser paparicado o tempo todo.

Eu acredito que o Stallman tem uma importância histórica muito grande por ter
criado o movimento Software Livre. Sou um admirador pela sua coragem e
perseverança em continuar defendendo aquilo que ele acredita e viajando o mundo
todo para falar sobre isso. Não sou daquelas pessoas que tem um Stallman como um
"modelo de vida a ser seguido". Nunca fui de ter ídolos como cantores(as),
artistas, etc, e acredito que como todo ser humano, o Stallman tem qualidades e
defeitos (alguns dos quais eu vi pessoalmente). Existem muitas histórias
(ou estórias) sobre o Stallman principalmente sobre ele ser uma pessoa grosseira,
mas minha vontade era justamente saber o que daquilo era verdade e o que era
mentira (ou exagero). No final eu pensei: mesmo se ele realmente for uma pessoa
muito difícil de lidar, quantas pessoas poderão dizer que tiveram a oportunidade
de conviver três dias com o cara que criou a porra toda??? :-)

## A vinda para o Brasil

A vinda do Stallman para o Brasil este ano foi promovida pelo
[Prof. Loïc Cerf<i class="fa fa-external-link"></i>](http://www.dcc.ufmg.br/dcc/?q=pt-br/node/221)
do Departamento de Ciência da Computação da Universidade Federal de Minas Gerais
(UFMG). A palestra do Stallman em Belo Horizonte (a primeira no Brasil) fez parte
das atividades para comemorar os
[90 anos<i class="fa fa-external-link"></i>](https://www.ufmg.br/90anos/) da
instituição. Então as passagens internacionais para ele vir dos Estados Unidos e
voltar, foram custeadas por esse evento.

Quando o Stallman vem ao Brasil trazido por algum evento, ele procura ficar mais
alguns dias para que outras pessoas o levem para outras cidades, seja para
palestrar em um evento (como foi na Campus Party Brasília) ou apenas para uma
palestra solo (como foi em Curitiba e Campinas). Para quem quer promover uma
palestra do Stallman, o custo das passagens nacionais dentro do Brasil fica
muito menor do que se tivesse que trazê-lo dos Estados Unidos com passagens
internacionais.

Para conseguir atender a todos, o Stallman precisa montar uma agenda para saber
quando ele vai chegar na cidade, palestrar e ir embora para outra cidade. Também
é preciso combinar quais passagens serão necessárias custear. Por exemplo, para
trazê-lo, nós compramos 2 passagens: de Campinas (onde ele havia palestrado
antes) para Curitiba, e de Curitiba para Foz do Iguaçu (ele ia cruzar a fronteira
com a Argentina por terra para ir palestrar em algumas cidades por lá). Já o
pessoal da Campus Party precisou comprar duas passagens: de Buenos Aires para
Puerto Iguazú (Argentina), para ele cruzar a fronteira por terra, e de Foz do
Iguaçu para Brasília. Então a logística de passagens pode ser simples como foi
para ele vir para Curitiba, ou pode ser complexa como foi para ele ir para a
Brasília.

![Mapa Stallman tour](/assets/img/mapa-brasil-stallman-2017.png){:width="600px"}

<center>Stallman Tour Brasil e Argentina 2017</center>

## Primeiro contato e palestra em 2012

Essa foi a segunda vez que eu ajudei a organizei uma palestra do Stallman em
Curitiba. Em dezembro 2012 uma instituição o trouxe para uma reunião em Curitiba,
e entraram em contato comigo para aproveitar a presença dele e promover uma
palestra. O contato comigo foi feito no dia 10 e como a palestra deveria ser no
dia 13, foi preciso correr contra o tempo para reservar o auditório e divulgar.
Por ter estudado na UFPR e organizado alguns eventos por lá, foi mais fácil
resolver a logística do auditório por e-mails e telefone. Dessa vez eu só tive
contato com o Stallman durante a palestra, não o encontrei nos outros dias porque
eu havia operado o tornozelo e estava usando muletas para me locomover como pode
ser visto nessa foto.

![Paulo e Stallman em 2012](/assets/img/paulo-stallman-2012.jpg){:width="600px"}

No dia da palestra só foi possível eu ir para o local porque meu pai estava de
babá comigo e me levou. O auditório estava lotado, mesmo não havendo tradução
simultânea. O Stallman fala bastante devagar durante a palestra, então quem tem
um mínimo de noção em inglês consegue entender a grande maioria das coisas que
ele diz.

Tudo ocorreu bem, ou quase tudo: havíamos comprado algumas garrafas de água da
marca Crystal que predomina no mercado. Quando o Stallman pegou uma delas, viu
que era fabricado pela da Coca-Cola e se recusou a tomar explicando que ele faz
parte de uma campanha que boicota os produtos dessa marca. Pedi pro meu pai
correr nas lanchonetes da Universidade para comprar outra marca e foi bastante
difícil de achar, mas ainda bem que ele conseguiu e o problema foi resolvido.
Os detalhes da palestra podem ser vistos
[nessa<i class="fa fa-external-link"></i>](http://www.curitibalivre.org.br/blog/palestra-stallman-ufpr/)
notícia, nas
[fotos<i class="fa fa-external-link"></i>](https://www.flickr.com/photos/curitibalivre/albums/72157657347552866),
e no
[vídeo<i class="fa fa-external-link"></i>](http://audio-video.gnu.org/video/palestra-stallman-ufpr.ogg)
que eu gravei e está disponível no site da FSF.

A vinda do Stallman em 2012 para o Brasil e Argentina foi bastante controversa.
Em Goiânia aconteceu um desentendimento entre ele e a organização do
[Fórum Goiano de Software Livre (FGSL)<i class="fa fa-external-link"></i>](http://fgsl.net/)
por causa da mudança do idioma durante a palestra. Pelo que entendi, eles haviam
combinado que a palestra seria em inglês, mas o Stallman resolveu falar em
espanhol e o pessoal foi saindo do auditório. Então no meio da palestra a
organização pediu para ele trocar para o inglês, o que o deixou muito furioso,
a ponto dele bater várias vezes o microfone na mesa e dizer que não continuaria
a palestra porque se as pessoas não tinham entendido nada do que ele disse até
ali em espanhol, e por isso teria que recomeçar tudo de novo em inglês. Após
alguns minutos, ele se acalmou e retomou a palestra em inglês. Existe um vídeo
por aí mostrando o quanto a situação foi complicada, especialmente porque o
pessoal na plateia começou a rir achando que ele estava brincando quando na
verdade ele estava muito nervoso. Infelizmente as pessoas lembram desse episódio
até hoje e não sei porque cargas d'água acham que isso aconteceu em Curitiba. Em
Brasília ele ficou hospedado na casa de uma pessoa que não tinha ar-condicionado
no quarto, e como era dezembro e estava muito quente foi preciso achar de última
hora outra casa para ele ficar. Na Argentina o Stallman teve os equipamentos e
todo o dinheiro roubados.

## Outras conversas

No início de 2015 surgiu a possibilidade do Stallman vir ao Brasil participar de
um evento em João Pessoa, então fui incluído na conversa para organizar a
logística para ele ir também para o
[FISL16<i class="fa fa-external-link"></i>](http://softwarelivre.org/fisl16) em
Porto Alegre (eu estava trabalhando na organização da programação do evento) e
para a Campus Party Recife (eu era curador de Software Livre). O pessoal da
organização da Campus Party ficou bastante animado em ter o Stallman palestrando
no palco principal e claro, eu também. Após várias trocas de e-mails com o
Stallman, ele acabou desistindo de vir por causa de outros compromissos.

Nessa época o Stallman recebeu uma mensagem de divulgação do FISL e no texto de
um dos palestrantes estava escrito "biblioteca open source de vídeo". Ele
questionou porque o FISL estava usando a palavra "open source" ao invés de
"software livre". Passamos alguns meses trocando e-mails sobre isso e o que eu
tentei explicar é que para algumas pessoas da comunidade, usar "open source /
código aberto" era a mesma coisa que usar "software livre". Então ele ressaltou
a importância de usar sempre o termo software livre.

No final de 2015 entrei em contato com o Stallman para saber se ele poderia vir
para o 
[FISL17<i class="fa fa-external-link"></i>](http://softwarelivre.org/fisl17)
(eu estava novamente trabalhando na organização) e ele respondeu
que sim. Mas em abril de 2016 tivemos que desistir do convite por causa da falta
de recursos para trazê-lo. Quem acompanhou o FISL17 sabe que o evento quase foi
cancelado por falta de dinheiro, e para economizar, não foi possível custear a
vinda de palestrantes internacionais como nos outros anos.

Depois disso conversamos outras vezes para saber se ele poderia vir para algumas
das edições da Campus Party em 2016 em Recife, em Brasília (para o lançamento) e
em Belo Horizonte, e para a Campus Party Brasil em São Paulo em 2017, mas ele já
tinha outros compromissos e não foi possível. Nesse último convite para a Campus
Party Brasil ele me avisou que deveria vir ao Brasil entre maio e junho de 2017.

Em todas essas trocas de e-mails eu procurava sempre copiar o
[Alexandre Oliva<i class="fa fa-external-link"></i>](http://www.fsfla.org/~lxoliva/),
diretor da [FSF Latin America<i class="fa fa-external-link"></i>](http://www.fsfla.org/)
e contato primário do Stallman aqui no Brasil, para que ele pudesse acompanhar as
conversas e ficar a par de tudo, e ajudar quando eu precisasse de um socorro. Na
palestra em 2012 o Oliva também estava acompanhando as conversas.

##  Planejamento

No final de fevereiro de 2017 o Oliva entrou em contato comigo para avisar que
estava confirmada a vinda do Stallman para o Brasil para palestrar em Belo
Horizonte no final de maio. Ele perguntou se eu tinha interesse em levar o
Stallman para palestrar em Curitiba no começo de junho depois dele palestrar em
Campinas. O Oliva me avisou também que depois de Curitiba o Stallman seguiria
para Argentina para palestrar em algumas cidades por lá e depois retornaria ao
Brasil para ir embora para os EUA. Respondi que sim, que queríamos trazer o
Stallman para palestrar em Curitiba, e que como na metade de junho teríamos a
[Campus Party de Brasília<i class="fa fa-external-link"></i>](http://brasil.campus-party.org/brasilia/),
podíamos propor ao Stallman ir palestrar lá também quando ele retornasse da
Argentina.

Alguns dias depois o Stallman respondeu pra mim e para o Oliva avisando que
tinha interesse em ir para a Campus Party de Brasília e para começarmos a
acertar a logística das viagens. A organização da Campus Party ficou feliz em
finalmente ter o Stallman em uma das suas edições como um dos palestrantes principais, depois de várias tentativas.

Começamos a acertar os detalhes da vinda para Curitiba e coloquei o Stallman em
contato com a organização da Campus Party para eles acertarem os detalhes de
passagens e hospedagem para Brasília. Como comentei antes, a vinda para Curitiba
foi simples porque eram duas passagens diretas: Campinas -> Curitiba e Curitiba
-> Foz do Iguaçu, e os horários dos voos ficaram bons para ele. A ida para
Brasília foi um pouco mais complicada porque ele queria três passagens: Buenos
Aires -> Puerto Iguazú, Foz do Iguaçu -> Campinas (ele queria dormir uma noite
lá), e Campinas -> Brasília. Mas os horários dos voos de Foz para Campinas não
se encaixavam no que ele gostaria de fazer. Após vários e-mails trocados entre o
Stallman e o pessoal da Campus para tentar achar os voos, tive que explicar que
estava difícil conseguir o que ele queria porque o aeroporto de Foz é pequeno e
tem poucas opções de voos. Então ele desistiu de ir dormir em Campinas e
concordou em pegar um voo pra Brasília.

Aqui em Curitiba nossa ideia inicial era usar o
[Teatro da Reitoria<i class="fa fa-external-link"></i>](http://www.teatrodareitoria.ufpr.br/)
da UFPR que tem capacidade para 700 pessoas, mas ele já tinha uma reserva para
outra atividade no dia 2 de junho. Então passamos para segunda opção em termo de
capacidade: o auditório do
[Setor de Ciências Sociais Aplicadas<i class="fa fa-external-link"></i>](http://www.sociaisaplicadas.ufpr.br/)
da UFPR onde cabem 350 pessoas, é o que tem a melhor infraestrutura e é o mais
confortável da Universidade.

![Auditório da UFPR 1](/assets/img/auditorio-4.jpg){:width="300px"}
![Auditório da UFPR 2](/assets/img/auditorio-3.jpg){:width="300px"}

<center>Auditório do Setor de Ciências Sociais Aplicadas - Fotos: Rebeca Mileski - galeria da APUFPR</center>
<br />

Entrei em contato com o Matheus Horstmann, estudante de Ciência da Computação e
então diretor do Centro Acadmico do curso (chamado 
[CEI - Centro de Estudos de Informática<i class="fa fa-external-link"></i>](http://cei.inf.ufpr.br/))
para ajudar na organização porque ele já tinha organizado uma caravana de
estudantes para o FISL em 2016 e é interessado em Software Livre. O Matheus
curtiu muito a ideia e então por meio do CEI, pedimos ajuda aos professores
[Eduardo Todt<i class="fa fa-external-link"></i>](http://www.inf.ufpr.br/todt/) e
[Daniel Weingaertner<i class="fa fa-external-link"></i>](http://web.inf.ufpr.br/danielw/)
do [Departamento de Informática<i class="fa fa-external-link"></i>](http://www.inf.ufpr.br/dinf/)
da UFPR e do
[Centro de Computação Científica e Software Livre (C3SL)<i class="fa fa-external-link"></i>](https://www.c3sl.ufpr.br/),
para custear as passagens e a hospedagem do Stallman, e reservar o auditório
junto ao Setor de Ciências Sociais Aplicadas.

Como as passagens do Stallman não eram de ida e volta para a mesma cidade, o C3SL
só teve como comprar uma passagem, a de Campinas para Curitiba. Com a hospedagem
no hotel estava tudo certo. O complicador foi que o Setor de Ciências Sociais
Aplicadas nos pediu para pagar R$ 500,00 para o uso do auditório, mesmo a gente
explicando que não se tratava de um evento com fins lucrativos, e que não
estávamos cobrando inscrição. A direção do Setor foi irredutível e aí o professor
[Marcos Sunye<i class="fa fa-external-link"></i>](http://web.inf.ufpr.br/msunye/),
diretor do
[Setor de Ciências Exatas<i class="fa fa-external-link"></i>](http://www.exatas.ufpr.br/),
nos salvou fazendo uma transferência de recursos entre os dois setores.

Sobraram então três custos: passagem de Curitiba para Foz do Iguaçu, alimentação
e outras eventuais despesas. Com o lucro que obtemos com a venda dos nossos
[produtos da comunidade<i class="fa fa-external-link"></i>](http://loja.curitibalivre.org.br/),
seria possível cobrir estes custos. Tivemos então a ideia de contratar uma
empresa para fazer a tradução simultânea da palestra, o que permitiria a
participação de mais pessoas. Após alguns orçamentos, concluímos que seria
necessário R$ 3.000,00 para contratar os equipamentos e o tradutor. Perguntei ao
Stallman se ele concordava em a gente cobrar uma taxa de inscrição para pagar a
tradução e ele disse que não concordava, porque ele preferia que a palestra fosse
gratuita para permitir que qualquer pessoa pudesse assistir, e sugeriu a gente
chamar alguém para ficar ao lado dele traduzindo as falas. Outras pessoas da
comunidade também sugeriram essa solução, mas eu já tinha assistido palestras
nesse formato e é bastante complicado porque atrapalha o andamento da palestra.
Nossa intenção era fazer algo profissional e confortável para os participantes,
mesmo que tivesse um custo mais elevado. Então tivemos a ideia de fazer um
financiamento coletivo, e isso o Stallman concordou.

Decidimos não usar uma plataforma de crowdfunding como o Catarse porque eles
descontam uma taxa alta, o resgate do dinheiro demora um pouco e não é Software
Livre :-) Usamos nossas contas bancárias pessoais e nossas contas no PayPal e no
PagSeguro para receber, o que deu um certo trabalho porque a conferência
precisava ser feita com bastante cuidado. Como não foi um volume grande de
doações, foi viável fazer assim. Quando conversei com o Stallman pessoalmente
sobre a nossa campanha, ele nos elogiou por não ter usado uma dessas plataformas
prontas e disse que a FSF usa uma
[ferramenta própria<i class="fa fa-external-link"></i>](https://civicrm.org/).

O financiamento coletivo foi um sucesso porque as pessoas se sentiram motivas a
contribuir, mesmo aquelas que moram em outras cidades. As doações ficaram um
pouco abaixo da meta estabelecida, mas adicionamos o saldo que havia sobrado do
patrocínio da [APUFPR<i class="fa fa-external-link"></i>](http://apufpr.org.br/)
para o
[FLISOL 2017<i class="fa fa-external-link"></i>](http://softwarelivre.org/flisol2017-curitiba)
em Curitiba, o patrocínio da empresa
[Ambiente Livre<i class="fa fa-external-link"></i>](http://www.ambientelivre.com.br/)
para a palestra, e usamos um pouco do lucro das vendas dos produtos. Com esse
montante, foi possível contratar a tradução e pagar todas as despesas. Os
detalhes do financiamento coletivo e a prestação de contas estão abaixo.

![Prestação de contas](/assets/img/prestacao-contas-stallman-2017.png){:width="600px"}

<center>Prestacao contas Stallman 2017</center>
<br />

Pouco antes de vir para Curitiba, o Stallman me perguntou se não teria uma casa
para ele ficar ao invés do hotel. Acredito que a imensa maioria das pessoas acha
estranho um convidado preferir ficar hospedado na casa de uma pessoa que ele não
conhece do que ficar no conforto de um hotel. Já tínhamos ouvido muitas histórias
sobre o quanto poderia ser complicado hospedar o Stallman em casa, como os casos
que eu citei no início desse texto quando em 2000 ele veio pra Curitiba, e em
2012 ele foi para Brasília. Como seriam apenas duas noites, eu e
[Adriana<i class="fa fa-external-link"></i>](http://mulheres.eti.br/) aceitamos
hospedar o Stallman em nossa casa, e se desse algum problema, poderíamos levá-lo
para o hotel. Mandei para o Stallman fotos do quarto e avisei que não tínhamos
ar-condicionado, mas ele disse que não teria problema porque ele sabia que era
inverno e que não passaria calor, bastava o quarto ter uma janela e ser ventilado.

## Recomendações

Logo no início da troca de e-mails com o Stallman, quando ficou certo que iríamos
trazê-lo para Curitiba, ele me enviou um arquivo txt com todas as recomendações
sobre como organizar tudo. Em 2012 eu já tinha recebido esse arquivo, mas naquela
ocasião só precisei me preocupar com os itens relacionados a palestra. Agora nós
tivemos que ver tudo, incluindo alimentação, hospedagem, passagens, etc.

O conteúdo inteiro desse aquivo no Writer deu 24 páginas, e não vou publicá-lo
aqui porque não tenho certeza se ele é público. O Stallman provavelmente continua
ditando e inserindo novas coisas, então em algum momento o texto ficaria
defasado. Mas vou colocar os tópicos desse arquivo, para assim você possa ter uma
ideia do que é necessário para levar o Stallman para palestrar na sua cidade.

 1. Speeches
 1. Abstract
 1. Brief bio
 1. Introducing the speech
 1. Photo
 1. Asking for the text
 1. Breaks
 1. Size of talk
 1. Participation in a larger event
 1. Erecting a larger event
 1. Multiple events
 1. Not exclusive
 1. Venues and planning
 1. Facilities
 1. Languages
 1. Registration
 1. No Signing In
 1. Restricting admission
 1. Sponsors
 1. Web Announcements
 1. Directing publicity
 1. Avoiding errors in publicity
 1. Other Terminology Issues
 1. Invite Attendees to Bring Cash
 1. Selling Free Software, Free Society
 1. At the speech
 1. Changes of plans
 1. Scheduling other meetings
 1. Interviews
 1. Recorded interviews for broadcast
 1. Interviews not for broadcast
 1. Recording my speech
 1. Recording formats
 1. Putting my speech on the net
 1. Streaming the speech
 1. Remote speeches by video connection
 1. Warning about giveways
 1. Flights
 1. Additional surprise expenses
 1. Bus and train tickets
 1. Other expenses
 1. Accommodations
 1. Temperature
 1. Pets
 1. Hospitality
 1. Email
 1. Paying me a reimbursement or a fee
 1. Dinners
 1. Food
 1. Wine
 1. Restaurants
 1. Sightseeing
 1. Presents
 1. More arrangements

Pode parecer um exagero tantas recomendações, e algumas partes parecem muito
excêntricas, mas depois você percebe que é muito útil saber todos esses detalhes
como por exemplo como ele gosta de ser tratado, o que ele não come, o que ele
precisa durante a palestra, qual a temperatura ideal do quarto para conseguir
dormir (se for um lugar quente precisa ter um ar-condicionado), etc.

Algumas partes são muito curiosas, como a que ele diz que gosta muito de
papagaio mas que (em letras maiúsculas) você não deve comprar um apenas para
agradá-lo. No primeiro dia perguntei se ele citava isso no texto porque alguém
tinha comprado um papagaio, e ele disse que sim, duas vezes... O Stallman disse
que todas as recomendações que estão no texto foram baseadas em situações reais
que aconteceram com ele.

Outra coisa que vem escrito é que você não precisa tratá-lo como uma pessoa que
precisa de ajuda o tempo todo, ou seja, quando ele precisar de algo, ele vai te
pedir. Imagino que deve ser muito chato quando as pessoas ficam o tempo todo te
perguntando se você precisa de algo, e você tendo que responder que não.

Também é muito importante não planejar nada sem antes falar com ele para saber
se ele pode ou não ir. Se você quer levar o Stallman para um passeio, conhecer
algum ponto turístico, dar uma entrevista, ele pede que você sempre pergunte
antes porque ele não está indo para a sua cidade a passeio e tem muito trabalho
para fazer.

E uma recomendação muito importante do Stallman é não achar que você sabe o que
é melhor para ele. Se ele decidir algo, não questione se realmente ele quer
aquilo.

Antes do Stallman vir perguntei se poderia agendar um encontro com algumas
pessoas da comunidade de Curitiba no primeiro dia a noite. Nossa ideia era
promover uma conversa com umas 10 pessoas para que todos tivessem a oportunidade
de conhecê-lo pessoalmente com mais tranquilidade, mas ele disse que preferia
não ir porque ele tinha muita coisa para fazer a noite e por isso era melhor
deixar para jantarmos todos juntos depois da palestra.

Alguns meios de comunicação entraram em contato pedindo para entrevistar o
Stallman, entre eles a
[Gazeta do Povo<i class="fa fa-external-link"></i>](http://www.gazetadopovo.com.br/economia/nova-economia/todos-os-olhos-em-voce-a0cxax3k55o1oc9iqzuur74t4),
principal jornal do Paraná. Então perguntei para o Stallman se eu poderia agendar
as entrevistas para o dia da palestra após o almoço, porque a palestra estava
programada para acontecer das 18:00h até às 20:30. Ele respondeu que não, porque
era melhor o pessoal assistir a palestra e depois tirar as dúvidas na entrevista.
Mas eu insisti se não ficaria muito tarde para ele dar a entrevista após às 20:30
e sem jantar, então ele respondeu que eu não devia supor o que era melhor ou não
para ele, porque se ele tava dizendo que a entrevista podia ser depois, é porque
ele sabe disso. Ah, e ele lembrou que não estava vindo para Curitiba a passeio,
e por isso precisava ficar o maior tempo possível trabalhando.

Veja quando Stallman chega a Curitiba na
[parte 2<i class="fa fa-external-link"></i>](http://phls.com.br/como-foi-a-vinda-do-richard-stallman-para-curitiba-em-2017-parte-2)
:-)