---
layout: post
title: Incentivo ao aumento de mulheres palestrantes no FISL17
date: 2016-03-10 16:00
author: Paulo Henrique de Lima Santana
categories: geral
tags: fisl eventos
---

# Incentivo ao aumento de mulheres palestrantes no FISL17

Publiquei o texto abaixo no
[Blog Mulheres, Tecnologia e Oportunidades<i class="fa fa-external-link"></i>](http://mulheres.eti.br/incentivo-ao-aumento-de-mulheres-palestrantes-no-fisl17/)
no dia 08 de março de 2016 para celebrar o Dia Internacional da Mulher.

Ano passado fiz parte do grupo que organizou a programação do
[FISL16 - Fórum Internacional Software Livre<i class="fa fa-external-link"></i>](http://softwarelivre.org/fisl16)
e durante os trabalhos que antecederam o evento decidimos convidar várias
mulheres para palestrarem. Em parceria com o grupo
[Mulheres na Tecnologia<i class="fa fa-external-link"></i>](http://mulheresnatecnologia.org/noticias-mnt/621-mulheres-na-tecnologia-sao-parceiras-oficiais-do-fisl16)
organizamos alguns painéis que contaram com a participação apenas de mulheres
debatendo temas como "cenário tecnológico (o que nos espera? o que mais pode ser
feito?)", "como contribuir com projetos de Software Livre", "mercado de trabalho
de TI", "mulheres da TI e nas comunidades de Software Livre".

Para ver as mulheres que foram convidadas para o FISL16, você pode acessar as
páginas de
[destaques<i class="fa fa-external-link"></i>](http://softwarelivre.org/fisl16/conteudos/destaques) e
[convidados(as)<i class="fa fa-external-link"></i>](http://softwarelivre.org/fisl16/conteudos/convidados).
Além destas, várias outras mulheres palestraram porque foram bem votadas na
avaliação pública e posteriormente confirmadas na programação. A Adriana até fez
um post
[aqui<i class="fa fa-external-link"></i>](http://mulheres.eti.br/sim-teremos-mais-mulheres-nos-eventos-de-software-livre/)
no blog sobre a participação das mulheres nas atividades do FISL16. O resultado
deste trabalho é que tivemos 25% de mulheres entre todos os palestrantes do
evento, o que foi um recorde entre todas as edições do FISL!

Como hoje é celebrado o **Dia Internacional da Mulher**, gostaria de aproveitar
esta data para incentivar que mais mulheres palestrem no FISL porque esta
campanha não pode se limitar apenas ao ano passado. O incentivo por mais mulheres
palestrantes precisa ser contínuo e com isso passar a mostrar o crescimento a
cada ano. Ontem (07 de março) a organização do FISL17 abriu a
[chamada de trabalhos<i class="fa fa-external-link"></i>](http://softwarelivre.org/fisl17/_todas-as-noticias/chamada-de-trabalhos-fisl17-esta-aberta)
para que a comunidade envie propostas de palestras e oficinas que serão avaliadas
pelo público e as melhores classificadas serão selecionadas para o evento. O
FISL17 acontecerá de 13 a 16 de julho de 2016 no Centro de Eventos da PUCRS em
Porto Alegre - RS.

Então, convido as mulheres da comunidade de Software Livre para enviarem
propostas de palestras e/ou oficinas para o FISL17. Independente do tema ser
técnico ou teórico/filosófico/social, se a palestrante tem muita ou pouca
experiência de falar em público, o importante é que todas aproveitem esta
oportunidade para transmitirem seus conhecimentos no mais importante evento de
Software Livre do país!

Também aproveito para solicitar as mulheres que palestraram em edições
anteriores do FISL a incentivarem outras mulheres a palestrarem na próxima
edição. Vocês que já palestraram podem servir de exemplo para as novatas e
mostrar o quanto é legal e importante participar do FISL. E claro, as veteranas
também devem enviar suas propostas porque queremos ver vocês novamente este ano
lá no evento :-)