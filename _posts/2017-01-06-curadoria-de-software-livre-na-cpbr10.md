---
layout: post
title: Curadoria de Software Livre na CPBR10
date: 2017-01-06 16:00
author: Paulo Henrique de Lima Santana
categories: geral
tags: softwarelivre campus-party
---

# Curadoria de Software Livre na #CPBR10

Como algumas pessoas andaram me perguntando sobre as atividades de Software Livre
na
[Campus Party Brasil 2017<i class="fa fa-external-link"></i>](http://brasil.campus-party.org/)
deste ano, acredito que vale um esclarecimento público: Para a Campus Party
Brasil deste ano (**CPBR10**) houve uma mudança na estrutura dos palcos e por
isso não haverá um palco exclusivo de Software Livre. Em anos anteriores, havia um
[palco para "Software Livre"<i class="fa fa-external-link"></i>](https://www.flickr.com/photos/slcampusparty/albums),
outro palco para "Desenvolvimento" e outro palco para "Segurança/Redes".

Na CPBR10 teremos um palco com o nome de "Inovação" para as atividades dessas
três áreas citadas anteriormente. Haverá também uma área de Workshops também com
o nome de "Inovação" para essas mesmas três áreas. Então serão dois espaços: um
palco e um espaço de workshops.

Consequentemente houve uma redução no número de slots disponíveis e por isso
coube a mim como curador indicar 5 atividades entre palestras e workhops para
preencher os slots de Software Livre.

A organização da CPBR10 criou uma ação chamada "Vire um Curador" onde qualquer
pessoa podia enviar sua proposta de palestra/workshop diretamente para eles sem
precisar passar pelos curadores, e que depois participaram de uma votação pública
no site campuse.ro. As 50 propostas mais votadas foram aprovadas para entrar na
grade de atividades, e entre elas existem algumas relacionadas a Software Livre.
Pessoalmente eu acho muita boa essa ação e um avanço importante na organização
da programação do evento, pois permite que qualquer pessoa possa participar sem
a intervenção dos curadores.

Só para lembrar que este formato de um palco para as três áreas (Software Livre,
Desenvolvimento, Segurança/Redes) já era usado na Campus Party Recife desde 2012.

Estou publicando no blog do SLCampusParty as atividades de software livre já
confirmadas na programação oficial:

<http://softwarelivre.org/slcampusparty/blog/palestras-e-workshops-de-software-livre-na-cpbr10>

Ah, já ia esquecendo: este ano teremos uma palestrante internacional de Software
Livre: Karen Sandler - Diretora Executiva da
[Software Freedom Conservancy<i class="fa fa-external-link"></i>](https://sfconservancy.org/).
A Karen será um dos destaques no Palco Principal.
