---
layout: post
title: Outreachy com inscrições abertas até 23/10/2017
date: 2017-09-22 16:00
author: Paulo Henrique de Lima Santana
categories: geral
tags: software-livre outreachy
---

# Outreachy com inscrições abertas até 23/10/2017

O **Outreachy** é um projeto internacional que apoia a diversidade por meio de
bolsas para que as pessoas possam contribuir para projetos de Software Livre e
Código Aberto.

O público alvo são mulheres (cis & trans), homens trans, ou pessoas *genderqueer*
(incluindo *genderfluid* ou *genderfree*). Não é obrigatório ser estudante.

**A bolsa é de US$ 5.500,00.**

 * Período de candidatura: 07/09/2017 a 23/10/2017
 * Período do estágio: 05/12/2017 a 05/03/2018

Mais informações:

<https://www.outreachy.org>

Aqui tem um cartaz pdf que pode ser usado para divulgação:

<https://github.com/sarahsharp/outreachy/blob/master/creative-works/poster-applicants-A4-Brasil.pdf>

A [Karen Sandler<i class="fa fa-external-link"></i>](https://en.wikipedia.org/wiki/Karen_Sandler) -
co-organizadora do Outreachy fez uma palestra em agosto durante a
[DebConf17<i class="fa fa-external-link"></i>](https://debconf17.debconf.org/)
onde ela explica detlahes do projeto. Você pode assistir no link abaixo:

<https://saimei.ftp.acc.umu.se/pub/debian-meetings/2017/debconf17/debian-outreachy.vp9.webm>

![Cartaz Outreachy](/assets/img/cartaz-outreachy.png){:width="400px"}

