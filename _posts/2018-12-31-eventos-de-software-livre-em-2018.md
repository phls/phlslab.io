---
layout: post
title: Eventos de Software Livre em 2018
date: 2018-12-31 16:00
author: Paulo Henrique de Lima Santana
categories: eventos
tags: eventos
---

# Eventos de Software Livre em 2018

Obs: essa lista está em constante atualização.

Acesse também a agenda de eventos de Software Livre e Código Aberto no Brasil:

<http://agenda.softwarelivre.org>

## JANEIRO

~~**Campus Party Brasil 2018 - CPBR11**  
30 de janeiro a 04 de fevereiro de 2018  
São Paulo - SP  
<http://brasil.campus-party.org>~~

## FEVEREIRO

## MARÇO

~~**I Encontro PotiLivre do Oeste**  
02 e 03 de março de 2018  
Mossoró - RN  
<https://encontro.potilivre.org/encontro1>~~

~~**Open Data Day**  
03 de março de 2018  
Várias cidades  
<http://opendataday.org>~~

~~**Workshop gratuito de programação para mulheres - Django Girls Belém**  
10 de março de 2018  
Belém - PA  
<https://djangogirls.org/belem>~~

~~**Workshop gratuito de programação para mulheres - Django Girls São José**  
10 de março de 2018  
Belém - PA  
<https://djangogirls.org/saojosesantacatarina>~~

~~**Workshop gratuito de programação para mulheres - Django Girls São José dos Campos**  
17 e 18 de março de 2018  
São José dos Campos - SP  
<https://djangogirls.org/saojosedoscampos>~~

~~**Rails Girls Porto Alegre**  
23 e 24 de março de 2018  
Porto Alegre - RS  
<https://djangogirls.org/portoalegre>~~

~~**Workshop gratuito de programação para mulheres - Django Girls Maceió**  
24 de março de 2018  
Maceió - AL  
<https://djangogirls.org/maceio4>~~

~~**DFD 2018 - Document Freedom Day**  
28 de março de 2018  
Várias cidades  
<http://documentfreedom.org>~~

~~**3ª Conferência das Comunidades Python do Sudeste**  
30 de março a 01 abril de 2018  
São Paulo  
<https://2018.pythonsudeste.org>~~

## ABRIL

~~**Rails Girls Santa Maria**  
06 e 07 de abril de 2018  
Santa Maria - RS  
<https://djangogirls.org/santamaria>~~

~~**Python Sul 2018**  
06 a 08 de abril de 2018  
Florianópolis - SC  
<https://2018.pythonsul.org>~~

~~**WordCamp Floripa**  
07 de abril de 2018  
Florianópolis - SC  
<https://2018.floripa.wordcamp.org>~~

~~**Workshop gratuito de programação para mulheres - Django Girls Florianópolis**  
08 de abril de 2018  
Florianópolis - SC  
<https://djangogirls.org/maceio4>~~

~~**MiniDebConf Curitiba 2018**  
11 a 14 de abril de 2018  
Curitiba - PR  
<https://minidebconf.curitiba.br/2018>~~

~~**Rails Girls Brasília**  
13 e 14 de abril de 2018  
Brasília - DF  
<http://railsgirls.com/brasilia>~~

~~**FLISOL 2018 - Festival Latino-americano de Instalação de Software Livre**  
28 de abril de 2018  
Várias cidades  
<http://www.flisol.org.br>~~

## MAIO

~~**CryptoRave**  
04 e 05 de maio de 2018  
São Paulo - SP  
<https://cryptorave.org>~~

~~**WordCamp Porto Alegre**  
05 de maio de 2018  
Porto Alegre - RS  
<https://2018.portoalegre.wordcamp.org>~~

~~**Tchelinux Rio Grande**  
19 de maio de 2018  
Rio Grande - RS  
<https://riogrande.tchelinux.org>~~

~~**Workshop gratuito de programação para mulheres - Django Girls João Pessoa**  
19 de maio de 2018  
João Pessoa - PB  
<https://djangogirls.org/joaopessoa>~~

~~**Workshop gratuito de programação para mulheres - Django Girls Niterói**  
19 de maio de 2018  
Niterói - RJ  
<https://djangogirls.org/niteroi>~~

~~**Workshop gratuito de programação para mulheres - Django Girls Bento Gonçalves**  
19 de maio de 2018  
Bento Gonçalves - RS  
<https://djangogirls.org/bentogoncalves>~~

~~**VI Python Nordeste**  
24 a 26 de maio de 2018  
Campina Grande - PB  
<https://2018.pythonnordeste.org>~~

## JUNHO

~~**Workshop gratuito de programação para mulheres - Django Girls São Carlos**  
8 de junho de 2018  
São Carlos - SP  
<https://djangogirls.org/saocarlos>~~

~~**Tchelinux Santa Cruz**  
9 de junho de 2018  
Santa Cruz - RS  
<https://santacruz.tchelinux.org>~~

~~**Rails Girls Sorocaba**  
15 e 16 de junho de 2018  
Sorocaba - SP  
<http://railsgirls.com/sorocaba>~~

## JULHO

~~**Caipyra - Conferência de Python em Ribeirão Preto**  
08 a 11 de junho de 2018  
Ribeirão Preto - SP  
<http://caipyra.python.org.br>~~

~~**FISL18 - Fórum Internacional Software Livre**  
11 a 14 de julho de 2018  
Porto Alegre - RS  
<http://fisl18.softwarelivre.org>~~

~~**OpenStack Day 2018**  
28 de julho de 2018  
São Paulo - SP  
<http://openstackbr.com.br/events/2018>~~

~~**Workshop gratuito de programação para mulheres - Django Girls São Paulo**  
28 de julho de 2018  
São Paulo - SP  
<https://djangogirls.org/saopaulo>~~

## AGOSTO

~~**PGConf.Brasil 2018**  
3 e 4 de agosto de 2018  
São Paulo - SP  
<https://www.pgconf.com.br/2018>~~

~~**JoomlaDay Brasil 2018**  
10 a 12 de agosto de 2018  
São Paulo - SP  
<https://www.joomladaybrasil.com.br>~~

~~**Workshop gratuito de programação para mulheres - Django Girls Brasília**  
11 de agosto de 2018  
Brasília - DF  
<https://djangogirls.org/brasilia>~~

~~**Debian Day ou Dia do Debian**  
16 (ou 18) de agosto de 2018  
Várias cidades  
<https://wiki.debian.org/DebianDay/2018>~~

~~**Rails Girls São Paulo**  
17 e 18 de agosto de 2018  
São Paulo - SP  
<http://railsgirls.com/saopaulo>~~

~~**Tchelinux Caxias do Sul**  
18 de agost de 2018  
Caxias do Sul - RS  
<https://caxias.tchelinux.org>~~

~~**2ª conferência da comunidade Python do Norte do Brasil - PyCon Amazônia 2017**  
18 e 19 de agosto de 2018  
Manaus - AM  
<http://amazonia.python.org.br>~~

~~**Rails Girls Florianópolis**  
24 e 25 de agosto de 2018  
Florianópolis - SC  
<http://railsgirls.com/florianopolis>~~

~~**Linux Developer Conference Brazil**  
25 e 26 de agosto de 2018  
Campinas - SP  
<http://linuxdev-br.net>~~

~~**SciPyLA 2018 - Conferência Latino Americana de Python Científico**  
29 de agosto e 1 de setembro de 2018  
Curitiba - PR  
<http://conf.scipyla.org>~~

~~**Workshop gratuito de programação para mulheres - Django Girls Caxias do Sul**  
31 de agosto a 01 de setembro de 2018  
Caxias do Sul - RS  
<https://djangogirls.org/caxiasdosul2>~~

## SETEMBRO

~~**Tchelinux Pelotas**  
01 de setembro de 2018  
Pelotas - RS  
<https://pelotas.tchelinux.org>~~

~~**MicroDebConf Brasilia 2018**  
08 de setembro de 2018  
Brasília -DF  
<http://microdebconf.debianbrasilia.org>~~

~~**SFD 2018 - Software Freedom Day**  
15 de setembro de 2018  
Várias cidades  
<http://softwarefreedomday.org>~~

~~**Tchelinux Bagé**  
29 de setembro de 2018  
Bagé - RS  
<https://bage.tchelinux.org>~~

~~**Workshop gratuito de programação para mulheres - Django Girls Novo Hamburgo**  
28 e 29 de setembro de 2018  
Novo Hamburgo - RS  
<https://djangogirls.org/novohamburgo>~~

~~**Workshop gratuito de programação para mulheres - Django Girls Passo Fundo**  
29 de setembro de 2018  
Passo Fundo - RS  
<https://djangogirls.org/passofundo>~~

## OUTUBRO

~~**PHP Day Curitiba 2018**  
06 de outubro de 2018   
Curitiba - PR  
<http://www.phpdaycuritiba.com.br>~~

~~**LaKademy 2018 - 6º Encontro LatinoAmericano dos Colaboradores do KDE**  
11 a 14 de outubro de 2018  
Florianópolis - SC  
<https://kde.vilarejo.pro.br/2018/07/06/quer-colaborar-com-a-comunidade-kde-que-tal-participar-do-lakademy-2018/>~~

~~**PHPeste**  
12 a 14 de outubro de 2018  
São Luís - MA  
<http://phpeste.net>~~

~~**WordCamp São Paulo 2018**  
13 de outubro de 2018  
São Paulo - SP  
<https://2018.saopaulo.wordcamp.org>~~

~~**PythonBrasil[14] - 14ª Conferência Brasileira da Comunidade Python**  
17 a 22 de outubro de 2018  
Natal - RN  
<http://2018.pythonbrasil.org.br>~~

~~**XV Latinoware - Conferência Latino-americana de Software Livre**  
17 a 19 de outubro de 2018  
Foz do Iguaçu - PR  
<http://www.latinoware.org>~~

~~**Workshop gratuito de programação para mulheres - Django Girls Natal**  
18 de outubro de 2018  
Natal - RN  
<https://djangogirls.org/natal>~~

~~**Workshop gratuito de programação para mulheres - Django Girls São Leopoldo**  
19 e 20 de outubro de 2018  
São Leopoldo - RS  
<https://djangogirls.org/saoleopoldo>~~

~~**III SEMDSL - Seminário de Educação, Mídias Digitais e Software Livre**  
23 e 24 de outubro de 2018  
Conceição do Coité - BA  
<http://semdsl.uneb.br>~~

~~**X FTSL - Fórum de Tecnologia em Software Livre**  
24 a 26 de outubro de 2018  
Curitiba - PR  
<http://ftsl.org.br/ftsl10>~~

~~**UbaTech - Fórum de Tecnologias Livres**  
25 a 27 de outubro de 2018  
Ubatuba - SP  
<http://ubatech.ubatuba.cc>~~

## NOVEMBRO

~~**Workshop gratuito de programação para mulheres - Django Girls Campinas**  
3 de novembro de 2018  
Campinas - SP  
<https://djangogirls.org/campinas>~~

~~**COMSOLiD 11 - 11° Encontro da Comunidade Maracanauense de Software Livre e Inclusão Digital**  
07 a 11 de novembro de 2018  
Maracanaú - CE  
<http://sige.comsolid.org>~~

~~**Qt Con Brasil**  
08 a 11 de novembro de 2018  
São Paulo - SP  
<https://br.qtcon.org>~~

~~**Tchelinux Santa Maria**  
10 de novembro de 2018  
Santa Maria - RS  
<https://santamaria.tchelinux.org>~~

~~**Rails Girls Belém**  
17 e 18 de novembro de 2018  
Belém - PA  
<https://railsgirls.com/belem>~~

~~**XV FGSL - Fórum Goiano de Software Livre**  
22 a 24 de novembro de 2018  
Goiânia - GO  
<http://2018.fgsl.net>~~

~~**Tchelinux Erechim**  
24 de novembro de 2018  
Erechim - RS  
<https://erechim.tchelinux.org>~~

~~**Workshop gratuito de programação para mulheres - Django Girls Recife**  
24 de novembro de 2018  
Recife - PE  
<https://djangogirls.org/recife>~~

~~**Workshop gratuito de programação para mulheres - Django Girls Maceió**  
24 de novembro de 2018  
Maceió - AL  
<https://djangogirls.org/maceio>~~

~~**Workshop gratuito de programação para mulheres - Django Girls Teresina**  
24 de novembro de 2018  
Teresina - PI  
<https://djangogirls.org/teresina>~~

~~**Workshop gratuito de programação para mulheres - Django Girls São Gonçalo**  
24 de novembro de 2018  
São Gonçalo - RJ  
<https://djangogirls.org/saogoncalo>~~

~~**3ª PotiCon - Conferência Potiguar de Software Livre**  
24 e 25 de novembro de 2018  
Natal - RN  
<http://www.potilivre.org/poticon>~~

~~**Rails Girls Porto Alegre**  
30 de novembro e 1 de dezembro de 2018  
Porto Alegre - RS  
<http://railsgirls.com/portoalegre2018>~~

## DEZEMBRO

~~**Tchelinux Porto Alegre**  
01 de dezembro de 2018  
Porto Alegre - RS  
<https://poa.tchelinux.org>~~

~~**13ª PHP Conference Brasil**  
06 a 08 de dezembro de 2018  
Porto Aegre - RS  
<http://www.phpconference.com.br>~~

~~**12ª RubyConf Brasil**  
13 e 14 de dezembro de 2018  
São Paulo - SP  
<https://eventos.locaweb.com.br/proximos-eventos/rubyconf-brasil-2018>~~

## Anos anteriores

[2017](http://softwarelivre.org/blog/eventos-de-software-livre-em-2017)  
[2016](http://softwarelivre.org/blog/eventos-de-software-livre-em-2016)  
[2015](http://softwarelivre.org/blog/eventos-de-software-livre-em-2015)  
[2014](http://softwarelivre.org/blog/eventos-de-software-livre-em-2014)  
[2013](http://softwarelivre.org/blog/eventos-de-software-livre-em-2013)  
[2012](http://softwarelivre.org/blog/eventos-de-software-livre-em-2012)  
[2011](http://softwarelivre.org/blog/eventos-de-software-livre-em-2011)  
