---
layout: post
title: Eventos de Software Livre em 2014
date: 2014-12-31 16:00
author: Paulo Henrique de Lima Santana
categories: eventos
tags: eventos
---

# Eventos de Software Livre em 2014

Obs: essa lista está em constante atualização.

Acesse também a agenda de eventos de Software Livre e Código Aberto no Brasil:

<http://agenda.softwarelivre.org>

## JANEIRO

~~**Education Freedom Day**  
18 de janeiro de 2014  
Várias cidades  
<http://www.educationfreedomday.org>~~

~~**Campus Party Brasil 2014**  
27 de janeiro a 02 de fevereiro de 2014  
São Paulo - SP  
<http://www.campus-party.com.br/2014/socrates.html>~~

## FEVEREIRO

~~**Python Day Alagoas**  
15 de fevereiro de 2014  
Maceió - AL  
<http://www.pug.al>~~

## MARÇO

~~**III Encontro Catarinense de LibreOffice**  
14 e 15 de março de 2014  
Concórdia - SC  
<http://www.unc.br/uncmkt/libre-office>~~

~~**Hardware Freedom Day**  
15 de março de 2014  
Várias cidades  
<http://www.hfday.org>~~

~~**RuPy Sergipe**  
22 de março de 2014  
Aracaju - SE  
<http://www.eventick.com.br/rupy-sergipe>~~

~~**Document Freedom Day**  
26 de março de 2014  
Várias cidades  
<http://documentfreedom.org>~~

~~**II Encontro Nacional de Mulheres na Tecnologia**  
28 e 29 de março  de 2014  
Goiânia - GO  
<http://mulheresnatecnologia.org/encontro2014>~~

~~**PyLadies Day**  
29 de março  de 2014  
Natal - RN  
<http://pyladiesnatal.github.io>~~

## ABRIL

~~**Pré-Fisl15 edição Amazônia**  
10 e 11 de abril de 2014  
Belém - PA  
<http://softwarelivre.org/fislamazonia>~~

~~**V Workshop da Comunidade Demoiselle**  
24 de abril de 2014  
Curitiba - PR  
<http://www.ur1.ca/gvvl1>~~

~~**Abril pro Ruby**  
24 a 27 de abril de 2014  
Porto de Galinhas - PE  
<http://tropicalrb.com/2014>~~

~~**FLISOL 2014 - Festival Latino Americano de Instalação de Software Livre**  
26 de abril de 2014  
Várias cidades  
<http://www.flisol.info>~~

~~**Consoline - Congresso de Software Livre do Nordeste**  
26 de abril de 2014  
Recife - PE  
<http://www.softwarelivrene.org>~~

## MAIO

~~**II Python Nordeste**  
01 e 03 de maio de 2014  
Salvador - BA  
<http://2014.pythonnordeste.org>~~

~~**Joomla Day Brasil 2014**  
02 e 03 de maio de 2014  
São Paulo - SP  
<http://www.joomladaybrasil.org>~~

~~**15º FISL - Fórum Internacional de Software Livre**  
07 a 10 de maio de 2014  
Porto Alegre - RS  
<http://www.fisl.org.br>~~

~~**Pentaho Day 2014**  
16 de maio de 2014  
São Paulo - SP  
<http://www.pentahobrasil.com.br/eventos/PentahoDay_2014>~~

~~**WordCamp Belo Horizonte**  
17 de maio de 2014  
Belo Horizonte - MG  
<http://2014.belohorizonte.wordcamp.org>~~

~~**Culture Freedom Day**  
17 de maio de 2014  
Várias cidades  
<http://www.culturefreedomday.org>~~

~~**DrupalDay SP 2014**  
24 de maio de 2014  
São Paulo - SP  
<http://drupal-br.org/drupalday-sp-2014>~~

~~**6º Fórum Espírito Livre**  
29 de maio de 2014  
Vitória - ES  
30 de maio de 2014  
Serra - ES  
<http://forum.espiritolivre.org/viforumel>~~

## JUNHO

~~**XI EVIDOSOL - Encontro Virtual de Documentação em Software Livre e VIII CILTEC-online - Congresso Internacional de Linguagem e Tecnologia online**  
02 a 04 de junho de 2014  
Online  
<http://evidosol.textolivre.org>~~

~~**I Ciclo de Palestras em Software Livre**  
04 de junho de 2014  
Santa Izabel - PA  
<http://enclibre.blogspot.com.br/2014/05/i-ciclo-de-palestras-em-software-livre.html>~~

## JULHO

~~**Campus Party Recife 2014**  
23 a 27 de julho de 2014  
Recife - PE  
<http://recife.campus-party.org>~~

## AGOSTO

~~**Debian Day ou Dia do Debian**  
16 de agosto de 2014  
Várias cidades  
<https://wiki.debian.org/DebianDay/2014>~~

~~**LaKademy 2014 - Conferência Latino-Americana do KDE**  
27 a 30 de agosto de 2014  
São Paulo - SP  
<http://br.kde.org/lakademy-2014>~~

~~**RubyConf Brasil 2014**  
28 deagosto de 2014  
São Paulo - SP  
<http://eventos.locaweb.com.br/rubyconf-brasil-2014>~~

~~**VI Maratona de Software Livre**  
29 e 30 de agosto de 2014  
Volta Redonda - RJ  
<http://iaesmevr.net/maratona2014>~~

## SETEMBRO

~~**ESL – Encontro de Software Livre**  
06 de setembro de 2014  
Gama - DF  
<https://www.doity.com.br/encontro-software-livre>~~

~~**Semana do Software Livre no Tecnopuc**  
09 a 11 de setembro de 2014  
Porto Alegre - RS  
<http://va.mu/AHfWJ>~~

~~**PgDay Campinas**  
10 de setembro de 2014  
Campinas - SP  
<http://pgdaycampinas.com.br>~~

~~**VI FTSL - Fórum de Tecnologia em Software Livre**  
18 e 19 de setembro de 2014  
Curitiba - PR  
<http://www.ftsl.org.br>~~

~~**PgDay Curitiba**  
19 de setembro de 2014  
Curitiba - PR  
<http://pgdaycuritiba.blogspot.com.br>~~

~~**7ª GNUGRAF**  
19 e 20 de setembro de 2014  
Niterói - RJ  
<http://gnugraf.org>~~

~~**SFD 2014 - Software Freedom Day**  
20 de setembro de 2014  
Várias cidades  
<http://softwarefreedomday.org>~~

~~**II Encontro Nacional do LibreOffice**  
26 e 27 de setembro de 2014  
São Paulo - SP  
<http://encontro.libreoffice.org>~~

~~**Fórum TchêLinux**  
27 de setembro de 2014  
Pelotas - RS  
<http://www.eventick.com.br/tchelinuxpelotas>~~

## OUTUBRO

~~**Dia do Blender 2014**  
08 a 11 de outubro de 2014  
Rondonópolis - MT  
<http://www.ufmt.br/blender2014>~~

~~**III Encontro da Comunidade Python de Minas Gerais**  
10 a 11 de outubro de 2014  
Santa Rita do Sapucaí - MG  
<http://www.inatel.br/pythonday>~~

~~**XI Latinoware - Conferência Latino Americana de Software Livre**  
15 a 17 de outubro de 2014  
Foz do Iguaçu - PR  
<http://www.latinoware.org>~~

~~**RuPy Natal 2014**  
17 e 18 de outubro de 2014  
Natal - RN  
<http://natal.rupy.com.br>~~

~~**WordCamp São Paulo**  
18 de outubro de 2014  
São Paulo - SP  
<http://2014.saopaulo.wordcamp.org>~~

~~**VI FSLDC - Fórum de Software Livre de Duque de Caxias**  
25 de outubro de 2014  
Duque de Caxias - RJ  
<http://www.fsldc.org>~~

~~**Dia do Blender 2014**  
25 de outubro de 2014  
Curitiba - PR  
<http://diadoblender.com.br>~~

~~**2º Café com Software Livre**  
25 de outubro de 2014  
Blumenau - SC  
<http://blusol.org>~~

## NOVEMBRO

~~**Consoline - Congresso de Software Livre do Nordeste**  
01 de novembro de 2014  
Recife - PE  
<http://www.softwarelivrene.org>~~

~~**7º Fórum Espírito Livre**  
04 a 06 de novembro 2014  
São Mateus - ES  
<http://forum.espiritolivre.org/viiforumel>~~

~~**PythonBrasil[10]**  
06 a 09 de novembro de 2014  
Porto de Galinhas - PE  
<http://2014.pythonbrasil.org.br>~~

~~**Rails Girls em Salvador -  2ª edição**  
07 e 08 de novembro de 2014  
Salvador - BA  
<http://railsgirls.com/salvador201411>~~

~~**8º Fórum Espírito Livre**  
12 a 15 de novembro 2014  
Vitória - ES  
<http://forum.espiritolivre.org/viiiforumel>~~

~~**XI FGSL - Fórum Goiano de Software Livre**  
21 e 22 de novembro de 2014  
Goiânia - GO  
<http://fgsl.aslgo.org.br>~~

~~**9º Fórum Espírito Livre**  
25 a 26 de novembro 2014  
Belém - PA  
<http://forum.espiritolivre.org/ixforumel>~~

~~**Rails Girls Porto Alegre**  
28 e 29 de novembro de 2014  
Porto Alegre - RS  
<http://railsgirls.com/portoalegre2014>~~

~~**RuPy Brazil 2014**  
29 de novembro 2014  
São José dos Campos - SP  
<http://rupy.com.br>~~

~~**Ubuntu Day Hortolândia**  
29 de novembro 2014  
Hortolândia - SP  
<http://ubuntubrsp.org/ubuntu-day-hortolandia>~~

## DEZEMBRO

~~**10º Fórum Espírito Livre**  
01 e 02 de dezembro 2014  
Cachoeiro de Itapemirim - ES  
<http://forum.espiritolivre.org/xforumel>~~

~~**PHP Conference Brasil 2014 - 9ª edição**  
04 a 07 de dezembro de 2014  
São Paulo - SP  
<http://www.phpconference.com.br>~~

~~**Seminário de Tecnologia em Software Livre TcheLinux 2014 - Edição Porto Alegre**  
06 de dezembro de 2014  
Porto Alegre - RS  
<http://poa.tchelinux.org>~~

~~**WordCamp Salvador**  
06 de dezembro de 2014  
Salvador - BA  
<http://2014.salvador.wordcamp.org>~~

~~**Drupal Day POA**  
13 de dezembro de 2014  
Porto Alegre - RS  
<http://www.drupaldaypoa.com.br>~~

~~**2º Fórum TchêLinux de Santiago**  
13 de dezembro de 2014  
Santiago - RS  
<http://santiago.tchelinux.org>~~

~~**COMSOLiD 7 - 7° Encontro da Comunidade Maracanauense de Software Livre e Inclusão Digital**  
16 a 19 de dezembro de 2014  
Maracanaú - CE  
<http://www.comsolid.org>~~

## Anos anteriores

[2013](http://softwarelivre.org/blog/eventos-de-software-livre-em-2013)  
[2012](http://softwarelivre.org/blog/eventos-de-software-livre-em-2012)  
[2011](http://softwarelivre.org/blog/eventos-de-software-livre-em-2011)  
