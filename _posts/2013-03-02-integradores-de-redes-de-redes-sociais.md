---
layout: post
title: Integradores de redes de redes sociais
date: 2013-03-02 16:00
author: Paulo Henrique de Lima Santana
categories: geral
tags: redes-sociais
---

# Integradores de redes de redes sociais

Para não esquecer :-)

## http://dlvr.it

Envia feed para:

 * Twitter
 * Facebook
 * Status.net (identi.ca)

Bom: Saída para identi.ca. Tem agendador de tweets.

Ruim: Só aceita feed na entrada.

## http://twitterfeed.com

Envia feed para:

 * Twitter
 * Facebook

Bom: normal

Ruim: Só aceita feed na entrada. Não tem saída para identi.ca. Tem expiração das
configurações.

## http://ifttt.com

Bom: Várias opções de entrada e saída.

Ruim: Não tem saída para identi.ca.

## https://hootsuite.com

Pareceido com o TweetDeck.

Bom: Aceita identi.ca. Várias redes para leitura. Feed para várias redes.

## http://brdcst.it

Envia feed para:

 * Twitter
 * Facebook
 * Status.net (identi.ca)

Bom: Saída para identi.ca.

Ruim: Só aceita feed na entrada.

### Configuração:

Usando o brdcst.it

 * Feed do softwarelivre.org -> Identi.ca -> Twitter

Usando o twitterfeed

 * Feed do softwarelivre.org -> Facebook -> Twitter

Postagem direta no Facebook

 * Facebook -> Twitter

Obs: Desliguei a o envio do Facebook para o Twitter
