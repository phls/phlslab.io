---
layout: post
title: Parabéns a comunidade brasileira de usuários e desenvolvedores Debian!
date: 2014-10-28 16:00
author: Paulo Henrique de Lima Santana
categories: geral
tags: eventos debian minidebconf
---

# Parabéns a comunidade brasileira de usuários e desenvolvedores Debian!

![Banner Debian 2015](/assets/img/banner-minidebconfs-2015.png){:width="400px"}

