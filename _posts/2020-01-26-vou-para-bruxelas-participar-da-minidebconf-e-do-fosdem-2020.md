---
layout: post
title: Vou para Bruxelas novamente participar da MiniDebConf e do FOSDEM 2020
date: 2020-01-26 16:00
author: Paulo Henrique de Lima Santana
categories: geral
tags: bruxelas bélgica debian fosdem
---

# Vou para Bruxelas novamente participar da MiniDebConf e do FOSDEM 2020

Ano passado tive a oportunidade de fazer minha primeira viagem para a Europa para
participar do
[sprint](https://wiki.debian.org/Sprints/2019/DebConf%20Video%20team%20sprint%20%40%20FOSDEM%202019)
do [time de vídeo](https://video.debconf.org) do Debian (de 28 de janeiro a 01
de fevereiro), do [FOSDEM 2019 - Free and Open source Software Developers' European
Meeting](https://fosdem.org/2019) (02 e 03 de fevereiro), e conversar com o
pessoal do Debian sobre a [DebConf19](https://debconf19.debconf.org) que na
época estávamos organizando e que aconteceu em julho.

![Bruxelas](/assets/img/bruxelas-2019-1.jpg)

Eu tinha bastante vontade de participar do FOSDEM porque tinha lido sobre o seu
formato de organização diferente do que estamos acostumados nos nossos eventos
de Software Livre no Brasil. São dois dias de evento gratuito (sábado e
domingo), não existe inscrição prévia e não existe crendencimento, você chega lá
no evento e participa. Alguns meses antes a organização faz uma chamada para as
comunidades que querem organizar suas salas, e depois que elas são
[selecionadas](https://fosdem.org/2020/news/2019-10-01-accepted-developer-rooms/),
cada uma monta a sua programação que pode ser: apenas pela manhã, apenas pela
tarde ou o dia todo. Isso para sábado ou domingo. Então é um evento
verdadeiramente comunitário, porque as salas são auto-gestionadas.

E realmente o evento foi incrível! Assisti algumas palestras, fui voluntário
para operar a câmera em uma das salas, ganhei uma camiseta de voluntário, ajudei
no stand do Debian, peguei vários adesivos de outras comunidades, conversei com
pessoas de vários países (incluindo outros brasileiros que estavam lá).

![Bruxelas](/assets/img/bruxelas-2019-3.jpg)

Viajei com Adriana e ficamos 11 dias no total em Bruxelas (de 28 de janeiro a 07
de fevereiro), então depois do sprint e do FOSDEM tivemos quatro dias para
passear pela cidade e foi muito legal. Conhecemos vários pontos turísticos,
bebemos cervejas locais, vimos neve, comemos, entre outras coisas, as comidas
típicas de lá: chocolate, batata frita e waffles.

![Bruxelas](/assets/img/bruxelas-2019-2.jpg)

Estou escrevendo tudo isso para dizer que este ano vou novamente para Bruxelas
participar da
[MiniDebCamp](https://wiki.debian.org/DebianEvents/be/2020/MiniDebCamp) e do
[FOSDEM 2020](https://fosdem.org/2020). Dessa vez ficarei menos dias, serão 9 (de 28
de janeiro a 05 de fevereiro) e viajarei sozinho. Diferentemente do ano passado,
quando ficamos na casa de um casal que nos hospedou sem cobrar nada, dessa vez
ficarei em um hostel.

Já me inscrevi como voluntário novamente no FOSDEM e vou ajudar no stand do
Debian. Depois do FOSDEM terei dois dias e meio para passear e ainda não sei
muito bem o que vou fazer por lá. Pretendo visitar novamente o [Royal Museum of
the Armed Forces and of Military History](http://www.klm-mra.be/D7t/en) que é um
museu da guerra gigante, e a estátua do [Jean-Claude Van
Damme](https://visit.brussels/en/place/Jean-Claude-Van-Damme-statue) que fica
pertinho do local da MiniDebConf :-D

De dezembro de 2001 até abril de 2002 fiz um intercâmbio de quatro meses nos EUA
com uma ex-namorada e na época escrevemos um blog para contar sobre o nosso dia
a dia por lá. O blog não está mais online mas foi uma experiência legal e por
isso estou planejando escrever diariamente sobre essa viagem para Bruxelas.
Quero compartilhar as experiências que terei por lá.

Só foi possível ir Bruxelas ano passado e ir novamente este ano porque o Debian
patrocinou as minhas despesas dessas viagens.

Próximo texto: [Viagem de Curitiba para Bruxelas](/viagem-de-curitiba-para-bruxelas)

[link-fosdem]: http://fosdem.org
[imagem-fosdem]: https://fosdem.org/2020/support/promote/box.png "FOSDEM"

[![FOSDEM][imagem-fosdem]][link-fosdem]

