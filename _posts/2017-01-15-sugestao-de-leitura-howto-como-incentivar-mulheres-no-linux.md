---
layout: post
title: Sugestão de leitura HOWTO - Como incentivar mulheres no Linux
date: 2017-01-15 16:00
author: Paulo Henrique de Lima Santana
categories: geral
tags: software-livre mulheres linux
---

# Sugestão de leitura: HOWTO - Como incentivar mulheres no Linux 

![Logo LinuxChix](/assets/img/linuxchix.png){:width="200px"}

Alguns meses atrás, ajudei a traduzir as páginas do
[Projeto Debian Women<i class="fa fa-external-link"></i>](https://www.debian.org/women/)
como forma de contribuir com a iniciativa do Eriberto Mota incentivar a
participação de mais meninas/garotas/mulheres para contribuir com o Debian. Na
[Latinwore de 2016<i class="fa fa-external-link"></i>](https://2016.latinoware.org/)
ele fez uma lightning talk com o título "Mulheres do meu Brasil, cadê vocês?" e
o arquivo usado na apresentação
pode ser visto
[aqui<i class="fa fa-external-link"></i>](http://eriberto.pro.br/palestras/mulheres_brasil.pdf).

Na página da FAQ do Projeto Debian Women ao responder a questão "Por que existem
tão poucas mulheres na área de computação/FLOSS?" é sugerido a leitura do
[HOWTO Encourage Women in Linux<i class="fa fa-external-link"></i>](http://www.tldp.org/HOWTO/Encourage-Women-Linux-HOWTO/)
escrito por Valerie Anita Aurora em 2002. Alguns anos atrás era muito comum o
pessoal escrever HOWTOs como tutoriais técnicos mas também para ensinar sobre
assuntos diversos. Achei o texto muito interessante e pensei o quanto seria
legal se esse texto fosse traduzido, mas pessoalmente não me motivei a traduzir
porque o texto é razoavelmente longo.

Passaram-se alguns meses e enquanto estava montando o meu projeto chamado
[Timeline<i class="fa fa-external-link"></i>](http://timeline.softwarelivre.org/)
dos eventos de Software Livre e Código Aberto no Brasil, achei citações ao
Encontro Nacional LinuxChix Brasil que foi realizado de 2003 a 2007. Buscando
no web.archive.org achei o antigo site das
[LinuxChix Brasil<i class="fa fa-external-link"></i>](http://web.archive.org/web/20080206101627/http://www.linuxchix.org.br/)
e para minha surpresa lá tem um link para o HOWTO da Valerie Aurora traduzido
pela
[Sulamita Garcia<i class="fa fa-external-link"></i>](https://twitter.com/sulagarcia)
em 2003.

Lembro que quando comecei a ir para os meus primeiros eventos de Software Livre
como o FISL em 2005, a Sulamita era uma referência em palestras sobre os
problemas com preconceitos que as mulheres enfrentavam (e ainda enfrentam) na
área de TI e especialmente nas comunidades de Software Livre. Ela liderava as
LinuxChix Brasil, e obviavamente também era referência em palestras de assuntos
técnicos diversos sobre Software Livre.

Decidi então fazer o download dos arquivos do HOWTO e revisar a tradução,
fazendo pequenas correções para deixar o o texto mais claro e coerente.

Como citei anteriormente, o HOWTO foi escrito em 2002 mas é incrível como os
problemas citados são ainda muito atuais. É bom lembrar que naquela época o
principais meios de comunicação em grupo na internet eram as listas de discussões
por email e os canais de IRC. Ainda não existiam redes sociais como Facebook e
Twitter. Se por um lado ao longo dos anos a maioria dos homens está aprendendo a
lidar de forma correta com a participação das mulheres e passando a respeitar
suas opiniões, especificidades, etc, por outro lado as redes sociais
potencializaram a voz daqueles que ainda insistem infelizmente em externalizar
seus preconceitos.

É muito triste ler as diversas formas que as mulheres podem ser discriminadas,
desde pequenos gestos involuntários que ajudam a afastá-las do Software Livre
até atos deliberadamente ofensivos. Como homem, me senti muito impactado pelo
texto principalmente por ter uma irmã e uma sobrinha, e principalmente uma
esposa que é da área de TI, e pensar que todas elas podem passar por isso.

Então recomendo fortemente que todos, mas especialmente aos homens, reservem
um tempinho e leiam o **HOWTO - Como incentivar mulheres no Linux** no link
abaixo:

<http://mulheres.eti.br/howto-como-incentivar-mulheres-no-linux>

Obs: A autora original realmente preferiu escrever "Linux" e não "GNU/Linux" ou
"Software Livre", e a tradução respeitou isso. Mas ao longo do texto é citado
"Software Livre".