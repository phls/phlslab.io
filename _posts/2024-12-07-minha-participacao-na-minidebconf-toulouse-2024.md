---
layout: post
title: Minha participação na MiniDebConf Toulouse 2024
date: 2024-12-08 10:00
author: Paulo Henrique de Lima Santana
categories: geral
tags: debian toulouse minidebconf planetdebian-pt
---

# Introdução

Eu sempre acho incrível as oportunidades que tenho graças as minhas contribuições para o Projeto Debian. Fico feliz em receber esse reconhecimento por meio da ajuda que recebo com viagens para participar de eventos em outros países.

Este ano foi programado para o segundo semestre duas MiniDebConfs na Europa: a tradição edição em [Cambridge](https://wiki.debian.org/DebianEvents/gb/2024/MiniDebConfCambridge) na Inglaterra e uma edição estreante em [Toulouse](https://wiki.debian.org/DebianEvents/fr/2024/Toulouse) na França. Após pesar as dificuldades e facilidades que eu teria para participar de uma delas, decidi escolher a de Toulouse, principalmente pelo custo menor e por ser em novembro, me proporcionando mais tempo para o planejamento da viagem. Entrei em contato com o atual DPL Andreas Tille explicando o meu desejo de participar do evento e ele gentilmente aprovou o meu pedido para o Debian custear as passagens. Obrigado novamente ao Andreas!

A MiniDebConf Toulouse 2024 foi realizada nos dias 16 e 17 de novembro (sábado e domingo) e aconteceu em uma das salas de um tradicional evento de Software Livre da cidade chamado [Capitole du Libre](https://www.capitoledulibre.org/). Antes da MiniDebConf o pessoal organizou uma MiniDebCamp nos dias 14 e 15 de novembro em um coworking.

Toda a experiência prometia ser muito incrível, e foi! Desde visitar pela primeira vez uma cidade da França, conhecer um evento de Software Livre local, e compartilhar quatro dias com pessoas da comunidade Debian de vários países.

## Viagem e cidade

Meu planejamento foi sair de Belo Horizonte na segunda-feira, passar por São Paulo, e chegar em Toulouse na terça a noite. Eu iria aproveitar a quarta inteira para passear pela cidade e a partir de quinta participar da MiniDebCamp.

Mas o voo que deveria ter saído de São Paulo na madrugada de segunda para terça foi cancelado por um problema no avião e tive que passar toda a terça esperando. Fui realocado para outro voo que saiu a noite e cheguei em Toulouse na quarta-feira a tarde. Mesmo bastante cansado da viagem ainda aproveitei o final do dia para passear um pouco pela cidade. Mas foi uma pena ter perdido um dia inteiro de passeio. 

Na quinta saí de manhã cedo para passear um pouco mais antes de ir para o local da MiniDebCamp. Andei bastante e vi vários pontos turísticos. A cidade é realmente muito bonita como dizem, principalmente pelas casas e prédios feitos com tijolos rosa. Fiquei impressionado com as ruas estreitas e curvas, em certo momento parecia andar em um labirinto. Eu chegava em uma esquina e tinham 5 ruas se cruzando em diferentes direções.

A orla do rio que corta a cidade é muito bonita e a noite do pessoal fica por lá passando o tempo. Todo por ali tinha muita história.

Fiquei hospedado em um airbnb a 25 minutos a pé do coworking e apenas 10 minutos do local do evento. Um apartamento bastante amplo e que ficou muito mais barato do que hotel.

![MiniDebConf Toulouse 2024](/assets/img/minidc-toulouse-cidade-1.jpg)
<br /><br />
![MiniDebConf Toulouse 2024](/assets/img/minidc-toulouse-cidade-2.jpg)

## MiniDebCamp

Cheguei no coworking onde o pessoal tava fazendo a MiniDebCamp e reencontrei vários amigos. Também conheci algumas pessoas novas, conversei sobre o trabalho de tradução que fazemos no Brasil, e outros assuntos.

Já sabíamos que nos dois dias da MiniDebCamp o pessoal da organização ia pagar o almoço para todos, e em determinado momento nos avisaram que já podíamos ir para a sala (que ficava embaixo do coworking) para almoçar. Eles montaram uma mesa com quiches, pães, charcutaria e MUITOS queijos :-) Tinha queijos de vários tipos e todos eram muito bons. Só estranhei um pouco porque não estou acostumado a almoçar queijos :-)

![MiniDebConf Toulouse 2024](/assets/img/minidc-toulouse-queijos-1.jpg)
<br /><br />
![MiniDebConf Toulouse 2024](/assets/img/minidc-toulouse-queijos-2.jpg)

A noite fomos em um grupo para jantar em um restaurante em frente ao Capitolium, principal ponto turístico da cidade.

No segundo dia de manhã passeei mais um pouco pela cidade, depois fui para o coworking e tivemos novamente a incrível mesa de queijos no almoço.

## Time de vídeo

Uma das minhas ideias de ir para Toulouse era poder ajudar o [time de vídeo](https://debconf-video-team.pages.debian.net/docs/) na montagem dos equipamentos para a transmissão e gravação das palestras. Eu queria acompanhar esse trabalho desde o início e aprender alguns detalhes, coisa que não consigo fazer antes das DebConfs porque sempre chego após o pessoal já ter montado a infra. E posteriormente reproduzir esse trabalho nas MiniDebConfs do Brasil como a de [Maceió](https://maceio.mini.debconf.org/) que já está programada para 1 a 4 de maio de 2025.
 
Como eu havia combinado com o pessoal do time de vídeo que iria ajudar na montagem dos equipamentos, na sexta-feira a noite fomos para a Universidade e ficamos na sala trabalhando. Fiz várias perguntas sobre o que eles estavam fazendo, sobre os equipamentos e pude tirar várias dúvidas. Nos dois dias seguintes fiquei manuseando uma das câmeras durante as palestras. E no domingo a noite ajudei a desmontar tudo.

Obrigado ao olasd, tumbleweed e ivodd pelas orientações e paciência.

![MiniDebConf Toulouse 2024](/assets/img/minidc-toulouse-preparacao.jpg)

## Evento em geral

Também rolou uma reunião entre alguns membros do time de publicidade que estavam presentes lá com o DPL. Fomos para um café e conversamos principalmente sobre pontos que podem melhorar no time.

As [palestras](https://toulouse2024.mini.debconf.org/schedule/) da MiniDebConf foram muito boas e as gravações também estão disponíveis [aqui](https://meetings-archive.debian.net/pub/debian-meetings/2024/MiniDebConf-Toulouse/).

Acabei não assistindo nenhuma palestra da programação geral do Capitole du Libre porque elas eram em francês. É sempre muito legal ver eventos de software livre no exterior para aprender como eles são feitos por lá e trazer algumas dessas experiências para os nossos eventos.

Espero que a MiniDebConf em Toulouse continue acontecendo todos os anos, ou que a comunidade francesa realize a próxima edição em outra cidade e eu possa novamente participar :-) Se tudo der certo, em julho do ano que vem voltarei para a França para participar da [DebConf25](https://debconf25.debconf.org/) na cidade de Brest.


![MiniDebConf Toulouse 2024](/assets/img/minidc-toulouse-cracha.jpg)

[Álbum com todas as fotos](https://photos.app.goo.gl/zX9byG6v2ZCxJmeCA)



