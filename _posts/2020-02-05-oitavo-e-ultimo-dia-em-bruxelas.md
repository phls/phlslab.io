---
layout: post
title: Oitavo e último dia em Bruxelas
date: 2020-02-05 23:59
author: Paulo Henrique de Lima Santana
categories: geral
tags: bruxelas bélgica debian fosdem
---

# Oitavo e último dia em Bruxelas

Veja o texto anterior: [Sétimo dia em Bruxelas](/setimo-dia-em-bruxelas).

Meu último dia em Bruxelas começou mais cedo do que eu gostaria porque o pessoal
começou a fazer barulho no quarto e acordei antes do previsto. Pretendia acordar
às 9h mas acordei pouco antes das 8h.

Desci para tomar o café da manhã, voltei para o quarto, tomei banho, terminei de
arrumar a mala e desci às 10h para o checkout. O quarto fica numa parte externa
a área principal onde está a recepção e a cozinha/refeitório. É preciso descer
três lances de escada que fica molhada quando chove. Por sorte não estava
chovendo nessa manhã, então desci com a mala pesada mas com o chão seco.

![Bruxelas](/assets/img/bruxelas-2020-34.jpg)

Fiquei ainda um tempo no refeitório arrumando a mala para diminuir o peso e saí
pouco depois das 11h para pegar o ônibus e começar a minha ida para o aeroporto.
Como o vôo era às 15:05 eu queria chegar no aeroporto antes das 14h e pretendia
almoçar em algum lugar no centro.

Fiz exatamente o caminho inverso que eu havia feito na chegada. O ponto fica há
duas quadras do hostel, então tive que puxar a mala por uma rua de calçamento e
com uma subida leve.

![Bruxelas](/assets/img/bruxelas-2020-35.jpg)

Quando tava chegando no ponto passou o ônibus e tive que esperar o próximo. O
legal é que em todos os pontos tem uma placa com os minutos para os próximos. E
dentro dos ônibus e dos trens tem telas exibindo as próximas paradas, além de
uma gravação falando.

![Bruxelas](/assets/img/bruxelas-2020-36.jpg)

![Bruxelas](/assets/img/bruxelas-2020-37.jpg)

Peguei o ônibus da linha 48 no sentido estação "Anneessens". O ponto que eu
tinha que descer é a parada final dessa linha que fica na "Boulevard Maurice
Lemonnier". Desci e caminhei até a estação de trem "Anneessens". Lá peguei o
trem da linha 4 até a estação "Gare Noord”, ou "Gare Du Nord" ou “Station
Brussel Noord”.

![Bruxelas](/assets/img/bruxelas-2020-38.jpg)

Como já era pouco mais de meio-dia resolvi almoçar por ali. Comprei uma porção
de salada no Carrefour dentro da estação e também lá achei uma
lanchonete/restaurante que tinha sanduíches e massas. Pedi um macarrão a
bolonhesa para comer com a salada.

Ví que o horário tava começando a ficar apertado porque na vinda eu levei uma
hora de ônibus do aeroporto até alí. Acabei de almoçar e já fui para o ponto do
ônibus da linha 272 da empresa Lijn, que é aquela que faz as linhas da cidade de
Bruxelas para as cidades da "região metropolitana". Às 13:05 passou o ônibus e
cheguei no aeroporto quase às 14h.

Então chegou a hora da verdade: despachar a mala que provavelmente estava com
mais de 23kg. Quando coloquei na balança, deu 24,5kg. Mas a mulher a empresa
área não falou nada e a mala foi despachada assim. Ufa!

Já ia esquecendo: o Nícolas publicou um texto sobre o sprint do time de vídeo e
ele cita o que eu fiz lá. [DebConf Video team sprint (and stuff) @ MiniDebCamp
FOSDEM 2020](https://blog.olasd.eu/2020/02/minidebcamp-fosdem-2020)

Uma coisa que esqueci de comentar antes: no segundo dia do FOSDEM encontrei com
o Rytis no corredor. O Rytis e a Alice nos hospedaram ano passado na casa deles
sem cobrar nada e foram muito legais comigo e com a Adriana. No ano passado após
a DebConf19 mandei um email pra eles dizendo que tinha dado tudo certo no evento
e ele me respondeu dizendo que a Alice estava grávida e teriam bebê em breve.
Por isso quando eu me planejei para ir para Bruxelas não cogitei a possibilidade
de ficar na casa deles porque muito provavelmente seria difícil pra eles me
receber lá com uma bebê. Então quando encontrei com ele no FOSDEM, conversamos
por alguns minutos sobre a bebê e a nova vida deles, falei que eu e Adriana não
estávamos mais juntos, ele perguntou onde eu tava hospedado e me disse que (como
eu havia previsto) não daria pra me hospedar dessa vez por causa da bebê. No fim
ele me convidou para jantar e disse que era só mandar uma mensagem para
combinarmos. Acabei não mandando a mensagem porque sei que me sentiria estranho
indo sozinho na casa deles. Pensei algumas vezes se deveria ir ou não, e no fim
achei melhor não ir. Isso é uma merda, mas é assim que a vida é.

No meu último post vou escrever sobre a viagem de Bruxelas para Curitiba.

Próximo texto: [Viagem de Bruxelas para
Curitiba](/viagem-de-bruxelas-para-curitiba)
