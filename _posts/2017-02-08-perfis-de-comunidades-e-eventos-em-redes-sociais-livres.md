---
layout: post
title: Perfis de comunidades e eventos em redes sociais livres
date: 2017-08-02 16:00
author: Paulo Henrique de Lima Santana
categories: geral
tags: software-livre eventos
---

# Perfis de comunidades e eventos em redes sociais livres

## GNU Social / Mastodon

 * Debian: [@debian@framapiaf.org<i class="fa fa-external-link"></i>](https://framapiaf.org/@debian)

## Pump.io

 * Debian: [debian@identi.ca<i class="fa fa-external-link"></i>](https://identi.ca/debian)
 * Debian Brasil: [	debianbrasil@identi.ca<i class="fa fa-external-link"></i>](https://identi.ca/debianbrasil)
 * FISL: [fisl@identi.ca<i class="fa fa-external-link"></i>](https://identi.ca/fisl)
 * Flisol Brasil: [flisolbr@identi.ca<i class="fa fa-external-link"></i>](https://identi.ca/flisolbr)
 * Software Livre: [softwarelivre@identi.ca<i class="fa fa-external-link"></i>](https://identi.ca/softwarelivre)
