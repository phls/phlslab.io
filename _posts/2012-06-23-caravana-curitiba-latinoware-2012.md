---
layout: post
title: Caravana Curitiba Latinoware 2012
date: 2012-06-23 16:00
author: Paulo Henrique de Lima Santana
categories: geral
tags: eventos latinoware
---

# Caravana Curitiba Latinoware 2012

Por favor leia com atenção as orientações abaixo:

**Organizador da caravana de Curitiba:** Paulo Henrique de Lima Santana.

Segue abaixo algumas orientações sobre a Caravana de Curitiba para a
[Latinoware 2012<i class="fa fa-external-link"></i>](http://www.latinoware.org/)
que acontecerá de 17 a 19 de outubro em Foz do Iguaçu.

O custo para participar da Latinoware é: Inscrição no evento + Taxa de embarque
na caravana + Hotel + Alimentação.

**1) Ônibus**

O ônibus fretado será pago pela organização da Latinoware e é aberto a qualquer
pessoa, de qualquer cidade, interessada em participar do evento. Não é um ônibus
da UFPR e não é restrito a estudantes.

Para viajar na caravana é obrigatório estar com a inscrição paga na Latinoware.
No momento do embarque todos devem ter o boleto pago em mãos para comprovação

**2) Taxa de embarque**

Todos os passageiros devem pagar uma taxa de embarque no valor de R$ 50,00 que
será usado para cobrir os custos do aluguel de todos os ônibus. Essa taxa deve
ser pagar para mim (Paulo) no momento do embarque em Curitiba.

**3) Inscrição na caravana**

Para se inscrever na caravana de Curitiba você deve usar o próprio sistema de
inscrição da Latinoware. Após fazer o login no sistema, selecione a aba
"Caravanas" e depois "Localizar Caravana". Procure a caravana de Curitiba e
confirme a inscrição.

Depois de alguns dias que você pagar o boleto de inscrição da Latinoware, o
sistema habilita para mim a opção de confirmar a sua inscrição na caravana.

Não se preocupe se demorar um pouco. O banco demora alguns dias para confirmar o
pagamento para a organização da Latinoware, e eles demoram alguns dias para
confirmar no sistema. Não se desespere, sua inscrição será confirmada em algum
momento. :-)

**4) Saídas - dias e horários**

Em Curitiba o ônibus sairá no dia 16/10 (terça) às 18:00h da sede da APUFR -
Associação dos Professores da UFPR. O endereço é: Rua Dr Alcides Vieira
Arcoverde, 1305 - Jardim das Américas.

Em Foz do Iguaçu o ônibus sairá no dia 20/10 (sábado) às 15h do Hotel Tulipa.
Assim todos podem aproveitar o sábado pela manhã para visitar as Cataratas ou o
Paraguai. :-)

**5) Hotel**

Vamos ficar hospedados no Hotel Tulipa.

O valor total é de R$ 160,00 para 3 diárias e 4 cafés-da-manhã em quartos
triplos e quádruplos.

Quartos duplos podem ser usados também mas provavelmente há uma diferença no
valor. Entramos no hotel no dia 17 assim que chegarmos a Foz pela manhã
(previsão é chegar às 6:00h) e saímos no dia 20 após o almoço.

**6) Contato**

Qualquer dúvida entre em contato comigo no e-mail: phls@latinoware.org