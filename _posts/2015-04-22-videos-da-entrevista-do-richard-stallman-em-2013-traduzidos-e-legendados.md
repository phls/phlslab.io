---
layout: post
title: Vídeos da entrevista do Richard Stallman em 2013 traduzidos e legendados
date: 2015-04-22 16:00
author: Paulo Henrique de Lima Santana
categories: geral
tags: software-livre richard-stallman
---

# Vídeos da entrevista do Richard Stallman em 2013 traduzidos e legendados

Decidi traduzir e legendar os vídeos de uma entrevista do Richard Stallman
concedida ao Swapnil Bhartiya (Muktware.com) em 2013 falando sobre como começou
o Projeto GNU, o que é GNU/Linux, as 4 liberdades do Software Livre,
hackerativismo, protestos virtuais, Anonymous e o spyware do Ubuntu.

**Como ele começou o projeto GNU**

[![Foto Stallman](/assets/img/entrevista-stallman-1.png)](https://www.youtube.com/embed/gweplAYaFFU){:width=400px"}

**As 4 liberdades do Software Livre**

[![Foto Stallman](/assets/img/entrevista-stallman-2.png)](https://www.youtube.com/embed/cUzwSwAou-s){:width="400px"}

**Hackerativismo, protestos virtuais e Anonymous**

[![Foto Stallman](/assets/img/entrevista-stallman-3.png)](https://www.youtube.com/embed/oX77WHnIs1Y){:width="400px"}

**Richard Stallman - Ubuntu com spyware**

[![Foto Stallman](/assets/img/entrevista-stallman-4.png)](https://www.youtube.com/embed/hgTQvCnhfQ4){:width="400px"}

Os vídeos originais publicados pelo muktware estão em:

<https://www.youtube.com/playlist?list=PLqJap3FMiIH_OBTBZULzpy086JcxsnxYW>

Estes e outros vídeos e legendas podem ser baixados aqui:

<https://github.com/phls/videos-software-livre>

Também existe uma coletânea de vídeos sobre Sofwtare Livre e GNU/Linux:

<http://curitibalivre.org.br/blog/videos-sobre-software-livre-e-gnulinux/>