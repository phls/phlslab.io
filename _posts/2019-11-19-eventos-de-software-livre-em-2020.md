---
layout: post
title: Eventos de Software Livre em 2020
date: 2019-11-19 16:00
author: Paulo Henrique de Lima Santana
categories: eventos
tags: eventos
---

# Eventos de Software Livre em 2020

Obs: essa lista está em constante atualização.

Acesse também a agenda de eventos de Software Livre e Código Aberto no Brasil:

<http://agenda.softwarelivre.org>

## JANEIRO

## FEVEREIRO

## MARÇO

~~**Open Data Day**  
02 de março de 2020  
Várias cidades  
<http://opendataday.org>~~

~~**MiniDebConf Maceió 2020 - CANCELADO**  
25 e 28 de março de 2020  
Maceió - AL  
<https://maceio2020.debian.net>~~

## ABRIL

~~**FLISOL 2020 - Festival Latino-americano de Instalação de Software Livre - CANCELADO**  
25 de abril de 2020  
Várias cidades  
<http://www.flisol.org.br>>~~

## MAIO

~~**Python Sul 2020 - CANCELADO**  
01 a 03 de maio de 2020  
Jaraguá do sul - SC  
<http://pythonsul.org>>~~

~~**CryptoRave - 7ª edição - CANCELADO**  
15 e 16 de maio de 2020  
São Paulo - SP  
<https://cryptorave.org>>~~

## JUNHO

## JULHO

~~**Linux Developer Conference Brazil - CANCELADO**  
16 a 18 de julho de 2020  
São Paulo - SP  
<http://linuxdev-br.net>>~~

~~**VI Python Nordeste - CANCELADO**  
17 a 19 de julho de 2020  
Fortaleza - CE  
<https://2020.pythonnordeste.org>>~~

## AGOSTO

~~**PGConf.Brasil 2020 - CANCELADO**  
21 e 22 de agosto de 2020  
São Paulo - SP  
<https://www.pgconf.com.br/2020>~~

~~**ConFLOSS - Conferência de Free/Libre e Open Source Software**  
21 a 23 de agosto de 2020  
On-line  
<https://confloss.com.br>~~

~~**Debian Day ou Dia do Debian**  
16 de agosto de 2020  
Várias cidades  
<https://wiki.debian.org/DebianDay/2020>~~

## SETEMBRO

**WordCamp São Paulo 2020**  
12 de setembro de 2020  
On-line  
<https://saopaulo.wordcamp.org/2020>


**SFD 2020 - Software Freedom Day**  
19 de setembro de 2020  
On-line  
<http://softwarefreedomday.org>

## OUTUBRO

**14ª RubyConf Brasil**  
15 e 16 de outubro de 2020  
On-line  
<https://www.rubyconf.com.br>

## NOVEMBRO

**Python Brasil 2020 - Conferência Brasileira da Comunidade Python**  
02 a 08 de novembro de 2020  
On-line  
<http://2020.pythonbrasil.org.br>

## DEZEMBRO

**15ª PHP Conference Brasil**  
03 a 05 de dezembro de 2020  
On-line  
<http://www.phpconference.com.br>


## Anos anteriores

[2019](http://softwarelivre.org/blog/eventos-de-software-livre-em-2019)  
[2018](http://softwarelivre.org/blog/eventos-de-software-livre-em-2018)  
[2017](http://softwarelivre.org/blog/eventos-de-software-livre-em-2017)  
[2016](http://softwarelivre.org/blog/eventos-de-software-livre-em-2016)  
[2015](http://softwarelivre.org/blog/eventos-de-software-livre-em-2015)  
[2014](http://softwarelivre.org/blog/eventos-de-software-livre-em-2014)  
[2013](http://softwarelivre.org/blog/eventos-de-software-livre-em-2013)  
[2012](http://softwarelivre.org/blog/eventos-de-software-livre-em-2012)  
[2011](http://softwarelivre.org/blog/eventos-de-software-livre-em-2011)  
