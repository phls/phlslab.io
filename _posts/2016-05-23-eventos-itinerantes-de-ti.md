---
layout: post
title: Eventos itinerantes de TI
date: 2016-05-23 16:00
author: Paulo Henrique de Lima Santana
categories: geral
tags: eventos
---

# Eventos itinerantes de TI

Eventos de TI que acontecem em várias cidades durante o ano.

## TDC - The Developers Conference

 * Empresa: Global Code
 * <http://www.thedevelopersconference.com.br>
 * Cidades em 2016:
    * Florianópolis
    * Porto Alegre
    * São Paulo

## Encontro Locaweb de Profissionais de Internet

 * Empresa: Locaweb
 * <http://eventos.locaweb.com.br>
 * Cidades em 2016:
    * Belo Horizonte
    * Curitiba
    * Porto Alegre
    * Recife
    * São Paulo

## Roadsec

 * Empresa: Flipside
 * <http://roadsec.com.br>
 * Cidades em 2016:
    * Aracaju
    * Belo horizonte
    * Brasília
    * Campo grande
    * Cuiabá
    * Curitiba
    * Fortaleza
    * Londrina
    * Maceió
    * Manaus
    * Natal
    * Porto Alegre
    * Recife
    * Rio de Janeiro
    * Salvador
    * São Paulo
    * Teresina
