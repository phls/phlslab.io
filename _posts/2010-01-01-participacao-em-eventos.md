---
layout: post
title: Participação em eventos
date: 2010-01-01 16:00
author: Paulo Henrique de Lima Santana
categories: geral
tags: cargos
---

# Participação em eventos


**XIV FGSL - Fórum Goiano de Software Livre**

 * Data: 18 e 19 de novembro de 2017
 * Local: UFG - Goiânia - GO

**Campus Party Minas Gerais 2017**

 * Data: 01 a 05 de novembro de 2017
 * Local: Expominas - Belo Horizonte - MG

**Campus Party Weekend Pato Branco 2017**

 * Data: 14 e 15 de outubro de 2017
 * Local: Pato Branco - PR

**IX FTSL - Fórum de Tecnologia em Software Livre**

 * Data: 27 a 29 de setembro de 2017
 * Local: Universidade Tecnológica Federal do Paraná (UTFPR) - Curitiba - PR

**SFD 2017 - Software Freedom Day**

 * Data: 16 de setembro de 2017
 * Local: Jupter - Curitiba - PR
 * Organizador

**Campus Party Bahia 2017**

 * Data: 09 a 13 de agosto de 2017
 * Local: Estádio Fonte Nova - Salvador - BA

**Debian Day 2017**

 * Data: 19 de agosto de 2017
 * Local: Universidade Tecnológica Federal do Paraná (UTFPR) - Curitiba - PR
 * Organizador

**Campus Party Brasília 2017**

 * Data: 14 a 18 de junho de 2017
 * Local: Centro de Convenções - Brasília - DF

**Palestra Richard Stallman em Curitiba**

 * Data: 2 de junho de 2017
 * Local: Universidade Federal do Paraná (UFPR) - Curitiba - PR
 * Organizador

**PentahoDay 2017**

 * Data: 11 e 12 de maio de 2017
 * Local: Universidade Positivo - Curitiba - PR

**FLISOL 2017 - Festival Latino-americano de Instalação de Software Livre**

 * Data: 08 de abril de 2017
 * Local: SEPT UFPR - Curitiba - PR
 * Organizador

**MiniDebConf Curitiba 2017**

 * Data: 17 a 19 de março de 2017
 * Local: UTFPR - Curitiba - PR
 * Organizador

**Campus Party Brasil 2017**

 * Data: 31 de de Janeiro a 05 de fevereiro de 2017
 * Local: Anhembi Parque - São Paulo - SP
 * Curador da Área de Software Livre

**Campus Party Minas Gerais 2016**

 * Data: 09 a 13 de novembro de 2016
 * Local: Expominas - Belo Horizonte - MG
 * Curador da Área de Software Livre

**VIII FTSL - Fórum de Tecnologia em Software Livre**

 * Data: 31 de agosto a 02 de setembro de 2016
 * Local: Universidade Tecnológica Federal do Paraná (UTFPR) - Curitiba - PR

**Campus Party Recife 2016**

 * Data: 20 e 21 de agosto de 2016
 * Local: Centro de Convenções de Pernambuco - Recife - PE
 * Curador da Área de Software Livre

**Debian Day 2016**

 * Data: 13 de agosto de 2016
 * Local: Uniandrade - Curitiba - PR
 * Organizador

**FISL 17 - Fórum Internacional de Software Livre**

 * Data: 13 a 16 de julho de 2016
 * Local: PUC - Porto Alegre - RS
 * Organizador

**Circuito Curitibano de Software Livre - 8ª etapa**

 * Data: 31 de maio e 01 de junho de 2016
 * Local: Opet - Curitiba - PR
 * Organizador

**Circuito Curitibano de Software Livre - 7ª etapa**

 * Data: 17 de maio de 2016
 * Local: Unicuritiba - Curitiba - PR
 * Organizador

**CryptoRave 2016**

 * Data: 06 e 07 de maio de 2016
 * Local: Centro Cultural São Paulo - São Paulo - SP

**FLISOL 2016 - Festival Latino-americano de Instalação de Software Livre**

 * Data: 16 de abril de 2016
 * Local: Pontifícia Universidade Católica (PUC) - Curitiba - PR
 * Organizador

**MiniDebConf Curitiba 2016**

 * Data: 05 e 06 de março de 2016
 * Local: Aldeia Coworking - Curitiba - PR
 * Organizador

**Campus Party Brasil 2016**

 * Data: 26 a 31 de janeiro de 2016
 * Local: Anhembi Parque - São Paulo - SP
 * Curador da Área de Software Livre

**Circuito Curitibano de Software Livre - 6ª etapa**

 * Data: 07 de outubro de 2015
 * Local: PUC-PR - Curitiba - PR
 * Organizador

**Aniversário de 30 anos da Free Software Foudation**

 * Data: 03 de outubro de 2015
 * Local: Aldeia Coworking - Curitiba - PR
 * Organizador

**SFD 2015 - Software Freedom Day**

 * Data: 19 de setembro de 2015
 * Local: Faculdade de Tecnologia de Curitiba (FATECPR) - Curitiba - PR
 * Organizador

**Circuito Curitibano de Software Livre - 5ª etapa**

 * Data: 02 de setembro de 2015
 * Local: Opet - Curitiba - PR
 * Organizador

**Circuito Curitibano de Software Livre - 4ª etapa**

 * Data: 26 de agosto de 2015
 * Local: Spei - Curitiba - PR
 * Organizador

**Debian Day 2015**

 * Data: 15 de agosto de 2015
 * Local: Setor de Educação Profissional e Tecnológica da Universidade Federal do Paraná (SEPT UFPR) - Curitiba - PR
 * Organizador

**Circuito Curitibano de Software Livre - 3ª etapa**

 * Data: 05 de agosto de 2015
 * Local: Uniandrade - Curitiba - PR
 * Organizador

**Campus Party Recife 2015**

 * Data: 23 a 26 de julho de 2015
 * Local: Centro de Convenções de Pernambuco - Recife - PE
 * Curador da Área de Software Livre

**FISL 16 - Fórum Internacional de Software Livre**

 * Data: 08 a 11 de julho de 2015
 * Local: PUC - Porto Alegre - RS
 * Organizador

**Expotec**

 * Data: 27 a 30 de maio de 2015
 * Local: Centro de Convenções Ronaldo Cunha Lima - João Pessoa - PB

**Circuito Curitibano de Software Livre - 2ª etapa**

 * Data: 20 de maio de 2015
 * Local: Faculdades Santa Cruz - Curitiba - PR
 * Organizador

**Circuito Curitibano de Software Livre - 1ª etapa**

 * Data: 18 de maio de 2015
 * Local: Unibrasil - Curitiba - PR
 * Organizador

**PentahoDay 2015**

 * Data: 15 e 16 de maio de 2015
 * Local: Universidade Positivo - Curitiba - PR

**FLISOL 2015 - Festival Latino-americano de Instalação de Software Livre**

 * Data: 25 de abril de 2015
 * Local: Pontifícia Universidade Católica (PUC) - Curitiba - PR
 * Organizador

**Campus Party Brasil 2015**

 * Data: 03 a 08 de fevereiro de 2015
 * Local: Centro de Exposições Imigrantes - São Paulo - SP
 * Curador da Área de Software Livre

**Latinoware 2014 - XI Conferência Latino-americana de Software Livre**

 * Data: 15 a 17 de outubro de 2014
 * Local: Itaipu - Foz do Iguaçu - PR

**VI FTSL - Fórum de Tecnologia em Software Livre**

 * Data: 18 e 19 de setembro de 2014
 * Local: Universidade Tecnológica Federal do Paraná (UTFPR) - Curitiba - PR
 * Organizador

**SFD 2014 - Software Freedom Day**

 * Data: 20 de setembro de 2014
 * Local: Universidade Tecnológica Federal do Paraná (UTFPR) - Curitiba - PR
 * Organizador

**Aniversário da ASL.Org**

 * Data: 10 de setembro de 2014
 * Local: Aldeia Coworking - Curitiba - PR
 * Organizador

**Debian Day 2014**

 * Data: 16 de agosto de 2014
 * Local: Universidade Tecnológica Federal do Paraná (UTFPR) - Curitiba - PR
 * Organizador

**Campus Party Recife 2014**

 * Data: 23 a 27 de julho de 2014
 * Local: Centro de Convenções de Pernambuco - Recife - PE
 * Curador da Área de Software Livre

**FISL 15 - Fórum Internacional de Software Livre**

 * Data: 07 a 10 de maio de 2014
 * Local: PUC - Porto Alegre - RS

**FLISOL 2014 - Festival Latino-americano de Instalação de Software Livre**

 * Data: 26 de abril de 2014
 * Local: Pontifícia Universidade Católica (PUC) - Curitiba - PR
 * Organizador

**EFD 2014 - Education Freedom Day**

 * Data: 08 de março de 2014
 * Local: Universidade Tecnológica Federal do Paraná (UTFPR) - Curitiba - PR
 * Organizador

**Campus Party Brasil 2014**

 * Data: 27 de Janeiro a 02 de Fevereiro de 2014
 * Local: Anhembi Parque - São Paulo - SP
 * Curador da Área de Software Livre

**Latinoware 2013 - X Conferência Latino-americana de Software Livre**

 * Data: 16 a 18 de outubro de 2013
 * Local: Itaipu - Foz do Iguaçu - PR

**SFD 2013 - Software Freedom Day**

 * Data: 21 de setembro de 2013
 * Local: Faculdade de Educação Superior do Paraná - Curitiba - PR
 * Organizador

**Campus Party Recife 2013**

 * Data: 17 a 21 de julho de 2013
 * Local: Centro de Convenções de Pernambuco - Recife - PE
 * Curador da Área de Software Livre

**FISL 14 - Fórum Internacioal de Software Livre**

 * Data: 03 a 06 de julho de 2013
 * Local: PUC - Porto Alegre - RS

**FLISOL 2013 - Festilval Latino Americano de Instalação de Software Livre**

 * Data: 27 de abril de 2013
 * Local: Faculdade de Educação Superior do Paraná - Curitiba - PR
 * Organizador

**Campus Party Brasil 2013**

 * Data: 28 de Janeiro a 03 de Fevereiro de 2013
 * Local: Anhembi Parque - São Paulo - SP
 * Curador da Área de Software Livre

**Palestra com Richard Stallman na UFPR**

 * Data: 13 de dezembro de 2012
 * Local: Centro Politécnico da Universidade Federal do Paraná - Curitiba - PR
 * Organizador

**Latinoware 2012 - IX Conferência Latino-americana de Software Livre**

 * Data: 17 a 19 de outubro de 2012
 * Local: Itaipu - Foz do Iguaçu - PR

**SFD 2012 - Software Freedom Day**

 * Data: 15 de setembro de 2012
 * Local: Faculdade de Educação Superior do Paraná - Curitiba - PR
 * Organizador

**Debian Day 2012**

 * Data: 18 de agosto de 2012
 * Local: Faculdade de Educação Superior do Paraná - Curitiba - PR
 * Organizador

**Campus Party Recife 2012**

 * Data: 26 a 30 de julho de 2012
 * Local: Centro de Convenções de Pernambuco - Recife - PE
 * Curador da Área de Software Livre

**FISL 13 - Fórum Internacioal de Software Livre**

 * Data: 25 a 28 de julho de 2012
 * Local: PUC - Porto Alegre - RS

**WordCamp Curitiba 2012**

 * Data: 15 e 16 de junho de 2012
 * Local: Faculdade de Educação Superior do Paraná - Curitiba - PR

**FLISOL 2012 - Festilval Latino Americano de Instalação de Software Livre**

 * Data: 28 de abril de 2012
 * Local: Faculdade de Educação Superior do Paraná - Curitiba - PR
 * Organizador

**Document Freedom Day**

 * Data: 28 de março de 2012
 * Local: Associação dos Professores da UFPR - Curitiba - PR
 * Organizador

**Campus Party Brasil 2012**

 * Data: 06 a 12 de fevereiro de 2012
 * Local: Anhembi Parque - São Paulo - SP
 * Equipe de organização da Área de Software Livre

**Latinoware 2011 - VIII Conferência Latino-americana de Software Livre**

 * Data: 19 a 21 de outubro de 2011
 * Local: Itaipu - Foz do Iguaçu - PR

**SFD 2011 - Software Freedom Day**

 * Data: 17 de setembro de 2011
 * Local: Associação dos Professores da UFPR - Curitiba - PR
 * Organizador

**V ENSOL – Encontro de Software Livre da Paraíba**

 * Data: 20 e 23 de julho de 2011
 * Local: Estação Ciência, Cultura e Artes Cabo Branco – João Pessoa – PB

**XXVIII ENECOMP - Encontro Nacional dos Estudantes de Computação**

 * Data: 20 e 21 de julho de 2011
 * Local: Centro de Convenções de Natal – RN
 * Organizador

**FISL 12 - Fórum Internacioal de Software Livre**

 * Data: 29 de junho a 02 de julho de 2011
 * Local: PUC - Porto Alegre - RS

**Campus Party Brasil 2011**

 * Data: 17 a 23 de aaneiro de 2011
 * Local: Centro de Exposições Imigrantes - São Paulo - SP
 * Equipe de organização da Área de Software Livre

**SFD 2010 - Software Freedom Day**

 * Data: 18 de setembro de 2010
 * Local: Faculdade de Educação Superior do Paraná - Curitiba - PR
 * Organizador

**FISL 11 - Fórum Internacioal de Software Livre**

 * Data: 21 a 24 de julho de 2010
 * Local: PUC - Porto Alegre - RS

**FLISOL 2010 - Festilval Latino-americano de Instalação de Software Livre**

 * Data: 24 de abril de 2010
 * Local: Faculdades Santa Cruz - Curitiba - PR
 * Organizador

**Latinoware 2009 - VI Conferência Latino-americana de Software Livre**

 * Data: 22 a 24 de outubro de 2009
 * Local: Itaipu - Foz do Iguaçu - PR

**XXVII ENECOMP - Encontro Nacional dos Estudantes de Computação**

 * Data: 04 a 08 de setembro de 2009
 * Local: Colégio Estadual - Curitiba - PR
 * Organizador

**FISL 10 - Fórum Internacioal de Software Livre**

 * Data: 24 a 27 de junho de 2009
 * Local: PUC - Porto Alegre - RS

**FLISOL 2009 - Festilval Latino-americano de Instalação de Software Livre**

 * Data: 25 de abril de 2009
 * Local: Faculdades Santa Cruz - Curitiba - PR
 * Organizador

**Latinoware 2008 - V Conferência Latino-americana de Software Livre**

 * Data: 30 de outubro a 01 de novembro de 2008
 * Local: Itaipu - Foz do Iguaçu - PR

**XXVI ENECOMP - Encontro Nacional dos Estudantes de Computação**

 * Data: 28 de julho a 01 de agosto de 2008
 * Local: Centro de Convenções Raymundo Asfora - Campina Grande - PB

**FISL 9.0 - Fórum Internacioal de Software Livre**

 * Data: 17 a 19 de abril de 2008
 * Local: PUC - Porto Alegre - RS

**FISL 8.0 - Fórum Internacioal de Software Livre**

 * Data: 12 a 14 de abril de 2007
 * Local: Centro de Exposições FIERGS - Porto Alegre - RS

**I ERECOMP-AL - Encontro Alagoano de Estudantes de Computação**

 * Data: 15 a 18 de março de 2007
 * Local: CEFET-AL - Maceió - AL

**Latinoware 2006 - III Conferência Latino-americana de Software Livre**

 * Data: 16 e 17 de novembro de 2006
 * Local: Itaipu - Foz do Iguaçu - PR

**I ENCASOL - Encontro Campinense de Software Livre / I ERECOMP-PB - Encontro Paraibano de Estudantes de Computação**

 * Data: 03 a 05 de novembro de 2006
 * Local: UEPB - Campina Grande - PB

**XXIV ENECOMP - Encontro Nacional dos Estudantes de Computação**

 * Data: 31 de julho a 04 de agosto de 2006
 * Local: Poços de Caldas - MG
 * Organizador

**FISL 7.0 - Fórum Internacioal de Software Livre**

 * Data: 19 a 22 de abril de 2006
 * Local: Centro de Exposições FIERGS - Porto Alegre - RS

**FLISOL - Festival Latino-americano de Instalação de Software Livre**

 * Data: 25 de março de 2006
 * Local: CEP - Colégio Estadual do Paraná - Curitiba - PR
 * Organizador

**Latinoware 2005 - II Conferência Latinoamericana de Software Livre**

 * Data: 26 e 27 de novembro de 2005
 * Local: UFPR - Curitiba - PR

**CONISLI 2005 - III Congresso Internacional de Software Livre**

 * Data: 03 a 05 de novembro de 2005
 * Local: SESC - São Paulo - SP

**I SECOMP UFPR - Semana de Computação da Universidade Federal do Paraná**

 * Data: 03 a 07 de outubro de 2005
 * Local: Curitiba - PR
 * Organizador

**I ERECOMP-PR - I Encontro Paranaense dos Estudantes de Computação**

 * Data: 15 a 17 de setembro de 2005
 * Local: Cascavel - PR
 * Organizador

**XXIII ENECOMP - Encontro Nacional dos Estudantes de Computação**

 * Data: 01 a 05 de agosto de 2005
 * Local: Bonito - MS

**FISL 6.0 - Fórum Internacioal de Software Livre**

 * Data: 01 a 04 de junho de 2005
 * Local: PUC - Porto Alegre - RS

**FLISOL - Festival Latino-americano de Instalação de Software Livre**

 * Data: 02 de abril de 2005
 * Local: CEP - Colégio Estadual do Paraná - Curitiba - PR
 * Organizador

**Fórum Social Mundial 2005**

 * Data: 26 a 31 de janeiro de 2005
 * Local: Porto Alegre - RS

**CONISLI 2004 - II Congresso Internacional de Software Livre**

 * Data: 05 e 06 de novembro de 2004
 * Local: Palácio Convenções do Anhembi - São Paulo - SP

**2o. Congresso Catarinense de Software Livre**

 * Data: 08 e 09 de outubro de 2004
 * Local: Joinville - SC

**II Semana de Software Livre da UFPR**

 * Data: 16 a 20 de agosto de 2004
 * Local: UFPR - Curitiba - PR
 * Organizador

**XXII ENECOMP - Encontro Nacional dos Estudantes de Computação**

 * Data: 02 a 06 de agosto de 2004
 * Local: Salvador - BA

**I Semana de Software Livre da UFPR**

 * Data: 29 de março a 02 de abril de 2004
 * Local: UFPR - Curitiba - PR
 * Organizador

**XXI ENECOMP - Encontro Nacional dos Estudantes de Computação**

 * Data: 04 a 08 de agosto de 2003
 * Local: Campinas - SP
 * Organizador

**XX ENECOMP - Encontro Nacional dos Estudantes de Computação**

 * Data: 14 a 18 de julho de 2002
 * Local: Florianópolis - SC

**III Seminário para Centros Acadêmicos da UFPR**

 * Data: 1998
 * Local: Lapa - PR

**36º Congresso da UPE**

 * Data: 19 a 21 de setembro de 1997
 * Local: Cascavel - PR

**5º Congresso de Estudantes da UFPR**

 * Data: 29 a 31 de agosto de 1997
 * Local: Quatro Barras - PR
 * Organizador

**45º CONUNE - Congresso da UNE**

 * Data: 02 a 06 de julho de 1997
 * Local: Belo Horizonte - MG

**II Seminário para Centros Acadêmicos da UFPR**

 * Data: 13 a 15 de junho 1997
 * Local: Lapa - PR
 * Organizador

**4º Congresso de Estudantes da UFPR**

 * Data: 25 a 27 de outubro de 1996
 * Local: Quatro Barras - PR

**I Seminário para Centros Acadêmicos da UFPR**

 * Data: 1996
 * Local: Quatro Barras - PR