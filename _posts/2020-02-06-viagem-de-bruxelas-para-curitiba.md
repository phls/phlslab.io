---
layout: post
title: Viagem de Bruxelas para Curitiba
date: 2020-02-06 23:59
author: Paulo Henrique de Lima Santana
categories: geral
tags: bruxelas bélgica debian fosdem
---

# Viagem de Bruxelas para Curitiba

Veja o texto anterior: [Oitavo e último dia em
Bruxelas](/oitavo-e-ultimo-dia-em-bruxelas).

Despachei a minha mala no balcão da Lufthansa e fui para o portão de embarque.
Na esteira a mulher pediu para abrir a mochila que ela queria dar uma olhada
mais minunciosa, e falei que tudo bem. Ela olhou e liberou.

Embarquei e o vôo saiu no horário previsto (15:05) para Frankfurt. A viagem foi
tranquila e durante o vôo tomei uma cerveja alemã. Cheguei em Frankfurt às 15:55
e o vôo para São Paulo era só às 22h. Eu havia pesquisado antes se dava para
passear em Frankfurt durante uma escala de seis horas e lí que dava para ir até o
centro de metrô. Mas eu tava tão cansado das caminhadas dos dias anteriores e de
puxar a mala do hostel até o aeroporto que acabei desistindo da ideia. Então
resolvi ficar as seis horas por lá mesmo.

Desembarquei e tome andar até a alfândega pra fazer a saída. O aeroporto é
gigante, então tive que andar bastante. Não havia fila do balcão da alfândega e
já fui atendido. Passaporte carimbado, e tome andar mais um tanto pra chegar na
esteira antes de acessar a área de embarque. Minha outra bolsa foi selecionada
para a revista então demorei uns 20 minutos na esteira porque tive que esperar o
pessoal terminar a revista das bagegens de um grupo de gurias que pareciam
russas pela língua que estavam falando. Na minha bolsa o segurança queria ver se
a minha garrafa térmica estava vazia, e estava. Para quem não sabe, é proibido
passar pela esteira com mais de 100ml de líquido para vôos internacionais.

Passei pela esteira e tome andar mais até o portão de embarque. Devo ter levado
quase uma hora do desembarque até chegar nesse portão. Ainda bem que resolvi não
sair do aeroporto.

Enquanto esperava o embarque, escrevi dois textos que estavam atrasados para o
blog, comi o sanduiche que eu havia comprado junto com a salada no Carrefour na
estação de ônibus em Bruxelas, tentei ajudar um brasileiro que veio pedir o
carregador para celular emprestado mas não deu certo porque o conector dele era
diferente, e esperei...

Resolvi adotar uma tática para escolher a poltrona nos vôos internacionais nos
aviões grandes. Os aviões tem 3 fileiras de poltronas: ABC DEFG HIJ. Se eu
escolher a poltrona da janela (A ou J) e quiser levantar para ir no banheiro,
terei que acordar 2 pessoas (BC ou HI). Se eu escolher a poltrona do meio (B ou
I) serei acordado pela pessoa da janela (A ou J) e terei que acordar a pessoa do
corredor (C ou H). Então a melhor fileira é a do meio, nunca as fileiras dos
cantos. Se eu escolher o corredor (D ou G) serei acordado por apenas uma pessoa
(E ou F), e se eu escolher a poltrona do meio (E ou F) terei que acordar apenas
uma pessoa (D ou G). Então a melhor poltrona é a do corredor (D ou G) porque tem
mais espaço pro lado no corredor e não preciso acordar ninguém para levantar
quando quiser.

Na ida e na volta peguei a poltrona G. Na ida as poltronas do meio eram 3, então
como tinha um casal nas duas outras poltronas, o cara do meu lado não me pediu
pra levantar nenhuma vez porque ele pedia pra mulher levantar. Na volta a guria
que tava do meu lado só pediu pra levantar uma vez e mesmo assim eu tava
acordado assistindo um filme.

A viagem de Frankfurt para São Paulo também foi tranquila. No jantar tomei dois
copos de vinho branco pra dar uma relaxada :-) Uma das comissárias era alemã e
queria falar com os passageiros em português. Então era engraçado ela falando
algumas coisas. Como ví que ela tava querendo praticar, comecei a pedir as
coisas em português. Uma hora ela perguntou pro comissário brasileiro como
perguntava que bebida o passageiro queria, e ele ficou um tempo tentanto fazer ela
decorar "o que o você gostaria de beber?" até que ele foi diminuindo a frase até
chegar em "quer beber?".

Cheguei em São Paulo às 6h da manhã. Peguei a mala, passei pela alfândega
(novamente não fui selecionado para mostrar a bagagem) e fui despachar no balcão
da Latam mas tava um caos de gente! Acho que tinham chegado vários vôos e tava
uma confusão de filas, o pessoal da Latam na correria, uma confusão danada.
Consegui despachar minha mala às 7:20 e fui pro portão de embarque porque meu
vôo para Curitiba saia às 8:30.

Uma coisa irritante nos aeroportos do Brasil é a gritaria do pessoal que
trabalha neles. Na esteira o pessoal gritando "próximo", ou para o pessoal tirar
o notebook das bolsas, e mais outras coisas. No balcão de embarque é outra
gritaria para formar as filas, mostrar um documento com foto, etc. Daqui a pouco
alguém começa a gritar nos autofalantes a chamada para o vôo tal. Isso é
bastante estressante e só cria um clima de desespero quando eu acho que deveria
ser mais tranquilo. E parece que tudo tá sempre atrasado, e as pessoas são
tratadas feito bichos. Sei lá, isso só cria um ambiente hostil.

Cheguei em Curitiba às 9:55, 20 minutos além do previsto porque a saída de São
Paulo (pra variar) demorou um pouco. E finalmente cheguei em casa :-)

Toda a viagem foi ótima, as experiências que vivi, a oportunidade de voltar a
Europa, as pessoas que conheci ou reencontrei, as conversas que tive sobre
Debian e outros assuntos, os lugares que visitei.

Minhas fotos estão
[aqui](https://www.flickr.com/photos/phls/albums/72157713036743007)

Se você leio meus textos e chegou até aqui, obrigado por me acompanhar. Espero
poder escrever sobre outras viagens no futuro porque foi legal voltar a falar
sobre as minhas experiências depois de tantos anos.
