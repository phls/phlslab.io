---
layout: post
title: Timeline dos eventos de Software Livre e Código Aberto no Brasil
date: 2016-11-21 16:00
author: Paulo Henrique de Lima Santana
categories: geral
tags: softwarelivre eventos
---

# Timeline dos eventos de Software Livre e Código Aberto no Brasil

Alguns dias atrás durante a
[Campus Party Minas Gerais<i class="fa fa-external-link"></i>](http://softwarelivre.org/slcampusparty/blog/palestras-e-oficinas-de-software-livre-na-cpmg1),
o Giovani Ferreira me mostrou um site chamado
[Timeline of the Debian Project<i class="fa fa-external-link"></i>](https://timeline.debian.net/)
onde o pesosal mantém um histórico das atividades do Debian com um visual
bastante interessante como uma linha temporal.

Achei o projeto muito legal e resolvi instalar o software que inclusive tem um
pacote no Debian chamado "debian-timeline".

Como desde 2011 mantenho no meu site uma
[lista<i class="fa fa-external-link"></i>](http://phls.com.br/eventos-de-software-livre-em-2019)
de eventos de software livre e código aberto que acontecem no Brasil que eu fico
sabendo por meio de listas, sites e redes sociais, decidi montar uma timeline
desses eventos.

Acabei descobrindo vários outros eventos antigos e que infelizmente muitos deles
os sites não estão mais on-line, mas que consegui achar no 
[Internet Archive: Waybak Machine<i class="fa fa-external-link"></i>](https://archive.org/web/).

Então convido a todos a acessarem a timeline dos eventos de Software Livre e
Código Aberto no Brasil:

<http://timeline.softwarelivre.org>