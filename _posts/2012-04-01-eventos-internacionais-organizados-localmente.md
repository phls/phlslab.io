---
layout: post
title: Eventos internacionais organizados localmente
date: 2012-04-01 16:00
author: Paulo Henrique de Lima Santana
categories: geral
tags: eventos software-livre
---

# Eventos internacionais organizados localmente

Veja alguns eventos internacionais de Software Livre que são organizados em
várias cidades no mundo, inclusive no Brasil.

Você pode contribuir organizando ou participando localmente na sua cidade.

## DFD - Document Freedom Day

 * Em português: Dia da Liberdade dos Documentos
 * Ano de criação: 2008
 * Data: Março (normalmente na última quarta-feira do mês)
 * Site: <http://documentfreedom.org>
 * Lista internacional: df-coordination

## FLISOL - Festival Latinoamericano de Instalación de Software Libre

 * Em português: Festival Latino-americano de Instalação de Software Livre
 * Ano de criação: 2005
 * Data: Abril (normalmente no último sábado do mês)
 * Site: <http://www.flisol.info>
 * Lista nacional: flisol-br
 * Observação: é obrigatório ter install fest

## Culture Freedom Day

 * Em português: Dia da Liberdade da Cultura
 * Ano de criação: 2012
 * Data: Maio (normalmente no 3o. sábado do mês)
 * Site: <http://www.culturefreedomday.org>

## Debian Day

 * Em português: Dia do Debian ou Dia D
 * Ano de criação: 2007
 * Data: 16 de agosto (ou no final de semana mais próximo)
 * Site: <http://wiki.debian.org/DebianDay>
 * Lista nacional: debian-BR-geral

## SFD - Software Freedom Day

 * Em português: Dia da Liberdade do Software
 * Ano de criação: 2004
 * Data: Setembro (normalmente no 3o. sábado do mês)
 * Site: <http://softwarefreedomday.org>
 * Lista internacional: sfd-discuss
 * Observação: a organização mundial envia um kit para o organizador local com
faixa, camisetas, balões, adesivos e DVDs.
