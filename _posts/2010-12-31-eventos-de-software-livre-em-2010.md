---
layout: post
title: Eventos de Software Livre em 2010
date: 2010-12-31 16:00
author: Paulo Henrique de Lima Santana
categories: eventos
tags: eventos
---

# Eventos de Software Livre em 2010

Obs: essa lista está em constante atualização.

Acesse também a agenda de eventos de Software Livre e Código Aberto no Brasil:

<http://agenda.softwarelivre.org>

~~**FLISOL 2010**  
Festival Latinoamericano de Instalação de Software Livre  
Data: 24 de abril de 2010  
Faculdades Santa Cruz  
<http://www.flisol.net/FLISOL2010/Brasil/Curitiba>~~

~~**Dia D 2010**  
21 de agosto de 2010  
FESP - Faculdade de Educação Superior do Paraná  
<http://wiki.debianbrasil.org/GUD/PR/DiaDebian2010PrCuritiba>~~

~~**SFD 2010 - Sofwtare Freedom Day**  
18 de setembro de 2010  
FESP - Faculdade de Educação Superior do Paraná  
<http://wiki.softwarefreedomday.org/2010/SouthAmerica/Brazil/Curitiba/GUD-BR-PR>~~

~~**6º Encontro Brasileiro da Comunidade Python - PythonBrasil[6]**  
21 a 23 de outubro de 2010  
Curitiba - PR  
Centro Politécnico da UFPR  
<http://www.pythonbrasil.org.br>~~

~~**WordCamp Curitiba**  
22 a 23 de outubro de 2010  
Curitiba - PR  
FESP - Faculdade de Educação Superior do Paraná  
<http://curitiba.wordcamp.com.br>~~

~~**Encontro Mineiro de Software Livre - EMSL**  
Uberlândia-MG  
12 a 16 de outubro de 2010  
<http://emsl.softwarelivre.org>~~

~~**5º Congresso Catarinense de Software Livre - SOLISC**  
Florianópolis-SC  
22 a 23 de outubro de 2010  
<http://www.solisc.org.br>~~

~~**IV Encontro Nordestino de Software Livre - ENSL**  
Natal-RN  
05 a 06 de novembro de 2010  
<http://www.ensl.org.br>~~

~~**VII Conferência Latino Americana - de Software Livre - LATINOWARE**  
Foz do Iguaçu-PR  
10 a 12 de novembro de 2010  
<http://www.latinoware.org>~~