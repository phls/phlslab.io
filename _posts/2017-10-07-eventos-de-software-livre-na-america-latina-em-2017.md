---
layout: post
title: Lista de eventos de Software Livre na América Latina confirmados em 2017
date: 2017-10-07 16:00
author: Paulo Henrique de Lima Santana
categories: eventos
tags: eventos
---

# Lista de eventos de Software Livre na América Latina confirmados em 2017

**FLISOL - Festival Latinoamericano de Instalación de Software Libre**  
22 de abril de 2017  
Várias cidades  
<http://flisol.info>

**Conferencia Internacional de Software Libre - Cuba Conf 2017**  
07 a 09 de novembro de 2017  
Havana - Cuba  
<https://www.cubaconf.org>

**The 13th International Conference on Open Source Systems - OSS 2017**  
22 e 23 de maio de 2017  
Buenos Aires - Argentina  
<http://oss2017.lifia.info.unlp.edu.ar>

**9º Encuentro Centroamericano de Software Libre - ECSL 2017**  
21 e 22 de julho de 2017  
Alajuela - Costa Rica  
<http://ecsl2017.softwarelibre.ca>

**Conferencia Internacional de Software Libre - Cuba Conf 2017**  
07 a 09 de novembro de 2017  
Havana - Cuba  
<https://www.cubaconf.org>

**Conferencia Latinoamericana de Computación Científica con Python - SciPyLa 2017**  
22 a 25 de novembro de 2017  
Sancti Spiritus - Cuba  
<http://conf.scipyla.org>

**Eventos no Brasil em 2017 podem ser vistos aqui:**

<http://phls.com.br/blog/eventos-de-software-livre-em-2017>