---
layout: post
title: Uma proposta de FISLs itinerantes e temáticos
date: 2016-06-14 16:00
author: Paulo Henrique de Lima Santana
categories: geral
tags: softwarelivre eventos fisl
---

# Uma proposta de FISL's itinerantes e temátticos

Comecei a participar do
[FISL - Fórum Internacional Software Livre<i class="fa fa-external-link"></i>](http://www.fisl.org.br/),
em 2005 quando ainda era estudante ciência da computação na UFPR e de lá para cá
participei de todas as edições. Vi o FISL chegar ao seu auge em termos de público
em 2009 quando o Presidente Lula participou da 10ª edição deixando uma marca
histórica no evento com quase 10.000 participantes. Sempre tive amigos(as) entre
pessoal da organização, e algumas deles(as) eu já conhecia da época que
participava do movimento estudantil de computação, antes mesmo de ir ao FISL.

Lembro que nas primeiras edições que fui, era até difícil andar pelos corredores
de tanta gente que tinha, era comum não conseguir entrar em um auditório porque
já estava lotado, e eram usados auditórios espalhados pela PUCRS nos levando a
andar de um lado para o outro do Campus. E sempre os encerramentos eram grandes
festas para celebrar o fim de mais uma edição.

A sensação que tinha era que o pessoal envolvido na organização, apesar do
esforço gigantesco, tinha muito prazer em trabalhar naquilo, e os participantes
admiravam muito o FISL principalmente por reconhecer o evento como o principal
local de encontro da comunidade de Software Livre no Brasil.

Provavelmente motivados por esse cenário de sucesso, era comum ver nas semanas
seguintes a realização do FISL pessoas levantando o debate principalmente em
listas de discussão como do 
[PSL-Brasil<i class="fa fa-external-link"></i>](http://listas.softwarelivre.org/cgi-bin/mailman/listinfo/psl-brasil)
sobre o FISL se tornar um evento itinerante, ou seja, ele mudar de cidade a cada
ano ao invés de acontecer sempre em Porto Alegre. Algumas opiniões eram de que
se o FISL fosse para outras regiões do país, pessoas que não podiam viajar para
o Sul, poderiam então participar. Por outro lado, o argumento era de que o FISL
é um evento de Porto Alegre, organizado por pessoas em sua maioria residentes em
Porto Alegre, e gerenciado pela ASL.Org que é uma entidade sediada em Porto
Alegre. Alguns amigos me disseram pessoalmente que havia também a preocupação
que se grupos de outras cidades organizassem o FISL, o evento poderia não ter a
mesma qualidade por não ter pessoas com experiência suficiente envolvidas
diretamente na organização local.

Em um dos primeiros FISL que participei o Roberto Requião, então governador do
Paraná, participou da cerimônia de abertura do FISL porque o Paraná estava se
destacando no apoio ao Software Livre principalmente por meio da Celepar. O
boato que rolou naquele ano era que o Requião ia propor trazer o FISL para
Curitiba porque existia uma incerteza se a prefeitura de Porto Alegre ou o
governo do Rio Grande do Sul (não lembro exatamente qual dos dois) continuaria
apoiando o Software Livre e consequentemente o FISL, porque havia acontecido uma
mudança de cargo. E mais recentemente a comunidade de Software Livre do Pará fez
uma proposta para levar o FISL para Belém. Não sei detalhes dessas e outras
propostas e não sei exatamente porque foram recusadas, mas o fato é que FISL
continoou em Porto Alegre.

Nessa discussão eu sempre achei que quem queria organizar um evento de Software
Livre na sua cidade, não precisava ser "o FISL", mas poderiam organizar eventos
com outros nomes. Ao longo dos anos vimos vários eventos acontecendo pelo país
como por exemplo: ESLAM em Manaus; FASOL  em Santarém;
[ENSL<i class="fa fa-external-link"></i>](https://pt.wikipedia.org/wiki/Encontro_Nordestino_de_Software_Livre)
em 5 capitais do Nordeste;
[ENSOL<i class="fa fa-external-link"></i>](http://www.ensol.org.br/)
em João Pessoa;
[FSLBA<i class="fa fa-external-link"></i>](http://wiki.softwarelivre.org/Festival4)
em Salvador;
[FGSL<i class="fa fa-external-link"></i>](http://fgsl.net/)
em Goiânia;
[Fórum Espírito<i class="fa fa-external-link"></i>](http://forum.espiritolivre.org/)
Livre em cidades do Espírito Santo;
[CONISLI<i class="fa fa-external-link"></i>](https://pt.wikipedia.org/wiki/Conisli)
em São Paulo;
[Latinoware<i class="fa fa-external-link"></i>](http://www.latinoware.org/)
em Foz do Iguaçu;
[SOLISC<i class="fa fa-external-link"></i>](http://solisc.org.br/)
em Florianópolis e outras cidades de Santa Catarina;
[FTSL<i class="fa fa-external-link"></i>](http://www.ftsl.org.br/)
em Curitiba;
[FLISOL<i class="fa fa-external-link"></i>](http://www.flisol.info/) em dezenas de cidades brasileiras. Alguns
desses eventos não existem mais e outros continuam acontecendo até hoje. A
principal característica desses eventos sempre foi reunir a comunidade de
Software Livre para atividades de diversas áreas, desde as discussões
políticas/filosóficas/sociais do movimento software livre até questões mais
técnicas como linguagens de programação, banco de dados, redes, segurança, etc,
por isso vou chamá-los de "eventos generalistas".

É perceptível que nos últimos anos o FISL passou a ter menos participantes. Não
tenho números oficiais, mas olhando a ocupação dos auditórios e da quantidade de
pessoas nos corredores durante os 4 dias do evento, o público diminuiu bastante.
Assim como diminuiu também o número de patrocinadores, especialmente as grande
empresas. Lembro de ter visto estandes do Terra, Globo.com, Google, Intel,
Redhat, Canonical, Oracle, além de empresas dos governos como Serpro, Dataprev,
Banco do Brasil, Caixa Economica, Celepar, Procergs e Procempa, e Ministérios
como da Cultura, do Planejamento, da Comunicação, da Ciência e Tecnologia, etc.

No FISL de 2014 decidi me associar a ASL.Org, e em 2015 fui convidado a fazer
parte do GT-Temário que é o grupo de pessoas que organiza programação do evento.
A experiência foi muito legal e este ano estou novamente participando do
GT-Temário do FISL17. Então o FISL de 2016 será minha segunda vez participando
da organização.

Por estar associado a ASL.Org e envolvido na organização do FISL, fiquei
pensando sobre quais seriam as causas que levaram a diminuição dos participantes.
Vou aproveitar trechos de duas mensagens que mandei recentemente para a lista de
associados da ASL.Org, e editá-las para expor o que eu penso sobre qual é o
problema atualmente e provocar possíveis soluções.

## O problema

Já faz algum tempo que percebo que os eventos organizados por comunidades no
Brasil sobre temas ou softwares específicos tem recebido bons públicos e ajuda
de várias empresas. Alguns desses eventos tem mais 1.000 participantes e recebem
patrocínios de grandes empresas nacionais e até mesmo internacionais. Alguns
eventos que aconteceram em 2105 e 2016 são:
[PHP Conference Brasil<i class="fa fa-external-link"></i>](http://www.phpconference.com.br/),
[RubyConf Brasil<i class="fa fa-external-link"></i>](http://rubyconf.com.br),
[Tropical Ruby<i class="fa fa-external-link"></i>](http://tropicalrb.com/),
[Python Brasil<i class="fa fa-external-link"></i>](http://pythonbrasil.org.br/),
[Python Nordeste<i class="fa fa-external-link"></i>](http://pythonnordeste.org/),
[PGBR<i class="fa fa-external-link"></i>](https://www.pgconf.com.br/),
PGDay ([Campinas<i class="fa fa-external-link"></i>](http://pgdaycampinas.com.br/),
[Curitiba<i class="fa fa-external-link"></i>](http://pgdaycuritiba.blogspot.com.br/)),
[PentahoDay<i class="fa fa-external-link"></i>](http://www.pentahobrasil.com.br/eventos/pentahoday2019/),
[Encontro Nacional do LibreOffice<i class="fa fa-external-link"></i>](http://encontro.libreoffice.org/),
Joomla Day
([Brasil<i class="fa fa-external-link"></i>](http://joomladaybrasil.org/),
[Ribeirão Preto<i class="fa fa-external-link"></i>](http://joomladaysp.com.br/2015/),
[São Paulo<i class="fa fa-external-link"></i>](https://jdaysp.com.br/),
[Salvador<i class="fa fa-external-link"></i>](https://jdayba.com.br/)),
WordCamp
([Belo Horizonte<i class="fa fa-external-link"></i>](https://2015.belohorizonte.wordcamp.org/),
[Porto Alegre<i class="fa fa-external-link"></i>](https://portoalegre.wordcamp.org/),
[São Paulo<i class="fa fa-external-link"></i>](https://saopaulo.wordcamp.org/)),
DrupalCamp/DrupalDay
([São Paulo<i class="fa fa-external-link"></i>](http://saopaulo.drupalcamp.com.br/),
[Porto Alegre<i class="fa fa-external-link"></i>](http://www.drupaldaypoa.com.br/),
[Campinas<i class="fa fa-external-link"></i>](http://campinas2016.drupalcamp.com.br/)),
[Lakademy<i class="fa fa-external-link"></i>](https://kde.vilarejo.pro.br/2018/07/06/quer-colaborar-com-a-comunidade-kde-que-tal-participar-do-lakademy-2018/),
[Rails Girls<i class="fa fa-external-link"></i>](http://railsgirls.com/events.html),
[Workshop Django Girls<i class="fa fa-external-link"></i>](https://djangogirls.org/events/),
[Cryptorave<i class="fa fa-external-link"></i>](https://cryptorave.org/), etc.
São eventos essencialmente técnicos e que eu acredito que não tem restrições
quanto a receber apoios de empresas como Google, Facebook, e mesmo Microsoft,
que para eventos mais ligados a filosofia do software livre como o FISL
poderiam causar críticas à organização.

Até porque como bem lembrou um outro associado em uma reunião da ASL.Org, pessoas
das comunidades trabalham nessas empresas e conseguem acesso ao pessoal de
marketing. Recentemente ouvi que um desses eventos conseguiu patrocínio do
Google e da Globo.com porque tem pessoas da comunidade que trabalham nessas
empresas e se tornam facilitadores para uma conversa sobre patrocínio.

Eu sempre tive o FISL como o principal evento para as comunidades se encontrarem,
mas parece que isso está deixando de existir justamente porque as comunidades
estão preferindo se encontrar nos seus eventos. E dessa forma, eventos
generalistas como FISL acabam ficando em um certo limbo, talvez isso explique a
queda no número de inscritos. Se no passado eu achava que FISL, Latinoware,
Campus Party, eram eventos "concorrentes" entre si, eu passei a ver que os
verdadeiros concorrentes hoje para o FISL são os eventos das comunidades como
esses citados anteriormente.

## Possíveis soluções

Se há uma mudança no comportamento das comunidades em relação ao FISL, acredito
que o FISL deveria se adequar a isso e estudar formas de, ou voltar a atrair
essas comunidades e/ou focar em outros perfis de pessoas. Por exemplo, sabemos
que estudantes de TI gostam de ir aos eventos para atividades mais práticas como
oficinas, será que o se o FISL então tiver mais oficinas, teremos mais público?
E o que o FISL pode oferecer às comunidades para que o evento volte a ter
relevância para elas?

Uma pequena tentativa de mudar o formato padrão está sendo feita este ano no
[FISL17<i class="fa fa-external-link"></i>](http://softwarelivre.org/fisl17)
com a criação dos
[minieventos<i class="fa fa-external-link"></i>](http://softwarelivre.org/fisl17/programacao/minieventos)
das comunidades. Esses minieventos são justamente espaços para que as comunidades
realizem seus eventos dentro do espaço do FISL e assim o Fórum se torne uma
espécie de guarda-chuva.

Outra sugestão é a ASL.org aproveitar que marca "FISL" se tornou ao longo desses
17 anos uma marca forte e reconhecida pela comunidade de Software Livre e atender
a algumas demandas:

 1. Grupos querendo organizar um FISL nas suas cidades;
 1. Eventos focados em temáticas específicas;
 1. Continuidade na divulgação dos benefícios do Software Livre pelo país;
 1. Aumento do número de participantes e patrocinadores de forma distribuída;
 1. Geração de renda com Software Livre;
 1. Necessidade de aumentar a arredação financeira da ASL.Org para investir em outros projetos.

Como eu acredito que é muito difícil o FISL voltar a ter 5 mil, 6 mil, 7 mil
participantes em uma única edição em Porto Alegre como no passado, a ideia então
é criar FISL's temáticos espalhados pelo país acontecendo durante 2 ou 3 dias,
organizados por comunidades locais das cidades com a ajuda do pessoal da ASL.Org.
Sendo assim, teríamos edições com temas próximos aos que são as
[trilhas e áreas<i class="fa fa-external-link"></i>](http://softwarelivre.org/fisl17/trilhas)
do FISL.

Alguns exemplos de FISL's temáticos:

 * **FISL Negócios:** discussões com grande e pequenas empresas, startups, etc,
sobre como gerar mercado com software livre, apresentando fornecedores, clientes,
cases de sucesso. O foco principal não são as discussões técnicas e
filosóficas/sociais do SL.
 * **FISL Desenvolvimento:** atividades técnicas sobre linguagens livres de
programação como PHP, Python, Ruby, etc.
 * **FISL CMS:** atividades sobre WordPress, Joomla, Drupal, Plone, etc.
 * **FISL Segurança/Privacidade:** atividades sobre segurança, privacidade,
espionagem, criptografia, etc.
 * **FISL Aplicações Desktop:** atividades sobre Gimp, Inkscape, Blender,
LibreOffice, Mozilla, KDE, Gnome, jogos, etc.
 * **FISL Banco de Dados:** atividades sobre PostgreSQL, Mysql, Mariadb, NoSQL, etc.
 * **FISL Sistemas Operacionais:** atividads desde o kernel Linux até
distribuições como Debian, Ubuntu, Fedora, Arch, etc.
 * **FISL Educação:** atividades relacionados a educação com a ajuda do pessoal
do GT-Educação.
 * **FISL Cultura, Filosofia, e Política:** discussões de temas filosóficos,
sociais e políticos do movimento software livre.
 * **FISL Políticas Públicas:** discussões sobre o software livre nos governos
federal, estaduais e municipais.
 * **FISL Administração de Redes e Sistemas:** atividades focadas nas tarefas de
sysadmin, devops, etc.
 * **FISL Mulheres:** focado em incentivar a participação de mais mulheres na
comunidade de software livre, parecido com o Encontro MNT.

Assim, nós aqui da comunidade de Software Livre em Curitiba poderíamos organizar
por exemplo o "FISL Negócios". Outros exemplos: o pessoal de Belém poderia
organizar o "FISL Desenvolvimento", o pessoal de São Paulo o "FISL CMS", o
pessoal do Rio de Janeiro o "FISL Aplicações Desktop", o pessoal de Brasília o
"FISL Políticas Públicas", o pessoal do João Pessoa o "FISL Cultura, Filosofia e
Política", e por aí vai. Todos esses FISL's teriam patrocinadores e inscrições
pagas para custear o próprio evento, incluindo pagamento de algumas pessoas
envolvidas na organização como forma de gerar renda para elas, e o que sobrar
seria revertido para a ASL.Org.

Obviamente o FISL generalista que conhecemos hoje continuaria acontecendo uma
vez por ano em Porto Alegre como o encontro das comunidades, com objetivo
principal de planejar as ações futuras. Mas com a diminuição da expectativa e da
pressão de ser "o evento de software livre único e principal". Os FISL's
temáticos inclusive ajudariam a divulgar esse FISL principal de Porto Alegre e a
vender ingressos.

A organização dos FISL's temáticos não será algo simples de acontecer,
principalmente porque dependerá do compromisso das comunidades locais em
trabalhar sob a orientação da Coordenação Geral da ASL.Org, mas tenho certeza
que muita gente se sentirá motivada em colaborar.

