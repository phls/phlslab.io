---
layout: post
title: Dia da Liberdade do Software 18/09/10
date: 2010-09-11 16:00
author: Paulo Henrique de Lima Santana
categories: geral
tags: software-livre sfd
---

# Dia da Liberdade do Software 18/09/10

O [Software Freedom Day (SFD)<i class="fa fa-external-link"></i>](https://www.softwarefreedomday.org/)
ou em português, Dia da Liberdade do Software é uma celebração mundial do
Software Livre e de Código Aberto (SL/CA). O objetivo com essa festa é mostrar
ao público em todo o mundo os benefícios de usar SL/CA de alta qualidade na
educação, no governo, em casa, e nos negócios, ou seja, em todos os lugares!

A organização sem fins lucrativos
[Digital Freedom Foundation<i class="fa fa-external-link"></i>](https://www.digitalfreedomfoundation.org/)
coordena o SFD em nível global, oferecendo apoio, brindes e um ponto de
colaboração, mas times de voluntários em todo o mundo organizam os eventos SFD
locais para atingir suas próprias comunidades.

A 7a. edição do Dia da Liberdade do Software acontecerá em diversas cidades no
mundo no próximo sábado 18 de setembro de 2010.

Para consultar quais cidades no Brasil estarão promovendo atividades, consulte o
link abaixo:

<http://wiki.softwarefreedomday.org/2019/Brazil>