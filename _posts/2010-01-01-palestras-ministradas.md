---
layout: post
title: Palestras ministradas
date: 2010-01-01 16:00
author: Paulo Henrique de Lima Santana
categories: geral
tags: palsstras
---

# Palestras ministradas

**Semana Acadêmica do curso de Tecnologia em Análise e Desenvolvimento de Sistemas da UFPR 2018**

 * Data: 11 de setembro de 2018
 * Local: Setor de Educação Profissional e Tecnológica da Universidade Federal do Paraná (SEPT UFPR) - Curitiba - PR
 * Palestra: O Projeto Debian quer você!

**DebConf18 - Conferência mundial do Projeto Debian**

 * Data: 29 de julho a 05 de agosto de 2018
 * Local: NCTU - Hsinchu - Taiwan
 * Palestra: DebConf19: Curitiba

**DebConf18 - Conferência mundial do Projeto Debian**

 * Data: 29 de julho a 05 de agosto de 2018
 * Local: NCTU - Hsinchu - Taiwan
 * BoF: Debian Brasil and DebConf19

**FLISOL - Festival Latino-americano de Instalação de Software Livre 2018**

 * Data: 28 de abril de 2018
 * Local: Universidade Positivo - Curitiba - PR
 * Palestra: Papo Livre: Introdução ao Software Livre em podcast

**Campus Party Brasil 2018**

 * Data: 30 de de janeiro a 04 de fevereiro de 2018
 * Local: Anhembi Parque - São Paulo - SP
 * Palestra: Software livre: muito além do código aberto

**Campus Party Brasil 2018**

 * Data: 30 de de janeiro a 04 de fevereiro de 2018
 * Local: Anhembi Parque - São Paulo - SP
 * Painel: Debian - ouvindo experiências e contribuindo para o projeto

**Campus Party Brasil 2018**

 * Data: 30 de de janeiro a 04 de fevereiro de 2018
 * Local: Anhembi Parque - São Paulo - SP
 * Workshop: Contribuindo para o Projeto Debian

**XIV FGSL - Fórum Goiano de Software Livre**

 * Data: 18 e 19 de novembro de 2017
 * Local: UFG - Goiânia - GO
 * Palestra: O Projeto Debian quer você!

**Campus Party Minas Gerais 2017**

 * Data: 01 a 05 de novembro de 2017
 * Local: Expominas - Belo Horizonte - MG
 * Workhop: Debian GNU/Linux: conheça, instale e use o maior projeto de software livre do mundo!

**Campus Party Weekend Pato Branco 2017**

 * Data: 14 e 15 de outubro de 2017
 * Local: Pato Branco - PR
 * Workshop: O Projeto Debian quer você!

**IX FTSL - Fórum de Tecnologia em Software Livre**

 * Data: 27 a 29 de setembro de 2017
 * Local: Universidade Tecnológica Federal do Paraná (UTFPR) - Curitiba - PR
 * Palestra: O Projeto Debian quer você!

**Campus Party Bahia 2017**

 * Data: 09 a 13 de agosto de 2017
 * Local: Estádio Fonte Nova - Salvador - BA
 * Palestra: O Projeto Debian quer você!

**Campus Party Brasília 2017**

 * Data: 14 a 18 de junho de 2017
 * Local: Centro de Convenções Ulysses Guimarães - Brasília - DF
 * Palestra: O Projeto Debian quer você!

**MiniDebConf Curitiba 2017**

 * Data: 17 a 19 de março de 2017
 * Local: UTFPR - Curitiba - PR
 * Painel: Debian Teams

**MiniDebConf Curitiba 2017**

 * Data: 17 a 19 de março de 2017
 * Local: UTFPR - Curitiba - PR
 * Debate: Eventos Debian Brasil em 2017

**Campus Party Brasil 2017**

 * Data: 31 de de Janeiro a 05 de fevereiro de 2017
 * Local: Anhembi Parque - São Paulo - SP
 * Painel: Debian - Ouvindo experiências e contribuindo para o projeto

**Campus Party Brasil 2017**

 * Data: 31 de de Janeiro a 05 de fevereiro de 2017
 * Local: Anhembi Parque - São Paulo - SP
 * Palestra: Como organizar eventos de Software Livre na sua cidade

**Campus Party Brasil 2017**

 * Data: 31 de de Janeiro a 05 de fevereiro de 2017
 * Local: Anhembi Parque - São Paulo - SP
 * Palestra: Redes sociais livres e federadas como alternativas às redes privadas e espionadas

**Campus Party Minas Gerais 2016**

 * Data: 09 a 13 de novembro de 2016
 * Local: Expominas - Belo Horizonte - MG
 * Palestra: Redes sociais livres e federadas como alternativas às redes privadas e espionadas

**VIII FTSL - Fórum de Tecnologia em Software Livre**

 * Data: 31 de agosto a 02 de setembro de 2016
 * Local: Universidade Tecnológica Federal do Paraná (UTFPR) - Curitiba - PR
 * Palestra: Redes socias livres e federadas como alternativas as redes privadas e espionadas

**Campus Party Recife 2016**

 * Data: 20 e 21 de agosto de 2016
 * Local: Centro de Convenções de Pernambuco - Recife - PE
 * Palestra: Bom dia com GNU!

**FISL 17 - Fórum Internacional de Software Livre**

 * Data: 13 a 16 de julho de 2016
 * Local: PUC - Porto Alegre - RS
 * Palestra: Redes socias livres e federadas como alternativas as redes privadas e espionadas

**FISL 17 - Fórum Internacional de Software Livre**

 * Data: 13 a 16 de julho de 2016
 * Local: PUC - Porto Alegre - RS
 * Encontro Comunitário MaisGNU

**Circuito Curitibano de Software Livre - 8ª etapa**

 * Data: 31 de maio e 01 de junho de 2016
 * Local: Opet - Curitiba - PR
 * Palestra: Participe da Comunidade Curitiba Livre

**Circuito Curitibano de Software Livre - 7ª etapa**

 * Data: 17 de maio de 2016
 * Local: Unicuritiba - Curitiba - PR
 * Palestra: Software Livre e a Comunidade Curitiba Livre

**CryptoRave 2016**

 * Data: 06 e 07 de maio de 2016
 * Local: Centro Cultural São Paulo - São Paulo - SP
 * Palestra: Conheça as principais redes sociais livres como alternativas as redes fechadas

**FLISOL - Festival Latino-americano de Instalação de Software Livre 2016**

 * Data: 16 de abril de 2016
 * Local: Pontifícia Universidade Católica (PUC) - Curitiba - PR
 * Palestra: Conheça as principais redes sociais livres como alternativas as redes fechadas

**Campus Party Brasil 2016**

 * Data: 26 a 31 de de Janeiro de 2016
 * Local: Anhembi Parque - São Paulo - SP
 * Painel: O movimento software livre como agente transformador da sociedade

**Campus Party Brasil 2016**

 * Data: 26 a 31 de de Janeiro de 2016
 * Local: Anhembi Parque - São Paulo - SP
 * Painel: II Encontro de Usuários Diaspora do Brasil

**IV JAI - Jornada de Atualização em Informática**

 * Data: 29 de outubro de 2015
 * Local: UniCuritiba - Curitiba - PR
 * Palestra: Software Livre e a Comunidade Curitiba Livre

**Circuito Curitibano de Software Livre - 6ª etapa**

 * Data: 07 de outubro de 2015
 * Local: Pontifícia Universidade Católica (PUC) - Curitiba - PR
 * Palestra: Software Livre e a Comunidade Curitiba Livre

**Aniversário de 30 anos da Free Software Foudation**

 * Data: 03 de outubro de 2015
 * Local: Aldeia Coworking - Curitiba - PR
 * Palestra: Comunidade Curitiba Livre

**Circuito Curitibano de Software Livre - 5ª etapa**

 * Data: 02 de setembro de 2015
 * Local: Opet - Curitiba - PR
 * Palestra: Participe da Comunidade Curitiba Livre

**Circuito Curitibano de Software Livre - 4ª etapa**

 * Data: 26 de agosto de 2015
 * Local: Spei - Curitiba - PR
 * Palestra: Software Livre e a Comunidade Curitiba Livre

**Semana Acadêmica do curso de Tecnologia em Análise e Desenvolvimento de Sistemas da UFPR 2015**

 * Data: 12 de agosto de 2015
 * Local: Setor de Educação Profissional e Tecnológica da Universidade Federal do Paraná (SEPT UFPR) - Curitiba - PR
 * Palestra: Junte-se a Comunidade Curitiba Livre

**Circuito Curitibano de Software Livre - 3ª etapa**

 * Data: 05 de agosto de 2015
 * Local: Uniandrade - Curitiba - PR
 * Palestra: Software Livre e a Comunidade Curitiba Livre

**Campus Party Recife 2015**

 * Data: 23 a 26 de julho de 2015
 * Local: Centro de Convenções de Pernambuco - Recife - PE
 * Painel: Linguagens de programação livres: qual escolher?

**Campus Party Recife 2015**

 * Data: 23 a 26 de julho de 2015
 * Local: Centro de Convenções de Pernambuco - Recife - PE
 * Painel: Encontro da Comunidade Debian Brasil

**FISL 16 - Fórum Internacional de Software Livre**

 * Data: 08 a 11 de julho de 2015
 * Local: PUC - Porto Alegre - RS
 * Painel: Redes sociais privadas e a concentração de informação: como elas afetam o Sofware Livre?

**FISL 16 - Fórum Internacional de Software Livre**

 * Data: 08 a 11 de julho de 2015
 * Local: PUC - Porto Alegre - RS
 * Painel: O processo de construção colaborativo de revistas eletrônicas com Software Livre

**Expotec**

 * Data: 27 a 30 de maio de 2015
 * Local: Centro de Convenções Ronaldo Cunha Lima - João Pessoa - PB
 * Palestra: Quer organizar um evento de Software Livre na sua cidade? Pergunte-me como.

**Expotec**

 * Data: 27 a 30 de maio de 2015
 * Local: Centro de Convenções Ronaldo Cunha Lima - João Pessoa - PB
 * Oficina: Crie seu blog ou site no softwarelivre.org

**Semana de Atualização em Engenharia Elétrica (SEATEL)**

 * Data: 21 de maio de 2015
 * Local: Universidade Federal do Paraná - Curitiba - PR
 * Palestra: Junte-se a Comunidade Curitiba Livre

**Circuito Curitibano de Software Livre - 2ª etapa**

 * Data: 20 de maio de 2015
 * Local: Faculdades Santa Cruz - Curitiba - PR
 * Palestra: Software Livre e a Comunidade Curitiba Livre

**Circuito Curitibano de Software Livre - 1ª etapa**

 * Data: 18 de maio de 2015
 * Local: Unibrasil - Curitiba - PR
 * Palestra: Software Livre e a Comunidade Curitiba Livre

**Campus Party Brasil 2015**

 * Data: 03 a 08 de fevereiro de 2015
 * Local: Centro de Exposições Imigrantes - São Paulo - SP
 * Painel: Mídais sociais livres como alternativas as redes devassas

**Campus Party Brasil 2015**

 * Data: 03 a 08 de fevereiro de 2015
 * Local: Centro de Exposições Imigrantes - São Paulo - SP
 * Painel: Existe Movimento Software Livre x Open Source Initiative, ou somos todos iguais?

**Campus Party Brasil 2015**

 * Data: 03 a 08 de fevereiro de 2015
 * Local: Centro de Exposições Imigrantes - São Paulo - SP
 * Palestra: Organize um evento de software livre na sua cidade você também

**Latinoware 2014 - XI Conferência Latino-americana de Software Livre**

 * Data: 15 a 17 de outubro de 2014
 * Local: Itaipu - Foz do Iguaçu - PR
 * Palestra: Quer organizar um evento de Software Livre na sua cidade? Pergunte-me como

**VI FTSL - Fórum de Tecnologia em Software Livre**

 * Data: 18 e 19 de setembro de 2014
 * Local: Universidade Tecnológica Federal do Paraná (UTFPR) - Curitiba - PR
 * Mesa-redonda: Negócios de TI com Software Livre

**VI FTSL - Fórum de Tecnologia em Software Livre**

 * Data: 18 e 19 de setembro de 2014
 * Local: Universidade Tecnológica Federal do Paraná (UTFPR) - Curitiba - PR
 * Mesa-redonda: Você sabe o quanto está sendo espionado? E como evitar?

**Aniversário da ASL.Org**

 * Data: 10 de setembro de 2014
 * Local: Aldeia Coworking - Curitiba - PR
 * Palestra: Participe da Comunidade Curitiba Livre

**Campus Party Recife 2014**

 * Data: 23 a 27 de julho de 2014
 * Local: Centro de Convenções de Pernambuco - Recife - PE
 * Palestra: Quer organizar um evento de software livre na sua cidade? Pergunte-me como

**Campus Party Recife 2014**

 * Data: 23 a 27 de julho de 2014
 * Local: Centro de Convenções de Pernambuco - Recife - PE
 * Palestra: Organize uma edição do FLISOL em sua cidade e faça parte da comunidade

**Campus Party Recife 2014**

 * Data: 23 a 27 de julho de 2014
 * Local: Centro de Convenções de Pernambuco - Recife - PE
 * Mesa-redonda: Você sabe o quanto é espionado? E como evitar?

**FISL 15 - Fórum Internacional de Software Livre**

 * Data: 07 a 10 de maio de 2014
 * Local: PUC - Porto Alegre - RS
 * Mesa-redonda: Distribuições GNU/Linux - entenda como funciona

**FLISOL - Festival Latino-americano de Instalação de Software Livre 2014**

 * Data: 26 de abril de 2014
 * Local: Pontifícia Universidade Católica (PUC) - Curitiba - PR
 * Palestra: O que é Software Livre e Linux

**EFD - Education Freedom Day 2014**

 * Data: 08 de março de 2014
 * Local: Universidade Tecnológica Federal do Paraná (UTFPR) - Curitiba - PR
 * Mesa-redonda: Politicas públicas para REA e software livre na educação

**Campus Party Brasil 2014**

 * Data: 27 de Janeiro a 02 de Fevereiro de 2014
 * Local: Anhembi Parque - São Paulo - SP
 * Mesa-redonda: Distribuições GNU/Linux - entenda como funciona

**Campus Party Brasil 2014**

 * Data: 27 de Janeiro a 02 de Fevereiro de 2014
 * Local: Anhembi Parque - São Paulo - SP
 * Mesa-redonda: É possível empreender e ganhar dinheiro com software livre?

**Campus Party Brasil 2014**

 * Data: 27 de Janeiro a 02 de Fevereiro de 2014
 * Local: Anhembi Parque - São Paulo - SP
 * Mesa-redonda: Você sabe o quanto é espionado? E como evitar?

**Campus Party Brasil 2014**

 * Data: 27 de Janeiro a 02 de Fevereiro de 2014
 * Local: Anhembi Parque - São Paulo - SP
 * Mesa-redonda: O processo de construção colaborativo de revistas eletrônicas, fanzines e publicações em geral com software livre

**Campus Party Brasil 2014**

 * Data: 27 de Janeiro a 02 de Fevereiro de 2014
 * Local: Anhembi Parque - São Paulo - SP
 * Mesa-redonda: Governos e software livre - investindo dinheiro público em tecnologias abertas

**1ª Semana Acadêmica de Informática e Cidadania**

 * Data: 17 de setembro de 2013
 * Local: Universidade Federal do Paraná (UFPR Litoral) - Matinhos - PR
 * Mesa-redonda: Software Livre

**Campus Party Recife 2013**

 * Data: 17 a 21 de Julho de 2013
 * Local: Centro de Convenções de Pernambuco - Recife - PE
 * Mesa-redonda: Neutralidade na Rede

**FISL 14 - Fórum Internacional de Software Livre**

 * Data: 03 a 06 de Julho de 2013
 * Local: PUC - Porto Alegre - RS
 * Mesa-redonda: Encontro dos Promotores e Organizadores de Flisol

**FISL 14 - Fórum Internacional de Software Livre**

 * Data: 03 a 06 de Julho de 2013
 * Local: PUC - Porto Alegre - RS
 * Mesa-redonda: Em defesa do software livre nos cursos de computação das universidades no Brasil

**FISL 14 - Fórum Internacional de Software Livre**

 * Data: 03 a 06 de Julho de 2013
 * Local: PUC - Porto Alegre - RS
 * Mesa-redonda: Plenária da Comunidade Debian Brasil

**Campus Party Brasil 2013**

 * Data: 28 de Janeiro a 03 de Fevereiro de 2013
 * Local: Anhembi Parque - São Paulo - SP
 * Mesa-redonda: Como Contribuir em Projetos de Software Livre

**Campus Party Recife 2012**

 * Data: 26 a 30 de Julho de 2012
 * Local: Centro de Convenções de Pernambuco - Recife - PE
 * Mesa-redonda: Como a comunidade brasileira contribui com as Distribuições GNU/Linux

**FISL 13 - Fórum Internacional de Software Livre**

 * Data: 25 a 28 de Julho de 2012
 * Local: PUC - Porto Alegre - RS
 * Mesa-redonda: Encontro dos Promotores e Organizadores de Flisol

**Campus Party Brasl 2012**

 * Data: 06 a 11 de Fevereiro de 2012
 * Local: Anhembi Parque - São Paulo - SP
 * Mesa-redonda: Mulheres e Software Livre

**V ENSOL - Encontro de Software Livre da Paraíba**

 * Data: 20 a 23 de Julho de 2011
 * Local: Estação Ciência, Cultura e Artes Cabo Branco - João Pessoa - PB
 * Palestra: A ENEC

**XXVIII ENECOMP - Congresso Nacional dos Estudantes de Computação**

 * Data: 20 e 21 de Julho de 2011
 * Local: Centro de Convenções de Natal – RN
 * Mesa-redonda: Software Livre e Cultura Livre

**FISL 12 - Fórum Internacional de Software Livre**

 * Data: 29 de Junho a 02 de Julho de 2011
 * Local: PUC - Porto Alegre - RS
 * Palestra: A ENEC

**XXVII ENECOMP - Congresso Nacional dos Estudantes de Computação**

 * Data: 04 a 08 de Setembro de 2009
 * Local: Colégio Estadual - Curitiba - PR
 * Mesa-redonda: Mulheres e Tecnologia

**XXVII ENECOMP - Congresso Nacional dos Estudantes de Computação**

 * Data: 04 a 08 de Setembro de 2009
 * Local: Colégio Estadual - Curitiba - PR
 * Mesa de abertura

**FISL 10 - Fórum Internacional de Software Livre**

 * Data: 24 a 27 de Junho de 2009
 * Local: PUC - Porto Alegre - RS
 * Palestra: Movimento estudantil em prol do software livre

**Latinoware 2008 - V Conferência Latino-americana de Software Livre**

 * Data: 30 de Outubro a 01 de Novembro de 2008
 * Local: Itaipu - Foz do Iguaçu - PR
 * Mesa redonda: A ENEC e o projeto Currículo Livre

**XXVI ENECOMP - Encontro Nacional dos Estudantes de Computação**

 * Data: 28 de Julho a 01 de Agosto de 2008
 * Local: Centro de Convenções Raymundo Asfora - Campina Grande - PB
 * Palestra: Movimento Estudantil e Organização da ENEC

**FISL 9.0 - Fórum Internacional de Software Livre**

 * Data: 17 a 19 de Abril de 2008
 * Local: PUC - Porto Alegre - RS
 * Mesa-redonda: Software Livre na Educação Universitária : Os estudantes de computação e o software livre

**FISL 8.0 - Fórum Internacional de Software Livre**

 * Data: 12 a 14 de Abril de 2007
 * Local: Centro de Exposições FIERGS - Porto Alegre - RS
 * Palestra: Como economizamos tempo e dinheiro usando software livre em uma empresa privada

**I ERECOMP-AL - Encontro Alagoano de Estudantes de Computação**

 * Data: 15 a 18 de Março de 2007
 * Local: CEFET-AL - Maceió - AL
 * Palestra: A ENEC e o Currículo Livre

**I ERECOMP-AL - Encontro Alagoano de Estudantes de Computação**

 * Data: 15 a 18 de Março de 2007
 * Local: CEFET-AL - Maceió - AL
 * Mini-curso: TWiki

**Latinoware 2006 - III Conferência Latino-americana de Software Livre**

 * Data: 16 e 17 de Novembro de 2006
 * Local: Itaipu - Foz do Iguaçu - PR
 * Palestra: Como economizamos tempo e dinheiro usando software livre em uma empresa privada

**I ENCASOL - Encontro Campinense de Software Livre / I ERECOMP-PB - Encontro Paraibano de Estudantes de Computação**

 * Data: 03 a 05 de Novembro de 2006
 * Local: UEPB - Campina Grande - PB
 * Palestra: ENEC e Software Livre

**I ENCASOL - Encontro Campinense de Software Livre / I ERECOMP-PB - Encontro Paraibano de Estudantes de Computação**

 * Data: 03 a 05 de Novembro de 2006
 * Local: UEPB - Campina Grande - PB
 * Mini-curso: Instalação Debian-BR-CDD

**XXIV ENECOMP - Encontro Nacional dos Estudantes de Computação**

 * Data: 31 de Julho a 04 de Agosto de 2006
 * Local: PUC - Poços de Caldas - MG
 * Palestra: ENEC e o Movimento Estudantil

**IV Semana de Atualização em Tecnologia da Informação - SATI**

 * Data: 05 a 09 de Junho de 2006
 * Local: UEPG Universidade Estadual de Ponta Grossa - Ponta Grossa - PR
 * Palestra: A ENEC

**FISL 7.0 - Fórum Internacional de Software Livre**

 * Data: 19 a 22 de Abril de 2006
 * Local: Centro de Exposições FIERGS - Porto Alegre - RS
 * Palestra: Inclusão Digital: O que os estudantes têm a ver com isso?

**CONISLI 2005 - III Congresso Internacional de Software Livre**

 * Data: 03 a 05 de Novembro de 2005
 * Local: SESC - São Paulo - SP
 * Palestra: Software Livre e Movimento Estudantil: uma perspectiva revolucionária

**II SBSI - Simpósio Brasileiro de Sistemas de Informação**

 * Data: 26 a 28 de Outubro de 2005
 * Local: UFSC - Florianópolis - SC
 * Mesa redonda: Regulamentação da profissão

**I ERECOMP-PR - I Encontro Paranaense dos Estudantes de Computação**

 * Data: 15 a 17 de Setembro de 2005
 * Local: Cascavel - PR
 * Palestra: A ENEC

**I Semana de Software Livre de Ponta Grossa**

 * Data: 13 a 17 de Junho de 2005
 * Local: UEPG Universidade Estadual de Ponta Grossa - Ponta Grossa - PR
 * Palestra: Software Livre e Movimento Estudantil: uma perspectiva revolucionária

**4a. Semana de Informática - CEFET - PR**

 * Data: 08 a 10 de Junho de 2005
 * Local: CEFET - Curitiba - PR
 * Mesa de abertura

**FISL 6.0 - Fórum Internacional de Software Livre**

 * Data: 01 a 04 de Junho de 2005
 * Local: PUC - Porto Alegre - RS
 * Mesa redonda: Regulamentação da profissão e seu impacto na comunidade de software livre

**FISL 6.0 - Fórum Internacioal de Software Livre**

 * Data: 01 a 04 de Junho de 2005
 * Local: PUC - Porto Alegre - MG
 * Palestra: Software Livre e Movimento Estudantil: uma perspectiva revolucionária

**3o. EMECOMP - Encontro Mineiro dos Estudantes de Computação**

 * Data: 14 a 17 de Abril de 2005
 * Local: Campus PUC Minas - Poços de Caldas - MG
 * Mesa redonda: Software Livre

**Seminário Nacional - Regulamentação do Profissão em Informática**

 * Data: 09 de Março de 2005
 * Local: EFTI Escola de Formação dos Trabalhadores de Informática - Brasília - DF
 * Painel: Mercado e a falta de regulamentação da profissão

**Semana dos calouros de Ciência da Computação da UFPR**

 * Data: 01 de Março de 2005
 * Local: Universidade Federal do Paraná - Curitiba - PR
 * Tema: A ENEC

**Seminário Região Sul - Regulamentação do Profissão em Informática**

 * Data: 24 de Fevereiro de 2005
 * Local: SERPRO - Curitiba - PR
 * Painel: Mercado e a falta de regulamentação do profissão

**Fórum Social Mundial 2005**

 * Data: 26 a 31 de Janeiro de 2005
 * Local: Porto Alegre - RS
 * Painel: A contribuição dos estudantes universitários na inclusão digital

**Fórum Social Mundial 2005**

 * Data: 26 a 31 de Janeiro de 2005
 * Local: Porto Alegre - RS
 * Painel: Currículo Livre: o Software Livre e a batalha por uma nova educação

**Fórum Social Mundial 2005**

 * Data: 26 a 31 de Janeiro de 2005
 * Local: Porto Alegre - RS
 * Painel: Software Livre e Movimento estudantil: uma perspectiva revolucionária

**2a. Info Week**

 * Data: 01 de Novembro de 2004
 * Local: UNIPAM - Centro Universitário de Patos de Minas - Patos de Minas - MG
 * Palestra: ENEC e o Movimento Estudantil

**1o. Ciclo de Palestras sobre Tecnologias da Informação Aplicadas nas Organizações**

 * Data: 28 de Outubro de 2004
 * Local: Unibrasil - Faculdades Integradas do Brasil - Curitiba - PR
 * Palestra: ENEC, Software Livre e Inclusão Digital

**II Semana de Software Livre do Rio da Janeiro**

 * Data: 20 de Outubro de 2004
 * Local: Universidade Estadual do Norte Fluminense UENF - Campos - RJ
 * Painel: ENEC, Software Livre e o Currículo Livre

**Semana de Extensão de Tecnologia em Informática**

 * Data: 14 de Outubro de 2004
 * Local: Escola Técnica - Curitiba - PR
 * Palestra: ENEC e Software Livre

**XXII ENECOMP - Encontro Nacional dos Estudantes de Computação**

 * Data: 02 a 06 de Agosto de 2004
 * Local: UFBA - Salvador - BA
 * Mesa-redonda: ENEC e o Movimento Estudantil

**I Semana de Software Livre da UFPR**

 * Data: 29 de Março a 02 de Abril de 2004
 * Local: UFPR - Curitiba - PR
 * Tema: Como instalar o Conectiva Linux

**UEPG**

 * Data: 07 de Maio de 2003
 * Local: Universidade Estadual de Ponta Grossa UEPF - Ponta Grossa - PR
 * Tema: A ENEC

**Semana dos calouros de Ciência da Computação da UFPR**

 * Data: 13 de Abril de 2003
 * Local: Universidade Federal do Paraná - Curitiba - PR
 * Tema: A ENEC

**UEM**

 * Data: 13 de Fevereiro de 2003
 * Local: Universidade Estadual de Maringá - Maringá - PR
 * Tema: A ENEC

**Semana dos calouros de Ciência da Computação da UFPR**

 * Data: 15 de Outubro de 2002
 * Local: Universidade Federal do Paraná - Curitiba - PR
 * Tema: A ENEC

**SECOMP UNIVAP 2002**

 * Data: 02 de Setembro de 2002
 * Local: Universidade do Vale do Paraíba - São José dos Campos - SP
 * Tema: A ENEC
