---
layout: post
title: Viagem de Curitiba para Bruxelas
date: 2020-01-29 20:00
author: Paulo Henrique de Lima Santana
categories: geral
tags: bruxelas bélgica debian fosdem
---

# Viagem de Curitiba para Bruxelas

Veja o texto anterior: [Vou para Bruxelas novamente participar da MiniDebConf e
do FOSDEM 2020](/vou-para-bruxelas-participar-da-minidebconf-e-do-fosdem-2020).

Como meu vôo saindo de Curitiba na terça-feira (28/01) estava marcado para às
13:20 eu já havia me planejado para acordar um pouco mais tarde, almoçar antes
do meio-dia e ir para o aeroporto. Só que no final da tarde da segunda-feira,
recebi uma ligação de uma pessoa do rh de uma empresa que eu havia mandando meu
currículo querendo marcar uma entrevista para o outro dia a tarde. Expliquei pra
ela que eu iria viajar e remarcamos para às 10h. Eu sabia que seria uma correria
porque eu moro em São José dos Pinhais e a empresa fica em Curitiba, mas eu não
podia perder essa oportunidade. O bom é que eu moro do lado do aeroporto, e o
trajeto dá uns de 10 minutos de carro.

E realmente foi um correria: a entrevista foi das 10h às 11h30min, e assim que
saí peguei um Uber pra casa. Ainda durante o trajeto, pedi para um restaurante
perto de casa entregar uma marmitex e logo depois que cheguei, pouco antes do
meio-dia, eles já entregaram. Em menos de 40 minutos engoli o almoço rápido, fiz
dois sanduíches e espremi laranjas pra fazer suco pra levar pra comer em São
Paulo, joguei o lixo fora, tomei banho, terminei de arrumar as coisas e chamei
um Uber. Resultado: cheguei no aeroporto 12h42min, menos de 40 minutos antes do
embarque.

O cara da empresa aérea que fica na entrada fila do check-in disse que o
despacho das malas estava encerrado há 2 minutos e que eu havia perdido. Mas aí
percebi que o cara tava querendo ser escroto, provavelmente querendo dar uma
liçãozinha para eu não atrasar mais quando for viajar, porque depois de eu dizer
algumas vezes que não podia perder o vôo e de pedir pra ele dar um jeito, ele
chamou a guria do balcão que disse que dava pra despachar ainda. Pra confirmar
ela chamou o pessoal da esteira pelo rádio e perguntou se dava mesmo, e deu pra
ouvir o cara bem tranquilo dizer que sim, que podia mandar minha mala. Depois eu
entendi porque o cara da esteira tava tranquilo: o vôo tava atrasado e saiu 30
minutos depois do previsto.

Em São Paulo foi tudo muito tranquilo. Fiz o check-in na Lufthansa três horas
antes do vôo, que estava marcado para sair às 18h. Fiz meu lanche quando bateu a
fome, e fiquei esperando. Percebi que não tinha fila nas esteira para entrar na
parte do embarque internacional, e o cara da esteira comentou isso, disse que
tava estranho porque naquele horário era para estar cheio.

O vôo de São Paulo para Munique foi tranquilo, mas como sempre não conseguí
dormir quase nada, mesmo tomando duas cervejas e um copo de vinho branco durante
as quase 12 horas do vôo :-) Olhei os horários e ví que estava previsto chegar
em Munique às 9h35min (horário local, que dá 4 horas na frente do Brasil onde
era 5h35min da manhã) e sair para Bruxelas às 10h25min, ou seja, menos de uma
hora de conexão. Fiquei preocupado porque eu ainda teria que passar pela
imigração nesse tempo. O jeito era torcer para não haver nenhum atraso, e tudo
estava indo bem porque durante o vôo a previsão para pousar baixou às 9h15min.
Mas quando nos aproximamos de Munique o piloto avisou que havia nevado e como a
pista estava com gelo, ele teria que esperar o pessoal limpar para poder pousar.
Então bateu a preocupação de novo porque ficamos meia-hora sobrevoando
esperando. Resultado, pousamos quase às 10h. Na saída do avião já deu pra sentir
o frio! Pelo que ví, tava 3 graus.

E lá vou eu na correria. Logo no desembarque dois policiais estava olhando os
passaportes de todos os passageiros, como se estivessem procurando alguém.
Mostrei o meu, passei por eles e tome andar até a imigração, que por sorte não
tinha fila. O agente perguntou três coisas: qual era o meu destino, o que eu ia
fazer lá e quantos dias ia ficar. Respondi, ele carimbou o passaporte e passei.
E tome andar mais, tive que pegar o metrô interno do aeroporto para chegar na
outra parte onde ficava o portão de embarque do meu vôo. Cheguei no portão e o
vôo estava atrasado 10 minutos, então fiquei tranquilo novamente. Embarquei às
11:40 e durante o vôo tomei mais uma cerveja pra poder relaxar, claro :-)

Cheguei em Bruxelas meio-dia. Passei no banheiro pra jogar uma água no rosto pra
tirar a cara de sono. Peguei a mala e vesti mais uma blusa pra encarar o frio lá
de fora. O dia estava ensolarado mas frio. O ônibus do aeroporto pra cidade usa
um tipo de cartão específico da empresa Lijn, e como eu tinha guardado o cartão
do ano passado com saldo de três passagens, pude usá-lo novamente. Peguei a
linha 272 “Brussel-Noord”. Essa empresa de ônibus faz as linhas entre a cidade
de Bruxelas e a “região metropolitana”, digamos assim. Enquanto esperava,
coloquei créditos usando a máquina de auto atendimento no outro cartão chamado
Mobib que eu também havia guardado e que serve para pegar ônibus, trem (tipo
vlt) e metrô na cidade de Bruxelas.

O ponto final do ônibus é uma estação no centro de Bruxelas chamada “Gare
Noord”, ou “Station Brussel Noord”. Quando desci do ônibus fiquei meio perdido
sem achar o acesso a estação do trem, nisso passaram dois homens conversando em
português. Resolvi pedir ajudar pra eles, e disse que eu era brasileiro. Um era
português e o outro brasileiro, e eles me indicaram a entrada. Como eu já tava
varado de fome, parei pra comer um sanduíche numa rede chamada Quick, que eu
acho que é uma concorrente local do McDonald’s.

Peguei o trem da linha 4 até a estação “Anneessens”, e lá peguei o ônibus da
linha 48 “Decroly” até a parada chamada “Jeu de Balle”, próxima do hostel. O bom
é que o cartão Mobib faz integração durante uma hora, então só paguei uma
passagem.

Vou continuar esse relato desse dia em outra publicação.

Próximo texto: [Primeiro dia em Bruxelas](/primeiro-dia-em-bruxelas)