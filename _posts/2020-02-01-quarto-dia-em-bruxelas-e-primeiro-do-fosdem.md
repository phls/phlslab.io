---
layout: post
title: Quarto dia em Bruxelas e primeiro do FOSDEM
date: 2020-02-01 23:59
author: Paulo Henrique de Lima Santana
categories: geral
tags: bruxelas bélgica debian fosdem
---

# Quarto dia em Bruxelas e primeiro do FOSDEM

Veja o texto anterior: [Terceiro dia em Bruxelas e segundo da
MiniDebCamp](/terceiro-dia-em-bruxelas-e-segundo-da-minidebcamp).

Finalmente chegou o dia do FOSDEM! Eu havia programado para acordar às 7:30 mas
devido a noite, digamos um pouco difícil por causa das cervejas que tomei na
noite anterior, acordei às 8h. Enquanto esperava o trem (tipo um VLT chamado de
"train" aqui) para o evento ouvi uns brasileiros conversando com português,
aparentemente eram funcionários da RedHat. O trem saiu lotado e quase todo mundo
desceu no ponto da [ULB - Universidade Livre de Bruxelas](https://www.ulb.be)
onde acontece o FOSDEM.

Quando cheguei na ULB às 9:30 já fui para o stand do Debian deixar as camisetas
e adesivos que eu trouxe e dar uma ajuda ao pessoal. Além desses produtos, o
DDs da França trouxeram outros modelos de camisetas e adesivos, bonés,
pendrives, moletons, lápis, um tipo de proteção para o pescoço pro frio, e o
Jonathan trouxe da Africa do Sul um adesivo grande com a espiral. E o pessoal
fica louco para comprar os produtos com a marca do Debian. Tanto que no final da
tarde as minhas camisetas já tinham acabado.

![Bruxelas](/assets/img/bruxelas-2020-17.jpg)

Foto do Joost van Baal-Ilić

![Bruxelas](/assets/img/bruxelas-2020-32.jpg)

Foto da Anisa Kuci:

![Bruxelas](/assets/img/bruxelas-2020-33.jpg)

Como eu havia me inscrito como voluntário para operar a câmera que filma a
palestra no auditório principal, fiquei das 11h às 13h fazendo essa tarefa. Eu
já havia feito isso no ano passado e achei legal fazer novamente esse ano. É uma
forma de contribuir para o evento e você ainda ganha uma camiseta e um vale no
valor de € 15,00 para comer em um dos food-trucks que ficam dentro da
Universidade. Antes de ir para o auditório, passei no balcão de atendimento e
peguei minha camisa laranja de voluntário, pela que só tinha XL e ficou gigante.
No auditório fiquei operando a câmera enquanto outro voluntário cuidavo do som e
da operação geral de filmagem. Antes de final do meu horário outro voluntário
passou lá e deixou o vale pra comer.

![Bruxelas](/assets/img/bruxelas-2020-14.jpg)

Quando saí do auditório fui comer e pedi 2 sanduíches: um de hamburguer e outro
de porco desfiado. Os sanduíches eram meio sem graça pra dizer a verdade, o de
hamburguer era só o pão, carne, umas folhinhas de alface e uma rodela de tomate,
nem queijo tinha. Os dois custaram € 14,00 no total e os food-trucks só podem
vender comidas, não vendem bebidas nenhuma, nem água. Só como curiosidade, uma
lata de coca-cola na máquina de vendas na Universidade custa € 0,70, foi o lugar
mais barato que já em toda viagem.

![Bruxelas](/assets/img/bruxelas-2020-15.jpg)

![Bruxelas](/assets/img/bruxelas-2020-13.jpg)

Quem quiser assistir as palestras que filmei no auditório
[Janson](https://fosdem.org/2020/schedule/track/history), elas são: "LibreOffice
turns ten and what's next" e "Over Twenty Years Of Automation".

Depois de almoçar fiquei curtindo o evento. Andei pelos standes, peguei
adesivos, conversei com algumas pessoas (estrangeiros e brasileiros). No stande
do OpenSuse estavam dando cervejas, então eu, o Kanashiro e o Samuel
aproveitamos :-)

![Bruxelas](/assets/img/bruxelas-2020-16.jpg)

Quando o evento acabou, fui para o hostel deixar minha mochila porque às 20:30
estava marcado um jantar com algumas pessoas do Debian. Todos os anos alguns
dias antes do FOSDEM um DD manda um e-mail pra lista perguntando quem quer
participar do jantar no [Chezleon](http://www.chezleon.be/en/), que é um
restaurante famoso em Bruxelas e que muitos turistas vão lá. Dessa vez eu me
inscrevi e fui. Fui o primeiro a chegar na frente do restaurante e a medida que
o pessoal ia chegando, ia se apresentando porque a maioria se conhece apenas por
email, então como o pessoal diz: "é bom dar uma cara a um nick". Estávamos em 25
pessoas no total. O restaurante é especializado em frutos do mar, mas tem outras
opções e os pratos em geral não são caros. Eu pedi carne de porco, que veio
acompanhada de uma folhinha e batata frita (tudo vem acompanhando de batata
frita aqui). Junto com uma cerveja, minha conta ficou em € 18,00 mas o pessoal
da nossa mesa resolveu dividir a conta por igual e ficou € 25,00 pra cada um.
Mal negócio pra mim.

Foi uma interação legal. Curiossamente o DD que organiza o jantar não é de
Bruxelas, ele é italiano. Além dele, na minha mesa ainda tinham pessoas do EUA,
da Índia, da Alemanha e da Inglaterra. Nas outras mesas tinham mais outras
nacionalidades. Depois do jantar fomos comer waffles. Já era pouco mais de
meia-noite, então decidi ir embora enquanto alguns seguiram pra Delirium. Eu
estava cansado e tinha que caminhar sozinho uns 20 minutos até o hostel.

Cheguei no hostel tranquilamente e fui dormir. O segundo dia de FOSDEM prometia!

Próximo texto: [Quinto dia em Bruxelas e segundo do
FOSDEM](/quinto-dia-em-bruxelas-e-segundo-do-fosdem)
