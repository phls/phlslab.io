---
layout: post
title: Em outubro dois eventos em Curitiba discutirão o Marco Civil, entre outras coisas
date: 2014-09-29 16:00
author: Paulo Henrique de Lima Santana
categories: geral
tags: marco-civil
---

# Em outubro dois eventos em Curitiba discutirão o Marco Civil, entre outras coisas

No mês de outubro acontecerão dois eventos em Curitiba que não são diretamente
relacionados a TI e Software Livre, mas que trarão para a nossa cidade alguns
ativistas do software livre e da liberdade do conhecimento.

Os eventos são gratuitos e acontecerão nos Campi da UFPR.

## VI Enpecom - Encontro de Pesquisa em Comunicação

 * Tema: Comunicação e Democracia.
 * Data: 09 a 11 de outubro de 2014
 * Local: Campus Juvevê da UFPR - Rua Bom Jesus, 650.
 * Site: <http://www.enpecom.ufpr.br>
 * Destaque: Mesa-redonda: Compreendendo o Marco Civil da Internet
    * Dia 10/10 às 19h
    * Sérgio Amadeu da Silveira (UFABC) e João Paulo Mehl (UFPR)

## VIII C0DAIP - Congresso de Direito de Autor e Interesse Público

 * Tema: Direitos autorais e a inclusão tecnológica e cultural: movimentos rumos
à sociedade democrática do conhecimento.
 * Data: 27 e 28 de outubro de 2014
 * Local: Prédio Central da UFPR - Praça Santos Andrade, 50
 * Site: <http://gedai.com.br/?q=pt-br/node/601>
 * Destaques:
    * Sérgio Amadeu
    * Ronaldo Lemos
    * Corinto Meffe
    * Pedro Paranaguá
    * Omar Kaminski