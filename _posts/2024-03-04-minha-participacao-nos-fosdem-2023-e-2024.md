---
layout: post
title: Minha participação nos FOSDEM 2023 e 2024
date: 2024-03-04 10:00
author: Paulo Henrique de Lima Santana
categories: geral
tags: debian bruxelas bélgica fosdem planetdebian-pt
---

# Introdução

Desde 2019 tenho viajado para Bruxelas no começo do ano para participar do [FOSDEM](https://fosdem.org/2024/), considerado o maior e mais importante evento de Software Livre da Europa. A edição de 2024 foi a quarta presencial seguida que eu participei (2021 e 2022 não houve por causa da COVID-19) e sempre com a ajuda financeira do Debian, que gentilmente paga as minhas passagens áreas após ter o meu pedido de ajuda para viajar aprovado pelo líder do Debian.

Em 2020 escrevi [vários posts](https://phls.com.br/viagem-de-curitiba-para-bruxelas) com um relato bastante completo dos dias que passei em Bruxelas. Mas em 2023 não escrevi nada, e como ano passado e este ano eu coordenei uma sala dedicada a traduções de projetos de Software Livre e Código Aberto, vou aproveitar para escrever sobre os dois anos e como foi essa experiência.

Depois da minha primeira ida ao FOSDEM, eu comecei a pensar que poderia participar de uma forma mais ativa do que apenas um participante normal, então eu tinha o desejo de propor uma palestra para alguma das salas. Mas então pensei que poderia ao invés de propor uma palestra, eu poderia organizar uma sala de palestras :-) e sobre um tema "traduções" que é algo que eu tenho bastante interesse, porque já faz alguns anos que eu eu ajudo a traduzir o Debian para português.

# Participação no FOSDEM 2023

No segundo semestre de 2022 fiz uma pesquisa e vi que nunca havia acontecido uma sala dedicada a traduções, então quando a organização do FOSDEM abriu a [chamada](https://archive.fosdem.org/2023/news/2022-09-29-call_for_devrooms/) para receber proposta de salas (chamdas de DevRoom) para a edição de 2023, eu mandei uma proposta para uma sala de traduções e ela foi [aceita](https://archive.fosdem.org/2023/news/2022-11-07-accepted-developer-rooms/)!

Depois que a sala foi confirmada, o próximo passo foi eu, como coordenador da sala, divulgar a [chamada de propostas de palestras](https://lists.fosdem.org/pipermail/fosdem/2022q4/003441.html). Passei algumas semanas na expectativa de saber se receberia uma quantidade boa de propostas ou se seria um fracasso. Mas para a minha felicidade, recebi oito propostas e tive que selecionar seis para entrar entrar na [grade de programação da sala](https://archive.fosdem.org/2023/schedule/track/translations/) devido a limitação de horário.

O [FOSDEM 2023](https://archive.fosdem.org/2023) aconteceu nos dias 4 e 5 de fevereiro e a sala de traduções ficou agendada para o segundo dia a tarde.

![Fosdem 2023](/assets/img/fosdem-2023-063.jpg)

As palestras realizadas na sala foram essas abaixo, e em cada uma delas é possível assistir o vídeo da gravação.

- [Welcome to the Translations DevRoom](https://archive.fosdem.org/2023/schedule/event/translations_welcome_to_the_translations_devroom/).
    - Paulo Henrique de Lima Santana
- [Translate All The Things!](https://archive.fosdem.org/2023/schedule/event/translations_translate_all_the_things/) An Introduction to LibreTranslate.
    - Piero Toffanin
- [Bringing your project closer to users - translating libre with Weblate](https://archive.fosdem.org/2023/schedule/event/translations_bringing_your_project_closer_to_users_translating_libre_with_weblate/). News, features and plans of the project.
    - Benjamin Alan Jamie
- [20 years with Gettext](https://archive.fosdem.org/2023/schedule/event/translations_20_years_with_gettext/). Experiences from the PostgreSQL project.
    - Peter Eisentraut
- [Building an atractive way in an old infra for new translators](https://archive.fosdem.org/2023/schedule/event/translations_building_an_atractive_way_in_an_old_infra_for_new_translators/).
    - Texou
- [Managing KDE's translation project](https://archive.fosdem.org/2023/schedule/event/translations_managing_kdes_translation_project/). Are we the biggest FLOSS translation project?
    - Albert Astals Cid
- [Translating documentation with cloud tools and scripts](https://archive.fosdem.org/2023/schedule/event/translations_translating_documentation_with_cloud_tools_and_scripts/). Using cloud tools and scripts to translate, review and update documents.
    - Nilo Coutinho Menezes

E no primeiro dia do FOSDEM fiquei no estande do Debian vendendo as camisetas que eu havia levado do Brasil. O pessoal da França também estava lá vendendo outros produtos e foi legal a interação com o pessoal que visitava o estante para comprar e/ou conversar sobre o Debian.

<br />
![Fosdem 2023](/assets/img/fosdem-2023-016.jpg)
<br /><br />
![Fosdem 2023](/assets/img/fosdem-2023-019.jpg)
<br />

[Álbum completo de fotos](https://photos.app.goo.gl/fB6wH37b2pFBqiNZ9)

# Participação no FOSDEM 2024

O resultado de 2023 me motivou a propor novamente a sala de traduções quando a organização do FOSDEM 2024 abriu a [chamada para salas](https://fosdem.org/2024/news/2023-09-29-devrooms-cfp/). Fiquei na expectativa para saber se a organização do FOSDEM aceitaria uma sala sobre esse tema pelo segundo ano consecutivo e para minha felicidade, minha proposta foi [aceita](https://fosdem.org/2024/news/2023-11-08-devrooms-announced/) novamente :-)

Dessa vez recebi 11 proposta! E novamente devido a limitação de horário, tive que selecionar seis para entrar na [grade de programação da sala](https://fosdem.org/2024/schedule/track/translations/).

O [FOSDEM 2024](https://fosdem.org/2024/) aconteceu nos dias 3 e 4 de fevereiro e a sala de traduções ficou agendada para o segundo dia novamente, mas desta vez pela mamhã.

As palestras realizadas na sala foram essas abaixo, e em cada uma delas é possível assistir o vídeo da gravação.

- [Welcome to the Translations DevRoom](https://fosdem.org/2024/schedule/event/fosdem-2024-3516-welcome-to-the-translations-devroom/).
    - Paulo Henrique de Lima Santana
- [Localization of Open Source Tools into Swahili](https://fosdem.org/2024/schedule/event/fosdem-2024-2624-localization-of-open-source-tools-into-swahili/).
    - Cecilia Maundu
- [A universal data model for localizable messages](https://fosdem.org/2024/schedule/event/fosdem-2024-1759-a-universal-data-model-for-localizable-messages/).
    - Eemeli Aro
- [Happy translating! It is possible to overcome the language barrier in Open Source!](https://fosdem.org/2024/schedule/event/fosdem-2024-3236-happy-translating-it-is-possible-to-overcome-the-language-barrier-in-open-source-/)
    - Wentao Liu
- [Lessons learnt as a translation contributor the past 4 years](https://fosdem.org/2024/schedule/event/fosdem-2024-1906-lessons-learnt-as-a-translation-contributor-the-past-4-years/).
    - Tom De Moor
- [Long Term Effort to Keep Translations Up-To-Date](https://fosdem.org/2024/schedule/event/fosdem-2024-2071-long-term-effort-to-keep-translations-up-to-date/).
    - Andika Triwidada
- [Using Open Source AIs for Accessibility and Localization](https://fosdem.org/2024/schedule/event/fosdem-2024-3348-using-open-source-ais-for-accessibility-and-localization/).
    - Nevin Daniel

Dessa vez não fiquei ajudando no estande do Debian porque não consegui levar camisetas para vender. Então apenas dei uma passada por lá e conversei com algumas pessoas que estavam por lá como alguns DDs. Mas fui voluntário por algumas horas para operar a câmera de transmissão em um dos auditórios principais.

<br />
![Fosdem 2024](/assets/img/fotos-fosdem-2024-037.jpg)
<br /><br />
![Fosdem 2024](/assets/img/fotos-fosdem-2024-015.jpg)
<br />

[Álbum completo de fotos](https://photos.app.goo.gl/KrSvUFYTGkzb9kfz5)

# Balanço geral

Os temas das palestras nestes dois anos foram bastante diversificados, e todas as palestras foram realmente muito boas. Nas 12 palestras podemos ver como as traduções acontecem em alguns projetos como KDE, PostgreSQL, Debian e Mattermost. Tivemos a apresentação de ferramentas como LibreTranslate, Weblate, scripts, IA, data model. E ainda relatos dos trabalhos realizados por comunidades da África, China e Indonésia.

As salas ficaram lotadas para algumas palestras, um pouco mais vazia para outras, mas fiquei muito satisfeito com o resultado final destes dois anos.

Deixo meu agradecimento especial para o [Jonathan Carter](https://jonathancarter.org/), Líder do Debian que aprovou os meus pedidos de passagens para que eu pudesse participar dos FOSDEM 2023 e 2024. Essa ajuda foi essencial para viabilizar a minha viagem para Bruxelas porque as passagens de avião não são nada baratas.

Agradeço também a minha esposa Jandira, que tem sido minha companheira de viagem :-)

![Bruxelas](/assets/img/bruxelas-2023-187.jpg)

Como houve um aumento do número de propostas recebidas, acredito que está crescendo o interesse pela sala de traduções. Então pretendo enviar a proposta da sala para o FOSDEM 2025, e se for aceita, esperar que o(a) futura(a) Líder do Debian aprove novamente me ajudar com as passagens. Veremos.
