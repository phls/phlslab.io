---
layout: post
title: Meus perfis em mídias sociais livres
date: 2019-09-29 16:00
author: Paulo Henrique de Lima Santana
categories: geral
tags: software-livre redes-sociais
---

# Meus perfis em mídias sociais livres

*Texto atualizado em 29/09/2019 para substituir algumas ferramentas citadas
anteriormente*

## jabber

Alternativa ao GTalk, MSN.

Meu usuário: <phls@jabber.org>

## ~~quitter~~ mastodon

Alternativa ao Twitter.

Meu perfil: <https://masto.donte.com.br/@phls>

## pump.io

Alternativa ao telegram.

Meu perfil: <https://identi.ca/phls00>

## telegram

Alternativa ao Whatsapp

Meu perfil: <https://telegram.me/phls00>

## softwarelivre.org (noosfero)

Alternativa ao Facebook e ao Google Plus.

Meu perfil: <http://softwarelivre.org/profile/phls>

## diaspora

Apaguei o meu perfil nessa rede.

## hubzilla

Apaguei o meu perfil nessa rede.
