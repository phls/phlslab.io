---
layout: post
title: Eventos de Software Livre em 2015
date: 2015-12-31 16:00
author: Paulo Henrique de Lima Santana
categories: eventos
tags: eventos
---

# Eventos de Software Livre em 2015

Obs: essa lista está em constante atualização.

Acesse também a agenda de eventos de Software Livre e Código Aberto no Brasil:

<http://agenda.softwarelivre.org>

## JANEIRO

~~**HFD 2015 - Hardware Freedom Day**  
17 de janeiro de 2015  
Várias cidades  
<http://www.hfday.org>~~

## FEVEREIRO

~~**Campus Party Brasil 2015 - CPBR8**  
03 a 08 de fevereiro de 2015  
São Paulo - SP  
<http://www.campus-party.com.br>~~

~~**Open Data Day**  
21 de fevereiro de 2015  
Várias cidades  
<http://opendataday.org>~~

## MARÇO

~~**Tropical Ruby 2015**  
05 a 08 de março de 2015  
Porto de Galinhas - PE  
<http://tropicalrb.com>~~

~~**Open Education Week**  
09 a 13 de março de 2015  
Várias cidades  
<http://www.openeducationweek.org>~~

~~**Joomla Day Ribeirão Preto**  
20 e 21 de março de 2015  
Ribeirão Preto - SP  
<http://joomladaysp.com.br/2015>~~

~~**EFD 2015 - Education Freedom Day**  
21 de março de 2015  
Várias cidades  
<http://www.educationfreedomday.org>~~

~~**IV Encontro Catarinense de LibreOffice**  
21 de março de 2015  
Chapecó - SC  
<http://www.oesc-livre.org/libreoffice>~~

~~**DFD 2015 - Document Freedom Day**  
25 de março de 2015  
Várias cidades  
<http://documentfreedom.org>~~

~~**Arduino Day**  
28 de março de 2015  
Várias cidades  
<http://day.arduino.cc>~~

## ABRIL

~~**IX Solisc - Congresso Catarinense de Software Livre**  
17 e 18 de abril de 2015  
Florianópolis - SC  
<http://www.solisc.org.br>~~

~~**FLISOL 2015 - Festival Latino Americano de Instalação de Software Livre**  
25 de abril de 2015  
Várias cidades  
<http://www.flisol.info>~~

~~**Consoline - Congresso de Software Livre do Nordeste**  
25 de abril de 2015  
Recife - PE  
<http://www.softwarelivrene.org>~~

~~**10º Moodle Moot Brasil**  
29 e 30 de abril de 2015  
São Paulo - SP  
<http://www.moodlebrasil.org/course/view.php?id=5>~~

## MAIO

~~**Scratch Day**  
09 de maio de 2015  
Várias cidades  
<http://day.scratch.mit.edu>~~

~~**PentahoDay 2015**  
15 e 16 de maio de 2015  
Curitiba - PR  
<http://www.pentahobrasil.com.br/eventos/pentahoday2015>~~

~~**III Python Nordeste**  
15 e 16 de maio de 2015  
Natal - RN  
<http://2015.pythonnordeste.org>~~

~~**CFD 2015 - Culture Freedom Day**  
16 de maio de 2015  
Várias cidades  
<http://www.culturefreedomday.org>~~

~~**Exportec 2015**  
27 a 30 de maio de 2015  
João Pessoa - PB  
<http://www.expotec.org.br>~~

~~**2º Encontro Python Vale**  
30 de maio de 2015  
São José dos Campos - SP  
<http://pythonvale.com.br>~~

## JUNHO

~~**LaKademy 2015 - 3º Encontro LatinoAmericano dos Colaboradores do KDE**  
03 a 06 de Junho de 2015  
Salvador - BA  
<http://br.k​de.o​rg>~~

~~**II ESL – Encontro de Software Livre**  
13 de junho de 2015  
Gama - DF  
<https://doity.com.br/ii-encontro-software-livre>~~

~~**WordCamp Belo Horizonte**  
13 de junho de 2015  
Belo Horizonte - MG  
<http://belohorizonte.wordcamp.org/2015>~~

~~**RuPy Campinas 2015**  
20 de junho de 2015  
Campinas - SP  
<http://campinas.rupy.com.br>~~

~~**Workshop gratuito de programação para mulheres - Django Girls de Niterói**  
20 de junho de 2015  
Niterói - RJ  
<https://djangogirls.org/niteroi>~~

~~**BHack Conference**  
20 e 21 de junho de 2015  
Belo Horizonte - MG  
<http://www.bhack.com.br>~~

~~**DrupalCamp São Paulo 2015**  
26 e 27 de junho de 2015  
São Paulo - SP  
<http://saopaulo.drupalcamp.com.br/2015>~~

## JULHO

~~**16º FISL - Fórum Internacional de Software Livre**  
08 a 11 de julho de 2015  
Porto Alegre - RS**  
<http://www.fisl.org.br>~~

~~**Campus Party Recife 2015**  
23 a 26 de julho de 2015  
Recife - PE  
<http://recife.campus-party.org/2015>~~

## AGOSTO

~~**JoomlaDay São Paulo**  
01 de agosto de 2015  
São Paulo - SP  
<http://jdaysp.com.br>~~

~~**PGDay Campinas 2015**  
07 de agosto de 2015  
Campinas - SP  
<http://pgdaycampinas.com.br>~~

~~**1º Tech Day do GURU-PR**  
08 de agosto de 2015  
Curitiba - PR  
<http://www.gurupr.org/eventos/1-tech-day-do-guru-pr>~~

~~**Debian Day ou Dia do Debian**  
16 (ou 15) de agosto de 2015  
Várias cidades  
<https://wiki.debian.org/DebianDay/2015>~~

~~**Seminário de Tecnologia em Software Livre TchêLinux**  
22 de agosto de 2015  
Novo Hamburgo  - RS  
<http://nh.tchelinux.org>~~

~~**1º Seminário PHP Curitiba**  
25 de agosto de 2015  
Curitiba - PR  
<http://phpparanaconference.blogspot.com.br/2015/08/1-seminario-php-curitiba.html>~~

~~**2º Python Day Santa Maria**  
29 de agosto de 2015  
Santa Maria - RS  
<http://pysm.github.io>~~

## SETEMBRO

~~**COMSOLiD 8 - 8° Encontro da Comunidade Maracanauense de Software Livre e Inclusão Digital**  
01 a 04 de setembro de 2015  
Maracanaú - CE  
<http://www.comsolid.org>~~

~~**Joomla Day Brasil 2015**  
04 a 06 de setembro de 2015  
Brasília - DF  
<http://joomladaybrasil.org/2015>~~

~~**III Encontro Nacional de Mulheres na Tecnologia**  
11 e 12 de setembro de 2015  
Goiânia - GO  
<http://mulheresnatecnologia.org/encontro2015>~~

~~**Rails Girls Recife**  
11 e 12 de setembro de 2015  
Recife - PE  
<http://railsgirls.com/recife2015>~~

~~**Rails Girls Belo Horizonte**  
11 e 12 de setembro de 2015  
Belo Horizonte - MG  
<http://railsgirls.com/belohorizonte2015>~~

~~**Seminário de Tecnologia em Software Livre TchêLinux**  
12 de setembro de 2015  
Caxias do Sul  
<http://caxias.tchelinux.org>~~

~~**VII FTSL - Fórum de Tecnologia em Software Livre**  
16 a 18 de setembro de 2015  
Curitiba - PR  
<http://www.ftsl.org.br>~~

~~**PgDay Curitiba**  
18 de setembro de 2015  
Curitiba - PR  
<http://pgdaycuritiba.blogspot.com.br>~~

~~**RubyConf Brasil 2015**  
18 e 19 de setembro de 2015  
São Paulo - SP  
<http://www.rubyconf.com.br>~~

~~**SFD 2015 - Software Freedom Day**  
19 de setembro de 2015  
Várias cidades  
<http://softwarefreedomday.org>~~

~~**11º Fórum Espírito Livre**  
24 e 25 de setembro de 2015  
Santa Teresa - ES  
<http://forum.espiritolivre.org/11ed>~~

~~**Rails Girls Porto Alegre**  
25 e 26 de setembro de 2015  
Porto Alegre - RS  
<http://railsgirls.com/portoalegre>~~

## OUTUBRO

~~**VII Maratona de Software Livre**  
02 de outubro de 2015  
Volta Redonda - RJ  
<https://doity.com.br/vii-maratona-de-software-livre-de-volta-redonda>~~

~~**Rails Girls Maceió**  
02 e 03 de outubro de 2015  
Maceió - AL  
<http://railsgirls.com/maceio>~~

~~**Festa de 30 anos da Free Software Foudation**  
03 de outubro de 2015  
Várias cidades  
<https://libreplanet.org/wiki/FSF30_Party_Network>~~

~~**PHPest - Conferência de PHP do Nordeste**  
03 e 04 de outubro de 2015  
João Pessoa - PB  
<http://phpeste.net>~~

~~**XII Latinoware - Conferência Latino-americana de Software Livre**  
14 a 16 de outubro de 2015  
Foz do Iguaçu - PR  
<http://www.latinoware.org>~~

~~**3º Café com Software Livre**  
24 de outubro de 2015  
Indaial - SC  
<http://blusol.org>~~

~~**WordCamp Porto Alegre 2015**  
31 de outubro de 2015  
Porto Alegre - RS  
<https://portoalegre.wordcamp.org/2015>~~

## NOVEMBRO

~~**Rails Girls Salvador - 3ª edição**  
06 e 07 de novembro de 2015  
Salvador - BA  
<http://railsgirls.com/salvador201511>~~

~~**III Encontro Nacional do LibreOffice**  
06 e 07 de novembro de 2015  
São José - SC  
<http://encontro.libreoffice.org>~~

~~**DrupalDay Porto Alegre 2015**  
07 de novembro de 2015  
Porto Alegre - RS  
<http://www.drupaldaypoa.com.br>~~

~~**Workshop gratuito de programação para mulheres - Django Girls de São José dos Campos**  
07 e 08 de novembro de 2015  
São José dos Campos - SP  
<https://djangogirls.org/saojosedoscampos>~~

~~**PythonBrasil[11]**  
07 a 11 de novembro de 2015  
São José dos Campos - SP  
<http://pythonbrasil.github.io/pythonbrasil11-site>~~

~~**Semana do Linux - 1° Congresso online e gratuito de GNU/LINUX e tecnologias livres**  
09 a 14 de novembro de 2015  
<http://www.semanadolinux.com.br>~~

~~**Workshop gratuito de programação para mulheres - Django Girls de Curitiba**  
14 de novembro de 2015  
Curitiba - PR  
<https://djangogirls.org/curitiba>~~

~~**Workshop gratuito de programação para mulheres - Django Girls de Maringá**  
14 de novembro de 2015  
Maringá - PR  
<https://djangogirls.org/maringa>~~

~~**PHP com Rapadura in Pentecoste**  
14 de novembro de 2015  
Pentecoste - CE  
<http://www.eventick.com.br/php-com-rapadura-in-pentecoste>~~

~~**Fórum de Software Livre de São Paulo**  
14 de novembro de 2015  
São Paulo - SP  
<https://www.sympla.com.br/forum-de-software-livre-de-sao-paulo__48228>~~

~~**Seminário de Tecnologia em Software Livre TchêLinux**  
14 de novembro de 2015  
Pelotas - RS  
<http://tchelinuxpelotas.com>~~

~~**PGBR 2015 - Conferência PostgreSQL**  
18 a 20 de novembro de 2015  
Porto Alegre - RS  
<http://pgbr.postgresql.org.br/2015>~~

~~**Consoline - Congresso de Software Livre do Nordeste**  
21 de novembro de 2015  
Recife - PE  
<http://www.softwarelivrene.org>~~

~~**Rails Girls São Paulo**  
27 e 28 de novembro de 2015  
São Paulo - SP  
<http://railsgirls.com/saopaulo2015>~~

~~**WordCamp São Paulo**  
28 de novembro de 2015  
São Paulo - SP  
<https://saopaulo.wordcamp.org/2015>~~

~~**Seminário de Tecnologia em Software Livre TchêLinux**  
28 de novembro de 2015  
Porto Alegre - RS  
<http://poa.tchelinux.org>~~

## DEZEMBRO

~~**10ª PHP Conference Brasil**  
02 e 06 de dezembro de 2015  
Osasco - SP  
<http://www.phpconference.com.br>~~

~~**XII FGSL - Fórum Goiano de Software Livre**  
04 e 05 de dezembro de 2015  
Goiânia - GO  
<http://fgsl.net>~~

~~**3° Teresina Hack Day**  
05 de dezembro de 2015  
Teresina - PI  
<http://teresinahc.org/hackday>~~

~~**Rails Girls Rio de Janeiro**  
12 de dezembro de 2015  
Rio de Janeiro - RJ  
<http://railsgirls.com/riodejaneiro>~~


## Anos anteriores

[2014](http://softwarelivre.org/blog/eventos-de-software-livre-em-2014)  
[2013](http://softwarelivre.org/blog/eventos-de-software-livre-em-2013)  
[2012](http://softwarelivre.org/blog/eventos-de-software-livre-em-2012)  
[2011](http://softwarelivre.org/blog/eventos-de-software-livre-em-2011)  
