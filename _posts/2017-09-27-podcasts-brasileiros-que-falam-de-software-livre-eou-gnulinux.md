---
layout: post
title: Podcasts brasileiros que falam de Software Livre e/ou GNU/Linux
date: 2017-09-27 16:00
author: Paulo Henrique de Lima Santana
categories: geral
tags: software-livre podcast
---

# Podcasts brasileiros que falam de Software Livre e/ou GNU/Linux

![Podcasts](/assets/img/podcast-header.jpg){:width="600px"}

Fiz uma relação de podcasts que falam de alguma forma de Software Livre e/ou
GNU/Linux. Alguns podcasts abordam esses temas de forma mais recorrente, e outros
apenas em alguns epsódios. Normalmente as discussões são voltadas para questões
mais técnicas do que filosóficas/sociais.

Eu participo da gravalçao do Papo Livre :-)

## Papo Livre

![Logo Papo Livre](/assets/img/gnu-papo-livre.jpg){:width="100px"}

O **Papo Livre** é o mais novo podcast brasileiro sobre essa temática de Software
Livre, Código Aberto e GNU/Linux.

Texto do site: O Papo Livre é um podcast que tem como missão discutir Software
Livre em todos os seus aspectos. Desenvolvimento, comunicação, filosofia,
comunidade, e tudo mais, em conversas informais com opiniões claras.

O Papo Livre começou a ser gravado em maio de 2017 e neste momento existem 9
epsódios disponíveis.

O Papo Livre é uma iniciativa do pessoal pessoal abaixo (eu inclusive) e
eventualmente tem a participação de alguns convidados:

 * Antonio Terceiro é baiano de Salvador, e mora em Curitiba. Engenheiro de
Software da Linaro Limited. Desenvolvedor de Software Livre, membro do projeto
Debian.
 * Paulo Santana mora em Curitiba e trabalha com administração de sistemas
GNU/Linux, organiza uma série de eventos de software livre. Colabora com o
projeto Debian, atualmente como Debian Maintainer.
 * Thiago Mendonça mora num buraco no estado do Rio de Janeiro chamado Conceição
de Macabu e gerencia o departamento de TI de uma fábrica de móveis. Participa de
várias comunidades de software livre e tem participado de diversos eventos de
software livre.

Site: <https://papolivre.org>

## Politicast

Texto do site: **Politicast** surgiu como uma iniciativa de Cárlisson Galdino e
Sivaldo Paulino, com o objetivo de levar a discussão sobre política até você.
Política é uma atividade essencial na vida em sociedade. Em 2016, o podcast está
sendo produzido por Cárlisson Galdino e Diógenes Laertius. Neste momento está
disponível o epsódio 90.

Alguns epsódios são sobre Software Livre como esses:

 * <http://politicast.info/2016/08/07/politicast-51-software-livre-1-de-3>
 * <http://politicast.info/2016/08/14/politicast-52-software-livre-2-de-3>
 * <http://politicast.info/2016/08/21/politicast-53-software-livre-3-de-3>

E sobre redes sociais lives como esses:

 * <http://politicast.info/2016/01/24/politicast-33-redes-sociais-1-de-3>
 * <http://politicast.info/2016/01/31/politicast-34-redes-sociais-2-de-3>
 * <http://politicast.info/2016/02/06/politicast-35-redes-sociais-3-de-3>

Site: <http://politicast.info>

## Opencast

Texto do site: o **Opencast** terá como tema principal o Software Livre. O
objetivo do Opencast será de trazer informações sobre o mundo do software livre,
sendo boas ou más notícias, sempre faremos nossos comentários pessoais e
técnicos sobre os assuntos. Ao contrário do que fazemos no grupo Caudilho Livre,
aqui comentaremos as intervenções políticas que envolvam software livre tanto no
Brasil quanto no mundo. Não criticaremos partidos ou pessoas, apenas daremos
nossa opinião sobre as decisões tomadas e os rumos aos quais tais decisões podem
levar o software livre.

O Opencast é gravado por um grupo coordenado pelo Ivan desde desde 2011. Nesse
momento está disponível o epsódio 81.

Site: <http://tecnologiaaberta.com.br/category/opencast>

## Hack ‘n’ Cast

Texto do site: o **Hack 'n' Cast** é um podcast livre e aberto, produzido
colaborativamente e tem como objetivo ser da comunidade, pela comunidade e para
a comunidade. Apesar de ser feito por várias pessoas, seu desenvolvimento é
coordenado por 17 pessoas.

O principal coordenador do Hack 'n' Cast é o Magnun - criador, desenvolvedor e
autor do blog
[Mind Bending<i class="fa fa-external-link"></i>](http://mindbending.org/pt/category/hack-n-cast).
Existe desde 2014 e nesse momento está disponível o epsódio 1.8. Os epsódios
tem temas diversos, entre eles GNU/Linux, Cultura Hacker e Python.

Site: <http://hackncast.org>

## Castálio Podcast

Texto do site: com o objetivo de entrevistar e ao mesmo tempo apresentar pessoas
e projetos que sejam fonte de inspiração para os ouvintes, este podcast trará de
15 em 15 dias uma nova vítima, err figura Brasileira que será sabatinada de todos
os ângulos para o seu deleite! O Castálio Podcast é fruto da imaginação e
curiosidade incansável de Og Maciel, um brasileiro que desde 1991 mora nos
Estados Unidos mas que nunca deixou de prestigiar suas raizes! Caso você tenha
alguma sugestão para o nosso próximo convidado, nos envie sua idéia para o nosso
e-mail.

O Castálio Podcast é gravado desde 2011 e nesse momento está disponivel o
epsódio 119. Alguns dos entrevistados são membros conhecidos da comunidade
brasileira de Software Livre e GNU/Linux, então esses temas acabam sendo
recorrentes.

Site: <http://castalio.info>

##  DatabaseCast

Texto do site: o **DatabaseCast** é o primeiro podcast brasileiro sobre banco de
dados. Mauro Pichiliani e Wagner Crivelini falam sobre banco de dados,
programação, administração, dados, carreira, mercado, SQL Server, Oracle, MySQL,
PostgreSQL, NoSQL, DB2, Informix, Sybase e diversos outros assuntos da área de
tecnologia voltados para o desenvolvimento de aplicações. 

Focado em banco de dados, o DatabaseCast existe desde 2010 e tem bastante
epsódios, vários deles falando sobre bancos de dados em software livre.

Site: <http://imasters.com.br/perfil/databasecast>

## DioCast

Texto do site: um programa muito bacana sobre Linux e Tecnologia para você ouvir
enquanto faz as suas atividades.

O **DioCast** é gravado desde 2014 pelo Dionatan Simioni, tem poucos epsódios
porque pelo que deu para perceber, ele passou a fazer vídeos para o seu canal no
youtube chamado DioLinux.

Site: <http://www.diolinux.com.br/search/label/DioCast>

Canal no youtube: <https://www.youtube.com/Diolinux>

## GuanaCast

O **GuanaCast** é (ou era) gravado pelo Gustavo Guanabara sobre temas diversos,
mas existem alguns epsódios que ele fala de temas relacionados a Software Livre
e/ou Linux.

 * [Software Público<i class="fa fa-external-link"></i>](http://www.guanabara.info/2011/04/software-publico-podcast/)
 * [Empreendedorismo com Software Livre<i class="fa fa-external-link"></i>](http://www.guanabara.info/2009/11/guanacast-72-empreendedorismo-com-software-livre/)
 * [Software Livre e Código Aberto<i class="fa fa-external-link"></i>](http://www.guanabara.info/2009/08/guanacast-68-software-livre-e-codigo-aberto/)
 * [Saiba tudo sobre CMS - Parte I<i class="fa fa-external-link"></i>](http://www.guanabara.info/2009/06/guanacast-66a-saiba-tudo-sobre-cms-parte-i/)
 * [Saiba tudo sobre CMS - Parte II<i class="fa fa-external-link"></i>](http://www.guanabara.info/2009/07/guanacast-66b-saiba-tudo-sobre-cms-parte-ii/)
 * [Latinoware 2008<i class="fa fa-external-link"></i>](http://www.guanabara.info/2008/11/guanacast-44-latinoware-2008-oah/)

Site: <http://www.guanabara.info>

## Nerdcast

O **Nerdcast** é o podcast mais conhecido no Brasil, gravado pelo pessal do
Jovem Nerd. Eles gravam um epsódio sobre a profissão de programador e em algum
momento falaram sobre Software Livre.

 * [Profissão: programador<i class="fa fa-external-link"></i>](http://jovemnerd.com.br/nerdcast/nerdcast-211-profissao-programador/)

Site: <http://jovemnerd.com.br>

## Free as in Freedom

Vale a citação do **Free as in Freedom**, um podcast em inglês gravado desde 2010
por [Bradley M. Kuhn<i class="fa fa-external-link"></i>](http://ebb.org/bkuhn) e [Karen Sandler<i class="fa fa-external-link"></i>](http://gnomg.org/),
ambos diretores da [Software Freedom Conservancy<i class="fa fa-external-link"></i>](https://sfconservancy.org/) e
importantes ativistas da comunidade Software Livre.

Mais informações
[aqui<i class="fa fa-external-link"></i>](http://softwarelivre.org/portal/noticias/conheca-o-podcast-free-as-in-freedom-apresentado-por-bradley-kuhn-e-karen-sandler).

Site: <http://faif.us>