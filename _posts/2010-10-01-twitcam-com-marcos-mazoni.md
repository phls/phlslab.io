---
layout: post
title: Twitcam com Marcos Mazoni
date: 2010-10-01 16:00
author: Paulo Henrique de Lima Santana
categories: geral
tags: software-livre
---

# Twitcam com Marcos Mazoni

Twitcam com o Marcos Mazoni neste sábado (02/10) às 11:00h.

Temas principais:

 * Plano Nacional de Banda Larga;
 * Software Livre na iniciativa privada e as suas bases estatais;
 * IV FTSL Fórum de Tecnologia em Software Livre de Curitiba.

Considerado um dos precursores do Software Livre no Brasil, Marcos Mazoni é o
atual Presidente da a maior empresa de tecnologia da informação da América
Latina, o Serviço Federal de Processamento de Dados (Serpro). Mazoni preside
ainda o Comitê de Implantação de Software Livre do Governo Federal.

É formado em Administração de Empresas, pós- graduado em Tecnologia da Informação
pela Fundação Getúlio Vargas e em Gestão Empresarial pela Universidade Federal
do Rio Grande do Sul. Comandava, desde 2003, a empresa estadual de informática
do Paraná (Celepar). Antes de atuar naquela empresa, Marcos Mazoni trabalhou na
presidência da Companhia de Processamento de Dados do Rio Grande do Sul
(Procergs), de 1999 a 2002. Atuou, durante 20 anos, na Companhia Riograndense de
Telecomunicações.

Participe e envie suas perguntas e comentários:

<http://twitter.com/marcosmazoni>