---
layout: post
title: My free software activities in February 2020
date: 2020-03-29 10:00
author: Paulo Henrique de Lima Santana
categories: geral
tags: debian english freesoftware planetdebian bruxelas bélgica fosdem flisol
---

# My free software activities in february 2020

March is ending but I finally wrote my monthly report about activities in Debian
and Free Software in general for February.

As I already wrote [here](/bits-from-minidebcamp-brussels-and-fosdem-2020), I
attended to FOSDEM 2020 on February 1st and 2nd in Brussels. It was a amazing
experience.

After my return to Curitiba, I felt my energies renewed to start new challenges.

## MiniDebConf Maceió 2020

I continued helping to organize MiniDebConf and I got positive answers from
[4Linux](https://www.4linux.com.br/) and [Globo.com](http://globo.com) and they
are sponsorsing the event.

## FLISOL 2020

I started to talk with Maristela from [IEP - Instituto de Engenharia do
Paraná](http://iep.org.br) and after some messages and I joined a meeting with
her and other members of Câmara Técnica de Eletrônica, Computação e Ciências de
Dados.

I explained about [FLISOL in Curitiba](flisol.curitibalivre.org.br/) to them and
they agreed to host the event at IEP. I asked to use three spaces: Auditorium
for FLISOL talks, Salão Nobre for meetups from
[WordPress](https://www.meetup.com/pt-BR/wpcuritiba/) and
[PostgreSQL](https://www.meetup.com/pt-BR/PostgreSQL-Curitiba/) Communities, and
the hall for Install Fest.

Besides FLISOL, they would like to host other events and meetups from
Communities in Curitiba as Python, PHP, and so on. At least one per month.

I helped to schedule a PHP Paraná Community
[meetup](https://www.meetup.com/pt-BR/PHP-PR/events/269030440/) on March. 

## New job

Since 17th I started to work at [Rentcars](https://www.rentcars.com) as
Infrastructure Analyst. I'm very happy to work there because we use a lot of
FLOSS and with nice people.

Ubuntu LTS is the approved OS for desktops but I could install Debian on my
laptop :-)

## Misc

I signed pgp keys from friends I met in Brussels and I had my pgp key signed by
them. 

Finally my MR to the [DebConf20](https://debconf20.debconf.org/) website fixing
some texts was accepted.

I have watched [vídeos](https://marcin.juszkiewicz.com.pl/fosdem/) from FOSDEM
2020. Until now, I saw these great talks:

* Growing Sustainable Contributions Through Ambassador Networks
* Building Ethical Software Under Capitalism
* Cognitive biases, blindspots and inclusion
* Building a thriving community in company-led open source projects
* Building Community for your Company’s OSS Projects
* The Ethics of Open Source
* Be The Leader You Need in Open Source
* The next generation of contributors is not on IRC
* Open Source Won, but Software Freedom Hasn't Yet
* Open Source Under Attack
* Lessons Learned from Cultivating Open Source Projects and Communities

That's all folks!
