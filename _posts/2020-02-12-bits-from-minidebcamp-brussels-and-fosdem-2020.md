---
layout: post
title: Bits from MiniDebCamp Brussels and FOSDEM 2020
date: 2020-02-12 10:00
author: Paulo Henrique de Lima Santana
categories: geral
tags: debian english freesoftware planetdebian bruxelas bélgica fosdem
---

# Bits from MiniDebCamp Brussels and FOSDEM 2020

I traveled to Brussels from January 28th to February 6th to join
[MiniDebCamp](https://wiki.debian.org/DebianEvents/be/2020/MiniDebCamp) and
[FOSDEM 2020](https://fosdem.org/2020/). It was my second trip to Brussels
because I was there in 2019 to join [Video Team
Sprint](https://wiki.debian.org/Sprints/2019/DebConf%20Video%20team%20sprint%20%40%20FOSDEM%202019)
and [FOSDEM](https://fosdem.org/2019)

MiniDebCamp took place at [Hackerspace Brussels (HSBXL)](https://hsbxl.be) for 3
days (January 29-31). My initial idea was travel on 27th and arrive in Brussels
on 28th to rest and go to MiniDebCamp on the first day, but I had buy a ticket
to leave Brazil on 28th because it was cheaper.

## Trip from Curitiba to Brussels

I left Curitiba on 28th at 13:20 and I arrived in São Paulo at 14:30. The
flight from São Paulo to Munich departured at 18h and after 12 hours I arrived
there at 10h (local time). The flight was 30 minutes late because we had to
wait airport staff remove ice on the ground. I was worried because my flight to
Brussels would departure at 10:25 and I had to get through by immigration yet.

![Bruxelas](/assets/img/bruxelas-2020-42.jpg)

After walked a lot, I arrrived at immigration desk (there wasn't line), I got my
passaport stamp, walked a lot again, took a train, I arrived in my gate and the
flight was late too. So, everything was going well. I departured Munich at 10:40
and I arrived in Brussels on 29th at 12h.

I went from airport to the [Hostel
Galia](https://www.booking.com/hotel/be/hostel-galia.html) by bus, by train and
by other bus to check-in and to leave my luggage. On the way I had lunch at
“Station Brussel Noord” because I was really hungry, and I arrived at hostel at
15h.

My reservation was on a coletive bedroom, and when I arrived there, I meet
Marcos, a brazilian guy from Brasília and he was there to join a internationl
Magic card [competion](https://www.cfbevents.com/events/MTGBrussels). He was in
Brussels for the first time and he was a little lost about what he could do in
the city. I invited him to go to downtown to looking for a cellphone store
because we needed to buy sim-cards. I wanted to buy from Base, and hostel
frontdesk people said to us to go to the store at Rue Neuve. I showed
Grand-Place to Marcos and after we bought sim-cards, we went to Primark because
he needed to buy a towel. It was night and we decided to buy food to have dinner
at Hostel. I gave up to go to HSBXL because I was tired and I thought it was not
a good idea to go there for the first time at night.

## MiniDebCamp day 1

On Thursday (30th) morning I went to HSBXL. I walked from the hostel to “Gare du
Midi”, and after walk from on side to other, I finally could find the bus stop.
I got off the bus at the fourth stop in front of the hackerspace building. It
was a little hard to find the right entrance, but I got it. I arrived at HSBXL
room, talked to other DDs there and I could find a empty table to put my laptop.
Other DDs were arriving during all day.

![Bruxelas](/assets/img/bruxelas-2020-43.jpg)

I read and answered e-mails and went out to walking in Anderlecht to meet the
city and to looking for a place to have lunch because I didn't want eat sandwich
at restaurant on the building. I stoped at Lidl and Aldi stores to buy some food
to eat later, and I stoped in a turkish restaurant to have lunch, and the food
was very good. After that, I decided to walk a little more to visit the
Jean-Claude Van Damme
[statue](https://visit.brussels/en/place/Jean-Claude-Van-Damme-statue) to take
some photos :-)

![Bruxelas](/assets/img/bruxelas-2020-5.jpg)

![Bruxelas](/assets/img/bruxelas-2020-6.jpg)

![Bruxelas](/assets/img/bruxelas-2020-3.jpg)

![Bruxelas](/assets/img/bruxelas-2020-4.jpg)

Backing to HSBXL my mostly interest at MiniDebCamp was to join the [DebConf Vídeo
Team
sprint](https://wiki.debian.org/Sprints/2020/DebConfVideoTeamSprintAtFOSDEM2020)
to learn how to setup a voctomix and gateway machine to be used in MiniDebConf
Maceió 2020. I was asking some questions to Nicolas about that and he suggested
I make a new instalation using the Video Team machine and Buster. 

![Bruxelas](/assets/img/bruxelas-2020-12.jpg)

I installed Buster and using [USB
installer](https://debconf-video-team.pages.debian.net/ansible/usb_install.html)
and [ansible](https://salsa.debian.org/debconf-video-team/ansible) playbooks it
was possible setup the machine as Voctotest. I already had done this setup at
home using a simple machine without a Blackmagic card and a camera. From that
point, I didn't know what to do. So, Nicolas come and started to setup the
machine first as Voctomix, and after as Gateway. I was watching and learning.
After a while, everything worked perfect with a camera.

It was night and the group ordered some pizzas to eat with beers sold by HSBXL. I was
celebreting too because during the day I received messages and a call from
Rentcars because I was hired by them! Before travel, I went to a interview at
Rentcars on the morning and I got a positive answer when I was in Brussels.

![Bruxelas](/assets/img/bruxelas-2020-44.jpg)

Before I left the hackerspace, I received doors codes to open HSBXL next day
early. Some days before MiniDebCamp, Holger had asked if someone could open the
room friday morning and I answered him I could. I left at 22h and back to the
hostel to sleep.

## MiniDebCamp day 2

On friday I arrived at HSBXL at 9h and opened the room and I took some photos
with empty space. It is amazing how we can use spaces like that in Europe. Last
year I was in [MiniDebConf
Hamburg](https://wiki.debian.org/DebianEvents/de/2019/MiniDebConfHamburg) at
[Dock Europe](https://www.dock-europe.net/). I miss this kind of building and
hackerspace in Curitiba.

![Bruxelas](/assets/img/bruxelas-2020-45.jpg)

![Bruxelas](/assets/img/bruxelas-2020-46.jpg)

I installed and setup the Video Team machine again, but this time, I was alone
following what Nicolas did before. And everything worked perfectly again.
Nicolas asked me to create a new ansible playbook joining voctomix and gateway
to make instalation easier, send it as a MR, and test it.

I went out to have lunch in the same restaurant the day before and I discoveried
there was a Leonidas factory outlet in front of HSBXL, meaning I could buy belgium
chocolates cheaper. I went there and I bought a box with 1,5kg of chocolates.

![Bruxelas](/assets/img/bruxelas-2020-47.jpg)

When I come back to HSBXL, I started to test the new ansible playbook. The test was
taking longer than I expected and on the end of the day, Nicolas needed to take
away the equipments. It was really great make this hands-on with real equipments
used by Video Team. I learned a lot! 

To celebrate the MiniDebCamp ending, we had free beer sponsored! I have to say I
drank to much and it was complicated arrived at hostel that night :-)

![Bruxelas](/assets/img/bruxelas-2020-11.jpg)

![Bruxelas](/assets/img/bruxelas-2020-7.jpg)
![Bruxelas](/assets/img/bruxelas-2020-8.jpg)

![Bruxelas](/assets/img/bruxelas-2020-9.jpg)
![Bruxelas](/assets/img/bruxelas-2020-10.jpg)

A complete report from DebConf Video Team can be read
[here](https://blog.olasd.eu/2020/02/minidebcamp-fosdem-2020/).

Many thanks to Martin Michlmayr for helping with flight tickets, to Nicolas
Dandrimont for teaching me Video Team stuff, to Kyle Robbertze for setting up
the Video Sprint, to Holger Levsen for organizing MiniDebCamp, and to HSBXL
people for receiving us there.

## FOSDEM day 1

FOSDEM 2020 took place at [ULB](https://www.ulb.be) on February 1st and 2nd. On
the first day I took a train and I listened a group of brazilians talking in
portuguese and they were going to FOSDEM too. I arrived there around 9:30 and I
went to Debian [booth](https://wiki.debian.org/DebianEvents/be/2020/FOSDEM)
because I was volunteer to help and I was taking t-shirts from Brazil to sell.
It was a madness with people buying Debian stuff.

![Bruxelas](/assets/img/bruxelas-2020-22.jpg)

![Bruxelas](/assets/img/bruxelas-2020-33.jpg)

![Bruxelas](/assets/img/bruxelas-2020-48.jpg)

After while I had to leave the booth because I was volunteer to film the talks
at Janson auditorium from 11h to 13h. I had done this job last year I decided to
do it again because It is a way to help the event, and they gave me a t-shirt
and a free meal ticket that I changed for two sandwiches :-)

![Bruxelas](/assets/img/bruxelas-2020-14.jpg)

![Bruxelas](/assets/img/bruxelas-2020-15.jpg)

![Bruxelas](/assets/img/bruxelas-2020-19.jpg)

After lunch, I walked around the booths, got some stickers, talked with peolple,
drank some beers from OpenSuse booth, until the end of the day. I left FOSDEM
and went to hostel to leave my bag, and I went to the Debian dinner
organized by Marco d'Itri at [Chezleon](http://www.chezleon.be/en/).  

![Bruxelas](/assets/img/bruxelas-2020-16.jpg)

The dinner was great, with 25 very nice Debian people. After the dinner, we ate
waflles and some of us went to Delirium but I decided to go to the hostel to
sleep.

## FOSDEM day 2

On the second and last day I arrived around 9h, spent some time at Debian booth
and I went to Janson auditorium to help again from 10h to 13h.

![Bruxelas](/assets/img/bruxelas-2020-18.jpg)

I got the free meal ticket and after lunch, I walked around, visited booths, and I
went to [Community
devroom](https://fosdem.org/2020/schedule/track/community_devroom/) to watch
talks. The first was ["Recognising
Burnout"](https://fosdem.org/2020/schedule/event/burnout/) by Andrew Hutchings
and listening him I believe I had bournet symptoms organizing DebConf19. The
second was ["How Does Innersource Impact on the Future of Upstream
Contributions?"](https://fosdem.org/2020/schedule/event/innersourceupstream/) by
Bradley Kuhn. Both talks were great.

After the end of FOSDEM, we went in a group to have dinner at a restaurant near
from ULB. We spent a great time together. After the dinnner we took the same
train and we did a group photo.

![Bruxelas](/assets/img/bruxelas-2020-21.jpg)

## Two days to join Brussels

With the end of MiniDebcamp and FOSDEM I had Monday and Tuesday free before
returning to Brazil on Wednesday. I wanted to join [Config Management
Camp](https://cfp.cfgmgmtcamp.be/2020/) in Ghent, but I decided to stay in
Brussels to visit some places. I visited:

* [House of European
History](https://historia-europa.ep.eu/en/welcome-house-european-history)

* [Docxs Bruxsel](https://docksbruxsel.be)

![Bruxelas](/assets/img/bruxelas-2020-23.jpg)

* [Master Frites](https://www.facebook.com/masterfritesbrussels/) to have lunch

![Bruxelas](/assets/img/bruxelas-2020-24.jpg)

* Carrefour - to buy beers to bring to Brazil :-)

![Bruxelas](/assets/img/bruxelas-2020-25.jpg)

* Parc du Cinquantenaire and [Royal Military Museum (War Heritage
Institute)](http://www.klm-mra.be/D7t/en)

![Bruxelas](/assets/img/bruxelas-2020-26.jpg)

![Bruxelas](/assets/img/bruxelas-2020-27.jpg)

![Bruxelas](/assets/img/bruxelas-2020-29.jpg)

![Bruxelas](/assets/img/bruxelas-2020-30.jpg)

## Last day and returning to Brazil

On Wednesday (5th) I woke up early to finish packing and do my check-out. I left
the hostel and took a bus, a train and other bus to Brussels Airport. My flight
departured at 15:05 to Frankfurt arriving there at 15:55. I thought to visit the
city because I had to wait for 6 hours and I read it was possible to looking
around with this time. But I was very tired and I decided to stay at airport.

I walked to my gate, got through by immigration to get my passaport stamp, and
waited until 22:05 when my flight departured to São Paulo. After 12 hours
flying, I arrived in São Paulo at 6h (local time). In São Paulo when we arrive
from international flight, we must to take all luggages, and get through
customs. After I left my luggage with local airplane company, I went to the gate
to wait my flight to Curitiba. 

The flight should departure at 8:30 but it was 20 minutes late. So I arrived in
Curitiba 10h, took a uber and finally I was at home.

## Last words

I wrote a diary (in portuguese) telling each of all my days in Brussels. It can
be read starting
[here](http://phls.com.br/vou-para-bruxelas-participar-da-minidebconf-e-do-fosdem-2020).

All my photos are
[here](https://www.flickr.com/photos/phls/albums/72157713036743007)

Many thanks to Debian for sponsoring my trip to Brussels, and to DPL Sam Hartman
for approving it. It's a unique opportunity to go to Europe to meet and to work
with a lot of DDs, and participate in a very important worldwide free software
event.
