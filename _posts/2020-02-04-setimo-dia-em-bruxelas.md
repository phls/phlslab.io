---
layout: post
title: Sétimo dia em Bruxelas
date: 2020-02-04 23:59
author: Paulo Henrique de Lima Santana
categories: geral
tags: bruxelas bélgica debian fosdem
---

# Sétimo dia em Bruxelas

Veja o texto anterior: [Sexto dia em Bruxelas](/sexto-dia-em-bruxelas).

E lá fui eu de novo para o [Royal Military Museum (War Heritage
Institute)](http://www.klm-mra.be/D7t/en) pela manhã.

![Bruxelas](/assets/img/bruxelas-2020-26.jpg)

Cheguei lá por volta das 11:20, paguei os € 10,00 de entrada e foi incrível!
Fiquei 3 horas no museu. Havia uma nova exposição chamada
[War-Occupation-Liberation](https://www.klm-mra.be/D7t/en/content/war-occupation-liberation-new-permanent-exhibition-royal-military-museum)
que foca na 2a guerra.

![Bruxelas](/assets/img/bruxelas-2020-27.jpg)

Andei na área que eu não havia visitado ano passado que mostra armaduras e armas
bem antigas. E ainda uma área sobre a época do Napoleão.

![Bruxelas](/assets/img/bruxelas-2020-28.jpg)

E fui no topo do prédio para uma área panorâmica que dá pra ver a cidade. Mas
estava chuviscando, ventando e muito frio, por isso não deu pra ficar muito
tempo lá em cima. Fiz fotos dos dois lados da Praça do Cinquentenário.

![Bruxelas](/assets/img/bruxelas-2020-29.jpg)

![Bruxelas](/assets/img/bruxelas-2020-30.jpg)

O legal de visitar um museu da guerra é ver o quanto o ser humano é, desde
sempre, transtornado o suficiente para criar tantas formas de matar e destruir.
Seja com espadas, lanças, revolvéres, espingardas, metralhadoras, aviões,
tanques, bombas, mísseis, balas, canhões, enfim, a gente sempre dá um jeito de
evoluir e fazer pior.

![Bruxelas](/assets/img/bruxelas-2020-31.jpg)

A entrada para cada um dos três muses (Royal Military, Autoworld e Art &
History) custa € 10,00. Mas tem uma promoção por € 22,00 que você pode visitar
os três museus. Esse ingresso é válido por três meses, veja no final [dessa
página](http://www.klm-mra.be/D7t/en/content/fees)

Saí do museu às 14h com fome. Lembrei que no ano passado havíamos almoçado em um
restaurante italiano ali perto, do outro lado da Praça do Cinquentenário perto
no prédio da União Europeia. Então fui ver se ainda estava aberto mas não
estava. Então vi um restaurante com uma placa escrito € 17,50 com buffet livre e
pensei "é nesse que eu vou!". Nem vi qual era o tipo da comida e entrei. O
buffet era de comida [libanesa](https://www.lebanonhouse.be/) e parecia muito
boa com arroz, carne, frango, salada, etc. A comida estava mesmo muito boa e fiz
valer os 17,50 :-)

Saí do restaurante e fui andando para a [House of European
History](https://historia-europa.ep.eu/en/welcome-house-european-history) que
ficava alí perto. Durante o FOSDEM o Samuel havia me dito que tinha visitado um
lugar legal da Europa e feito um tour lá dentro, eu me confundi achando que essa
Casa da História Europeia, mas depois ele me disse que foi no Parlamento
Europeu, que é outro lugar. Mas tudo bem, o passeio foi muito legal.

Não é cobrado ingresso, na entrada você recebe um tablet que você pode escolher
a língua, incluíndo portuquês de Portugal. Então você vai do primeiro até o
quinto andar olhando a exposição. Em cada andar existem as descrições escritas e
faladas no tablet dos objetos e imagens que você está vendo naquele momento.
Começa pela pré-história da Europa, passa pela 1a e pela 2a guerra, pelo
pós-guerra, pela guerra fria, pelo início da criação da União Europeia, e a
situação atual. Me chamou a atenção o destaque, geralmente negativo, que eles
dão para o comunismo na Rússia antes, durante e depois da 2a guerra.

Pensando no que eu ví no Royal Military Museum e na House of European History,
dá pra ver que a Europa é marcada pelos conflitos e guerras, com destaque
principalmente para a 2a guerra.

Fiquei mais de uma hora por lá e saí quando já estava fechando às 18h. Fui de
metrô para o centro fazer um último passeio. Fui dena loja da Media Markt que
fica no centro, e ela é maior do que a que fica no Docxs. Comprei um fone da
Sony por € 10,00 que tava na promoção, o modelo mais simples. Passei no
Carrefour pra comprar croissant pra jantar e fui pro hostel. De novo eu tava
muito cansado de andar e ficar em pé.

Cheguei no hostel por volta das 20h e fui arrumar a mala. No outro dia eu queria
acordar mais tarde e descansar o máximo possível porque o checkout era às 10h e
meu vôo às 15:05. Uma blusa que eu havia deixado no armário havia sumido. No
quarto já tinha mais três pessoas novas, e um cara que dormia lá e que eu tinha visto
que também trabalhava no hostel. Ele tava dormindo nesse momento. Fui na
recepção perguntar se a pessoa que limpava o quarto não teria deixado a blusa
lá, e por incrível que pareça, o cara não fava inglês, apenas francês com
sotaque russo. Achei um absurdo o que cara na recepção não falar inglês e fui
ficando irritado. Ele me respondia em francês como se eu tivesse entendendo, até
que ele chamou alguém pra ajudar. Expliquei a situação e o cara me disse que
existem os armários para os hóspedes trancarem com cadeado justamente pra evitar
o roubo das coisas.

Voltei pro quarto e me convenci que tinham levado a blusa. Depois de uns minutos
arrumando a mala, o cara que tava dormindo e que travalhava no hostel acordou e
quando olho, ele tava usando a minha blusa! Falei pra ele que a blusa era minha
e ele disse que se confundiu porque tinha uma igual. Falei ok, e fique esperando
ele me devolver para eu guardar. Então, antes de tirar a blusa, ele perguntou se
eu queria mesmo a blusa, e eu disse que sim. Quando ele tirou, a blusa tava
fedendo a suvaco, só peguei e guardei dentro de uma sacola.

Alias, isso é uma coisa bastante comum por aqui: pessoas fedendo a suvaco. Não
sei o que acontece, se o pessoal realmente não gosta de tomar banho, ou se não
usam desodorante. Mas é normal sentir esse cheiro no trem, nas lojas, etc.

Outra coisa é que em Bruxelas as pessoas falam holandês e principalmente
francês. Em muitas ocasiões foi difícil me comunicar em inglês. E percebi que os
imigrantes turcos, libaneses e de outros lugares daquela região falam apenas
francês e nada de inglês. Essa falta de entendimento em inglês aconteceu muito
nos restaurantes, mercadinhos e lanchonetes que fui.

Voltando para a arrumação da mala, enrolei as garrafas de cerveja nas roupas, e
quando coloquei todas as coisas dentro da mala e pesei, deu 30kg! O permitido
era 23kg, então eu ia ter que dar um jeito de tirar 7kg. Tirei os chocolates
(1,7kg) e coloquei numa caixa, tirei os tênis e coloquei junto com algumas
roupas na outra bolsa. Então tive que levar na mão a mochila, a caixa com
chocolates e a bolsa. Joguei fora os adesivos que eu tinha pego no FOSDEM. Deu
pra ver que a mala ainda estava com mais de 23kg e o jeito era tentar a sorte na
hora de despachar.

Fui dormir cansado mas feliz por todos esses dias que passei em Bruxelas. Eu
havia ficado na dúvida se aproveitaria os dois dias para passear em Bruxelas ou
se iria para um evento chamado [Config Management
Camp](https://cfp.cfgmgmtcamp.be/2020/) focado em DevOps que aconteceria na
cidade de Ghent. A cidade fica a 30 minutos de trem de Bruxelas e pelo que vi é
um evento muito legal e gratuito. Cheguei a me inscrever e pensei em ir na
terça-feira. Mas como não deu certo ir no museu da guerra na segunda e fui na
terça, acabei desistindo de ir nesse evento.

Próximo texto: [Oitavo e último dia em Bruxelas](/oitavo-e-ultimo-dia-em-bruxelas)
