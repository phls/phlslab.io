---
layout: post
title: Eventos de Software Livre em 2017
date: 2017-12-31 16:00
author: Paulo Henrique de Lima Santana
categories: eventos
tags: eventos
---

# Eventos de Software Livre em 2017

Obs: essa lista está em constante atualização.

Acesse também a agenda de eventos de Software Livre e Código Aberto no Brasil:

<http://agenda.softwarelivre.org>

## JANEIRO

~~**Workshop gratuito de programação para mulheres - Django Girls Curitiba**  
21 de janeiro de 2017  
Curitiba - PR  
<https://djangogirls.org/curitiba>~~

~~**Campus Party Brasil 2017 - CPBR10**  
31 de janeiro a 05 de fevereiro de 2017  
São Paulo - SP  
<http://brasil.campus-party.org>~~

## FEVEREIRO

~~**Rails Girls Pelotas**  
10 a 11 de fevereiro de 2017  
Pelotas - RS  
<http://railsgirls.com/pelotas>~~

## MARÇO

~~**Open Data Day**  
04 de março de 2017  
Várias cidades  
<http://opendataday.org>~~

~~**Rails Girls Salvador**  
10 a 11 de março de 2017  
Salvador - BA  
<http://railsgirls.com/salvador2017>~~

~~**Rails Girls Novo Hamburgo**  
10 a 11 de março de 2017  
Novo Hamburgo - RS  
<http://railsgirls.com/novohamburgo>~~

~~**MiniDebConf Curitiba 2017**  
17 a 19 de março de 2017  
Curitiba - PR  
<http://br2017.mini.debconf.org>~~

~~**Workshop gratuito de programação para mulheres - Django Girls Porto Alegre**  
17 e 18 de março de 2017  
Porto Alegre - RS  
<https://djangogirls.org/portoalegre3>~~

~~**EFD 2017 - Education Freedom Day**  
18 de março de 2017  
Várias cidades  
<http://www.educationfreedomday.org>~~

~~**Workshop gratuito de programação para mulheres - Django Girls São Paulo**  
25 de março de 2017  
São Paulo - SP  
<https://djangogirls.org/saopaulo1>~~

~~**PHP Experience '2017**  
27 e 28 de março de 2017  
São Paulo - SP  
<http://phpexperience2017.imasters.com.br>~~

~~**DFD 2017 - Document Freedom Day**  
29 de março de 2017  
Várias cidades  
<http://documentfreedom.org>~~

## ABRIL

~~**FLISOL 2017 - Festival Latino-americano de Instalação de Software Livre**  
08 de abril de 2017  
Várias cidades  
<http://www.flisol.org.br>~~

~~**HFD 2017 - Hardare Freedom Day**  
15 de abril de 2017  
Várias cidades  
<http://www.hfday.org>~~

~~**Libre Graphics Meeting 2017**  
20 a 23 de abril de 2017  
Rio de Janeiro - RJ  
<https://libregraphicsmeeting.org/2017>~~

~~**LaKademy 2017 - 5º Encontro LatinoAmericano dos Colaboradores do KDE**  
28 de abril a 01 de maio de 2017  
Belo Horizonte - MG  
<https://lakademy.kde.org/lakademy17.html>~~

~~**Conferência Python Espírito Santo 2017**  
29 de abril de 2017  
Serra - ES  
<http://python2017.grupyes.org.br>~~

~~**Tchelinux Lajeado**  
29 de abril de 2017  
Lajeado - RS  
<https://lajeado.tchelinux.org>~~

## MAIO

~~**CryptoRave**  
05 a 06 de maio de 2017  
São Paulo - SP  
<https://cryptorave.org>~~

~~**2ª Conferência das Comunidades Python do Sudeste**  
05 a 07 de maio de 2017  
Rio de Janeiro - RJ  
<http://2017.pythonsudeste.org>~~

~~**WordCamp Porto Alegre**  
06 de maio de 2017  
Porto Alegre - RS  
<https://2017.portoalegre.wordcamp.org>~~

~~**PentahoDay 2017**  
11 e 12 de maio de 2017  
Curitiba - PR  
<http://www.pentahobrasil.com.br/eventos/pentahoday2017>~~

~~**Workshop gratuito de programação para mulheres - Django Girls Maceió**  
12 e 13 de maio de 2017  
Maceió - AL  
<https://djangogirls.org/maceio3>~~

~~**Conferência PHPRS 2017**  
12 e 13 de maio de 2017  
Porto Alegre - RS  
<http://conf.phprs.com.br>~~

~~**Scratch Day**  
13 de maio de 2017  
Várias cidades  
<http://day.scratch.mit.edu>~~

~~**Tchelinux Porto Alegre**  
20 de maio de 2017  
Porto Alegre - RS  
<https://poa.tchelinux.org>~~

~~**Workshop gratuito de programação para mulheres - Django Girls São José dos Campos**  
20 de maio de 2017  
São José dos Campos - SP  
<https://djangogirls.org/saojosedoscampos2>~~

~~**Workshop gratuito de programação para mulheres - Django Girls Niterói**  
20 de maio de 2017  
Niterói- RJ  
<https://djangogirls.org/niteroi>~~

~~**Workshop gratuito de programação para mulheres - Django Girls Belo Horizonte**  
27 de maio de 2017  
Belo Horizonte - MG  
<https://djangogirls.org/belohorizonte1>~~

~~**Tchelinux Santa Cruz**  
27 de maio de 2017  
Santa Cruz - RS  
<https://santacruz.tchelinux.org>~~

## JUNHO

~~**Workshop gratuito de programação para mulheres - Django Girls São Leopoldo**  
02 de junho de 2017  
São Leopoldo - RS  
<https://djangogirls.org/sao_leopoldo>~~

~~**Workshop gratuito de programação para mulheres - Django Girls São Luís**  
08 de junho de 2017  
São Luís - MA  
<https://djangogirls.org/saoluis>~~

~~**V Python Nordeste**  
08 a 10 de junho de 2017  
São Luís - MA  
<https://2017.pythonnordeste.org>~~

~~**Tchelinux Pelotas**  
10 de junho de 2017  
Pelotas - RS  
<https://pelotas.tchelinux.org>~~

~~**Campus Party Brasília**  
14 a 18 de junho de 2017  
Brasília - DF~~

~~**Rails Girls Sorocaba**  
16 e 17 de junho de 2017  
Sorocaba - SP  
<http://railsgirls.com/sorocaba>~~

~~**Workshop gratuito de programação para mulheres - Django Girls Recife**  
17 de junho de 2017  
Recife - PE  
<https://djangogirls.org/recife>~~

~~**Workshop gratuito de programação para mulheres - Django Girls Ribeirão Preto**  
23 de junho de 2017  
Ribeirão Preto  - SP  
<https://djangogirls.org/ribeiraopreto>~~

~~**Rails Girls Sorocaba**  
23 e 24 de junho de 2017  
Sorocaba  - SP  
<http://railsgirls.com/sorocaba>~~

~~**Caipyra - Conferência de Python em Ribeirão Preto**  
23 a 27 de junho de 2017  
Ribeirão Preto - SP  
<http://2017.caipyra.python.org.br>~~

## JULHO

~~**OpenStack Day 2017**  
15 de julho de 2017  
São Paulo - SP  
<http://openstackbr.com.br/events/2017/openstack-day>~~

~~**Python Day Alagoas**  
22 de julho de 2017  
Maceió - AL  
<http://pythonday.guga.io>~~

~~**Workshop gratuito de programação para mulheres - Django Girls Salvador**  
29 de julho de 2017  
Salvador - BA  
<https://djangogirls.org/salvador>~~

## AGOSTO

~~**Campus Party Bahia**  
09 a 13 de agosto de 2017  
Salvador - BA  
<http://brasil.campus-party.org/bahia>~~

~~**Tchelinux Novo Hamburgo**  
12 de agosto de 2017  
Novo Hamburgo - RS  
<https://nh.tchelinux.org>~~

~~**1ª conferência da comunidade Python do Norte do Brasil - PyCon Amazônia 2017**  
12 e 13 de agosto de 2017  
Manaus - AM  
<http://amazonia.python.org.br>~~

~~**Debian Day ou Dia do Debian**  
16 (12 ou 19) de agosto de 2017  
Várias cidades  
<https://wiki.debian.org/DebianDay/2017>~~

~~**Rails Girls São Paulo**  
18 e 19 de de agosto de 2017  
São Paulo  - SP  
<http://railsgirls.com/saopaulo>~~

~~**Qt Con Brasil**  
18 a 20 de de agosto de 2017  
São Paulo - SP  
<https://br.qtcon.org>~~

~~**Workshop gratuito de programação para mulheres - Django Girls Novo Hamburgo**  
25 a 26 de agosto de 2017  
Novo Hamburgo - RS  
<https://djangogirls.org/novohamburgo>~~

~~**Workshop gratuito de programação para mulheres - Django Girls Natal**  
26 de agosto de 2017  
Natal - RN  
<https://djangogirls.org/natal>~~

~~**Workshop gratuito de programação para mulheres - Django Girls Arapiraca**  
26 de agosto de 2017  
Arapiraca - AL  
<https://djangogirls.org/arapiraca1>~~

~~**Workshop gratuito de programação para mulheres - Django Girls Rio de Janeiro**  
26 de agosto de 2017  
Rio de Janeiro - RJ  
<https://djangogirls.org/riodejaneiro>~~

~~**Tchelinux Bagé**  
26 de agosto de 2017  
Bagé - RS  
<https://bage.tchelinux.org>~~

## SETEMBRO

~~**Workshop gratuito de programação para mulheres - Django Girls Caxias do Sul**  
07 de setembro de 2017  
Caxias do Sul - RS  
<https://djangogirls.org/caxiasdosul1>~~

~~**Python Sul 2017**  
08 a 10 de setembro de 2017  
Caxias do Sul - RS  
<http://pythonsul.org>~~

~~**PGBR 2017 - Conferência PostgreSQL**  
14 a 16 de setembro de 2017  
Porto Alegre - RS  
<https://pgbr.postgresql.org.br/2017>~~

~~**SFD 2017 - Software Freedom Day**  
16 de setembro de 2017  
Várias cidades  
<http://softwarefreedomday.org>~~

~~**Tchelinux Gravataí**  
16 de setembro de 2017  
Gravataí - RS  
<https://gravatai.tchelinux.org>~~

~~**Workshop gratuito de programação para mulheres - Django Girls Teresina**  
16 de setembro de 2017  
Teresina - PI  
<https://djangogirls.org/teresina>~~

~~**Tchelinux Bento Gonçalves**  
23 de setembro de 2017  
Bento Gonçalves - RS  
<https://bento.tchelinux.org>~~

~~**IX FTSL - Fórum de Tecnologia em Software Livre**  
27 a 29 de setembro de 2017  
Curitiba - PR  
<http://ftsl.org.br/ftsl9>~~

~~**COMSOLiD 10 - 10° Encontro da Comunidade Maracanauense de Software Livre e Inclusão Digital**  
27 a 29 de setembro de 2017  
Maracanaú - CE  
<http://sige.comsolid.org>~~

~~**Tchelinux Erechim**  
30 de setembro de 2017  
Erechim - RS  
<https://erechim.tchelinux.org>~~

## OUTUBRO

~~**Conferência Scratch Brasil 2017**  
05 a 07 de outubro de 2017  
São Paulo - SP  
<https://www.scratchbrasil.org>~~

~~**PHPeste**  
06 a 08 de outubro de 2017  
Fortaleza - CE  
<http://phpeste.net>~~

~~**PythonBrasil[13] - 13ª Conferência Brasileira da Comunidade Python**  
06 a 11 de outubro de 2017  
Belo Horizonte - MG  
<http://2017.pythonbrasil.org.br>~~

~~**Workshop gratuito de programação para mulheres - Django Girls Belo Horizonte**  
09 de outubro de 2017  
Belo Horizonte - MG  
<https://djangogirls.org/belohorizonte>~~

~~**UbaTech - Fórum de Tecnologias Livres**  
11 a 13 de outubro de 2017  
Ubatuba - SP  
<http://ubatech.ubatuba.cc>~~

~~**Campus Party Weekend**  
14 e 15 de outubro de 2017  
Pato Branco - PR~~

~~**II SEMDSL - Seminário de Educação, Mídias Digitais e Software Livre**  
16 a 18 de outubro de 2017  
Conceição do Coité - BA  
<http://semdsl.uneb.br>~~

~~**XIV Latinoware - Conferência Latino-americana de Software Livre**  
18 a 20 de outubro de 2017  
Foz do Iguaçu - PR  
<http://2017.latinoware.org>~~

~~**Tchelinux Caxias do Sul**  
21 de outubro de 2017  
Caxias do Sul - RS  
<https://caxias.tchelinux.org>~~

## NOVEMBRO

~~**2ª Campus Party Minas Gerais**  
01 a 05 de novembo de 2017  
Belo Horizonte - MG  
<http://brasil.campus-party.org/minas-gerais>~~

~~**2ª PotiCon - Conferência Potiguar de Software Livre**  
04 e 05 de novembro de 2017  
Natal - RN  
<http://www.potilivre.org/poticon>~~

~~**Rails Girls Rio de Janeiro**  
10 e 11 de outubro de 2017  
Rio de Janeiro - RJ  
<http://railsgirls.com/riodejaneiro>~~

~~**Rails Girls Belo Horizonte**  
10 e 11 de outubro de 2017  
Belo Horizonte - MG  
<http://railsgirls.com/belohorizonte>~~

~~**Linux Developer Conference Brazil**  
11 de novembro de 2017  
Campinas - SP  
<http://linuxdev-br.net>~~

~~**Workshop gratuito de programação para mulheres - Django Girls Florianópolis**  
11 de novembro de 2017  
Florianópolis - SC  
<https://djangogirls.org/florianopolis2>~~

~~**Workshop gratuito de programação para mulheres - Django Girls São Paulo**  
11 de novembro de 2017  
São Paulo - SP  
<https://djangogirls.org/saopaulo>~~

~~**11ª RubyConf Brasil**  
17 e 18 de novembro de 2017  
São Paulo - SP  
<http://eventos.locaweb.com.br/proximos-eventos/rubyconf-2017>~~

~~**Rails Girls Porto Alegre**  
17 e 18 de novembro de 2017  
Porto Alegre - RS  
<http://railsgirls.com/portoalegre2017>~~

~~**XIV FGSL - Fórum Goiano de Software Livre**  
18 e 19 de novembro de 2017  
Goiânia - GO  
<http://2017.fgsl.net>~~

~~**3ª DrupalCamp São Paulo**  
24 e 25 de novembro de 2017  
São Paulo - SP  
<http://saopaulo.drupalcamp.com.br>~~

~~**Iº Fórum Baiano de Tecnologias Abertas**  
27 a 29 de novembro de 2017  
Feira de Santana - BA  
<http://fbta.uefs.br>~~

~~**15º Fórum Espírito Livre**  
29 de novembro a 01 de dezembro de 2017  
Santa Teresa - ES  
<http://forum.espiritolivre.org/15ed>~~

## DEZEMBRO

~~**WordCamp São Paulo 2017**  
02 de dezembro de 2017  
São Paulo - SP  
<https://2017.saopaulo.wordcamp.org>~~

~~**12ª PHP Conference Brasil**  
06 a 10 de dezembro de 2017  
Osasco - SP  
<http://www.phpconference.com.br>~~

~~**Rails Girls Florianópolis**  
08 e 09 de dezembro de 2017  
Florianópolis - SC  
<http://railsgirls.com/florianopolis>~~

~~**Workshop de raspagem de dados com Python em Curitiba**  
09 de dezembro de 2017  
Curitiba - PR  
<http://python.mulheres.eti.br>~~

~~**Workshop gratuito de programação para mulheres - Django Girls Feira de Santana**  
09 de dezembro de 2017  
Feira de Santana - BA  
<https://djangogirls.org/feiradesantana>~~

~~**Tchelinux Porto Alegre**  
09 de dezembro de 2017  
Porto Alegre - RS  
<https://poa.tchelinux.org>~~

## Anos anteriores

[2016](http://softwarelivre.org/blog/eventos-de-software-livre-em-2016)  
[2015](http://softwarelivre.org/blog/eventos-de-software-livre-em-2015)  
[2014](http://softwarelivre.org/blog/eventos-de-software-livre-em-2014)  
[2013](http://softwarelivre.org/blog/eventos-de-software-livre-em-2013)  
[2012](http://softwarelivre.org/blog/eventos-de-software-livre-em-2012)  
[2011](http://softwarelivre.org/blog/eventos-de-software-livre-em-2011)  
