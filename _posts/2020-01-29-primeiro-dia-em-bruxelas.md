---
layout: post
title: Primeiro dia em Bruxelas
date: 2020-01-29 23:59
author: Paulo Henrique de Lima Santana
categories: geral
tags: bruxelas bélgica debian fosdem
---

# Primeiro dia em Bruxelas

Veja o texto anterior: [Viagem de Curitiba para
Bruxelas](/viagem-de-curitiba-para-bruxelas).

Cheguei no Hostel Galia às 15h, fiz o check-in e fui pro quarto. O quarto tem 8
beliches divididos em 2 ambientes, além de um banheiro. O bom é que as camas tem
cortinas que dão uma certa privacidade. Quando entrei no quarto tinha uma
pessoa, então dei um "hi" e fui arrumar minhas coisas. Com acesso a wifi mandei
uma mensagem de voz pelo whastapp respondendo a guria do rh que me entrevistou
no dia anterior e havia mandando uma pergunta. Nisso o cara que dava deitado
chegou pra mim e perguntou em português: "você é brasileiro?". Começamos a
conversar sobre o que estávamos fazendo aqui, e o nome dele é Marcos. Ele é
advogado e veio participar de um [torneio de
Magic](https://www.cfbevents.com/events/MTGBrussels), e era a primeira vez na
Europa por isso tava meio perdido na cidade.

Eu pretendia já sair pra MiniDebCamp, mas queria comprar um chip pro celular o
quanto antes porque fica mais fácil andar por aqui usando o google maps. Como
ele também queria comprar um chip, resolvemos ir atrás de um shopping pra achar
uma loja da operadora Base porque ano passado eu havia comprado um chip deles e
queria saber se ainda dava pra usá-lo. Na recepção do hostel quando estávamos
saindo tinham três brasileiras fazendo check-in. Elas são Fortaleza e estavam
vindo de Amsterdam.

Andamos bastante e ví que estávamos no centro turístico, comecei a lembrar dos
lugares e aproveitei pra mostrar a Grand Place pro Marcos. Continuamos andando
até acharmos a loja da Base e tive que comprar um chip novo porque o meu estava
desligado por não ter recarregado no prazo de um ano. Passamos na loja da
Primark para ver se as roupas estavam baratas porque ano passado compramos
algumas lá, e estavam mesmo. Provavelmente eu compre algumas camisas antes de ir
embora. O Marcos aproveitou pra comprar uma toalha.

Paramos no Carrefour pra comprarmos comida. Comprei um café gelado latte
macchiato e outro cappuccino que eu gostei muito ano passado, e uns croissants.
Passamos de novo pela Grand Place e duas brasileiras estavam tentando achar
alguém pra tirar uma foto delas. Falamos com elas em português, tiramos as fotos
e começamos a conversar. Elas são de Manaus e pediram dicas de passeios por aqui
e se deviam contratar uma agência de turismo pra fazer um tour. Eu sugeri alguns
lugares pra elas visitarem e disse que na minha opinião elas não precisavm de
uma agência. Nos despedimos e seguimos pro hostel.

Como já era noite, achei melhor ficar no hostel e não ir mais pra MiniDebCamp.
Mesmo na Europa, prefiro não andar por aqui sozinho a noite (sabe como é, nós
brasileiros somos desconfiados em qualquer lugar). Tomei um banho, desci pro
espaço do Hostel onde tem algumas mesas, comi e fiquei vendo meus emails. Dois
colegas indianos que vieram pro evento contaram que roubaram a mochila de um
deles com o notebook no metrô. Um deles foi comprar as passagens, o outro ficou
cuidando das coisas. Alguém chegou puxando conversa, quando ele se distraiu,
outra pessoa levou a mochila. Uma pena mesmo.

Votei pro quarto e fui dormi. Assim terminou o meu primeiro dia em Bruxelas.

Próximo texto: [Segundo dia em Bruxelas e primeiro da MiniDebCamp](/segundo-dia-em-bruxelas-e-primeiro-da-minidebcamp)