---
layout: post
title: Cargos exercidos
date: 2010-01-01 16:00
author: Paulo Henrique de Lima Santana
categories: geral
tags: cargos
---

# Cargos exercidos em entidades e eventos estudantis e de software livre

**Evento: MiniDebConf 2017 Curitiba**

 * Cargo: Coordenador Geral
 * Período: 17/03/2017 a 19/03/2017
 * Local: Curitiba - PR

**Evento: Campus Party Brasil 2017**

 * Cargo: Curador da Área de Software Livre
 * Período: 31/01/2017 a 05/02/2017
 * Local: São Paulo - SP

**Evento: Campus Party Minas Gerais 2016**

 * Cargo: Curador da Área de Software Livre
 * Período: 09/11/2016 a 13/11/2016
 * Local: Belo Horizonte - MG

**Evento: Campus Party Recife 2016**

 * Cargo: Curador da Área de Software Livre
 * Período: 20/08/2015 a 21/07/2016
 * Local: Recide - PE

**Evento: FISL16 – Fórum Internacional Software Livre**

 * Cargo: Membro do GT-Temário
 * Período: 13/07/2016 a 16/07/2016
 * Local: Porto Aegre - RS

**Evento: MiniDebConf 2016 Curitiba**

 * Cargo: Coordenador Geral
 * Período: 05/03/2016 a 06/03/2016
 * Local: Curitiba - PR

**Evento: Campus Party Brasll 2016**

 * Cargo: Curador da Área de Software Livre
 * Período: 26/01/2016 a 31/01/2016
 * Local: São Paulo - SP

**Evento: SFD 2015 – Software Freedom Day**

 * Cargo: Coordenador Geral
 * Período: 19/09/2015
 * Local: Curitiba - PR

**Evento: Campus Party Recife 2015**

 * Cargo: Curador da Área de Software Livre
 * Período: 23/07/2015 a 26/07/2015
 * Local: Recide - PE

**Evento: FISL16 – Fórum Internacional Software Livre**

 * Cargo: Membro do GT-Temário
 * Período: 08/07/2015 a 11/07/2015
 * Local: Porto Aegre - RS

**Evento: Campus Party Brasll 2015**

 * Cargo: Curador da Área de Software Livre
 * Período: 03/02/2015 a 08/02/2015
 * Local: São Paulo - SP

**Evento: SFD 2014 – Software Freedom Day**

 * Cargo: Coordenador Geral
 * Período: 20/09/2014
 * Local: Curitiba - PR

**Evento: VI FTSL - Fórum de Tecnologia em Software Livre**

 * Cargo: Membro da Coordenação Geral
 * Período: 18/09/2013 a 20/09/2014
 * Local: Curitiba - PR

**Evento: Campus Party Recife 2014**

 * Cargo: Curador da Área de Software Livre
 * Período: 23/07/2013 a 27/07/2014
 * Local: Recide - PE

**Evento: FISL15 – Fórum Internacional Software Livre**

 * Cargo: Membro do GT-Comunicação
 * Período: 07/05/2014 a 10/05/2014
 * Local: Porto Aegre - RS

**Evento: FLISOL 2014 – Festilval Latino Americano de Instalação de Software Livre**

 * Cargo: Coordenador Geral
 * Período: 26/04/2014
 * Local: Curitiba - PR

**Evento: EFD 2014 – Education Freedom Day**

 * Cargo: Coordenador Geral
 * Período: 08/03/2014
 * Local: Curitiba - PR

**Evento: Campus Party Brasll 2014**

 * Cargo: Curador da Área de Software Livre
 * Período: 27/01/2014 a 02/02/2014
 * Local: São Paulo - SP

**Evento: SFD 2013 – Software Freedom Day**

 * Cargo: Coordenador Geral
 * Período: 21/09/2013
 * Local: Curitiba - PR

**Evento: Campus Party Recife 2013**

 * Cargo: Curador da Área de Software Livre
 * Período: 17/07/2013 a 21/07/2013
 * Local: Recide - PE

**Evento: FLISOL 2013 – Festilval Latino Americano de Instalação de Software Livre**

 * Cargo: Coordenador Geral
 * Período: 27/04/2013
 * Local: Curitiba - PR

**Evento: Campus Party Brasll 2013**

 * Cargo: Curador da Área de Software Livre
 * Período: 28/01/2013 a 03/02/2013
 * Local: São Paulo - SP

**Evento: SFD 2012 – Software Freedom Day**

 * Cargo: Coordenador Geral
 * Período: 15/09/2012
 * Local: Curitiba - PR

**Evento: Debian Day 2012**

 * Cargo: Coordenador Geral
 * Período: 18/08/2012
 * Local: Curitiba - PR

**Evento: Campus Party Recife 2012**

 * Cargo: Curador da Área de Software Livre
 * Período: 26/07/2012 a 30/07/2012
 * Local: Recide - PE

**Evento: FLISOL 2012 – Festilval Latino Americano de Instalação de Software Livre**

 * Cargo: Coordenador Geral
 * Período: 28/04/2012
 * Local: Curitiba - PR

**Evento: Document Freedom Day**

 * Cargo: Coordenador Geral
 * Período: 28/03/2012
 * Local: Curitiba - PR

**Evento: SFD 2011 – Software Freedom Day**

 * Cargo: Coordenador Geral
 * Período: 17/09/2011
 * Local: Curitiba - PR

**Evento: XXVIII ENECOMP - Congresso Nacional dos Estudantes de Computação**

 * Cargo: Coordenador Geral
 * Período:20/07/2011 e 21/07/2011
 * Local: Natal - RN

**Evento: SFD 2010 – Software Freedom Day**

 * Cargo: Coordenador Geral
 * Período: 18/09/2010
 * Local: Curitiba - PR

**Entidade: CEI - Centro de Estudos de Informática da UFPR**

 * Cargo: Primeiro Tesoureiro
 * Período: 14/11/2008 a 27/11/2009

**Evento: XXVII ENECOMP - Congresso Nacional dos Estudantes de Computação**

 * Cargo: Coordenador Geral
 * Período: 04/08/2009 a 08/09/2009
 * Local: Curitiba - PR

**Entidade: GUD-BR-PR - Grupo de Usuários Debian do Paraná**

 * Cargo: Líder
 * Período: 26/02/2008 a 27/10/2008

**Entidade: ENEC - Executiva Nacional dos Estudantes de Computação**

 * Cargo: Presidente
 * Período: 04/08/2005 a 04/08/2006

**Entidade: ENEC - Executiva Nacional dos Estudantes de Computação**

 * Cargo: Presidente
 * Período: 06/08/2004 a 04/08/2005

**Evento: XXI ENECOMP - Congresso Nacional dos Estudantes de Computação**

 * Cargo: Coordenador Geral
 * Período: 04/08/2003 a 08/08/2003
 * Local: Campinas - SP

**Entidade: ENEC - Executiva Nacional dos Estudantes de Computação**

 * Cargo: Primeiro Tesoureiro
 * Período: 18/07/2002 a 07/08/2003

**Entidade: CEI - Centro de Estudos de Informática da UFPR**

 * Cargo: Primeiro Tesoureiro
 * Período: 05/10/2000 a 19/02/2002

**Entidade: DCE - Diretório Central dos Estudantes da UFPR**

 * Cargo: Coordenador Geral Administrativo/Financeiro
 * Período: 20/12/1997 a 11/12/1998

**Evento: 5º Congresso de Estudantes da UFPR**

 * Cargo: Coordenador Geral
 * Período: 29/08/1997 a 31/08/1997
 * Local: Pinhais - PR

**Evento: II Seminário para Centros Acadêmicos da UFPR**

 * Cargo: Coordenador Geral
 * Período: 13/07/1997 a 15/07/1997
 * Local: Lapa - PR

**Entidade: DCE - Diretório Central dos Estudantes da UFPR**

 * Cargo: Coordenador Setorial de Ciências Exatas
 * Período: 06/12/1996 a 20/12/1997
