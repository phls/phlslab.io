---
layout: post
title: Reunião com o Prefeito de Curitiba
date: 2013-10-14 16:00
author: Paulo Henrique de Lima Santana
categories: geral
tags: software-livre
---

# Reunião com o Prefeito de Curitiba

Hoje (14/10/13) participei de uma reunião com o Prefeito de Curitiba Gustavo
Fruet e o Deputado Estadual Pastor Edson Praczyk onde conversamos sobre software
livre e a comunidade local. O Prefeito se mostrou bastante conhecedor do assunto
e boas notícias estão por vir.