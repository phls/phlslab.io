---
layout: post
title: Eventos de Software Livre em 2011
date: 2011-12-31 16:00
author: Paulo Henrique de Lima Santana
categories: eventos
tags: eventos
---

# Eventos de Software Livre em 2011

Obs: essa lista está em constante atualização.

Acesse também a agenda de eventos de Software Livre e Código Aberto no Brasil:

<http://agenda.softwarelivre.org>

## JANEIRO

~~**Campus Party Brasil 2011**  
17 a 23 de janeiro de 2011  
São Paulo - SP  
<http://www.campus-party.com.br/2011/software-livre.html>~~

## MARÇO

~~**DFD 2011 - Document Freedom Day (Dia da Liberdade do Documento)**  
30 de março de 2011  
Várias cidades  
<http://documentfreedom.org>~~

## ABRIL

~~**FLISOL 2011 - Festilval Latino Americano de Instalação de Software Livre**  
09 de abril de 2011  
Várias cidades  
<http://www.flisol.net>~~

## MAIO

~~**IV CONSEGI - Congresso Internacional Software Livre e Governo Eletrônico**  
11 a 13 de maio de 2011  
Brasília - DF  
<http://www.consegi.gov.br>~~

## JUNHO

~~**12º FISL - Fórum Internacional de Software Livre**  
29 de junho a 02 de julho de 2011  
Porto Alegre - RS  
<http://www.fisl.org.br>~~

## JULHO

~~**V ENSOL - Encontro de Software Livre da Paraíba**  
20 a 23 de julho de 2011  
João Pessoa - PB  
<http://www.ensol.org.br>~~

## SETEMBRO

~~**SFD 2011 - Software Freedom Day**  
17 de setembro de 2011  
Várias cidades  
<http://softwarefreedomday.org>~~

## OUTUBRO

~~**VIII Latinoware - Conferência Latino Americana de Software Livre**  
19 a 21 de outubro de 2011  
Foz do Iguaçu - PR  
<http://www.latinoware.org>~~

~~**6º SOLISC - Congresso Catarinense de Software Livre**  
21 a 22 de outubro de 2011  
Florianópolis - SC  
<http://www.solisc.org.br>~~

~~**V ENSL - Encontro Nordestino de Software Livre**  
28 a 30 de outubro de 2011  
Maceió - AL  
<http://www.ensl.org.br>~~

## NOVEMBRO

~~**CONISLI - Conferência Internacional de Software Livre**  
04 e 05 de novembro de 2011  
São Paulo - SP  
<http://www.conisli.org.br>~~

~~**LinuxCon Brazil 2011**  
17 a 18 de novembro de 2011  
São Paulo - SP  
<http://events.linuxfoundation.org/events/linuxcon-brazil>~~

~~**II Fórum da Revista Espírito Livre**  
29 de novembro de 2011  
Vila Velha - ES  
<http://forum.espiritolivre.org/iforumel>~~
