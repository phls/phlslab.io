---
layout: post
title: Segundo dia em Bruxelas e primeiro da MiniDebCamp
date: 2020-01-30 23:59
author: Paulo Henrique de Lima Santana
categories: geral
tags: bruxelas bélgica debian fosdem
---

# Segundo dia em Bruxelas e primeiro da MiniDebCamp

Quinta-feira - 30/01/2020

Veja o texto anterior: [Primeiro dia em Bruxelas](/primeiro-dia-em-bruxelas).

O hostel que estou foi o mais barato que encontrei no
[booking.com](https://www.booking.com/hotel/be/hostel-galia.pt-br.html?label=gen173nr-1FCAEoggI46AdIM1gEaBWIAQGYAS24ARnIAQ_YAQHoAQH4AQuIAgGoAgO4Aqrcz_EFwAIB;sid=fc2b55b65ac5e4b7467fef3be8eefe09;dest_id=-1955538;dest_type=city;dist=0;group_adults=2;group_children=0;hapos=4;hpos=4;no_rooms=1;room1=A%2CA;sb_price_type=total;sr_order=popularity;srepoch=1580461617;srpvid=306d401899940069;type=total;ucfs=1&#hotelTmpl).
A diária já contando com o imposto ficou por € 25,24 incluindo café da manhã. O
que chamou a minha atenção é que todos os comentários são bastante positivos
sobre o café da manhã. Então no meu segundo dia tive a oportunidade de comprovar
que os comentários estavam certos, realmente é muito bom. Outros hostels mais
caros não tem café da manhã como parte da diária.

Uma coisa boa é que consegui dormir bem a primeira noite aqui. Acordei umas duas
vezes durante a madrugada, mas voltei a dormir logo. Eu estava preocupado porque
quando estive em Hamburgo ano passado para participar da
[MiniDebCamp](https://wiki.debian.org/DebianEvents/de/2019/MiniDebConfHamburg)
não havia conseguido dormir bem na primeira noite e passei o resto do evento
meio quebrado. 

Após o café saí para a
[MiniDebCamp](https://wiki.debian.org/DebianEvents/be/2020/MiniDebCamp) que está
acontecendo no Hackerspace [Brussels (HSBXL)](https://hsbxl.be). O pessoal de lá
cedeu o espaço para a comunidade Debian usar de quarta até sexta. Fui para o
terminal de ônibus chamado "Gare du Midi" para pegar o ônibus da linha 78. Após
dar algumas voltas procurando o terminal e passar por dois restaurantes
brasileiros, consegui achá-lo. O HSBXL é relativamente perto, desci já no 4o
ponto após o terminal. O hackerspace fica no terceiro andar de um prédio grande
que aparentamente está passando por um reforma. Cheguei no local pouco depois
das 10h e vi alguns DDs que eu já conhecia, me acomodei em uma das mesas e
liguei o notebook para ler meus emails, escrever os textos para o blog, e
esperar o pessoal do time de vídeo para conversar e tirar algumas dúvidas que eu
tinha sobre o setup do computador que faz a transmissão das palestras.

![Bruxelas](/assets/img/bruxelas-2020-1.jpg)

![Bruxelas](/assets/img/bruxelas-2020-2.jpg)

No prédio tem um bar/restaurante que serve almoço, e as opções eram sanduíches
ou massas, então resolvi sair para andar pela região para procurar outras
coisas. O HSBXL na verdade fica em Anderlecht, uma
[comuna](https://pt.wikipedia.org/wiki/Comunas_da_Regi%C3%A3o_de_Bruxelas-Capital)
ao lado de Bruxelas. Eu já havia visto que próximo tem dois mercados das redes
que eu comprei coisas no ano passado: Lidl e Aldi. Pelo que entendi, essas redes
tem produtos mais baratos do que os supermercados grandes. Passei nos dois pra
dar uma olhada e continuei caminhando.

![Bruxelas](/assets/img/bruxelas-2020-5.jpg)

![Bruxelas](/assets/img/bruxelas-2020-6.jpg)

Eu também havia visto que há 3 km do HSBXL existe uma
[estátua](https://visit.brussels/en/place/Jean-Claude-Van-Damme-statue) do
Jean-Claude Van Damme, que para quem não sabe, nasceu na Bélgica. Para quem como
eu cresceu assistindo os filmes do Van Damme nos anos 80 e 90, ele foi um
representante máximo de filmes de ação com lutas. Então havia planejado ir fazer
umas fotos ao lado da estátua. No caminho parei para almoçar em um restaurante
de comida turca, o que é bastante comum por aqui. Ano passado havia descoberto
que existe uma grande comunidade de imigrantes turcos em Bruxelas. Era o tipo de
almoço que eu queria, com mais "sustança", com arroz, salada e frango, e custou
€ 9,50 o prato. Após o almoço continuei minha peregrinação para a estátua do
ídolo. Após uns 20 minutos cheguei lá e fiz as fotos. Algumas pessoas ficaram
olhando estranhamente para mim :-)

![Bruxelas](/assets/img/bruxelas-2020-3.jpg)

![Bruxelas](/assets/img/bruxelas-2020-4.jpg)

No caminho de volta para o hackespace, parei novamente no Aldi e comprei cafés
gelados (€ 0,40 cada) e algns croissants (€ 0,59 cada) pra lanchar. A marca
desse café que mais gostei no ano passado e que comprei novamente agora foi a
"Moreno" e acho que só vende no Aldi porque não achei em outros mercados. É bem
doce, tem nos sabores Latte Macchiato, Latte Cappucino e Café Espresso e na
verdade é uma bebida para o verão, ou seja, é ruim mas é bom. A marca que
comprei no no primeiro dia Carrefour é... 

Chegando de volta no HSBXL, fui tirar minhas dúvidas sobre o computador da
transmissão com o Nicolas e ele me disse que eu podia fazer uma instalação do
zero no computador que ele trouxe de Paris e que é usado para as transmissões
nas MiniDebConfs na Europa. Eu já havia visto esse computador em Hamburgo e
achei legal a oportunidade. A instalação e configuração levou o restante do dia
e deu pra ver que não é algo simples pra deixar funcionando já com a câmera
conectada. Como o Niculas teve que fazer vários ajustes, vou refazer tudo
novamente hoje.

A noite pedimos pizza e deu € 6,00 pra cada pessoa, comprei uma cerveja Duvel
que é vendida no HSBXL por € 3,50. Existe uma geladeira com várias bebidas, você
pega o que quiser, passa no leitor de código de barras e deixa o pagamento na
caixinha, tudo self-service.

Fui embora as 22h, peguei o ônibus de volta pro terminal, caminhei até o hostel
mas agora pelas ruas certas. Quando cheguei o Marcos veio conversar comigo sobre
o dia dele. Me falou que pegou o metrô errado para ir para a competição de
Magic, mas que depois conseguiu chegar lá. E na volta fez o caminho certo. Tomei
um banho e fui dormir cansado das aventuras do segundo dia :-)

Próximo texto: [Terceiro dia em Bruxelas e segundo da
MiniDebCamp](/terceiro-dia-em-bruxelas-e-segundo-da-minidebcamp)
