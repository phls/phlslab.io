---
layout: post
title: Sistema de agenda de eventos de Software Livre no Brasil
date: 2017-01-17 16:00
author: Paulo Henrique de Lima Santana
categories: geral
tags: software-livre eventos
---

# Sistema de agenda de eventos de Software Livre no Brasil

Ao longo dos anos surgiram algumas iniciativas de pessoas da comunidade de manter
agendas de eventos de software livre que aconteciam no Brasil. Infelizmente esses
projetos acabaram morrendo, e desde 2011 venho
[listando<i class="fa fa-external-link"></i>](http://phls.com.br/eventos-de-software-livre-em-2019)
no meu site os eventos que eu tenho conhecimento como forma de ajudar na divulgação.

Ano passado conheci um sistema livre desenvolvido na França chamado
["L'Agenda du Libre"<i class="fa fa-external-link"></i>](http://www.agendadulibre.org/)
e resolvi traduzir para o português e disponibilizar uma instância para os
eventos no Brasil.

Estou lançando agora a *Agenda de eventos de Software Livre e Código Aberto no
Brasil*, e convido as comunidades para cadastrarem os seus eventos no site:

<http://agenda.softwarelivre.org>

Qualquer pessoa pode enviar desde grandes eventos nacionais de Software Livre
até encontros locais de comunidades. Não importa o tamanho do evento, o
importante é usar a agenda como um local centralizado para divulgação.

Lembrando que ano passado lancei a
[timeline<i class="fa fa-external-link"></i>](http://timeline.softwarelivre.org/)
de eventos de Software Livre no Brasil.