---
layout: post
title: Bits from MiniDebConf Toulouse 2024
date: 2024-12-08 10:00
author: Paulo Henrique de Lima Santana
categories: geral
tags: debian english toulouse minidebconf planetdebian-en
---

# Intro

I always find it amazing the opportunities I have thanks to my contributions to the Debian Project. I am happy to receive this recognition through the help I receive with travel to attend events in other countries.

This year, two MiniDebConfs were scheduled for the second half of the year in Europe: the traditional edition in [Cambridge](https://wiki.debian.org/DebianEvents/gb/2024/MiniDebConfCambridge) in UK and a new edition in [Toulouse](https://wiki.debian.org/DebianEvents/fr/2024/Toulouse) in France. After weighing the difficulties and advantages that I would have to attend one of them, I decided to choose Toulouse, mainly because it was cheaper and because it was in November, giving me more time to plan the trip. I contacted the current DPL Andreas Tille explaining my desire to attend the event and he kindly approved my request for Debian to pay for the tickets. Thanks again to Andreas!

MiniDebConf Toulouse 2024 was held in November 16th and 17th (Saturday and Sunday) and took place in one of the rooms of a traditional Free Software event in the city named [Capitole du Libre](https://www.capitoledulibre.org/). Before MiniDebConf, the team organized a MiniDebCamp in November 14th and 15th at a coworking space.

The whole experience promised to be incredible, and it was! From visiting a city in France for the first time, to attending a local Free Software event, and sharing four days with people from the Debian community from various countries.

## Travel and the city

My plan was to leave Belo Horizonte on Monday, pass through São Paulo, and arrive in Toulouse on Tuesday night. I was going to spend the whole of Wednesday walking around the city and then participate in the MiniDebCamp on Thursday.

But the flight that was supposed to leave São Paulo in the early hours of Monday to Tuesday was cancelled due to a problem with the airplane and I had spent all Tuesday waiting. I was rebooked on another flight that left in the evening and arrived in Toulouse on Wednesday afternoon. Even though I was very tired from the trip, I still took advantage of the end of the day to walk around the city. But it was a shame to have lost an entire day of sightseeing.

On Thursday I left early in the morning to walk around a little more before going to the MiniDebCamp venue. I walked around a lot and saw several tourist attractions. The city is really very beautiful, as they say, especially the houses and buildings made of pink bricks. I was impressed by the narrow and winding streets; at one point it seemed like I was walking through a maze. I arrived to a corner and there would be 5 streets crossing in different directions.

The riverbank that runs through the city is very beautiful and people spend their evenings there just hanging out. There was a lot of history around there.

I stayed in an airbnb 25 minutes walking from the coworking space and only 10 minutes from the event venue. It was a very spacious apartment that was much cheaper than a hotel.

![MiniDebConf Toulouse 2024](/assets/img/minidc-toulouse-cidade-1.jpg)
<br /><br />
![MiniDebConf Toulouse 2024](/assets/img/minidc-toulouse-cidade-2.jpg)

## MiniDebCamp

I arrived at the coworking space where the MiniDebCamp was being held and met up with several friends. I also met some new people, talked about the translation work we do in Brazil, and other topics.

We already knew that the organization would pay for lunch for everyone during the two days of MiniDebCamp, and at a certain point they told us that we could go to the room (which was downstairs from the coworking space) to have lunch. They set up a table with quiches, breads, charcuterie and LOTS of cheese :-) There were several types of cheese and they were all very good. I just found it a little strange because I'm not used to having cheese for lunch, but the experience was amazing anyway :-)

![MiniDebConf Toulouse 2024](/assets/img/minidc-toulouse-queijos-1.jpg)
<br /><br />
![MiniDebConf Toulouse 2024](/assets/img/minidc-toulouse-queijos-2.jpg)

In the evening, we went as a group to dinner at a restaurant in front of the Capitolium, the city's main tourist attraction.

On the second day, in the morning, I walked around the city a bit more, then went to the coworking space and had another incredible cheese table for lunch.

## Video Team

One of my ideas for going to Toulouse was to be able to help the [video team](https://debconf-video-team.pages.debian.net/docs/) in setting up the equipment for broadcasting and recording the talks. I wanted to follow this work from the beginning and learn some details, something I can't do before the DebConfs because I always arrive after the people have already set up the infrastructure. And later reproduce this work in the MiniDebConfs in Brazil, such as the one in [Maceió](https://maceio.mini.debconf.org/) that is already scheduled for May 1-4, 2025.

As I had agreed with the people from the video team that I would help set up the equipment, on Friday night we went to the University and stayed in the room working. I asked several questions about what they were doing, about the equipment, and I was able to clear up several doubts. Over the next two days I was handling one of the cameras during the talks. And on Sunday night  I helped put everything away.

Thanks to olasd, tumbleweed and ivodd for their guidance and patience.

![MiniDebConf Toulouse 2024](/assets/img/minidc-toulouse-preparacao.jpg)

## The event in general

There was also a meeting with some members of the publicity team who were there with the DPL. We went to a cafeteria and talked mainly about areas that could be improved in the team.

The [talks](https://toulouse2024.mini.debconf.org/schedule/) at MiniDebConf were very good and the recordings are also available [here](https://meetings-archive.debian.net/pub/debian-meetings/2024/MiniDebConf-Toulouse/).

I ended up not watching any of the talks from the general schedule at Capitole du Libre because they were in French. It's always great to see free software events abroad to learn how they are done there and to bring some of those experiences to our events in Brazil.

I hope that MiniDebConf in Toulouse will continue to take place every year, or that the French community will hold the next edition in another city and I will be able to join again :-) If everything goes well, in July next year I will return to France to join [DebConf25](https://debconf25.debconf.org/) in Brest.

![MiniDebConf Toulouse 2024](/assets/img/minidc-toulouse-cracha.jpg)

[More photos](https://photos.app.goo.gl/zX9byG6v2ZCxJmeCA)

