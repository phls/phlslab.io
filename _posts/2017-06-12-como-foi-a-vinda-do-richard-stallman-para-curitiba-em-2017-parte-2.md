---
layout: post
title: Como foi a vinda do Richard Stallman para Curitiba em 2017 - parte 2
date: 2017-12-06 16:00
author: Paulo Henrique de Lima Santana
categories: eventos
tags: richard-stallman curitiba-livre software-livre
---

# Como foi a vinda do Richard Stallman para Curitiba em 2017 - parte 2

![Foto Stallman no palco](/assets/img/foto-stallman-curitiba-2017-1.jpg){:width="600px"}

[Richard Stallman (RMS)<i class="fa fa-external-link"></i>](https://stallman.org/),
fundador do movimento
[Software Livre<i class="fa fa-external-link"></i>](https://pt.wikipedia.org/wiki/Software_livre), do
[Projeto GNU<i class="fa fa-external-link"></i>](http://gnu.org/), e da
[Free Software Foundation (FSF)<i class="fa fa-external-link"></i>](http://fsf.org/),
e um dos autores da
[Licença Pública Geral GNU (GNU GPL)<i class="fa fa-external-link"></i>](https://www.gnu.org/licenses)
esteve no Brasil durante alguns dias de maio e junho de 2017 palestrando em
algumas cidades.

Esse texto é um relato da passagem do Stallman em duas cidades que eu ajudei a
organizar as palestras:

 * Curitiba - onde eu moro e faço parte da Comunidade
[Curitiba Livre<i class="fa fa-external-link"></i>](http://curitibalivre.org.br).
 * Brasília - durante a 1ª edição da Campus Party Brasília onde eu também
palestrei (e sou curador de Software Livre da Campus Party Brasil em São Paulo).

Dividi o texto em duas publicações para a leitura não ficar muito cansativa:

 1. O [planejamento<i class="fa fa-external-link"></i>](http://softwarelivre.org/blog/como-foi-a-vinda-do-richard-stallman-para-curitiba-em-2017-parte-1)
para a vinda do Stallman publicado em setembro.
 1. O Stallman em Curitiba (este texto).

Relembre
[aqui<i class="fa fa-external-link"></i>](http://softwarelivre.org/blog/como-foi-a-vinda-do-richard-stallman-para-curitiba-em-2017-parte-1#porque-escrevi-esse-texto)
porque eu escrevi esse texto.

## Stallman em Curitiba

O Stallman chegou em Curitiba no dia 1 de junho vindo de Campinas, pouco antes
das 17h e eu fui sozinho buscá-lo no aeroporto. Como não tenho carro, achei
melhor alugar um para que facilitasse os deslocamentos durante os dias em que
eles estaria aqui em Curitiba. Quando nos encontramos, ele estava puxando uma
mala gigante e com duas bolsas penduradas no pescoço, aí institivamente perguntei
se ele precisava de ajuda, e ele disse que não, que se precisasse ele pediria.
No primeiro momento, eu poderia ter achado essa resposta bastante grosseira, mas
tava lá nas
[recomendações<i class="fa fa-external-link"></i>](http://softwarelivre.org/blog/como-foi-a-vinda-do-richard-stallman-para-curitiba-em-2017-parte-1#recomendacoes)
não oferecer ajuda porque caso ele precisasse de algo, ele mesmo pediria. Como
moro do lado do aeroporto, perguntei se ele queria ir para casa, e ele me disse
que gostaria de comprar "castanhas do Brasil" (em português) e uma outra coisa
que não entendi o que era, e pedi para repetir. Depois de umas três tentativas
eu entendi que ele queria "chocolates da Garoto".

Ao sair do aeroporto, estava fazendo um vento frio no estacionamento, eu de
casaco e o Stallman de camiseta. Ele sorriu e disse que agora sim estava se
sentindo bem com aquele clima. Eu comentei que estava frio e ele disse que que
não, que estava ótimo daquele jeito.

Fomos para o
[Mercado Municipal<i class="fa fa-external-link"></i>](http://mercadomunicipaldecuritiba.com.br/)
que fica quase no centro de Curitiba porque lá vende castanhas a granel. Assim que
entrou no carro, o Stallman abriu o notebook e começou a digitar. Eu realmente
não me importei com isso porque eu não esperava ficar conversando bastante e
fazendo várias perguntas, minha intenção era deixá-lo a vontade. Ah, e estava lá
no texto das
[recomendações<i class="fa fa-external-link"></i>](http://softwarelivre.org/blog/como-foi-a-vinda-do-richard-stallman-para-curitiba-em-2017-parte-1#recomendacoes)
para não estranhar ou se sentir ofendido se em algum momento ele abrisse o
notebook porque ele tinha que responder vários e-mails durante o dia. Quando
ouço alguém dizendo que o Stallman é o seu exemplo de vida, fico imaginando como
seria se essa pessoa ficasse sozinha com ele, será que essa pessoa ficaria
enlouquecida tietando, como aqueles(as) adolecentes que encontram o seu ídolo?
Com certeza isso já deve ter acontecido.

Compramos as castanhas (de caju e do Pará), uma caixa de bombons e duas barras
de chocolate da Garoto. Quando estávamos indo buscar a Adriana, já tinha passado
das 18h e o Stallman me perguntou se ali era o centro comercial de Curitiba, e
respondi que sim. Ele explicou que fazia muito tempo que queria comprar um
despertador novo mas que não achava em lugar nenhum. Então se eu soubesse de
algum lugar por ali que vendesse despertadores, ele gostaria de ir. Mas não era
um despertador qualquer, ele tinha que ter as seguintes característcas:

 * Ser pequeno para ser fácil de carregar na bolsa poque ele não usa relógio de
pulso e muito menos celular.
 * Analógico.
 * Usar pilha AA para garantir que o som do alarme seria alto para fazê-lo acordar.
 * Ter luz no mostrador para poder consultar a hora de madrugada no escuro.
 * Os botões de regulagem da hora e do despertador deveriam ser embutidos para
não desregular no atrito dentro da bolsa.

Combinei com a Adriana de nos encontrarmos numa dessas lojas que vendem de tudo,
a maioria dos produtos importados e de qualidade duvidosa. Foi o primeiro
encontro da Adriana com o Stallman, talvez no futuro ela conte com foi esse
momento e a sua versão de como foi recebê-lo em casa :-) Nenhum dos
despertadores tinha as característcas que ele queria, então lembrei de uma
relojoaria ali perto onde já levei meu relógio para trocar pilha/pulseira e que
eu lembrava que tinha despertadores para vender. Assim que entramos, o Stallman
já viu um analógico do tamanho que ele queria, e pediu para ver. Os dois botões
eram embutidos, tinha luz, e usava pilha AA. Sim, tinhamos achado exatamente
como ele queria, o modelo é esse.

O dono da relojoaria ficou curioso ao nos ver falando em inglês e comecei a
explicar que o Stallman era americano e estava na cidade para palestrar. O dono
da loja disse que os pais eram da Ucrânia e então começamos a conversar os três.
O Stallman me pediu para explicar para o dono o que era Software Livre, então
falei que "software livre é gratuito..." nesse momento o Stallman me interrompeu
bruscamente e disse que isso estava errado. Então expliquei que eu ia completar
dizendo que "sofware livre era gratuito em sua maioria como no caso da
distribuição GNU/Linux que ele poderia baixar e instalar no computador que tava
ali perto". O Stallman então falou que o mais importante era a liberdade, e que
aquele computador com software privativo poderia ser vigiado, e se tivesse
software livre isso não aconteceria. Que ter software privativo seria o mesmo
que o governo colocasse uma câmera na loja pra ficar vendo o que o dono fazia.
Continuei explicando outras coisas até que finalizamos a compra e fomos embora.

Antes de ir, o Stallman perguntou pro dono se ele não teria aquele mesmo
despertador mas com apenas um botão para regular a hora e o ponteiro do
despertador. O dono da loja falou que não tinha naquele momento, mas que ia
chegar na semana que vem. Eu nunca vi um despertador com apenas um botão e achei
que o dono tava mentindo. Mas o Stallman me fez prometer que iria voltar na loja
na semana seguinte para comprar mais dois despertadores e levá-los dali a
15 dias para Brasília quando nos encontraríamos na Campus Party.

Assim que saímos, o Stallman nos parou na rua e me deu uma bronca, explicando
que eu nunca devo falar em preço, mas que devo falar sempre em liberdade. Tentei
explicar que eu havia falado da gratuidade porque provavelmente aquele lojista
não teria pago pelo windows que tava no computador, e que se ele achasse que
teria que pagar para ter GNU/Linux instalado, ele não iria se interessar. O
Stallman continuou repetindo que isso não era importante, e que o correto é
mostrar para as pessoas leigas que a liberdade sem vigilância é o principal
motivo para usar Software Livre, e que não era uma questão de preço. Ou seja,
que o ponto da vigilância iria despertar nas pessoas o interesse pelo Software
Livre.

O Oliva havia me dito que em Campinas o Stallman falou da vontade de tomar sopas,
mas que não tinham tomado lá porque não era fácil de achar, ao contrário de
Curitiba no inverno. Então perguntei pro Stallman se queria jantar num buffet de
sopas e ele gostou da ideia. Fomos em um
[restaurante<i class="fa fa-external-link"></i>](http://www.acrotona.com.br/)
pelo centro mesmo que serve pizzas e sopas. Eu e Adriana já tinhamos comido
pizza algumas vezes lá, mas nunca tomamos as sopas. Quando estacionei o carro,
ele continou usando o notebook, então perguntei se ele queria que a gente
esperasse um pouco até ele terminar. Então ele perguntou porque eu estava
perguntando aquilo, já que se ele precisasse de mais tempo no carro, ele iria
falar, e portanto eu não deveria supor que ele precisava de mais tempo. Jantamos
sopas e no final ele disse que não gostou muito porque esperava que elas
tivessem mais sabor, mas que a nossa tentativa foi válida.

## Em casa

Chegamos em casa já perto da meia-noite e assim que entramos mostrei o quarto
para o Stallman. Ele aprovou o colchão, deixou a mala no canto e foi para a mesa
da sala, sentou e ligou o notebook. Fiz as coisas de praxe como mostrar a tomada
e passar a senha do wi-fi, e vi que ali ele já estava acomodado.

Depois de alguns minutos comentei que havia lido que ele gostava de chá, e
perguntei se ele queria algo. Ele pediu para mostrar como ele poderia colocar a
água para esquentar, então mostrei a chaleira, como usar o fogão e dei uma
caneca. Dali em diante por várias vezes ele mesmo colocou a água para esquentar
durante os dias, e tinha os seus próprios sachês de chá preto.

A Adriana perguntou para o Stallman se ele já tinha tomado chá de alecrim porque
temos um pé e ela estava tomando diaramente porque tinha lido que faz bem para
evitar enxaquecas. Ele disse que nunca tinha tomado mas que gostaria de
experimentar, que tem uma amiga que sofre de enxaquecas e que se o resultado for
bom, a Adriana poderia falar pra ele depois. Então a Adriana deu um galho para o
Stallman fazer o chá e ele disse que gostou do sabor.

Nos três dias em que ficou hospedado em nossa casa, a rotina do Stallman foi
basicamente essa: ficar na mesa usando o notebook e fazer chá para tomar
enquanto comia as castanhas e os chocolates. Não fez calor mas também não estava
muito frio para a época do inverno, na primeira noite deve ter chegado a uns 12
graus, acho que ele até chegou a usar o cobertor que havíamos deixado no quarto
para dormir :-)

## Conversa tensa sobre a Campus Party em Brasília

Fiquei na dúvida se deveria relatar o que vem a seguir. Decidi escrever não com
o objetivo de prejudicar a imagem do Stallman, mas apenas para servir de alerta
sobre o que pode acontecer.

Bem antes do Stallman vir para o Brasil, durante a troca de e-mails para acertar
os detalhes para ele palestrar na Campus Party em Brasília, uma pessoa da
organização avisou ao Stallman (me copiando) que o hotel já estava reservado. O
Stallman então perguntou se ela poderia achar alguém em Brasília que pudesse
hospedá-lo em casa ao invés dele ir para o hotel. Ela achou esse pedido bastante
"estranho" e me perguntou o que estava acontecendo, qual era o motivo do Stallman
não querer ficar confortável em um hotel. Então expliquei que isso era normal
porque ele prefere ficar hospedado na casa de alguém, mesmo que ele não conheça,
do que ficar em hotel. A organização não conseguiu achar ninguém e informou ao
Stallman, que respondeu que tentaria achar por conta própria. Alguns dias depois
o Alexandre Oliva enviou um
[e-mail<i class="fa fa-external-link"></i>](http://listas.softwarelivre.org/pipermail/psl-brasil/2017-April/005298.html)
para uma lista de discussão perguntando se alguém estaria disposto a hospedar o
Stallman em Brasília, e foi atendido pelo Prof. Pedro Rezende.

O problema é que em 2012 houve uma
[queixa<i class="fa fa-external-link"></i>](http://listas.softwarelivre.org/pipermail/psl-brasil/2012-December/000405.html)
do Stallman sobre ficar em uma casa sem ar-condicionado em Brasília por causa do
calor, e se isso se repetisse, poderia trazer problemas para a Campus Party. A
pessoa da organização me explicou que se o Stallman não ficasse no hotel no
primeiro dia, não seria possível depois colocá-lo lá caso dele resolvesse ficar
na casa de alguém e tivesse que sair. O Prof. Pedro Rezende que ficou de
hospedá-lo, estava organizando outra
[palestra<i class="fa fa-external-link"></i>](http://listas.softwarelivre.org/pipermail/psl-brasil/2017-June/005318.html)
do Stlalman na UNB foi muito solícito e se comprometeu a fazer todo o translado
com o Stallman entre aeroporto, casa, Campus Party, etc, e assim a organização
da Campus Party não teria que se preocupar com nada. Diante de tudo isso, fiquei
então de conversar pessoalmente com o Stallman sobre isso e saber se ele
realmente não preferia ficar no hotel.

Então na primeira noite que o Stallman estava em casa, resolvi puxar esse
assunto. Já era mais de 1h da manhã, ele estava no notebook e perguntei se
poderiamos conversar sobre a Campus Party porque existia a preocupação com a
hospedagem dele em Brasília. Ele me interrompeu bruscamente e disse que eu não
deveria me preocupar com isso porque já estava tudo certo. Então insisti que a
organização da Campus já havia reservado um hotel caso ele preferisse, e então a
coisa ficou tensa. O Stallman começou a falar alto que a organização não deveria
ter reservado o hotel porque ele não tinha pedido, e que se ele fosse sentir
calor isso não era problema nosso. Como vi que esse assunto já estava encerrado
para ele, aproveitei e tentei alertar sobre como eram os palcos na Campus Party,
que não eram auditórios fechados como em outros eventos e que poderia existir
barulho em volta enquanto ele estivesse palestrando. Novamente, falando bastante
alto, ele repetiu que eu não deveria me preocupar com isso, que se houvesse
qualquer problema de barulho ele iria resolver lá e que eu o estava incomodando
com essa conversa porque ele queria trabalhar e eu o estava atrapalhando. Após
tentar explicar as minhas preocupações e ser interrompido pelo Stallman, fiquei
muito frustrado com a reação que ele teve e resolvi encerrar a conversa por ali.
Então pro sofá continuar fazendo minhas coisas.

Antes de encerramos esse assunto, ele explicou que não gosta de ficar em hoteis
porque os empregados agem como robôs e são pagos para servir os clientes de
forma muito submissa. E quando ele fica na casa de alguém, ele pode conhecer
novas pessoas e conversar, já no hotel ele só tem como alternativa assistir TV.

Após uns 30 minutos de silêncio, o Stallman se levantou e veio falar comigo em
um tom totalmente diferente, mais calmo, perguntou se eu tinha algumas músicas
para copiar num pendrive pra ele. No outro dia a Adriana me perguntou o que tinha
acontecido porque acordou com o Stallman falando alto e nervoso, então contei a
minha tentativa frustrada de conversa sobre Brasília.

Eu havia planejado de no dia seguinte perguntar ao Stallman se gostaria de
conhecer o
[Departamento de Informática da UFPR<i class="fa fa-external-link"></i>](http://web.inf.ufpr.br/dinf/)
que foi onde eu estudei e que tem nos laboratórios os computadores rodando apenas
GNU/Linux, mas essa conversa me deixou tão chateado/frustrado que eu desisti de
perguntar e resolvi deixá-lo quieto trabalhando antes de irmos para a palestra.

## O dia da palestra

Pela manhã (dia 2 de junho) o
[Daniel Lenharo<i class="fa fa-external-link"></i>](http://blog.lenharo.eti.br/),
que fez parte da organização da palestra, veio para minha casa para a gente organizar
os últimos detalhes e levar o Stallman para almoçar. O Alexandre Oliva havia me
dado algumas dicas sobre o Stallman, entre elas: sempre combinar com antecedência
o que você pretende fazer para dar tempo dele se preparar; e chegar com uma boa
antecedência ao local da palestra para ele separar os materiais da FSF que ele
traz para vender ou distribuir ao público.

Não comentei sobre isso na primeira parte: quando você acerta os detalhes para
receber o Stallman para palestrar, a FSF envia pelo correio uma caixa com
materias que o Stallman irá vender ou distribuir ao público. São
[adesivos<i class="fa fa-external-link"></i>](https://www.flickr.com/photos/curitibalivre/27099040049/in/album-72157684615095686/),
chaveiros, botons, etc.

Combinei com o Stallman de sairmos às 12h30 para almoçar em um restaurante perto
da minha casa. Minha ideia era sair para almoçar e de lá já ir para o local da
palestra na UFPR que estava marcada para começar às 18h. Assim teríamos bastante
tempo de preparar o local para gravar a palestra. No horário marcado nos
preparamos para sair e quando vi que ele não tinha pego os materiais, falei para
ele pegar tudo porque do restaurante já iríamos para a UFPR. Ele então ficou
nervoso pela segunda vez: começou a esbravejar que eu não tinha falado antes que
já íamos para a UFPR e que ele não tinha se preparado para ir, porque ele achou
que iríamos voltar pra casa ainda. Aí me caiu a ficha que eu realmente não havia
combinado com ele de ir para a UFPR, então falei que tudo bem, que voltaríamos
para casa e que a culpa foi minha por não ter falado antes.

Após o almoço voltamos para casa e pedi que ele se aprontasse assim que possível
para a gente ir. Ele ficou trabalhando no notebook e só saímos às 15h, o que me
deixou bastante preocupado porque seria bem corrido para organizar tudo. Quando
chegamos no local, o Stallman lembrou que havia deixado o carregador do notebook
e os materiais da FSF em casa, então tive que voltar, o que me deixou ainda mais
preocupado e meu nível de streess foi aumentando porque a possibilidade de dar
alguma m... estava piorando. Mas as outras
[pessoas<i class="fa fa-external-link"></i>](http://rms.curitibalivre.org.br/organizadores.shtml) da organização
omeçaram a preparar o auditório, e a empresa contratada começou a montagem da
cabine de tradução simultânea. Lembrando que a contratação da tradução 
simultânea só foi possível graças ao
[financimento coletivo<i class="fa fa-external-link"></i>](http://rms.curitibalivre.org.br/financiamento-coletivo.shtml)
que várias pessoas ajudaram.

Cheguei de volta na UFPR já próximo de 16h30 e já havia bastante gente na entrada
do auditório porque havíamos divulgado que as portas seriam abertas às 17h. No
hall o pessoal da organização **já havia montado as duas mesas que o Stallman
havia orientado:** uma mesa para vender os
[materiais<i class="fa fa-external-link"></i>](https://www.flickr.com/photos/curitibalivre/35125248785/in/album-72157684615095686/)
e outra mesa (a uma certa distância) para distribuir os adesivos gratuitamente.
Só deu tempo de finalizar a montagem dos equipamentos para a gravação e estávamos
prontos para começar.

![Foto Stallman e Paulo no palco](/assets/img/foto-stallman-curitiba-2017-3.jpg){:width="600px"}

Antes da palestra, colocamos um vídeo para exibir que era uma coletânea de outros
[vídeos<i class="fa fa-external-link"></i>](http://curitibalivre.org.br/blog/videos-sobre-software-livre-e-gnulinux)
falando o que é software livre, etc. Quando o Stallman começou a ouvir uma
[animação<i class="fa fa-external-link"></i>](https://www.youtube.com/watch?v=jWq_x8noBYk)
em espanhol, ele começou a fazer cara feia, balançar a cabeça negativamente e
colocar a mão no rosto. Eu subi no palco e perguntei qual era o problema, e ele
reclamou que o vídeo citava "software proprietário" como um antagonista ao
software livre e que isso estava errado. O correto era citar "software privativo",
e que o vídeo iria confundir as pessoas. Ainda bem que o vídeo era pequeno e
acabou logo, porque ele estava ficando nervoso com aquilo.

E então começou a palestra. O auditório estava lotado, e algumas pessoas ainda
tiveram que sentar no chão. Foram quase 2h30 entre a fala do Stallman, o leilão
do GNU de pelúcia (Curitiba foi a cidade brasileira que teve o maior lance: R$
475,00) e as perguntas do público. Você pode assistir o vídeo no link a seguir,
inclusive com o áudio da tradução simultânea:
<http://stallman-brasil.softwarelivre.org/2017/curitiba-ufpr>

![Foto plateia](/assets/img/foto-stallman-curitiba-2017-2.jpg){:width="600px"}

Depois da palestra ainda houve um tempo para o pessoal tirar fotos com o
Stallman e para as entrevistas que estavam combinadas. Aproveitamos esse tempo
para desmontar os equipamentos, e quando tudo foi finalizado, ficou apenas um
pequeno grupo da organização. Perguntamos ao Stallman se ele gostaria de jantar
em uma churrascaria e ele respondeu que como estava indo para a Argentina, lá ele
comeria bastante carne e que por isso ele preferia comer peixe. Achar lugar que
sirva peixe em Curitiba não é uma tarefa fácil, principalmente porque esses
restaurante são muito caros. Até que achamos o
[Peixinho<i class="fa fa-external-link"></i>](http://www.peixepizzariarestaurante.com.br/)
que tem rodízio de peixe e... pizza, e com um preço razoável. O problema é que
existem dois restaurantes chamados Peixinho, e fomos no que não tinha rodízio e
era mais caro. Descobrimos que telefonamos para um (o do rodízio) para pegar as
informações, e fomos para o outro (à la carte), isso deve ter sido o cansaço já
batendo em todos nós. Mas não ficamos no à la carte e fomos para o Peixinho
correto, o do rodízio :-)

O rodízio de peixes até era bom, mas era só de um tipo de peixe: tilápia. Então
tinha tilápia frita, tilápia assada, tilápia no molho, tilápia com pimenta,
tilápia empanada, e por aí vaí. Depois de tanta tilápia, o Stallman acabou
comendo umas fatias de pizza também, alias, todos nós comemos. Quando acabou de
comer e conversar um pouco, o Stallman abriu o notebook na mesa e começou a
trabalhar. Como todos já sabiam que isso poderia acontecer, ninguém estranhou, e
continuamos conversando sem incomodá-lo.

Chegamos em casa já bem tarde e cansados. Eu estava extremante aliviado porque
tinha dado tudo certo na palestra. O auditório estava cheio, as pessoas
aparentemente sairam satisfeitas, a gravação e a tradução simultânea funcionaram,
e o Stallman não teve nenhum aborrecimento. Meu maior medo era que acontecesse
algo parecido como em Goiânia em 2012 e que todo o nosso trabalho fosse por água
abaixo, e que ao invés das pessoas ficarem felizes, elas saissem com uma má
impressão do Stallman e acabassem o ridicularizando. Mas todo o nosso trabalho,
das pessoas envolvidas na organização, foi recompensado com um evento perfeito.
Perguntei para o Stallman se ele havia gostado da palestra, ele fez uma cara de
quem não entendeu muito bem a pergunta e disse que aquilo fazia parte do trabalho
dele e que portanto havia sido normal.

![Foto banners](/assets/img/foto-stallman-curitiba-2017-4.jpg){:width="600px"}

## A partida

O vôo do Stallman de Curitiba para Foz do Iguaçu estava marcado para às 11h30
(dia 3 de junho), então pela manhã não tinha muito o que fazer. A Adriana se
despediu dele logo cedo porque ela tinha que ir para a aula de inglês. Ela
agradeceu por ele ter vindo e disse que a palestra iria encorajar as pessoas a
conhecer mais sobre software livre. O Stallman respondeu que ele não é um
rockstar para ter seguidores, mas o que ele espera é que as pessoas acessem o
site do
[Projeto GNU<i class="fa fa-external-link"></i>](https://www.gnu.org/)
e que contribuam de verdade para o Software Livre.

Combinamos o horário de saída e fomos para o aeroporto. Uma das dicas do
Alexandre Oliva foi que o Stallman gosta muito de coxinhas, então talvez em
algum momento ele pediria para comer algumas. No aeroporto após o check-in, o
Stallman me perguntou onde poderia comprar algumas coxinhas para levar, e fomos
a uma lanchonete. Nos despedimos e ele foi
[embarcar<i class="fa fa-external-link"></i>](https://www.flickr.com/photos/curitibalivre/34315228113/in/album-72157684615095686/).
Dali a 2 semanas nos encontraríamos novamente na Campus Party em Brasília.

![Foto Paulo e Stallman no aeroporto](/assets/img/foto-stallman-curitiba-2017-5.jpg){:width="600px"}

## Campus Party Brasília

A palestra do Stallman na Campus Party em Brasília foi marcada para o dia 17 de
junho às 13h, último dia do evento, e no palco principal porque ele era um dos
palestrantes magistrais (keynote). A chegada do Stallman em Brasília no dia
anterior vindo de Foz do Iguaçu (porque ele cruzou a fronteira entre Argentina e
Brasil por terra), e a hospedagem na casa do Prof. Pedro Rezende tinha acontecido
sem nenhum problema. O Prof. Pedro Rezende levou o Stallman para o local da
Campus Party e para evitar que ele fizesse o procedimento normal de check-in no
evento que incluia ter que cadastrar a digital, a organização permitiu que ele
entrasse por uma entrada secundária.

Almoçamos no restaurante da Campus Party e fomos para o palco para o Stallman se
preparar para a palestra. Ele passou as instruções de quais materiais da FSF
deveriam ser vendidos e quais deveriam ser distribuidos gratuitamente e onde as
coisas deveriam ficar nas mesas, e foi para o palco. A palestra ocorreu também
sem nenhum problema. Foi a mesma palestra que ele fez em Curitiba só que um
pouco mais curta, com 2h de duração. Tinha muita gente assistindo em volta do
palco como dá para ver nas
[fotos<i class="fa fa-external-link"></i>](https://www.flickr.com/photos/slcampusparty/albums/72157685258765955).

![Foto plateia Campus Party Brasília](/assets/img/foto-stallman-cpbsb-2017-02.jpg){:width="600px"}

A Campus Party costuma transmitir ao vivo as palestras no seu canal no
[YouTube<i class="fa fa-external-link"></i>](https://www.youtube.com/channel/UCUendDl_o6qhA7GtHxH3PTA)
e deixar lá para quem quiser assistir depois. Mas como o Stallman pede que a
palestra dele não seja colocada no Youtube por questões de privacidade, orientei
o pessoal da organização para não transmtir, o que gerou um pouco de surpresa
principalmente no pessoal da área de comunicação que não está acostumado a
receber esse pedido. De qualquer forma, a palestra foi gravada e está e está
disponível nesse link:
<http://stallman-brasil.softwarelivre.org/2017/brasilia-campus-party>. Pena que
a pessoa que fez a tradução simultânea não permiriu que gravássemos o áudio para
disponibilizar depois, porque para isso teríamos que pagar um valor a mais, mesmo
eu explicando que seria muito útil para a comunidade e que daríamos os créditos
a ele. Mas não houve jeito, só pagando mesmo.

Como a palestra do Stallman era de um magistral, não estavam acontecendo
palestras nos outros palcos. Mas o tempo previsto é de mais ou menos 1h30, por
isso já no finalzinho da palestra começaram as movimentações nos outros palcos e
o Stallman achou estranho o barulho que o pessoal estava fazendo, principalmente
os "gritos". Ele parou a palestra e começou a falar para o pessoal não fazer
barulho porque ele ainda não havia acabado. Eu tive que subir ao palco e falar
para ele não se importar com isso porque eram atividades de outros palcos que
iriam começar e que ele poderia continuar falando que a plateia estava ouvindo
normalmente. Ele ainda parou mais uma duas vezes, e eu fui ficando apreensivo
que ele realmente se aborrecesse com aquilo, mas aí deu tempo de encerrar a
palestra antes das outras iniciarem.

Várias pessoas tiraram fotos com o Stallman, e o pessoal da comunidade local de
Software Livre de Brasília e que organiza o
[FLISOL<i class="fa fa-external-link"></i>](https://flisol.info/) na cidade
pediu que ele fosse até a bancada onde eles estavam para tirar uma foto com o
grupo. Mas quando o Stallman viu o banner do FLISOL atrás, ele pediu para
tirar o banner porque ele não gostaria de ser visto como um apoiador do FLISOL.
O pessoal perguntou porque e ele explicou que a maoria das cidades instalam
softwares não livres no FLISOL e ele era contra isso. Ele explicou para todos a
importância de instalar apenas softwares livres no FLISOL.

Antes de ir embora, entreguei os dois despertadores para o Stallman que eu havia
ficado de levar. Ele havia me enviado um e-mail enquanto estava na Argentina me
lembrando de passar na loja para ver se tinha chegado o despertador com apenas
um botão. E como não tinha, comprei o modelo igual ao que ele havia comprado.
Ainda dei a ele uma caixinha de acrílico para ele guardar o despertador dentro
da mala (o que ele achou bastante útil) e duas barras de chocolates Garoto que
ele tanto gosta.

O Stallman pediu para ir embora logo porque estava muito calor, e dava para ver
que ele estava cansado. Nos despedimos com ele dizendo: Happy hacking!

Mais uma vez deu tudo certo, e a organização da Campus Party ficou bastante
satisfeita com a participação do Stallman. Agradeço a coordenação da Campus
Party por disponibilizar o palco principal para o criador do movimento Software
Livre, e agradeço especialmente a duas pessoas que eu admiro muito:
[Ana Carolina Rodrigues<i class="fa fa-external-link"></i>](https://www.facebook.com/Acolirodrigues)
que na época era a Gerente de Conteúdos da Campus Party e que topou a ideia de
levar o Stallman, e a
[Juliana Tedoro<i class="fa fa-external-link"></i>](https://www.facebook.com/ajulianateodoro)
que foi a pessoa da organização que cuidou de toda a parte operacional para
viabilizar a ida dele.

![Foto Stallman palco Campus Party Brasília](/assets/img/foto-stallman-cpbsb-2017-01.jpg){:width="600px"}

## Agradecimentos

Nunca é demais agradecer a todas as pessoas ajudaram de alguma forma para a
realização da palestra do Stallman em Curitiba:

 * Adriana Cássia da Costa
 * Alexandre Oliva
 * Claudia Costa
 * Cleber Ianes
 * Daniel Lenharo de Souza
 * Daniel Weingaertner
 * Edemir Reginaldo Maciel
 * Eduardo Todt
 * Gabriela Bianchini Palumbo
 * Helen Mendes
 * Ianka Antunes
 * Leonardo Rodrigues
 * Marcos Sunye
 * Matheus Horstmann
 * Paulo Henrique de Lima Santana
 * Samira Chami Neves

## Links para vídeos, fotos e notícias

Palestra do Stallman em Curitiba

 * [Fotos<i class="fa fa-external-link"></i>](https://www.flickr.com/photos/curitibalivre/albums/72157684615095686)
 * [Vídeos<i class="fa fa-external-link"></i>](http://stallman-brasil.softwarelivre.org/2017/curitiba-ufpr/)

Palestra do Stallman na Campus Party Brasília

 * [Fotos<i class="fa fa-external-link"></i>](https://www.flickr.com/photos/slcampusparty/albums/72157685258765955)
 * [Vídeos<i class="fa fa-external-link"></i>](http://stallman-brasil.softwarelivre.org/2017/brasilia-campus-party)

Criador do Movimento Software Livre, Richard Stallman faz palestra para auditório lotado na UFPR

<http://www.ufpr.br/portalufpr/blog/noticias/criador-do-movimento-software-livre-richard-stallman-faz-palestra-para-auditorio-lotado-na-ufpr>

Quem controla o seu computador? Confira palestra do criador do sistema GNU em Curitiba

<http://jornalcomunicacaoufpr.com.br/quem-controla-o-seu-computador-confira-palestra-do-criador-do-sistema-gnu-em-curitiba>

Todos os olhos em você

<http://www.gazetadopovo.com.br/economia/nova-economia/todos-os-olhos-em-voce-a0cxax3k55o1oc9iqzuur74t4>
