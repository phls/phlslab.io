---
layout: post
title: Bits from FOSDEM 2023 and 2024
date: 2024-03-04 23:50
author: Paulo Henrique de Lima Santana
categories: geral
tags: debian english freesoftware planetdebian-en bruxelas bélgica fosdem
---

[Link para versão em português](https://phls.com.br/minha-participacao-nos-fosdem-2023-e-2024)

# Intro

Since 2019, I have traveled to Brussels at the beginning of the year to join [FOSDEM](https://fosdem.org/2024/), considered the largest and most important Free Software event in Europe. The 2024 edition was the fourth in-person edition in a row that I joined (2021 and 2022 did not happen due to COVID-19) and always with the financial help of Debian, which kindly paid my flight tickets after receiving my request asking for help to travel and approved by the Debian leader.

In 2020 I wrote [several posts](https://phls.com.br/viagem-de-curitiba-para-bruxelas) with a very complete report of the days I spent in Brussels. But in 2023 I didn't write anything, and becayse last year and this year I coordinated a room dedicated to translations of Free Software and Open Source projects, I'm going to take the opportunity to write about these two years and how it was my experience.

After my first trip to FOSDEM, I started to think that I could join in a more active way than just a regular attendee, so I had the desire to propose a talk to one of the rooms. But then I thought that instead of proposing a tal, I could organize a room for talks :-) and with the topic "translations" which is something that I'm very interested in, because it's been a few years since I've been helping to translate the Debian for Portuguese.

# Joining FOSDEM 2023

In the second half of 2022 I did some research and saw that there had never been a room dedicated to translations, so when the FOSDEM organization opened the [call](https://archive.fosdem.org/2023/news/2022-09- 29-call_for_devrooms/) to receive room proposals (called DevRoom) for the 2023 edition, I sent a proposal to a translation room and it was [accepted](https://archive.fosdem.org/2023/news /2022-11-07-accepted-developer-rooms/)!

After the room was confirmed, the next step was for me, as room coordinator, to publicize the [call for talk proposals](https://lists.fosdem.org/pipermail/fosdem/2022q4/003441.html). I spent a few weeks hoping to find out if I would receive a good number of proposals or if it would be a failure. But to my happiness, I received eight proposals and I had to select six to schedule the [room programming schedule](https://archive.fosdem.org/2023/schedule/track/translations/) due to time constraints .

[FOSDEM 2023](https://archive.fosdem.org/2023) took place from February 4th to 5th and the translation devroom was scheduled on the second day in the afternoon.

![Fosdem 2023](/assets/img/fosdem-2023-063.jpg)

The talks held in the room were these below, and in each of them you can watch the recording video.

- [Welcome to the Translations DevRoom](https://archive.fosdem.org/2023/schedule/event/translations_welcome_to_the_translations_devroom/).
    - Paulo Henrique de Lima Santana
- [Translate All The Things!](https://archive.fosdem.org/2023/schedule/event/translations_translate_all_the_things/) An Introduction to LibreTranslate.
    - Piero Toffanin
- [Bringing your project closer to users - translating libre with Weblate](https://archive.fosdem.org/2023/schedule/event/translations_bringing_your_project_closer_to_users_translating_libre_with_weblate/). News, features and plans of the project.
    - Benjamin Alan Jamie
- [20 years with Gettext](https://archive.fosdem.org/2023/schedule/event/translations_20_years_with_gettext/). Experiences from the PostgreSQL project.
    - Peter Eisentraut
- [Building an atractive way in an old infra for new translators](https://archive.fosdem.org/2023/schedule/event/translations_building_an_atractive_way_in_an_old_infra_for_new_translators/).
    - Texou
- [Managing KDE's translation project](https://archive.fosdem.org/2023/schedule/event/translations_managing_kdes_translation_project/). Are we the biggest FLOSS translation project?
    - Albert Astals Cid
- [Translating documentation with cloud tools and scripts](https://archive.fosdem.org/2023/schedule/event/translations_translating_documentation_with_cloud_tools_and_scripts/). Using cloud tools and scripts to translate, review and update documents.
    - Nilo Coutinho Menezes

And on the first day of FOSDEM I was at the Debian stand selling the t-shirts that I had taken from Brazil. People from France were also there selling other products and it was cool to interact with people who visited the booth to buy and/or talk about Debian.

<br />
![Fosdem 2023](/assets/img/fosdem-2023-016.jpg)
<br /><br />
![Fosdem 2023](/assets/img/fosdem-2023-019.jpg)
<br />

[Photos](https://photos.app.goo.gl/fB6wH37b2pFBqiNZ9)

# Joining FOSDEM 2024

The 2023 result motivated me to propose the translation devroom again when the FOSDEM 2024 organization opened the [call for rooms](https://fosdem.org/2024/news/2023-09-29-devrooms-cfp/) . I was waiting to find out if the FOSDEM organization would accept a room on this topic for the second year in a row and to my delight, my proposal was [accepted](https://fosdem.org/2024/news/2023-11-08- devrooms-announced/) again :-)

This time I received 11 proposals! And again due to time constraints, I had to select six to schedule the [room schedule grid](https://fosdem.org/2024/schedule/track/translations/).

[FOSDEM 2024](https://fosdem.org/2024/) took place from February 3rd to 4th and the translation devroom was scheduled for the second day again, but this time in the morning.

The talks held in the room were these below, and in each of them you can watch the recording video.

- [Welcome to the Translations DevRoom](https://fosdem.org/2024/schedule/event/fosdem-2024-3516-welcome-to-the-translations-devroom/).
    - Paulo Henrique de Lima Santana
- [Localization of Open Source Tools into Swahili](https://fosdem.org/2024/schedule/event/fosdem-2024-2624-localization-of-open-source-tools-into-swahili/).
    - Cecilia Maundu
- [A universal data model for localizable messages](https://fosdem.org/2024/schedule/event/fosdem-2024-1759-a-universal-data-model-for-localizable-messages/).
    - Eemeli Aro
- [Happy translating! It is possible to overcome the language barrier in Open Source!](https://fosdem.org/2024/schedule/event/fosdem-2024-3236-happy-translating-it-is-possible-to-overcome-the-language-barrier-in-open-source-/)
    - Wentao Liu
- [Lessons learnt as a translation contributor the past 4 years](https://fosdem.org/2024/schedule/event/fosdem-2024-1906-lessons-learnt-as-a-translation-contributor-the-past-4-years/).
    - Tom De Moor
- [Long Term Effort to Keep Translations Up-To-Date](https://fosdem.org/2024/schedule/event/fosdem-2024-2071-long-term-effort-to-keep-translations-up-to-date/).
    - Andika Triwidada
- [Using Open Source AIs for Accessibility and Localization](https://fosdem.org/2024/schedule/event/fosdem-2024-3348-using-open-source-ais-for-accessibility-and-localization/).
    - Nevin Daniel

This time I didn't help at the Debian stand because I couldn't bring t-shirts to sell from Brazil. So I just stopped by and talked to some people who were there like some DDs. But I volunteered for a few hours to operate the streaming camera in one of the main rooms.

<br />
![Fosdem 2024](/assets/img/fotos-fosdem-2024-037.jpg)
<br /><br />
![Fosdem 2024](/assets/img/fotos-fosdem-2024-015.jpg)
<br />

[Photos](https://photos.app.goo.gl/KrSvUFYTGkzb9kfz5)

# Conclusion

The topics of the talks in these two years were quite diverse, and all the lectures were really very good. In the 12 talks we can see how translations happen in some projects such as KDE, PostgreSQL, Debian and Mattermost. We had the presentation of tools such as LibreTranslate, Weblate, scripts, AI, data model. And also reports on the work carried out by communities in Africa, China and Indonesia.

The rooms were full for some talks, a little more empty for others, but I was very satisfied with the final result of these two years.

I leave my special thanks to [Jonathan Carter](https://jonathancarter.org/), Debian Leader who approved my flight tickets requests so that I could join FOSDEM 2023 and 2024. This help was essential to make my trip to Brussels because flight tickets are not cheap at all.

I would also like to thank my wife Jandira, who has been my travel partner :-)

![Bruxelas](/assets/img/bruxelas-2023-187.jpg)

As there has been an increase in the number of proposals received, I believe that interest in the translations devroom is growing. So I intend to send the devroom proposal to FOSDEM 2025, and if it is accepted, wait for the future Debian Leader to approve helping me with the flight tickets again. We'll see.
