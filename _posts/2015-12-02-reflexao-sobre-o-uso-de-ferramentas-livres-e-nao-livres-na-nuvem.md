---
layout: post
title: Reflexão sobre o uso de ferramentas livres e não livres na nuvem
date: 2019-09-30 14:00
author: Paulo Henrique de Lima Santana
categories: geral
tags: software-livre
---

# Reflexão sobre o uso de ferramentas livres e não livres na nuvem

*Texto atualizado em 30/09/2019 para substituir algumas ferramentas citadas
anteriormente*

Há alguns dias fizemos uma reunião para iniciar os trabalhos de organização do
FLISOL 2016 em Curitiba e alguns colegas que estavam participando pela primeira
vez do grupo sugeriram usar o
[Slack<i class="fa fa-external-link"></i>](https://slack.com/)
como meio de comunicação entre os participantes, em substituição ao e-mail.
Segundo o pessoal que sugeriu e que usa essa ferramenta em outros projetos, a
vantagem de usar uma plataforma de web chat é poder agilizar as conversas e
organizar melhor os trabalhos.

Na [Comunidade Curitiba Livre<i class="fa fa-external-link"></i>](http://www.curitibalivre.org.br/)
usamos basicamente duas formas para comunicação interna:
[lista de discussão<i class="fa fa-external-link"></i>](http://listas.softwarelivre.org/cgi-bin/mailman/listinfo/curitibalivre)
(e-mail) e o
[Telegram<i class="fa fa-external-link"></i>](https://t.me/softwarelivre_curitiba).
Por isso poucos dias após a reunião do FLISOL, começamos a discutir se realmente
era necessário usar uma nova ferramenta ou se deveríamos continuar com o que já
estamos habituados. Pessoalmente acredito que sempre é válido experimentar novas
soluções, desde que elas nos tragam benefícios reais como parece ser o caso de
uma plataforma web chat. Mas também ficamos preocupados que se insistirmos em
usar a lista de discussão, os novos voluntários possam se afastar por achar o
e-mail uma forma não muito atrativa para continuar ajudando.

Decidimos manter o Slack até que possamos fazer outra reunião presencial para
rediscutir o assunto, mas aí veio o segundo (e principal) problema: ele não é
Software Livre, ou seja, o código não está disponível para ser baixado e usado
livremente. Para quem acredita nos ideais do movimento Software Livre, usar
softwares não livres sempre incomoda bastante. Não que isso nunca tenha
acontecido no nosso grupo, o próprio Telegram não é Software Livre, o problema é
que quanto mais exceções abrimos, mais ficamos reféns de soluções fechadas. Essa
discussão sobre o uso de softwares fechados "na nuvem" já aconteceu em outras
oportunidades dentro do nosso grupo e a questão central sempre foi se devemos ou
não usar softwares não livres mesmo que eles tragam benefícios. Dessa vez fiquei
refletindo bastante sobre o tema e decidi escrever esse texto.

Vou definir como softwares "na nuvem" os sistemas desenvolvidos e hospedados por
terceiros e usados via web, ou seja, nós só usamos os serviços oferecidos por
eles e não precisamos "baixar e instalar" no nosso computador pessoal. Observando
todas as nossas discussões anteriores, percebi que para começar a usar um software
na nuvem precisamos analisar três aspectos:

 1. A qualidade do software (serviço);
 1. Se o código é livre ou não;
 1. Como funciona a hospedagem.

A primeira questão é se o software é realmente bom e se teremos ganhos de
produtividade ao utlizá-lo. No momento lembro dos softwares abaixo que, no meu
ponto de vista, apresentam esses benefícios e que foram (ou ainda são) utilizadas
no nosso grupo:

 * Google Docs para a criação compartilhada de documentos;
 * [Facebook<i class="fa fa-external-link"></i>](https://www.facebook.com/CuritibaLivre) e
[Twitter<i class="fa fa-external-link"></i>](http://www.twitter.com/curitibalivre)
para divulgação das nossas atividades principalmente para o público leigo;
 * [Meetup<i class="fa fa-external-link"></i>](http://www.meetup.com/pt/Comunidade-Curitiba-Livre)
também para a divulgação das nossas atividades mas como alternativa para quem não
quer usar o Facebook;
 * Dropox para armazenar arquivos compartilhados;
 * Telegram para comunicação em grupo;
 * Slack para comunicaçao em grupo e organização dos trabalhos por ter integração
com outras ferramentas.

Vou chamar esses softwares de "líderes de mercado" porque na minha opinião, eles
são os que desempenham melhor as funções a que se propõe.

O problema é que todos esses líderes de mercado não tem o código aberto, o que
nos leva ao ponto dois dos três aspectos citados anteriormente. Alguns desses
softwares possuem alternativas livres, mas infelizmente na grande maioria dos
casos esses substitutos não oferecem as mesmas vantagens ou não possuem a mesma
audiência (no caso das mídias sociais) que os softwares não livres. Podemos usar:

 * [Etherpad<i class="fa fa-external-link"></i>](http://piratepad.net/)
como alternativa ao Google Docs para criar textos
 * [Diaspora<i class="fa fa-external-link"></i>](https://diasporabr.com.br/),
[~~red#matrix~~ Hubzilla<i class="fa fa-external-link"></i>](https://hub.vilarejo.pro.br/) e
[~~GNU Social~~ Mastodon<i class="fa fa-external-link"></i>](https://mastodon.social/)
para substituir o Facebook e o Twitter;
 * [~~ownCloud~~ Nextcloud<i class="fa fa-external-link"></i>](https://owncloud.org/)
para subustituir o Dropbox;
 * [~~Actor~~ Matrix<i class="fa fa-external-link"></i>](https://matrix.org/)
para substituir o Telegram;
 * [Rocket.Chat<i class="fa fa-external-link"></i>](https://rocket.chat/)
para substituir o Slack.

Quero reforçar que todas essas soluções livres são ótimas alternativas para
evitar as soluções fechadas, mas infelizmente por melhores que elas sejam, não
conseguem (ainda) oferecer exatamente as mesmas vantagens dos líderes de mercado.

Para complicar um pouco mais temos o ponto três, que é a hospedagem. Quando
usamos os líderes do mercado, não nos preocupamos onde o software está hospedado
porque o serviço está alí sempre disponível quando precisamos, ele está na
"nuvem". Algumas das alternativas livres não são oferecidos como serviços, ou
seja, temos que baixar o software e fazer a nossa própria hospedagem. Idealmente
cada pessoa ou grupo deveria manter os softwares com que trabalha em um servidor
próprio afim de garantir a sua liberdade e privacidade. Mas muitas vezes não é
possível ter um servidor próprio seja por custo ou por desconhecimento técnico
de como fazer.

E aí nos colocamos em um dilema com duas possibildades principais:

 1. Usamos os serviços dos líderes de mercado que nos ajudarão a ter mais
produtividade ou a ter mais audência, sem a preocupação com hospedagem, abrindo
mão da nossa liberdade e privacidade, mesmo que estes não sejam softwares livres?
ou
 1. Usamos apenas soluções que tem o código aberto, com algumas limitações e que
na maioria dos casos teremos que providenciar a nossa própria hospedagem, mas nos
mantendo fieis aos ideais do movimento Software Livre?

É possível notar que grupos que participam de discussões mais tecnológicas como
os de linguagens de programação e CMS's, tendem a optar mais pela primeira
alternativa, ou seja, eles usam serviços que vão ajudar na comunicação e
produtividade indepentende se as ferramentas são ou não softwares livres. O que
mais é levado em consideração é se essas soluções apresentam vantagens para o
grupo. Tanto é que muitos desenvolvedores não usam GNU/Linux em seus computadores
pessoais, optando por sistemas operacionais proprietários. Obviamente isso não é
regra geral, sempre existem excessões nesses grupos com pessoas mais preocupadas
com a questão da liberdade e privacidade.

Quando o grupo é mais voltado as discussões filosóficas/sociais/culturais do uso
e desenvolvimento de Software Livre, como é o caso da
[Comunidade Curitiba Livre<i class="fa fa-external-link"></i>](http://curitibalivre.org.br/), da
[Associação Software Livre.Org<i class="fa fa-external-link"></i>](http://softwarelivre.org/asl),
e de
[vários outros grupos<i class="fa fa-external-link"></i>](http://softwarelivre.org/comunidades-brasileiras),
o fato da ferramenta ter o código aberto passa a ter mais importância do que as
vantagens oferecidas pelas soluções não livres. Mesmo que o software usado esteja
sendo hospedado por terceiros, ter acesso ao código e ter a possibilidade de
baixar e instalar a sua própria instância deixa as pessoas mais confortávéis
para usá-lo.

Percebo que a maioria desses grupos mais preocupados em usar softwares livres
tem adotado uma terceira via que não é tão complacente com todos os softwares
fechados na nuvem e não é tão "radical" para usar apenas softwares livres que é:
sempre que possível, usar ferramentas de código aberto para a organizar
atividades e eventos (indepentende se ele é hospedado por terceiros ou se será
necessário fazer a própria hospedagem). E usar ferramentas fechadas,
especialmente de mídias sociais, para divulgar as atividades para o público em
geral, possibilitando assim alcançar principalmente pessoas que não são da
comunidade de Software Livre.

Como mantenho no meu site uma
[lista<i class="fa fa-external-link"></i>](http://phls.com.br/eventos-de-software-livre-em-2019)
dos eventos de Software Livre que acontecem pelo país, não posso terminar esse
texto sem citar um fato que me chamou a atenção recentemente: a quantidade de
eventos que passaram a utilizar soluções fechadas como
[Eventbrite<i class="fa fa-external-link"></i>](http://www.eventbrite.com.br/) e
[Doity<i class="fa fa-external-link"></i>](https://www.doity.com.br/),
provavelmente pela facilidade que esses sistemas oferecem para organizar desde a
inscrição até a programação. Além de não ser necessário desenvolver um sistema e
manter em um servidor próprio. Mas o preço é novamente a falta de liberdade de
ter acesso ao código e muito provavelmente a falta de privacidade já que essas
empresas devem aproveitar de alguma forma os dados dos usuários cadastrados.
Esses sites são um grande exemplo do antagonismo criado pela "facilidade de
uso/comodidade" x "código aberto/liberdade/privacidade".

O objetivo desse texto não foi dizer "use isso" ou "não use aquilo", mas promover
uma reflexão sobre esse tema tão complicado e quem sabe provocar algumas
respostas. Por fim coloco abaixo dois banners de uma campanha da
[Free Software Foundation Europe (FSFE)<i class="fa fa-external-link"></i>](https://fsfe.org/):

![Banner no cloud](/assets/img/thereisnocloud.png){:width="400px"}

![Banner no cloud 2](/assets/img/thereisnocloud-2.png){:width="400px"}

