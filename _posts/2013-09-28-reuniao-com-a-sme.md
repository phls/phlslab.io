---
layout: post
title: Reunião com a SME Curitiba
date: 2013-09-28 16:00
author: Paulo Henrique de Lima Santana
categories: geral
tags: software-livre
---

# Reunião com a SME Curitiba

Ontem (27/09/13) participei de uma reunião com a equipe de Tecnologia da
Secretaria Municipal de Educação de Curitiba. O diálogo foi muito bom e vimos que
a prefeitura está comprometida com o uso de software livre nas escolas
municipais. São 18.000 professores de 184 escolas que precisarão de capacitação
em GNU/Linux e a comunidade Curitiba Livre está disposta a ajudar no processo.

Estiveram comigo na reunião representando a comunidade Curitiba Livre o Antonio
Marques e o Rodrigo Robles.

Acredito que sairão boas parcerias desse grupo.

Alguns links:

Descrição dos laboratórios do PROINFO/FNDE/MEC:

<http://www.fnde.gov.br/programas/programa-nacional-de-tecnologia-educacional-proinfo/proinfo-perguntas-frequentes>

Lançamento do Programa Conexão Educacional:

<http://www.curitiba.pr.gov.br/noticias/educacao-municipal-tera-uso-de-tecnologia-da-informacao-ampliado/30480>