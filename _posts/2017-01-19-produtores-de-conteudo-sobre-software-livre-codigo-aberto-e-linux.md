---
layout: post
title: Produtores de conteúdo sobre Software Livre, Código Aberto e Linux
date: 2017-01-19 16:00
author: Paulo Henrique de Lima Santana
categories: geral
tags: software-livre
---

# Produtores de conteúdo sobre Software Livre, Código Aberto e Linux

## Blog do Edivaldo

 * Edivaldo Brito
 * <http://www.edivaldobrito.com.br>
 * <https://twitter.com/edivaldobrito>
 * <https://www.facebook.com/blogdoedivaldo>

## Blog Seja Livre

 * Vinícius Vieira
 * <http://sejalivre.org>
 * <https://twitter.com/sejalivrelinux>
 * <https://www.facebook.com/BlogSejaLivre>

## BR-Linux.org

 * Augusto Campos
 * <http://br-linux.org>
 * <https://twitter.com/brlinuxblog>

## Canal Projeto Root

 * Diego Costa
 * <https://www.projetoroot.com.br>
 * <https://www.youtube.com/user/projetoroot>
 * <https://twitter.com/ProjetoRoot>
 * <https://www.facebook.com/projetoroot>

## Dicas-L

 * Rubens Queiroz de Almeida
 * <http://www.dicas-l.com.br>
 * <https://twitter.com/dicasl>
 * <https://www.facebook.com/Dicas-L-164460256967417>

## Diolinux

 * Dionatan Simioni
 * <http://www.diolinux.com.br>
 * <https://www.youtube.com/channel/UCEf5U1dB5a2e2S-XUlnhxSA>
 * <https://twitter.com/blogdiolinux>
 * <https://www.facebook.com/blogdiolinux>

## DuZeru GNU Linux

 * Cláudio Atônio Silva
 * <http://duzeru.org>
 * <https://www.youtube.com/channel/UCJRuNnRpsZ4T94zWu3ja6iA>
 * <https://www.facebook.com/groups/duzerulinux>

## Linux Aprendendo

 * Hebert Pezzoti Da Silva
 * <https://www.youtube.com/channel/UC_pPre9RGvnPIoQSXwImXZQ>
 * <https://www.facebook.com/groups/825668384243126>
 * <http://telegram.me/linuxaprendendo>
 * <http://telegram.me/linuxaprendendonews>
 * <http://telegram.me/linuxaprendendorepositorio>

## Linux Descomplicado

 * Ricardo Ferreira Costa
 * <http://www.linuxdescomplicado.com.br>
 * <https://www.youtube.com/c/LinuxDescomplicado>
 * <https://twitter.com/linux10complica>
 * <https://www.facebook.com/linux.descomplicado.page>

## LinuxTips

 * Jeferson Fernando
 * <https://www.youtube.com/user/linuxtipscanal>
 * <https://twitter.com/badtux_>
 * <https://www.facebook.com/linuxtipsbr>

## Livre Labs

 * <https://www.youtube.com/channel/UCotwwUkKH2h0f-GQhugZbnA>

## Oficina do Tux

 * <http://www.oficinadotux.com.br>
 * <https://www.youtube.com/OficinadotuxBRlinux>
 * <https://www.facebook.com/oficinadotux>
 * <https://www.facebook.com/groups/oficinadotux>
 * <https://twitter.com/oficinadotux>

## Revista Espirito Livre

 * João Fernando Costa Júnior
 * <http://www.revista.espiritolivre.org>
 * <https://twitter.com/EspiritoLivre>
 * <https://www.facebook.com/revistaespiritolivre>

## Semana do Linux

 * Paulo Henrique Oliveira
 * <http://coletivo.semanadolinux.com.br>
 * <https://www.youtube.com/channel/UCDiyY9euKgAHSHkcikbKk_g>
 * <https://twitter.com/semanadolinux>
 * <https://www.facebook.com/semanadolinux>

## SempreUPdate

 * Emanuel Negromonte
 * <http://www.sempreupdate.com.br>
 * <https://www.youtube.com/channel/UCyWIA_ZAAmDr3wzuLSFzixQ>
 * <https://twitter.com/sempreupdate>
 * <https://www.facebook.com/sempreupdate>

## Software Livre Brasil

 * <http://softwarelivre.org>
 * <https://quitter.se/pslbrasil>
 * <https://twitter.com/PSLBrasil>
 * <https://www.facebook.com/PSLBrasil>

## Taverna Linux

 * <https://medium.com/@tavernalinux>
 * <https://www.youtube.com/c/TavernaLinuxBR>
 * <https://quitter.se/tavernalinux>
 * <https://twitter.com/tavernalinux>
 * <https://www.facebook.com/tavernalinux>

## Tecnologia Aberta

 * Ivan Brasil Fuzzer
 * <http://tecnologiaaberta.com.br>
 * <https://www.youtube.com/user/tecnologiaaberta>
 * <https://twitter.com/tecnologiaabert>
 * <https://www.facebook.com/tecnologiaaberta>

## Toca do Tux

 * Gabriel Silveira
 * <http://www.tocadotux.com.br>
 * <https://www.youtube.com/user/tocadotux>
 * <https://twitter.com/tocadotux>
 * <https://www.facebook.com/Toca-do-Tux-764459056909996>

## Under-Linux

 * <https://under-linux.org>

## Viva o Linux

 * Fábio Berbert de Paula
 * <https://www.vivaolinux.com.br>
 * <https://twitter.com/vivaolinux_>
 * <https://www.facebook.com/vivaolinux>
