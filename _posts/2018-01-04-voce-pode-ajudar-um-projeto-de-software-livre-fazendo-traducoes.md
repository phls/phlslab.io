---
layout: post
title: Você pode ajudar um projeto de Software Livre fazendo traduções
date: 2018-01-04 16:00
author: Paulo Henrique de Lima Santana
categories: geral
tags: traducao debian gnu wordpress gnome kde libreoffice software-livre
---

# Você pode ajudar um projeto de Software Livre fazendo traduções

![Bandeiras](/assets/img/bandeiras-eua-br.png)

Projetos de Softwares Livre que pretendem receber contribuições de pessoas de
qualquer lugar do mundo adotam o inglês como língua padrão, por isso é comum ver
os sites, os manuais, as trocas de mensagens entre seus contribuidores e os
softwares originais em si, utilizando a língua universal. Os desenvolvedores de
softwares livres já estão acostumados com esse ambiente internacional mas quando
pensamos nos usuários, o uso de um software livre em inglês pode dificultar
bastante a sua adoção, especialmente aqui no Brasil.

Para facilitar a utilização de softwares livres internacionais por usuários que
tem dificuldades para ler em inglês é extremamente importante o trabalho de
tradução realizado pelas comunidades. A tradução, também chamada de localização,
é feita por pessoas que dedicam parte do seu tempo para converter os textos do
inglês (en) para a língua do seu país. Como não poderia ser diferente, no Brasil
existem várias comunidades que fazem as traduções para o **português brasileiro
(pt-br)**.

Em 2017 palestrei em alguns eventos mostrando como qualquer pessoa pode
contribuir para o
[Projeto Debian<i class="fa fa-external-link"></i>](https://www.debian.org)
de várias outras formas que não envolvem conhecimento técnico como empacotamento.
Tem um
[vídeo<i class="fa fa-external-link"></i>](http://videos.softwarelivre.org/campus-party-brasilia-cpbsb-2017/o-projeto-debian-quer-voce.html) disponível dessa palestra na Campus Party Brasília 2017 e um
[arquivo<i class="fa fa-external-link"></i>](http://debianbrasil.org.br/arquivos-de-apresentacoes/o-projeto-debian-quer-voce.pdf)
dessa apresentação. Decidi escrever esse texto para encorajar aquelas pessoas
que gostariam de contribuir para um projeto de Software Livre internacional mas
que não envolva o desenvolvimento de código. Se você é uma dessas pessoas, você
pode ajudar com as traduções, para isso basta ter um conhecimento mínimo em
inglês, uma vontade de trabalhar coletivamente e um espírito de colaboração. Os
projetos de Software Livre precisam basicamente traduzir/localizar os seguintes
itens:

 * O software propriamente dito;
 * O site oficial;
 * Manuais/tutoriais oficiais.

Uma terminologia importante para as equipes de tradução é a seguinte (crédito do
Adriano Rafael Gomes):

 * **I18N (internationalization)**: internacionalização é a modificação de um
programa para que ele possa lidar com os múltiplos idiomas e culturas do mundo
todo.
 * **L10N (localization)**: localização é a implementação de um idioma
específico para um programa já internacionalizada. Leia mais
[aqui<i class="fa fa-external-link"></i>](https://www.debian.org/doc/manuals/intro-i18n/ch-intro.en.html).

Em todos os projetos você pode assumir dois papeis:

 * Tradutor: aquele que vai iniciar uma tradução do zero;
 * Revisor: aquele que revisar a tradução realizada por um tradutor e eventualmente propor correções.

Se você é um iniciante, algumas pessoas recomendam começar fazendo apenas
revisões porque é mais fácil. Nas equipes de tradução também existem papeis que
ajudam na organização interna, então alguém mais experiente com todo o processo
pode assumir o papel de Coordenador para gerenciar os trabalhos.

Espero que esse texto te incentive a começar a contribuir com pelo menos um dos
projetos que irei listar a seguir.

## Debian

![Debian Brasil](/assets/img/logo-debian-brasil.png){:width="100px"}

Dos projetos que citados aqui, o Debian é o que eu tenho mais conhecimento
porque contribuo com a tradução do site (menos que eu gostaria). O time de
tradução do Debian para o Brasil é conhecido como: **debian-l10n-portuguese**.

Para ajudar o time de tradução o primeiro passo é se inscrever na lista de
discussão
[debian-l10n-portuguese<i class="fa fa-external-link"></i>](https://lists.debian.org/debian-l10n-portuguese/)
porque é lá que você poderá obter ajuda com os outros tradutores. O próximo
passo é ler a
[wiki do time<i class="fa fa-external-link"></i>](https://wiki.debian.org/Brasil/Traduzir)
para ter todas as informações necessárias para começar.

Dos itens que você pode traduzir do projeto Debian, dá para destacar as
[descrições de pacotes<i class="fa fa-external-link"></i>](https://wiki.debian.org/Brasil/Traduzir/DDTP)
e as
[páginas web<i class="fa fa-external-link"></i>](https://wiki.debian.org/Brasil/Traduzir/WebWML).
Para traduzir as descrições dos pacotes é utilizada uma ferramenta web própria
do Debian chamada
[DDTSS (Debian Distributed Translation Server Satellite)<i class="fa fa-external-link"></i>](https://ddtp2.debian.net/ddtss/index.cgi/pt_BR).
Já a tradução das páginas web envolve a troca de mensagens na lista de discussão
que no início pode parecer um pouco complicada porque envolve
[pseudo-urls<i class="fa fa-external-link"></i>](https://wiki.debian.org/Brasil/Traduzir/Pseudo-urls)
mas depois fica muito mais fácil de entender.

O Adriano Rafael Gomes fez uma palestra no
[FISL16<i class="fa fa-external-link"></i>](http://softwarelivre.org/fisl16)
explicando o trabalho da equipe de tradução.
[Vídeo<i class="fa fa-external-link"></i>](http://hemingway.softwarelivre.org/fisl16/high/41d/sala_41d-high-201507101159.ogv)
e [slides<i class="fa fa-external-link"></i>](https://bolicho.arg.eti.br/pub/eventos/2015/fisl16/debian-l10n-portuguese.pdf).

**Estatísticas:**

Existem várias estatísticas que mostram o quanto ainda precisa ser traduzido ou
revisado.

 * [PO files, i.e. how well the packages are translated<i class="fa fa-external-link"></i>](https://www.debian.org/international/l10n/po/pt_BR)
 * [Debconf templates files managed via gettext<i class="fa fa-external-link"></i>](https://www.debian.org/international/l10n/po-debconf/pt_BR)
 * [Documentation in PO files, managed with po4a<i class="fa fa-external-link"></i>](https://www.debian.org/international/l10n/po4a/pt_BR)
 * [Debian package descriptions<i class="fa fa-external-link"></i>](https://ddtp2.debian.net/)
 * [Debian web site<i class="fa fa-external-link"></i>](https://www.debian.org/devel/website/stats/pt)
 * [Debian-Installer<i class="fa fa-external-link"></i>](https://d-i.debian.org/l10n-stats/translation-status.html)

Página com [status<i class="fa fa-external-link"></i>](https://l10n.debian.org/coordination/brazilian/pt_BR.by_status.html)
das traduções em andamento.

**Contatos:**

 * Lista de discussão: <http://lists.debian.org/debian-l10n-portuguese>
 * Canal no IRC: #debian-l10n-br no servidor irc.debian.org

## GNU

![GNU Brasil](/assets/img/logo-gnu-brasil.png){:width="100px"}

O site [gnu.org<i class="fa fa-external-link"></i>](https://www.gnu.org/)
é a principal porta de entrada para quem quer saber mais sobre Software Livre.
Lá está todo a história do Projeto GNU, do Movimento Software Livre, das licenças,
etc. Nesse site também é publicados os
[textos escritos<i class="fa fa-external-link"></i>](https://www.gnu.org/philosophy/latest-articles.html)
pelo Richard Stallman onde ele dá a sua opinião sobre temas diversos.

Quando o
[Stallman esteve no Brasil<i class="fa fa-external-link"></i>](http://softwarelivre.org/blog/como-foi-a-vinda-do-richard-stallman-para-curitiba-em-2017-parte-2)
em junho do ano passado conversamos sobre a onda de hackathons que estavam
acontecendo pelo país, e ele ficou de escrever um texto explicando porque era
importante que os trabalhos produzidos fossem liberados sobre uma licença livre.
E em setembro o Stallman publicou o texto Why hackathons should insist on Free
Software. Sugeri que a
[Adriana Costa<i class="fa fa-external-link"></i>](http://mulheres.eti.br/)
fizesse a tradução desse texto como forma de contribuir para o projeto GNU e
também para aprender a dinâmica da equipe de tradução do site. Em novembro a
tradução foi publica em:
[Por que os hackathons devem insistir em Software Livre<i class="fa fa-external-link"></i>](https://www.gnu.org/philosophy/hackathons.pt-br.html).

O time de tradução para o português do Brasil do site do projeto GNU, também
chamado de **www-pt-br** utiliza a lista de discussão
[www-pt-br-general<i class="fa fa-external-link"></i>](https://lists.gnu.org/mailman/listinfo/www-pt-br-general)
para trocar informações, então é imprescindível que você se inscreve nela. Você
também deve criar um usuário na ferramenta
[savannah<i class="fa fa-external-link"></i>](http://savannah.gnu.org/)
que é utilizado para gerenciar as traduções e revisões. O  savannah é um
servidor de hospedagem disponível para projetos de software livre do GNU e não-GNU.

Para entender melhor a dinâmica do time e começar a contribuir, acesse a
[página do time<i class="fa fa-external-link"></i>](https://www.gnu.org/server/standards/translations/pt-br) e leia
especialmente a parte de
[documentação<i class="fa fa-external-link"></i>](https://www.gnu.org/server/standards/translations/pt-br/documentation.html).

**Estatísticas:**

No final do ano passado o Rafael Fontenelle (que
[assumiu<i class="fa fa-external-link"></i>](http://lists.gnu.org/archive/html/www-pt-br-general/2017-11/msg00018.html)
a função de Coordenador da equipe brasileira de tradução em novembro) publicou
uma [mensagem<i class="fa fa-external-link"></i>](http://lists.gnu.org/archive/html/www-pt-br-general/2017-12/msg00058.html)
informando que o foi alcançado 19% da tradução das páginas do projeto GNU web.
Veja o quando ainda precisa ser traduzido e revisado no link:
<https://www.gnu.org/software/gnun/reports/report-pt-br.html>

## WordPress

![WordPress Brasil](/assets/img/logo-wordpress-brasil.jpg){:width="100px"}

A comunidade [WordPress<i class="fa fa-external-link"></i>](https://wordpress.org/)
realizada periodicamente um evento chamado
[WordPress Translation Day<i class="fa fa-external-link"></i>](https://wptranslationday.org/)
para que pessoas no mundo todo possam contribuir com tradução durante um dia. A
última edição aconteceu em 30 de setembro de 2017 e como o pessoal de Curitiba
fez um encontro para fazer as traduções, fui conhecer a dinâmica, mas
infelizmente fiquei pouco tempo e não pude contribuir muito.

A ferramenta usada para tradução é GlotPress que fica acessível no
[sistema<i class="fa fa-external-link"></i>](https://translate.wordpress.org/)
oficial de tradução do WordPress. Lá você deverá criar o seu usuário e após fazer
o login escolher qual time (locale) você irá ajudar, no nosso caso Português do
Brasil. Depois é só escolher o que você quer traduzir: WordPress, Themes,
Plugins, Meta ou Apps.

A Sheila Gomes publicou um
[tutorial<i class="fa fa-external-link"></i>](https://medium.com/comunicaminhos/glotpress-e-outras-ferramentas-para-traduzir-n-o-wordpress-ebefbb49cdd)
bastante detalhado ensinando a fazer a tradução. Ela também tem os
[slides<i class="fa fa-external-link"></i>](https://pt.slideshare.net/SheilaCristineGomes/presentations) de
apresentações realizadas sobre esse tema.

Diferente de outros projetos de Software Livre que utilizam lista de discussão,
a comunidade WordPress usa um [grupo<i class="fa fa-external-link"></i>](http://slack-wpbrasil.herokuapp.com/) do
Slack para comunicação.

## Gnome

![Gnome Brasil](/assets/img/logo-gnome-brasil.jpg){:width="100px"}

Ano passado gravamos um episódio do podcast
[Papo Livre<i class="fa fa-external-link"></i>](https://papolivre.org/10/)
com o [Georges Stavracas<i class="fa fa-external-link"></i>](https://feaneron.com/)
(a.k.a feaneron) - colaborador do projeto GNOME, que comentou um pouco sobre o
trabalho do time de tradução.

A equipe utiliza a ferramenta
[Mentiras Cabeludas<i class="fa fa-external-link"></i>](https://l10n.gnome.org/)
(Damned Lies) para gerenciar as traduções do GNOME.

Na página do
[time brasileiro<i class="fa fa-external-link"></i>](http://wiki.softwarelivre.org/GNOMEBR/Traducao)
é possível encontrar todas as informações necessárias para contribuir e também é
importante se inscrever na lista de discussão
[gnome-pt_br-list<i class="fa fa-external-link"></i>](https://mail.gnome.org/mailman/listinfo/gnome-pt_br-list)
para entrar em contato com os tradutores.

## KDE

![KDE Brasil](/assets/img/logo-kde-brasil.png){:width="100px"}

Texto retirado do [site<i class="fa fa-external-link"></i>](https://br.kde.org/)
da comunidade KDE Brasil:

O KDE usa uma ferramenta personalizada chamada
[Lokalize<i class="fa fa-external-link"></i>](http://userbase.kde.org/Lokalize/pt-br)
para fazer a tradução dos seus arquivos GUI e DOC. Para entender um pouco melhor
sobre como funciona esse processo de tradução você pode visitar a página
[The Translation HowTo<i class="fa fa-external-link"></i>](http://l10n.kde.org/docs/translation-howto/).

Se você deseja se tornar um membro do time de tradução do KDE Brasil, basta
entrar em contato com a nossa equipe através da lista de discussão
[kde-i18n-pt_br<i class="fa fa-external-link"></i>](https://mail.kde.org/mailman/listinfo/kde-i18n-pt_br)

Na [página principal<i class="fa fa-external-link"></i>](https://br.kde.org/i18n/)
do nosso projeto de tradução, você também poderá encontrar tutoriais e um FAQ
com dicas sobre como configurar um ambiente de tradução no seu computador e
começar a colaborar com o KDE.

## LibreOffice

Não achei muitas informações sobre como contribuir com a tradução do LibreOffice.
Na página da comunidade LibreOffice Brasil tem uma relação de
[listas de discussão<i class="fa fa-external-link"></i>](https://pt-br.libreoffice.org/ajuda/listas-de-discussao/),
e entre elas está a lista de documentação (produção e tradução). No site diz:

 * Assinatura: envie um e-mail em branco para docs+subscribe@pt-br.libreoffice.org, e siga as instruções enviadas para o seu endereço de e-mail.
 * Arquivos: <http://listarchives.libreoffice.org/pt-br/docs>

Pelo que descobri, a ferramenta usada para a tradução se chama
[Pootle<i class="fa fa-external-link"></i>](https://translations.documentfoundation.org/pt_BR/).

**Existem muitos outros projetos de Software Livre que também precisam de ajuda.
Agora é só escolher um projeto e coloca a mão na massa!**
