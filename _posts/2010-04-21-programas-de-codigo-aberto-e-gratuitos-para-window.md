---
layout: post
title: Programas de código aberto e gratuitos para Window
date: 2010-04-21 16:00
author: Paulo Henrique de Lima Santana
categories: geral
tags: software-livre
---

# Programas de código aberto e gratuitos para Window

## Navegador web

![Firefox](http://opensourcewindows.org/icon/firefox.png)

**Mozilla Firefox**  
Navegador web mais rápido, seguro e com mais recursos.

[Página para download<i class="fa fa-external-link"></i>](http://www.mozilla.org/products/firefox/)

## Suíte de escritóio / Processador de textos

![LibreOffice](http://opensourcewindows.org/icon/libreoffice.png)

**LibreOffice**  
Suíte cheia de funcionalidades como editor de texto, planilha de cálculo,
apresentáção. Totalmente compatível como o Microsoft Office.

[Página para download<i class="fa fa-external-link"></i>](https://pt-br.libreoffice.org/)


![AbiWord](http://opensourcewindows.org/icon/abiword.png)

**AbiWord**  
Um processador de textos mais enxuto e rápido do que o BrOffice.org. Compatível
com documentos do .doc e .odt.

[Página para download<i class="fa fa-external-link"></i>](http://www.abisource.com/download/)

## Tocador de vídeo, BitTorrent, Podcasting

![Miro](http://opensourcewindows.org/icon/miro_icon.png)

**Miro**  
Interface bonita. Toca qualquer tipo de vídeo (muito mais do que o Windows Media
Player). Assine RSS de vídeo, baixe e assista todos eles. Suporte a torrent.
Busque e baixe do YouTube e outros.

[Página para download<i class="fa fa-external-link"></i>](http://www.getmiro.com/)

## Mensagens Instantâneas

![Pidgin](http://opensourcewindows.org/icon/pidgin.png)

**Pidgin**  
Conecte a múltiplas contas de mensagens instantâneas em um único aplicativo.
Funciona com MSN Messenger, Yahoo! Messenger, ICQ, Google Talk, Jabber e outros.

[Página para download<i class="fa fa-external-link"></i>](http://www.pidgin.im/pidgin/home/)

## Conversor de vídeo - MP4, iPhone, Android

![Miro](http://opensourcewindows.org/icon/mvc.png)

**Miro conversor de vídeo**  
Converte qualquer tipo de vídeo para mp4 ou theora. Converte qualquer vídeo para
usar com iPhone, iPod, Android, etc. Muito simples, e com interface fácil de
usar.

[Página para download<i class="fa fa-external-link"></i>](http://www.mirovideoconverter.com/)

## Auxiliador de download

![SkipScreen](http://opensourcewindows.org/icon/skipscreen0.jpg)

**SkipScreen**  
Uma extensão para o Firefox que pula cliques desnecessários e contadores do
tempo no Rapidshare, Megaupload, Mediafire, zShare, e outros.

[Página para download<i class="fa fa-external-link"></i>](http://skipscreen.com/)

## E-mail

![Thunderbird](http://opensourcewindows.org/icon/thunderbird.png)

**Mozilla Thunderbird**  
Poderoso filtro anti-spam, interface sólida, e todas as funcionalidade que você
precisa.

[Página para download<i class="fa fa-external-link"></i>](http://www.mozilla.org/products/thunderbird/)

## RSS

![RSSOwl](http://opensourcewindows.org/icon/rssowl.png)

**RSSOwl**  
Sólido cliente RSS multi-plataforma.

[Página para download<i class="fa fa-external-link"></i>](http://www.rssowl.org/download)

## Compartilhamento de arquivos P2P

![Cabosl](http://opensourcewindows.org/icon/cabos.png)

**Cabos**  
Um programa de compartilhamento de arquivos simples e fácil de usar. Rede
Gnutella.

[Página para download<i class="fa fa-external-link"></i>](http://cabos.sourceforge.jp/)

![Gnucleusl](http://opensourcewindows.org/icon/gnucleus.png)

**Gnucleus**  
Um buscado do Gnutella muito bom, e um programa para baixar arquivos
compartilhados.

[Página para download<i class="fa fa-external-link"></i>](http://www.gnucleus.org/Gnucleus/)

## Gravador de vídeo

![Miro](http://opensourcewindows.org/icon/miro_icon.png)

**Miro**  
Interface bonita. Toca qualquer tipo de vídeo (muito mais do que o Windows Media
Player). Assine RSS de vídeo, baixe e assista todos eles. Suporte a torrent.
Busque e baixe do YouTube e outros.

[Página para download<i class="fa fa-external-link"></i>](http://www.getmiro.com/)

![VLC](http://opensourcewindows.org/icon/vlc.png)

**VLC**  
Exibe mais arquivos de vídeo do que a maioria dos tocadores: Quicktime, AVI,
DIVX, OGG, e mais. Interface bem bonita.

[Página para download<i class="fa fa-external-link"></i>](http://www.videolan.org/vlc/)

![MPlayer](http://opensourcewindows.org/icon/mplayer.png)

**MPlayer**  
Similiar ao VLC, exibe vários tipos de formato de vídeos.

[Página para download<i class="fa fa-external-link"></i>](http://www.mplayerhq.hu/design7/dload.html)

![Media Player](http://opensourcewindows.org/icon/mediaplayerclassic.png)

**Media Player Classic**  
Tocador de vídeo compacto mas poderoso. Toca qualquer coisa. Não é necessário
instalar.

[Página para download<i class="fa fa-external-link"></i>](http://mpc-hc.sourceforge.net/)

## Podcasting

![Juice](http://opensourcewindows.org/icon/juice.png)

**Juice**  
Cliente podcasting sólido.

[Página para download<i class="fa fa-external-link"></i>](http://juicereceiver.sourceforge.net/index.php)

## Ripador de DVD

![Handbrake](http://opensourcewindows.org/icon/handbrake.png)

**Handbrake**  
Conversor/ripador de DVD para MPEG-4.

[Página para download<i class="fa fa-external-link"></i>](http://handbrake.fr/downloads.php)

## Gravador de som

![Audacity](http://opensourcewindows.org/icon/audacity.png)

**Audacity**  
Ferramenta para gravação de sons.

[Página para download<i class="fa fa-external-link"></i>](http://audacity.sourceforge.net/download/windows)

## Editor de fotos e imagens

![Gimp](http://opensourcewindows.org/icon/gimp.png)

**GIMP**  
Aplicativo para edição de fotos que compete com as funcionalidades do Photoshop.

[Página para download<i class="fa fa-external-link"></i>](http://gimp-win.sourceforge.net/stable.html)

![Paint](http://opensourcewindows.org/icon/paintnet.png)

**Paint.NET**  
Editor gráfico com uma interface muito legal.

[Página para download<i class="fa fa-external-link"></i>](http://www.getpaint.net/download.html)

![Inkscape](http://opensourcewindows.org/icon/inkscape.png)

**Inkscape**  
Aplicativo para edição de vetores gráficos.

[Página para download<i class="fa fa-external-link"></i>](http://www.inkscape.org/download.php)

## FTP / SFTP

![Filezilla](http://opensourcewindows.org/icon/filezilla.png)

**Filezilla**  
Programa excelente para FTP.

[Página para download<i class="fa fa-external-link"></i>](http://filezilla-project.org/)

## IRC

![X-Chat](http://opensourcewindows.org/icon/xchat2.png)

**X-Chat 2**  
Cliente IRC.

[Página para download<i class="fa fa-external-link"></i>](http://silverex.info/download/)

## Arquivamento

![PeaZip](http://opensourcewindows.org/icon/peazip.png)

**PeaZip**  
Fantástico utilitário de arquivamento para extratir e compactar arquivo de uma
grande variedade de formatos. PeaZip também oferece proteção dos arquivos por
senha, integração com o sistema, uma interface admirável e muito mais.

[Página para download<i class="fa fa-external-link"></i>](http://peazip.sourceforge.net/index.html)

## Referências

<http://www.opensourcewindows.org>

<http://wiki.cyaneus.net/cdteca>