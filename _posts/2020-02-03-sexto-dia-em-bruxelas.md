---
layout: post
title: Sexto dia em Bruxelas
date: 2020-02-03 23:59
author: Paulo Henrique de Lima Santana
categories: geral
tags: bruxelas bélgica debian fosdem
---

# Sexto dia em Bruxelas

Veja o texto anterior: [Quinto dia em Bruxelas e segundo do FOSDEM](/quinto-dia-em-bruxelas-e-segundo-do-fosdem).

Com o fim da MiniDebCamp e do FOSDEM, eu fiquei com 2 dias para passear antes de
viajar na quarta-feira. Então me programei para no primeiro dia (segunda-feira)
visitar o [Royal Military Museum (War Heritage
Institute)](http://www.klm-mra.be/D7t/en), o shopping [Docxs
Bruxsel](https://docksbruxsel.be), o Carrefour e o que mais visse ao longo do
dia.

Peguei um ônibus da [linha 27](https://goo.gl/maps/dp72sdVYuBqvTyUi6) a duas
quadras do hostel e ele parou do lado do "Parc du Cinquantenaire" (Praça do
Cinquentenário) onde ficam 3 museus: Royal Military Museum (guerra),
[Autoworld](https://www.autoworld.be/home) (automóvel) e [Art &
History](https://www.artandhistory.museum/) (arte e história).

Eu já havia visitado o Royal Military Museum no ano passado com a Adriana, mas
como havíamos chegado pouco antes das 16h e o museu fecha às 17h, não tivemos
tempo de ver tudo com calma e não visitamos uma das área. Então eu havia
prometido a mim mesmo que se um dia eu voltasse a Bruxelas, eu visitaria o museu
novamente. Então lá estava eu novamente para agora sim, chegando pela manhã,
poder ver tudo com tempo. Mas quando cheguei na porta do museu, ele estava
fechado porque não abre as segundas-feiras. Mesmo olhando antes no site, eu não havia
percebido isso. Então fui embora e já era perto das 11h.

Fui andando até a estação do trem mais próxima (Montgomery) e peguei a linha 7
em direção do Docxs. Esse trem era o que pegávamos no ano passado para ir da
casa do Rytis e da Alice (onde estávamos hospedados) até a ULB (local do FOSDEM).
Dá pra pensar nessa [grande avenida](https://goo.gl/maps/rNcgP4KCXygAU2St7) como
uma sequência de locais e seus pontos de parada: ULB (Buyl), Parque do
Cinquentenário (Montgomery), Master Frites (Meiser), início da avenida do
Carrefour (Leopold III), casa do Rytis e Alice (Louis Bertrand), Docxs (Docks
Bruxsel) e Atomium (Heisel).

![Bruxelas](/assets/img/bruxelas-2020-23.jpg)

No Docxs eu fui olhar duas lojas que eu havia gostado e comprado algumas coisas
no ano passado: Media Markt (eletrônicos) e SportsDirect.com (roupas e tênis).
Ano passado descobrimos que no final de janeiro as lojas do Docxs fazem promoção
de roupas de inverno, então vale a pena dar uma olhada. Na Media Markt fui ver
os preços dos powerbanks mas não comprei nada. Na SportsDirect comprei um tênis
e uma calça de malha. Fiquei por lá até quase às 14h e estava com fome.

Resolvi então pegar o trem para ir almoçar na [Master
Frites](https://www.facebook.com/masterfritesbrussels/) lanchonete que o Rytis e
a Alice nos levaram ano passado, que tem vários sanduíches e batata frita, que
segundo eles, é a melhor da cidade. Almocei um sanduíche americado que veio com
dois hamburgueres. Já era 14:30 e deu pra matar a fome.

![Bruxelas](/assets/img/bruxelas-2020-24.jpg)

De lá peguei outro tem em direção ao Carrefour porque eu queria comprar cervejas
para trazer pro Brasil. Pelo que percebi, o melhor lugar para comprar as
cervejas mais baratas é no Carrefour. Esse que eu fui é um hipermercado, ou
seja, o maior da cidade. Lá comprei quatro garrafas de 750ml da
[Chimay](https://chimay.com/) (uma de cada tipo) e quatro garrafas de 250ml da
Hoegaarden (cerveja de trigo). As garrafas da Chimay variam de € 3,29 até € 4,35
e a Hoegaarden custa € 0,72. É de ficar maluco na varidade de cervejas, algumas
vem em kits com copos com as marcas. Tinha Duvel, Delirium, várias marcas
trapistas (feitas por monges nos monastérios), fora outras marcas que eu não
conhecia. Eu queria ter trazido mais garrafas mas estava preocupado com o peso
da mala (o que se comprovou depois). Quando estava lá, vi um família de
brasileiros comprando cervejas também e deu pra ver o "desespero" do cara que
não sabia quais ele ia levar para presentear as pessoas por causa da quantidade
de opções.

![Bruxelas](/assets/img/bruxelas-2020-25.jpg)

Só como curiosidade, nesse carrefour vi uma promoção de garrafa de água de 500ml
por apenas € 0,17. Para ter uma idéia do quanto estava barato, eu paguei € 1,50
numa garrafa na máquina do metrô, e vi por € 2,00 numa cafeteria. Também comprei
um powerbank de 10 mil por € 20,00, foi o melhor preço que achei. Queria ter
comprado queijos para trazer, mas como eu só ia viajar na quarta-feira fiquei
preocupado com como guardar esses queijos. Até dava pra guardar na geladeira
comunitária do hostel, mas preferi não arriscar. Queijos na Europa é outra coisa
pra ficar maluco pela variedade e preços.

Fiquei um bom tempo no Carrefour e quando saí de lá estava chovendo e
anoitecendo. Estava com a mochila cheia de cervejas e resolvi ir para o hostel para
guardá-las. Depois fui andando do hostel até o centro para comprar umas camisas na
Primark. A mesma promoção do ano passado tinha esse ano, de camisetas por €
2,50. Por esse preço não dá pra exigir qualidade né? Acabei comprando outras um
pouquinhos melhores por € 4,00.

Passei no Carrefour perto da Primark para comprar croissant para jantar e fui
para o hostel porque estava muito, mas muito cansado. Cheguei por volta das 20h,
comi e passei o resto da noite no quarto. Minhas pernas estavam doendo de tanto
andar e ficar em pé.

Eu havia me voluntariado para ajudar a descarregar os equipamentos do FOSDEM na
casa onde eles guardam numa cidade próxima. Na descrição era preciso pegar um
ônibus até às 17:03 para chegar lá às 18:15. Acabei me enrolando nos horários e
não deu tempo de ir. Espero que não tenha feito falta, porque tinham outros
voluntários inscritos.

Uma coisa boa é que nessa noite não havia outros hóspedes no quarto, então dormi
sozinho. Lebrando que eu estava num hostel no quarto coletivo com oito camas.

Próximo texto: [Sétimo dia em Bruxelas](/setimo-dia-em-bruxelas)
