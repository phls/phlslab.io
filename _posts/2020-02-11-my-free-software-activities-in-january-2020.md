---
layout: post
title: My free software activities in january 2020
date: 2020-02-11 10:00
author: Paulo Henrique de Lima Santana
categories: geral
tags: debian english freesoftware planetdebian bruxelas bélgica fosdem
---

# My free software activities in january 2020

Hello, this is my first monthly report about activities in Debian and Free
Software in general.

Since the end of [DebConf19](https://debconf19.debconf.org/) in July 2020 I was
avoiding to work in Debian stuff because the event was too stresseful to me. For
months I felt discouraged to contribute to the project, until December.

## Packages

On december I watched two news video tutorials from [João
Eriberto](http://eriberto.pro.br/) about:

* Debian Packaging - using git and gbp, parts
  [1](https://www.youtube.com/watch?v=dd1AhiEuodA),
  [2](https://www.youtube.com/watch?v=DG2jqv5KFm0),
  [3](https://www.youtube.com/watch?v=K1vQ229hMAg),
  [4](https://www.youtube.com/watch?v=h2-MZKxqdt8),
  [5](https://www.youtube.com/watch?v=U29FkHXCXeg) and
  [6](https://www.youtube.com/watch?v=vF-4CkwpYes)
* Debian Packaging with docker, parts
  [1](https://www.youtube.com/watch?v=xXpX4MGqrUs) and
  [2](https://www.youtube.com/watch?v=HQJD7BogT9s)

Since then, I decided update my packages using gbd and docker and it have been
great. On December and January I worked on these following packages.

I did QA Uploads of:
* [lockout](https://tracker.debian.org/pkg/lockout) 0.2.3
* [pidgin-audacious](https://tracker.debian.org/pkg/pidgin-audacious) 2.0.0
* [trueprint](https://tracker.debian.org/pkg/trueprint) 5.4
* [xarclock](https://tracker.debian.org/pkg/xarclock) 1.0
* [xcolorsel](https://tracker.debian.org/pkg/xcolorsel) 1.1a

I adopted and packaged new release of:
* [ddir](https://tracker.debian.org/pkg/ddir) 2019.0505 closing bugs #903093
  and #920066.

I packaged new releases of:
* [acheck](https://tracker.debian.org/pkg/acheck) 0.5.8 closing bug #895109
* [bd](https://tracker.debian.org/pkg/bd) 1.02
* [cadubi](https://tracker.debian.org/pkg/cadubi) 1.3.4
* [dicteval](https://tracker.debian.org/pkg/dicteval) 0.0.6
* [dpo-tools](https://tracker.debian.org/pkg/dpo-tools) 1.3
* [kickpass](https://tracker.debian.org/pkg/kickpass) 0.2.0

I packaged new upstream versions of:
* [butt](https://tracker.debian.org/pkg/butt) 0.1.18
* [flask-socketio](https://tracker.debian.org/pkg/flask-socketio) 4.2.1 closing
  bugs #879631, #895967, 896360 and #943059
* [kytos-utils](https://tracker.debian.org/pkg/kytos-utils) 2019.2 closing bugs
  #887118 and #948185
* [python-engineio](https://tracker.debian.org/pkg/python-engineio) 3.11.1
  closing bug #932538
* [python-openflow](https://tracker.debian.org/pkg/python-openflow) 2019.2

I backported to buster-backports:
* [butt](https://tracker.debian.org/pkg/butt) 0.1.18

I packaged:
* [clap](https://tracker.debian.org/pkg/clap) 0.14.0 closing bug #949186
* [python-socketio](https://tracker.debian.org/pkg/python-socketio) 4.4.0
  closing bug #947447

## MiniDebConf Maceió 2020

I helped to edit the [MiniDebConf Maceió 2020](https://maceio2020.debian.net)
website.

I wrote the [sponsorship
brochure](https://salsa.debian.org/debconf-team/public/mini/maceio2020/raw/master/projeto-patrocinio/MiniDebConf-Maceio-projeto-patrocinio.pdf)
and I sent it some brazilian companies.

I sent a message with call for activities to
[national](https://alioth-lists.debian.net/pipermail/debian-br-geral/2020-January/001014.html)
and [international](https://lists.debian.org/debian-devel/2020/01/msg00307.html)
mailing lists.

I sent a post to Debian [Micronews](https://micronews.debian.org/).

## FLISOL 2020

I sent a message to UFPR Education Director asking him if we could use the
Campus Rebouças auditorium to organize
[FLISOL](https://flisol.info/FLISOL2020/Brasil/Curitiba) there on april, but he
denied. We still looking for a place to FLISOL.

## DevOps

I started to study DevOps culture and for that, I watch a lot of vídeos from
[LINUXtips](https://www.linuxtips.io/)
* [Curso Descomplicando o Docker -
  V1](https://www.youtube.com/watch?v=0xxHiOSJVe8&list=PLf-O3X2-mxDkiUH0r_BadgtELJ_qyrFJ_)
* [Semana DevOps
  2019](https://www.youtube.com/watch?v=aI8FeEhDqoc&list=PLf-O3X2-mxDm9DRnU0mJtyDooW6v31EQB)
* [IaaSWeek 2019](https://www.youtube.com/watch?v=Tloaql2twe0)
* [Docker Tutorial
  série](https://www.youtube.com/watch?v=0cDj7citEjE&list=PLf-O3X2-mxDk1MnJsejJwqcrDC5kDtXEb)

And I read the book ["Docker para
desenvolvedores"](https://leanpub.com/dockerparadesenvolvedores) wrote by
[Rafael Gomes](https://gomex.me/).

## MiniDebCamp in Brussels

I traveled to Brussels to join
[MiniDebCamp](https://wiki.debian.org/DebianEvents/be/2020/MiniDebCamp) on
January 29-31 and [FOSDEM](https://fosdem.org/2020/) on February 1-2.

At MiniDebCamp my mostly interest was to join the [DebConf Vídeo Team
sprint](https://wiki.debian.org/Sprints/2020/DebConfVideoTeamSprintAtFOSDEM2020)
to learn how to setup a voctomix and gateway machine to be used in MiniDebConf
Maceió 2020. I could setup the Video Team machine installing Buster and using
ansible playbooks. It was a very nice opportunity to learn how to do that.

A complete report from DebConf Video Team can be read
[here](https://blog.olasd.eu/2020/02/minidebcamp-fosdem-2020/).

I wrote a diary (in portuguese) telling each of all my days in Brussels. It can
be read starting
[here](http://phls.com.br/vou-para-bruxelas-participar-da-minidebconf-e-do-fosdem-2020).
I intend to write more in english about MiniDebCamp and FOSDEM in a specific
post.

Many thanks to Debian for sponsor my trip to Brussels. It's a unique opportunity
to go to Europe to meet and to work with a lot of DDs.

## Misc

I did a MR to the [DebConf20](https://debconf20.debconf.org/) website fixing
some texts.

I joined the WordPress
[Meetup](https://www.meetup.com/pt-BR/wpcuritiba/events/267135167/)

I joined a [live streaming](https://www.youtube.com/watch?v=iYxvHDzaX9U) from
Comunidade Debian Brasil to talk about MiniDebConf Maceió 2020.

I watched an interesting vídeo ["Who has afraid of Debian
Sid"](https://www.youtube.com/watch?v=GOP2pqIRbIg) from
[debxp](https://debxp.org) channel

I deleted the [Agenda de eventos de Sofware Livre e Código
Aberto](http://agenda.softwarelivre.org) because I wasn't receiving events to
add there, and I was not having free time to publicize it.

I started to write the list of [FLOSS
events](http://phls.com.br/eventos-de-software-livre-em-2020) for 2020 that I
keep in my website for many years.

Finally I have watched
[vídeos](https://meetings-archive.debian.net/pub/debian-meetings/2019/DebConf19/)
from DebConf19. Until now, I saw these great talks:

* Bastidores Debian - entenda como a distribuição funciona
* Benefícios de uma comunidade local de contribuidores FLOSS
* Caninos Loucos: a plataforma nacional de Single Board Computers para IoT 
* Como obter ajuda de forma eficiente sobre Debian
* Comunidades: o bom o ruim e o maravilhoso
* O Projeto Debian quer você!
* A newbie's perspective towards Debian
* Bits from the DPL
* I'm (a bit) sick of maintaining piuparts.debian.org (mostly) alone, please
  help

That's all folks!