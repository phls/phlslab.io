---
layout: post
title: Eventos de Software Livre em 2013
date: 2013-12-31 16:00
author: Paulo Henrique de Lima Santana
categories: eventos
tags: eventos
---

# Eventos de Software Livre em 2013

Obs: essa lista está em constante atualização.

Acesse também a agenda de eventos de Software Livre e Código Aberto no Brasil:

<http://agenda.softwarelivre.org>

## JANEIRO

~~**Campus Party Brasil 2013**  
28 de janeiro a 03 de fevereiro de 2013  
São Paulo - SP  
<http://www.campus-party.com.br/2012/software-livre.html>~~

## MARÇO

~~**1º Encontro Nacional Mulheres na Tecnologia**  
02 de março de 2013  
Goiânia - GO  
<http://mulheresnatecnologia.org/evento>~~

~~**I Encontro de Software Livre e InstallFest Castanhal**  
21 de março de 2013  
Castanhal - PA  
<http://enclibre.blogspot.com.br>~~

~~**Document Freedom Day**  
27 de março de 2013  
Várias cidades  
<http://documentfreedom.org>~~

## ABRIL

~~**Drupalcamp São Paulo 2013**  
19 e 20 de abril de 2013  
São Paulo - SP  
<http://www.drupalcampsp.com.br>~~

~~**II Semana de Software Livre da USP**  
22 a 27 de abril de 2013  
São Paulo - SP  
<http://social.stoa.usp.br/ssl2013>~~

~~**FLISOL 2013 - Festilval Latino Americano de Instalação de Software Livre**  
27 de abril de 2013  
Várias cidades  
<http://www.flisol.info>~~

~~**Abril pro Ruby**  
27 de abril de 2013  
Recife - PE  
<http://tropicalrb.com/2013>~~

~~**Consoline - Congresso de Software Livre do Nordeste**  
27 de abril de 2013  
Recife - PE  
<http://www.softwarelivrene.org>~~

## MAIO

~~**Software Livre Day**  
17 de maio de 2013  
Alagoinhas - BA  
<http://tecnosystem.uneb.br/sld.html>~~

~~**II Encontro Catarinense de LibreOffice**  
18 de maio de 2013  
Joinville - SC  
<http://www2.joinville.udesc.br/~libreoffice>~~

~~**I Encontro Nordestino de Python**  
24 e 25 de maio de 2013  
Fortaleza - CE  
<http://pythonnordeste.org>~~

~~**PHP Day Curitiba 2013**  
25 de maio de 2013  
Curitiba - PR  
<http://curitiba.phpday.com.br>~~

## JUNHO

~~**JustJava 2013**  
07 e 08 de Junho de 2013  
São Paulo - SP  
<http://www.justjava.com.br>~~

~~**Ubuntu Day São Paulo**  
22 de Junho de 2013  
São Paulo - SP  
<http://ubuntubrsp.org/ubuntuday>~~

## JULHO

~~**14º FISL - Fórum Internacional de Software Livre**  
03 a 06 de julho de 2013  
Porto Alegre - RS  
<http://www.fisl.org.br>~~

~~**WSL - Workshop Software Livre**  
03 a 06 de julho de 2013  
Porto Alegre - RS  
<http://softwarelivre.org/wsl>~~

~~**Joomla Day São Paulo**  
05 a 06 de julho de 2013  
São Paulo - SP  
<http://www.joomladaysp.com.br>~~

~~**WordCamp Porto Alegre 2013**  
13 de julho de 2013  
Porto Alegre - RS  
<http://2013.portoalegre.wordcamp.org>~~

~~**Campus Party Recife 2013**  
17 a 21 de julho de 2013  
Recife - PE  
<http://recife.campus-party.org>~~

~~**V FSLDC - Fórum de Software Livre de Duque de Caxias**  
20 de julho de 2013  
Duque de Caxias - RJ  
<http://www.fsldc.org>~~

## AGOSTO

~~**Fórum Software Livre Tchelinux.org edição Vale dos Sinos 2013**  
10 de agosto de 2013  
Novo Hamburgo - RS  
<http://tchelinux.org/site/doku.php?id=evento_2013_08_vs>~~

~~**VI CONSEGI - Congresso Internacional Software Livre e Governo Eletrônico**  
13 a 15 de agosto de 2013  
Brasília - DF  
<http://www.consegi.gov.br>~~

~~**PGBR 2013 - Conferência Brasileira de PostGreSQL**  
15 a 17 de agosto de 2013  
Porto Velho - RO  
<http://pgbr.postgresql.org.br>~~

~~**Debian Day ou Dia do Debian**  
16 (ou 17) de agosto de 2013  
Várias cidades  
<http://wiki.debianbrasil.org/Eventos/DebianDayBrasil2013>~~

~~**OpenStreet 2013**  
24 de agosto de 2013  
Nova Trento - SC  
<http://officelivre.ws/?p=1460>~~

## SETEMBRO

~~**XXIX ENECOMP - Congresso Nacional dos Estudantes de Computação**  
02 a 06 de setembro de 2013  
Vitória da Conquista - BA  
<http://www.enecomp.org.br>~~

~~**III Fórum da Internet no Brasil**  
03 a 05 de setembro de 2013  
Belém - PA  
<http://www.forumdainternet.cgi.br>~~

~~**V Fórum Espírito Livre**  
03 e 05 de setembro de 2013  
São Mateus - ES  
<http://forum.espiritolivre.org>~~

~~**Joomla Day Brasil 2013**  
13 e 14 de setembro de 2013  
Belém - PA  
<http://www.joomladaybrasil.org>~~

~~**SFD 2013 - Software Freedom Day**  
14 de setembro de 2013  
Várias cidades  
<http://softwarefreedomday.org>~~

~~**8º Solisc - Congresso Catarinense de Software Livre**  
20 e 21 de setembro de 2013  
Florianópolis - SC  
<http://www.solisc.org.br>~~

~~**6ª GNUGRAF**  
27 e 28 de setembro de 2013  
Rio de Janeiro - RJ  
<http://gnugraf.org>~~

## OUTUBRO

~~**PythonBrasil[9]**  
02 a 04 de outubro de 2013  
Brasília - DF  
<http://2013.pythonbrasil.org.br>~~

~~**Plone Conference 2013**  
02 a 08 de outubro de 2013  
Brasília - DF  
<http://2013.ploneconf.org>~~

~~**II Fórum TchêLinux de Software Livre**  
05 de outubro de 2013  
Sant'Ana do Livramento - RS  
<http://institutolivramentoil.com.br/tchelinux>~~

~~**X Latinoware - Conferência Latino Americana de Software Livre**  
16 a 18 de outubro de 2013  
Foz do Iguaçu - PR  
<http://www.latinoware.org>~~

~~**V FTSL - Fórum de Tecnologia em Software Livre**  
23 a 25 de outubro de 2013  
Curitiba - PR  
<http://www.ftsl.org.br>~~

~~**V Maratona de Software Livre**  
25 e 26 de outubro de 2013  
Volta Redonda - RJ  
<http://iaesmevr.net/maratona>~~

~~**Café com Software Livre**  
26 de outubro de 2013  
Blumenau - SC  
<http://www.blusol.com.br>~~

## NOVEMBRO

~~**COMSOLiD 6 - 6° Encontro da Comunidade Maracanauense de Software Livre e Inclusão Digital**  
05 a 08 de novembro de 2013  
Maracanaú - CE  
<http://www.comsolid.org>~~

~~**Consoline - Congresso de Software Livre do Nordeste**  
09 de novembro de 2013  
Recife - PE  
<http://www.softwarelivrene.org>~~

~~**YAPC::Brasil**  
15 e 16 de novembro de 2013  
Curitiba - PR  
<http://www.yapcbrasil.org.br>~~

~~**DrupalCamp Floripa 2013**  
15 e 16 de novembro de 2013  
Florianópolis - SC  
<https://drupalcampfloripa2013.eventbrite.pt>~~

~~**JoomlaDay Rio 2013**  
15 e 16 de novembro de 2013  
Rio de Janeiro - RJ  
<http://joomladayrio.com.br>~~

~~**Fórum Permanente em Prol do Software Livre no Paraná**  
23 de novembro de 2013  
Curitiba - PR  
<http://forum.curitibalivre.org.br>~~

~~**WordCamp São Paulo 2013**  
23 de novembro de 2013  
São Paulo - SP  
<http://2013.saopaulo.wordcamp.org>~~

~~**VI Festival de Software Livre do Vale do Sinos**  
23 de novembro de 2013  
Novo Hamburgo - RS  
<http://wiki.softwarelivre-vs.org/FestivalDeSoftwareLivreDoValeDoSinos2013>~~

~~**PHP Conference Brasil 2013 - 8ª edição**  
28 de novembro a 01 de dezembro de 2013  
São Paulo - SP  
<http://www.phpconference.com.br>~~

~~**RuPy Brazil 2013**  
29 de novembro 2013  
São José dos Campos - SP  
<http://2013.rupy.com.br>~~

~~**10° FGSL - Fórum Goiano de Software Livre**  
29 e 30 de novembro de 2013  
Goiânia - GO  
<http://fgsl.com.br>~~

~~**Openbeach 2013**  
29 de novembro e 01 de dezembro de 2013  
Florianópolis - SC  
<http://www.openbeach.org.br>~~

~~**CryptoParty Brasil - 1a. edição**  
30 de novembro de 2013  
São Paulo - SP  
<https://cryptoparty.inf.br>~~

~~**TchêLinux**  
30 de novembro de 2013  
Porto Alegre - RS  
<http://tchelinux.paralelo30.org/portoalegre2013>~~

## DEZEMBRO

~~**12º OID - Oficina de Inclusão Digital e Participação Social**  
11 a 13 de dezembro de 2013  
Brasília – DF  
<http://oficinainclusaodigital.org.br>~~

## Anos anteriores

[2012](http://softwarelivre.org/blog/eventos-de-software-livre-em-2012)  
[2011](http://softwarelivre.org/blog/eventos-de-software-livre-em-2011)  
