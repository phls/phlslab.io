---
layout: post
title: Terceiro dia em Bruxelas e segundo da MiniDebCamp
date: 2020-01-31 23:59
author: Paulo Henrique de Lima Santana
categories: geral
tags: bruxelas bélgica debian fosdem
---

# Terceiro dia em Bruxelas e segundo da MiniDebCamp

Veja o texto anterior: [Segundo dia em Bruxelas e primeiro da MiniDebCamp](/segundo-dia-em-bruxelas-e-primeiro-da-minidebcamp).

Meu terceiro dia em Bruxelas foi até agora o dia com menos aventuras. Alguns
dias atrás o Holger (DD que organizou a MiniDebCamp) perguntou na lista de
discussão se alguém poderia abrir o hackerspace na sexta-feira pela manhã porque
ele tinha outro compromisso e não poderia estar lá cedo. Me voluntariei e na
quinta-feira o pessoal me passou as senhas das portas e às 9h da sexta eu estava
lá. O pessoal foi então chegando.

Decidi refazer a instalação do computador do time de vídeo porque o Nícolas fez
várias configurações no dia anterior que não estavam documentadas, então
procurei refazer todos os passos e deu certo, funcionou tudo como previsto.
Depois ele pediu que eu fizesse um merge request com algumas alterações que
facilitariam refazer a instalação e então testá-la. Mas não consegui ir até o
final do teste porque o Nícolas precisou guardar os equipamentos no carro mais
cedo já que era o último dia da MiniDebCamp. De qualquer forma, deu para
entender como funciona o setup do computador com as configurações que o time de
víde chama de "voctomix + gateway". A configuração nova ficou com o nome de
"cube" (por causa do formato do gabinete do computador do time de vídeo).

![Bruxelas](/assets/img/bruxelas-2020-12.jpg)

Fui almoçar no mesmo lugar do dia anterior, ou seja, comida turca (frango,
salada e arroz) por € 9,50, mas dessa vez adicionei uma porção de batata frita
por € 1,00, e ainda peguei uma garrafa de água € 1,00. No total ficou € 11,50.

Também no dia anterior eu havia visto em frente ao prédio do hackerspace fica a
fábrica de chocolates Leônidas e tem uma loja outlet, ou seja, os chocolates são
mais baratos do que nas lojas normais e dá para provar :-) Então na volta do
almoço fui na loja e comi vários chocolates (até demais da conta) e comprei uma
caixa de chocolates sortidos com quase 2 quilos. Essa caixa é muito mais barata
porque é uma caixa grande, simples, sem nenhum enfeite, e o quilo custa € 16,75.
É mais ou menos a granel porque você tem que comprar a caixa com o peso que ela
tem, não dá para tirar ou colocar chocolates. Na verdade são caixas para
revendedores que vão embalar depois. Para ter uma ideia, a caixa bonitinha para
presente com um laço de fita com 1 quilo custava € 34,00. Tinham outras
embalagens como saquinhos com fitas e chocolates em barras. Alias, tomei uma
bronca da vendedora porque eu estava tirando fotos e não tinha visto uma placa
dizendo que era proibido. Por isso não vou postar as fotos dos chocolates.

Como o pessoal é muito nerd, resolveram organizar umas palestras de última hora
e pelo jeito o pessoal curtiu porque tinham várias pessoas. Pelo que ví, eram
pessoas daqui que souberam e foram lá assistir.

No início da noite uma empresa resolveu patrocinar a cerveja que era vendida no
hackerspace para quem tava lá e então pudemos beber de graça. Tomei uma de cada:
Duvel, Chimay, Jupiler e Leffe. Resultado: fique meio cozido :-)

Fui embora às 22h pegar o ônibus, e chegando no hostel tomei um banho e fui
dormir.

![Bruxelas](/assets/img/bruxelas-2020-11.jpg)

![Bruxelas](/assets/img/bruxelas-2020-7.jpg)
![Bruxelas](/assets/img/bruxelas-2020-8.jpg)

![Bruxelas](/assets/img/bruxelas-2020-9.jpg)
![Bruxelas](/assets/img/bruxelas-2020-10.jpg)

Próximo texto: [Quarto dia em Bruxelas e primeiro do
FOSDEM](/quarto-dia-em-bruxelas-e-primeiro-do-fosdem)
