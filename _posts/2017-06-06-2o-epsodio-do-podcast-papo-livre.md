---
layout: post
title: 2º epsódio do podcast Papo Livre
date: 2017-06-06 16:00
author: Paulo Henrique de Lima Santana
categories: geral
tags: podcast papo-livre
---

# 2º epsódio do podcast Papo Livre

![Banner Stretch](/assets/img/gnu-meditando.png){:width="200px"}

Hoje (06/06/2017) saiu um novo epsódio do podcast **Papo Livre**, do qual faço
parte :-)

Neste segundo episódio, 
[Antonio Terceiro<i class="fa fa-external-link"></i>](http://softwarelivre.org/terceiro),
[Paulo Santana<i class="fa fa-external-link"></i>](http://phls.com.br) e
[Thiago Mendonça<i class="fa fa-external-link"></i>](http://acesso.me/blog/)
discutem os diversos meios de comunicação em comunidades de software livre. A
discussão começa pelos meios mais "antigos", como
[IRC<i class="fa fa-external-link"></i>](http://freenode.net/) e
[listas de discussão<i class="fa fa-external-link"></i>](http://listas.softwarelivre.org/)
e chega aos mais "modernos", passando pelo meio livre e meio proprietário
[Telegram<i class="fa fa-external-link"></i>](https://telegram.org/), e chega à
mais nova promessa nessa área,
[Matrix<i class="fa fa-external-link"></i>](https://matrix.org/)
(e o seu cliente mais famoso/viável,
[Riot<i class="fa fa-external-link"></i>](https://about.riot.im/)).

Você pode ouvir o epsódio **#1 - meios de comunicação** aqui:

<https://papolivre.org/1>